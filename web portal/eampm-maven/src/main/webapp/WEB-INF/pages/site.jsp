<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To eAMPM ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
      <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

      <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      
      
      <!-- <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css"> -->
      <link href="resources/css/semantic.min_old.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >
      <link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      
      <link href="resources/css/jasny-bootstrap.css" rel="stylesheet" type="text/css" >
	  <link href="https://www.amcharts.com/lib/3/plugins/export/export.css" rel="stylesheet" type="text/css" media="all" />
      
     
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script src="resources/js/semantic.js" type="text/javascript" ></script> 
      
        
      <script type="text/javascript">
         $(document).ready(function() {
            $('#example').DataTable();
            $('#example1').DataTable();
            $(".bars").click(function() {
               $(".sidebar-menu").invisible();
            })
             $('#example2').calendar({
            type: 'date'
            });
            $("#searchSelect").change(function() {
                debugger;
               var value = $('#searchSelect option:selected').val();
             
               if(value == 0) {
                   window.location.replace("/eampm-maven/dashboard2");
                  }
                  else if (value == 1){
                   window.location.replace("/eampm-maven/customerlist2");
                  }
                  else if (value == 2){
                   window.location.replace("/eampm-maven/siteview7&ref=2");
                  }
                  else if (value == 3){
                   window.location.replace("/eampm-maven/sitelist2");
                  }
                  else if (value ==4){
                   window.location.replace("site.html");
                  }
                  else if (value == 5){
                   window.location.replace("/eampm-maven/equipmentlist2");
                  }
                  else if (value == 6){
                   window.location.replace("/eampm-maven/activities");
                  }
                  else if (value == 7){
                   window.location.replace("/eampm-maven/countryregions");
                  }
                  else if (value == 8){
                   window.location.replace("/eampm-maven/countries");
                  }
                  else if (value == 9){
                   window.location.replace("/eampm-maven/states");
                  }
                  else if (value == 10){
                   window.location.replace("/eampm-maven/currencies");
                  }
                  else if (value == 11){
                   window.location.replace("/eampm-maven/userroles");
                  }
                  else if (value == 12){
                   window.location.replace("/eampm-maven/users");
                  }
                  else if (value == 13){
                   window.location.replace("site.html");
                  }
                  else if (value == 14){
                   window.location.replace("site.html");
                  }
                  else if (value == 15){
                   window.location.replace("/eampm-maven/customers");
                  }
                  else if (value == 16){
                   window.location.replace("/eampm-maven/sitetypes");
                  }
                  else if (value == 17){
                   window.location.replace("/eampm-maven/sites");
                  }
                  else if (value == 18){
                   window.location.replace("/eampm-maven/equipmentcategories");
                  }
                  else if (value == 19){
                   window.location.replace("/eampm-maven/equipmenttypes");
                  }
                  else if (value == 20){
                   window.location.replace("/eampm-maven/equipments");
                  }
                  else if (value == 21){
                   window.location.replace("site.html");
                  }
                  else if (value == 22){
                   window.location.replace("site.html");
                  }
                  else if (value == 23){
                   window.location.replace("/eampm-maven/timezones");
                  }
                  else if (value == 24){
                   window.location.replace("/eampm-maven/unitofmeasurements");
                  }

                // alert(value);
            });
         } );
      </script>
        
        
      <script type="text/javascript">
         $(document).ready(function() {
           
        	 
        	 $('#example').DataTable();
            $('#example1').DataTable();
            $(".bars").click(function() {
               $(".sidebar-menu").invisible();
            })
             $('#example2').calendar({
            type: 'date'
            });
            
            
            
            
            
            
            $('#fetchdata').hide();
            $('#receivedata').hide();
            $('#apidata').hide();
               
               // var value = $('#operationmode option:selected').val();
               // alert(value);
           $("#ddlOperationMode").change(function() {
              var value = $('#ddlOperationMode option:selected').val();

              if(value == 0) {
               $('#fetchdata').hide();
               $('#receivedata').show();
               $('#apidata').hide();

              }
              else if(value == 1) {
                  $('#fetchdata').show();
                  $('#receivedata').hide();
                  $('#apidata').hide();

                 }
              else if(value == 2) {
                  $('#fetchdata').hide();
                  $('#receivedata').hide();
                  $('#apidata').show();

                 }
              else{
               $('#fetchdata').hide();
               $('#receivedata').hide();
               $('#apidata').hide();
              }
               // alert(value);
           });

           
         } );
      </script>
   
   	 <script type='text/javascript'>//<![CDATA[
      $(window).load(function(){
      $("input:text").click(function() {
        $(this).parent().find("input:file").click();
      });

      $('input:file', '.ui.action.input')
        .on('change', function(e) {
          var name = e.target.files[0].name;
          $('input:text', $(e.target).parent()).val(name);
        });

});//]]>
</script>
   
   
   
	   <script type='text/javascript'>//<![CDATA[
		$(window).load(function(){
		$('.ui.dropdown').dropdown();
		});
		</script>
			<script type='text/javascript'>//<![CDATA[
		  $(window).load(function(){
		  $("input:text").click(function() {
			$(this).parent().find("input:file").click();
		  });

		  $('input:file', '.ui.action.input')
			.on('change', function(e) {
			  var name = e.target.files[0].name;
			  $('input:text', $(e.target).parent()).val(name);
			});

	});//]]>
	</script>
	 
	   <style type="text/css">
			 #chartdiv {
			 width : 100%;
			 height   : 250px;
		  }  
		    .dropdown-menu {
		  	left:-100px !important;
		  }
		 
	</style>
   
    <script type='text/javascript'>//<![CDATA[
         $(document).ready(function(){
            
    $('.ui.form')
        .form({
            inline: true,
            on: "blur",
            fields: {
                ddlSiteTypeId:{
                  identifier:'ddlSiteTypeId',
                  rules:[{
                    type:'empty',
                    prompt:'Please Select SiteId'
                  }]
                },
                 ddlCustometrId:{
                    identifier: 'ddlCustometrId',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your CustometrId'
                    }]
                   },
                   txtSiteName: {
                       identifier :'txtSiteName',
                       rules : [{
                         type:"empty",
                         prompt :"Please Enter Your SiteName"
                       },
                      {
                       type   : 'regExp[^[ a-zA-Z]+$]',
                       prompt : 'Please enter only alphabets'
                       },
                       {
                           type: 'minLength[1]',
                           prompt: 'SiteName must be at least 1 characters in length'
                         },
                         {
                           type: 'maxLength[50]',
                           prompt: 'SiteName must be at least 50 characters in length'
                         }
                       ]
                     },
               
               txtSiteDescription:{
                    identifier: 'txtSiteDescription',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your SiteDescription '
                    },
                    {
                        type: 'minLength[1]',
                        prompt: 'SiteDescription Group must be at least 1 characters in length'
                      },
                      {
                        type: 'maxLength[200]',
                        prompt: 'SiteDescription Group must be at least 200 characters in length'
                      }
                    ]
                },
                txtContactPerson: {
                    identifier :'txtContactPerson',
                    rules : [/* {
                      type:"empty",
                      prompt :"Please Enter Your ContactPerson"
                    },
                   {
                    type   : 'regExp[^[ a-zA-Z]+$]',
                    prompt : 'Please enter only alphabets'
                    },
                    {
                        type: 'minLength[1]',
                        prompt: 'ContactPerson must be at least 1 characters in length'
                      },
                      {
                        type: 'maxLength[10]',
                        prompt: 'ContactPerson must be at least 50 characters in length'
                      } */
                    ]
                  },
               
                txtaddress : {
                    identifier :'txtaddress',
                    rules : [{
                      type:"empty",
                      prompt :"Please Enter Your Address"
                    },
                    {
                      type:"regExp[/^[ A-Za-z0-9_.,:&-]*$/]",
                      prompt :"invalid symbol"
                    },
                    {
                        type: 'minLength[1]',
                        prompt: 'ContactPerson must be at least 1 characters in length'
                      },
                      {
                        type: 'maxLength[300]',
                        prompt: 'ContactPerson must be at least 300 characters in length'
                      }
                    ]
                  },
                  txtCity: {
                      identifier :'txtCity',
                      rules : [{
                        type:"empty",
                        prompt :"Please Enter Your City"
                      },
                     {
                      type   : 'regExp[^[a-zA-Z]+$]',
                      prompt : 'Please enter only alphabets'
                      },
                      {
                          type: 'minLength[1]',
                          prompt: 'City must be at least 1 characters in length'
                        },
                        {
                          type: 'maxLength[10]',
                          prompt: 'City must be at least 50 characters in length'
                        }
                      ]
                    },

              
               ddlStateId:{
                  identifier:'ddlStateId',
                  rules:[{
                    type:'empty',
                    prompt:'Please Select stateID'
                  }]
                },
                 ddlCountryId:{
                    identifier: 'ddlCountryId',
                    rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your CountryId'
                    }]
                   },
                  

                   
                   txtPostalCode:{
                  identifier:'txtPostalCode',
                 rules: [{
                        type: 'number',
                        prompt: 'Please Enter Your PostalCode'
                    },
                    {
                        type: 'minLength[1]',
                        prompt: 'PostalCode must be at least 1 characters in length'
                      },
                      {
                        type: 'maxLength[20]',
                        prompt: ' PostalCode must be at least 20 characters in length'
                      }
                    ]
                },
                txtLongitude:{
                  identifier:'txtLongitude',
                 rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your Longitude'
                    },
                    {
                        type: 'minLength[1]',
                        prompt: 'Longitude must be at least 1 characters in length'
                      },
                      {
                        type: 'maxLength[20]',
                        prompt: ' Longitude must be at least 20 characters in length'
                      }
                    ]
                },
                txtLatitude:{
                  identifier:'txtLatitude',
                 rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your Latitude'
                    },
                    {
                        type: 'minLength[1]',
                        prompt: 'Latitude must be at least 1 characters in length'
                      },
                      {
                        type: 'maxLength[20]',
                        prompt: ' Latitude must be at least 20 characters in length'
                      }
                    ]
                },
                txtAltitude:{
                  identifier:'txtAltitude',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your Altitude'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'Altitude must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[20]',
                        prompt: ' Altitude must be at least 20 characters in length'
                      }
                    ]
                },

                txtSitePoNumber:{
                  identifier:'txtSitePoNumber',
                 rules: [/* {
                        type: 'empty',
                       
                        //prompt: 'Please Enter Your SitePONumber'
                       prompt: ' SitePONumber must be at least 50 characters in length'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'SitePONumber must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: ' SitePONumber must be at least 50 characters in length'
                      }
                    ]
                },
                 txtSiteOperator:{
                  identifier:'txtSiteOperator',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your SiteOperator'
                    }, */
                    /* {
                        type: 'minLength[1]',
                        prompt: 'SiteOperator must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: ' SiteOperator must be at least 50 characters in length'
                      }
                    ]
                },
                txtSitemanufacturer:{
                    identifier:'txtSitemanufacturer',
                   rules: [/* {
                          type: 'empty',
                          prompt: 'Please Enter Your Sitemanufacturer'
                      }, */
                      /* {
                          type: 'minLength[1]',
                          prompt: 'Sitemanufacturer must be at least 1 characters in length'
                        }, */
                        {
                          type: 'maxLength[100]',
                          prompt: ' Sitemanufacturer must be at least 100 characters in length'
                        }
                      ]
                  },
                txtModuleName:{
                  identifier:'txtModuleName',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your ModuleName'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'ModuleName must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: ' ModuleName must be at least 50 characters in length'
                      }
                    ]
                },
                 txtCommunicationType:{
                  identifier:'txtCommunicationType',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your CommunicationType'
                    }, */
                    /* {
                        type: 'minLength[1]',
                        prompt: 'CommunicationType must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: ' CommunicationType must be at least 50 characters in length'
                      }
                    ]
                },
                 txtCollectionType:{
                  identifier:'txtCollectionType',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your CollectionType'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'ModuleName must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[20]',
                        prompt: ' ModuleName must be at least 20 characters in length'
                      }
                    ]
                },
                txtFileype:{
                  identifier:'txtFileype',
                 rules: [ {
                        type: 'empty',
                        prompt: 'Please Enter Your Fileype'
                    }, 
                     {
                        type: 'minLength[1]',
                        prompt: 'Fileype must be at least 1 characters in length'
                      }, 
                      {
                        type: 'maxLength[10]',
                        prompt: 'Fileype must be at least 10 characters in length'
                      }
                    ]
                },
                txtIncome:{
                  identifier:'txtIncome',
                 rules: [/* {
                        type: 'number',
                        prompt: 'Please Enter Your Income'
                    }, */
                    /* {
                        type: 'minLength[1]',
                        prompt: 'Income must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[15]',
                        prompt: ' Income must be at least 15 characters in length'
                      }
                    ]
                },
                ddlCurrency:{
                  identifier:'ddlCurrency',
                  rules:[{
                    type:'empty',
                    prompt:'Please Select Currency'
                  }]
                },
                txtCustomerReference: {
                    identifier :'txtCustomerReference',
                    rules : [/* {
                      type:"empty",
                      prompt :"Please Enter Your customerReference"
                    },
                    {
                      type:"regExp[/^[a-z0-9]+$/i]",
                      prompt :"please enter the alphabets and numbers"
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'customerReference must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[30]',
                        prompt: ' customerReference must be at least 30 characters in length'
                      }]
                  },
                  txtCustomerNaming: {
                      identifier :'txtCustomerNaming',
                      rules : [/* {
                        type:"empty",
                        prompt :"Please Enter Your CustomerNaming"
                      },
                      {
                        type:"regExp[/^[a-z0-9]+$/i]",
                        prompt :"please enter the alphabets and numbers"
                      }, */
                    /*   {
                          type: 'minLength[1]',
                          prompt: 'customerNaming must be at least 1 characters in length'
                        }, */
                        {
                          type: 'maxLength[30]',
                          prompt: ' customerNaming must be at least 30 characters in length'
                        }]
                    },

             
                txtEmailId: {
                  identifier:'txtEmailId',
                  rules:[/* {
                      type:'email',
                      prompt : 'Please Enter a Valid E-mail'
                  } */]
                },
                
                txtMobile: {
                    identifier:'txtMobile',
                    rules:[/* {
                        type:'empty',
                        prompt : 'please enter MobileNumber'
                    },
                    {
                        type:'regExp[/^[ 0-9+]*$/]',
                        prompt : 'Your Mobile Number Must Be at +91 Format'
                    },
                    {
                        type   : 'minLength[10]',
                       
                        prompt : 'Your Mobile Number Must Be at Least 10 Characters'
                      },
                      {
                          type   : 'maxLength[13]',
                         
                          prompt : 'Your Mobile Number Must Be at Least 13 Characters'
                        } */
                    ]
                  },
                   txtCapacity: {
                      identifier:'txtCapacity',
                      rules:[/* {
                          type:'empty',
                          prompt : 'please enter Installation Capacity'
                      },
                      {
                          type:'regExp[[ 0-9+]]',
                          prompt : 'Your Mobile Number Must Be at +91 Format'
                      },
                      {
                          type   : 'minLength[10]',
                         
                          prompt : 'Your Mobile Number Must Be at Least 10 Characters'
                        },
                        {
                            type   : 'maxLength[13]',
                           
                            prompt : 'Your Mobile Number Must Be at Least 13 Characters'
                          } */
                      ]
                    }, 


              
                
                txtTelephone: {
                    identifier:'txtTelephone',
                    rules:[/* {
                        type:'empty',
                        prompt : 'Please enter Telephone no'
                    },
                   {
                        type:'regExp[/^[ 0-9-]*$/]',
                        prompt : 'Your Telephone number must be xxx-xxxxxxxx'
                    },
                    {
                        type   : 'minLength[12]',
                       
                        prompt : 'Your Telephone no Must Be at Least 11 Characters'
                      }, */
                      {
                          type   : 'maxLength[17]',
                         
                          prompt : 'YourTelephone no Must Be at Least 17 Characters'
                        }
                    ]
                  },

                txtFax: {
                  identifier:'txtFax',
                  rules:[/* {
                      type:'number',
                      prompt : 'Please Enter Fax no'
                  }, 
                  {
                      type   : 'length[8]',
                      prompt : 'Your Fax number must be at least 8 characters'
                    }*/]
                }, 
                ddlOperationMode:{
                  identifier:'ddlOperationMode',
                  rules:[{
                    type:'empty',
                    prompt:'Please Select OperationMode'
                  }]
                },
                 txtLocalFtpDirectory:{
                  identifier:'txtLocalFtpDirectory',
                 rules: [/* {
                        type: 'empty',
                       
                        prompt: 'Please Enter Your LocalFtpDirectory'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'LocalFtpDirectory must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: 'LocalFtpDirectory must be at least 50 characters in length'
                      }
                    ]
                },
                 txtLocalFtpDirectoryPath:{
                  identifier:'txtLocalFtpDirectoryPath',
                 rules: [ {
                        type: 'empty',
                        prompt: 'Please Enter Your LocalFtpDirectoryPath'
                    }, 
                  /*  {
                        type: 'minLength[1]',
                        prompt: 'LocalFtpDirectoryPath must be at least 0 characters in length'
                      }, */ 
                      {
                        type: 'maxLength[100]',
                        prompt: 'LocalFtpDirectoryPath must be at least 100 characters in length'
                      }
                    ]
                },
                 txtLocalFtpHomeDirectory:{
                  identifier:'txtLocalFtpHomeDirectory',
                 rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your LocalFtpHomeDirectory'
                    },
                    /* {
                        type: 'minLength[1]',
                        prompt: 'LocalFtpHomeDirectory must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[255]',
                        prompt: 'LocalFtpHomeDirectory must be at least 255 characters in length'
                      }
                    ]
                },
                txtLocalFtpUserName:{
                  identifier:'txtLocalFtpUserName',
                 rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your LocalFtpUserName'
                    },
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'LocalFtpUserName must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[255]',
                        prompt: 'LocalFtpUserName must be at least 255 characters in length'
                      }
                    ]
                },
                txtLocalFtpPassword:{
                  identifier:'txtLocalFtpPassword',
                 rules: [{
                        type: 'empty',
                        prompt: 'Please Enter Your LocalFtpPassword'
                    },
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'LocalFtpPassword must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[255]',
                        prompt: 'LocalFtpPassword must be at least 255 characters in length'
                      }
                    ]
                },
                txtRemoteFtpServer:{
                  identifier:'txtRemoteFtpServer',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your RemoteFtpServer'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'RemoteFtpServer must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: 'RemoteFtpServer must be at least 50 characters in length'
                      }
                    ]
                },
                txtRemoteFtpUserName:{
                  identifier:'txtRemoteFtpUserName',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your RemoteFtpUserName'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'RemoteFtpUserName must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: 'RemoteFtpUserName must be at least 50 characters in length'
                      }
                    ]
                },
                 txtRemoteFtpPassword:{
                  identifier:'txtRemoteFtpPassword',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your RemoteFtpPassword'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'RemoteFtpPassword must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: 'RemoteFtpPassword must be at least 50 characters in length'
                      }
                    ]
                },
                txtRemoteFtpServerPort:{
                  identifier:'txtRemoteFtpServerPort',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your RemoteFtpServerPort'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'RemoteFtpServerPort must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[10]',
                        prompt: 'RemoteFtpServerPort must be at least 10 characters in length'
                      }
                    ]
                },
                 txtRemoteFtpDirectoryPath:{
                  identifier:'txtRemoteFtpDirectoryPath',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your txtRemoteFtpDirectoryPath'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'RemoteFtpDirectoryPath must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[100]',
                        prompt: 'RemoteFtpDirectoryPath must be at least 100 characters in length'
                      }
                    ]
                },
                 txtFileNameStartWith:{
                  identifier:'txtFileNameStartWith',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your FileNameStartWith'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'FileNameStartWith must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: 'FileNameStartWith must be at least 50 characters in length'
                      }
                    ]
                },
                 txtFileNameEndsWith:{
                  identifier:'txtFileNameEndsWith',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your FileNameEndsWith'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'FileNameEndsWith must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: 'FileNameEndsWith must be at least 50 characters in length'
                      }
                    ]
                },
                 txtFileNameBeLike:{
                  identifier:'txtFileNameBeLike',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your FileNameBeLike'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'FileNameBeLike must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[50]',
                        prompt: 'FileNameBeLike must be at least 50 characters in length'
                      }
                    ]
                },
                 txtSubFolder:{
                  identifier:'txtSubFolder',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your SubFolder'
                    }, */
                    /* {
                        type: 'minLength[1]',
                        prompt: 'SubFolder must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[2]',
                        prompt: 'SubFolder must be at least 2 characters in length'
                      }
                    ]
                },
                 txtApiUrl:{
                  identifier:'txtApiUrl',
                 rules: [/* {
                        type: 'url',
                        prompt: 'Please Enter Your ApiUrl'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'ApiUrl must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[255]',
                        prompt: 'ApiUrl must be at least 255 characters in length'
                      }
                    ]
                },
                 txtApikey:{
                  identifier:'txtApikey',
                 rules: [/* {
                        type: 'empty',
                        prompt: 'Please Enter Your Apikey'
                    }, */
                   /*  {
                        type: 'minLength[1]',
                        prompt: 'Apikey must be at least 1 characters in length'
                      }, */
                      {
                        type: 'maxLength[255]',
                        prompt: 'Apikey must be at least 255 characters in length'
                      }
                    ]
                }, 
                ddlActivieFlag:{
                  identifier:'ddlActivieFlag',
                  rules:[{
                    type:'empty',
                    prompt:'Please Select Activity Flag'
                  }]
                }
            }
        });
         })

      </script>   
   
  </head>
  <body  class="fixed-header">
        
          <nav class="page-sidebar" data-pages="sidebar">
        
         <div class="sidebar-header">
            <a href="#"><img src="resources/img/logo.png" class="mt-5" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management"></a>
            <i class="fa fa-bars bars"></i>
         </div>
         
         <div class="sidebar-menu">
              <c:if test="${not empty access}">
          
            <ul class="menu-items">
            
           <c:if test="${access.dashboard == 'visible'}">
          
          		 <li class="" style="margin-top:2px;">
                  <a href="./dashboard" class="detailed">
                  <span class="title">Dashboard</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.overview == 'visible'}">
          		 <li>
                  <a href="#"><span class="title">Overview</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-list-alt"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.systemMonitoring == 'visible'}">
          		<li>
                  <a href="#"><span class="title">System Monitoring</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
              <c:if test="${access.visualization == 'visible'}">
           <li>
                  <a href="javascript:;"><span class="title">Visualization</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./customerlist">Customer View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
                     	</li>
                    </c:if>
           
           
           
                     <li>
                        <a href="./sitelist">Site View</a>
                        <span class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
                     </li>
                     <li>
                        <a href="./equipmentlist">Equipment View</a>
                        <span class="icon-thumbnail"><i class="fa fa-cog"></i></span>
                     </li>
                  </ul>
               </li>
              
           </c:if>

           
            <c:if test="${access.analytics == 'visible'}">
           <li>
                  <a href="#">
                  <span class="title">Analytics</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.portfolioManagement == 'visible'}">
          <li>
                  <a href="#">
                  <span class="title">Portfolio Manager</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
               </li>
           </c:if>
           
             <c:if test="${access.ticketing == 'visible'}">
            <li>
                    <a href="javascript:;"><span class="title">Operation & Maintenance</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./ticketdetails">Ticket View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-ticket"></i></span>
                     	</li>
                    </c:if>
           
                  </ul>
               </li>
           
           
           
           
           
           </c:if>
           
            <c:if test="${access.forcasting == 'visible'}">
          <li>
                  <a href="#"><span class="title">Forecasting</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
         
         
           
             <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                     <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                         <a href="./equipmentparameters">Equipment Parameters Config.</a>
                        <span class="icon-thumbnail">EA</span>
                         
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                         <a href="./eventtypes">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                        
                     </li>
                     
                     <li>
                       <a href="./events">Event  Config.</a>
                        <span class="icon-thumbnail">EV</span>
                        
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li>
                     
                      <li>
                        <a href="./userroles">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                          
                     </li>
                     
                      <li>
                        <a href="./users">User  Config.</a>
                        <span class="icon-thumbnail">US</span>
                          
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>
         
              
              
               
              
               
              
             
          
            </ul>
         
         
          </c:if>
          
              
            <div class="clearfix"></div>
         </div>
    
    
    
      </nav>
  
   
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
                  <img src="resources/img/logo.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos">
               </div>
            </div>
            <div class="d-flex align-items-center">
                <div class="form-group required field searchbx">
                           <select class="ui fluid search selection dropdown width oveallSearch" id="searchSelect">
                              <option value="">Search</option>
                              <option value="0">Dashboard</option>
                              <option value="1">Customer List</option>
                              <option value="2">Site View</option>
                              <option value="3">Site List</option>
                              <option value="4">Equipment View</option>
                              <option value="5">Equipment List</option>
                              <option value="6">Activity Master</option>
                              <option value="7">Country Region Master</option>
                              <option value="8">Country Master</option>
                              <option value="9">State Master</option>
                              <option value="10">Currency Master</option>
                              <option value="11">User Roles Master</option>
                              <option value="12">Users Master</option>
                              <option value="13">Role Mapping Master</option>
                              <option value="14">User Mapping Master</option>
                              <option value="15">Customer Master</option>
                              <option value="16">Site Type Master</option>
                              <option value="17">Site Master</option>
                              <option value="18">Equipment Category Master</option>
                              <option value="19">Equipment Type Master</option>
                              <option value="20">Equipment Master</option>
                              <option value="21">Datalogger Configuration</option>
                              <option value="22">Parameter Mapping</option>
                              <option value="23">Time Zone</option>
                              <option value="24">Unit of Measurement</option>
                           </select>
                        </div>
              <div class="dropdown pull-right hidden-md-down">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a>
            </div>
         </div>
         <div class="page-content-wrapper ">
         
         
         
            <div class="content ">
               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="#"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item"><a href="#">Configuration</a></li>
                     <li class="breadcrumb-item active">Site Configuration</li>
                  </ol>
               </div>
           
           
           
            	<div id="newconfiguration" class="container-fluid   container-fixed-lg">
     				<div  class="card card-transparent">
     				
     				
 	  		     <c:url var="addAction" value="/addnewsite" ></c:url>                  
                 
    			<form:form action="${addAction}" modelAttribute="site" class="ui form">   
                
               
                     <div class="card-header ">
                     
                     
                     <c:choose>
  							<c:when test="${empty site.siteId}">
    							<div class="card-title">Add New Site</div>
  							</c:when>
  							<c:when test="${site.siteId == 0}">
    							<div class="card-title">Add New Site</div>
  							</c:when>
  							<c:when test="${not empty site.siteId}">
    							<div class="card-title">Edit Site Details</div>
    							<form:hidden path="siteId" />
  							</c:when>
  							<c:otherwise>
    							
  							</c:otherwise>
						</c:choose>
						
								
                        
                     </div>
    
                     <div class="card-block sortable">
                        <div class="row">
                           <div class="col-lg-12">
                                  <div class="card card-default bg-default" data-pages="card">
                                 <div class="card-block">
                                   
                                   
                                    <div class="row">
                                 
                                   <div class="col-lg-4 mt-15">
                                           <div class="ui form">
                                              <div class="field">
                                              
                                              	
                                              	
                                                <label  class="fields">Site Code</label>
                                                
                                                <c:if test="${empty site.siteId}">
                     	 						 <label  class="fields">Auto-Generation</label>
    					
   			 									</c:if>
   			 			
                                                 <c:if test="${not empty site.siteId}">
    													 <form:input name="first-name" placeholder="Auto-Generation" readonly="true" disabled="true" type="text" path="siteCode" /> 
                                                
    						   			 		</c:if>
   			 			
                                                <form:hidden path="siteCode" />
                                                
                                                
                                              </div>
                                            </div>
                                   </div>
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                     <div class="form-group">
                                              <div class="field required">
                                              	
                                              	 <label  class="fields">Site Type</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width" id="ddlSiteTypeId" path="siteTypeID">
   													<form:option value="-1" label="--- Select ---"/>
   													<form:options items="${sitetypeList}" />
												</form:select>
                                              	
                                                 </div>
                                            </div>
                                   </div>
                                 
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                           <div class="form-group">
                                              <div class="field required">
                                              		
                                                <label  class="fields">Customer</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width" id="ddlCustometrId" path="customerID">
   													<form:option value="-1" label="--- Select ---"/>
   													<form:options items="${customerList}" />
												</form:select>  
                                               
                                              </div>
                                            </div>
                                   </div>
                                 
                                
                                 
                                    </div>
                                  
                                  
                                  
                                  
                                    <div class="row">
                                 
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                        <div class="ui form">
                                              <div class="field required">
                                              	
                                                   <label  class="fields">Site Name</label>
                                                 <form:input name="first-name" placeholder="Site Name" type="text" path="siteName" id="txtSiteName"/>
                                              
                                              	
                                               </div>
                                            </div>
                                             </div>
                                 
                                 
                                 
                                 
                                  
                                   <div class="col-lg-4 mt-15">
                                    <div class="ui form">
                                              <div class="field">
                                              	
                                                <label  class="fields">Site Description</label>
                                                 <form:input name="first-name" placeholder="Site Description" type="text" path="siteDescription" id="txtSiteDescription" />
                                              
                                              	
                                               </div>
                                            </div>
                                   </div>
                                 
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                        		<div class="field">
                                        		
            					   <label  class="fields">Contact Person</label>
                                                 <form:input name="first-name" placeholder="Contact Person" type="text" path="contactPerson" id="txtContactPerson" />
                                              				
                                         		</div>
                                                
                                             </div> 
                                       </div>
                                    </div>
                                  
                                  
                                  
                                  
                                     
                                    <div class="row">
                                 
                                   <div class="col-lg-4 mt-15">
                                            <div class="ui form">
                                              <div class="field required">
                                         		
												  <label  class="fields">Address</label>
                                                 <form:input name="first-name" placeholder="Address" style="margin-top:0px;"  type="text" path="address" id="txtAddress"/>
                                              
                                              </div>
                                            </div>
                                   </div>
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                    <div class="ui form">
                                              <div class="field required">
                                              	    <label  class="fields">City</label>
                                                 <form:input name="first-name" placeholder="City" type="text" path="city" id="txtCity"/>
                                              
                                               </div>
                                            </div>
                                   </div>
                                 
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="form-group">
                                              <div class="field required">
                                              
                                               <label  class="fields">State</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width" id="ddlStateId" path="stateID">
   													<form:option value="-1" label="--- Select ---"/>
   													<form:options items="${stateList}" />
												</form:select>
												
												
                                              </div>
                                            </div>
                                       </div>
                                   
                                  
                                 </div>
                                 
                                 
                                 
                                 
                                 
                                 
                                    
                                    <div class="row">
                                 
                                   <div class="col-lg-4 mt-15">
                                           <div class="form-group">
                                        		<div class="field required">
                                        		
            									 <label  class="fields">Country</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width" id="ddlCountryId" path="countryID">
   													<form:option value="-1" label="--- Select ---"/>
   													<form:options items="${countryList}" />
												</form:select>
												
												</div>
                                                
                                     		</div>
                                   </div>
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                    	<div class="ui form">
                                        		<div class="field required">
                                				
                                				  <label  class="fields">Postal Code</label>
                                                 <form:input name="first-name" placeholder="PostalCode" type="text" path="postalCode"  id="txtPostalCode"/>
                                               
												</div>
                                                
                                     	</div>
                                   </div>
                                 
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field required">
                                              
                                                <label  class="fields">Longitude</label>
                                                 <form:input name="first-name" placeholder="Longitude" type="text" path="longitude" id="txtLongitude"/>
                                              
                                              
                                              </div>
                                            </div>
                                       </div>
                                    </div>
                                  
                                 
                             
                                 
                                 
                                 
                                 
                                    
                                    <div class="row">
                                 
                                   <div class="col-lg-4 mt-15">
                                            <div class="ui form">
                                              <div class="field required">
                                              
                                              <label  class="fields">Latitude</label>
                                                 <form:input name="first-name" placeholder="Latitude" type="text" path="latitude" id="txtLatitude" />
                                              
                                               </div>
                                            </div>
                                   </div>
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                    	<div class="ui form">
                                        		
            								<div class="field">
                                              
                                                 <label  class="fields">Altitude</label>
                                                 <form:input name="first-name" placeholder="Altitude" type="text" path="altitude" id="txtAltitude"/>
                                              
                                         	</div>
                                         </div>
                                   </div>
                                 
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field">
                                                <label  class="fields">Site PO Number</label>
                                                 <form:input name="first-name" placeholder="Site PO Number" type="text" path="sitePONumber" id="txtSitePoNumber" />
                                               
                                              </div>
                                            </div>
                                       </div>
                                    </div>
                                  
                                 
                             
                             
                             
                             
                                    
                                    <div class="row">
                                 
                                   <div class="col-lg-4 mt-15">
                                           <div class="ui form">
                                              <div class="field">
                                              	  <label  class="fields">Site Operator</label>
                                                 <form:input name="first-name" placeholder="Site Operator" type="text" path="siteOperator" id="txtSiteOperator" />
                                               
                                               </div>
                                            </div>
                                   </div>
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                    	<div class="ui form">
                                        	<div class="field">
                                              <label  class="fields">Site Manafacturer</label>
                                                 <form:input name="first-name" placeholder="Site Manafacturer" type="text" path="siteManafacturer" id="txtSitemanufacturer"/>
                                               
                                            </div>
                                                
										</div>
                                   </div>
                                 
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field">
                                               <label  class="fields">Module Name</label>
                                                 <form:input name="first-name" placeholder="Module Name" type="text" path="moduleName" id="txtModuleName" />
                                               
                                               
                                              </div>
                                            </div>
                                       </div>
                                    </div>
                                  
                                 
                             
                             
                             
                                    
                                    <div class="row" >
                                 
                             
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field">
                                              
                                              	 <label  class="fields">Communication Type</label>
                                                 <form:input name="first-name" placeholder="Communication Type" type="text" path="communicationType" id="txtCommunicationType"/>
                                               
                                              </div>
                                            </div>
                                       </div>
                                       
                                       
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              	<div class="field">
                                              	
                                              	 <label  class="fields">Collection Type</label>
                                                 <form:input name="first-name" placeholder="Collection Type" type="text" path="collectionType" id="txtCollectionType"/>
                                               
                                            	</div>
                                   			</div>
                                       </div>
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                    	<div class="ui form">
                                        		<div class="field">
                                        		
                                        		
                                        		 <label  class="fields">File Type</label> 
                                                 <form:input name="first-name" placeholder="File Type" type="text" path="fileType" id="txtFileype"/>
                                                       
                                        		
												</div>
                                                
                                     	</div>
                                   </div>
                                 
                                       
                                       
                                       
                                    </div>
                                  
                                 
                                 
                                 
                            
                            
                            
                            
                            
                            
                            
                            
                            
                                    <div class="row" >
                                 
                             
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field">
                                              
                                               		<label class="fields">Income</label>
                                                	<input name="first-name" placeholder="Income" type="text" id="txtIncome">
                                                
                                                 
                                              </div>
                                            </div>
                                       </div>
                                       
                                       
                                 
                                   <div class="col-lg-4 mt-15">
                                    	<div class="form-group">
                                        		<div class="field">
                                        		
                                        		
                                        		 
                                                <label  class="fields">Currency</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width" id="ddlCurrency" path="currencyID">
   													<form:option value="-1" label="--- Select ---"/>
   													<form:options items="${currencyList}" />
												</form:select>
												  
                                        		
												</div>
                                                
                                     	</div>
                                   </div>
                                 
                                       
                                       
                                       
                                       
                                       
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              	<div class="field">
                                              
                                                <label  class="fields">Email Id</label>
                                                 <form:input name="first-name" placeholder="Email Id" type="text" path="emailID" id="txtEmailId"/>
                                              	
                                            	</div>
                                   			</div>
                                       </div>
                                 
                                 
                                       
                                    </div>
                                  
                                 
                            
                            
                            
                            
                            
                            
                            
                            
                                    <div class="row" >
                                 
                             
                                 
                                 
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field required">
                                              
                                                 <label  class="fields">Mobile</label>
                                                 <form:input name="first-name" placeholder="Mobile" type="text" path="mobile" id="txtMobile" />
                                               
                                              </div>
                                            </div>
                                       </div>
                                       
                                       
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              	<div class="field">
                                              	
                                              	 <label  class="fields">Telephone</label>
                                                 <form:input name="first-name" placeholder="Telephone" type="text" path="telephone" id="txtTelephone" />
                                               
                                               
                                            	</div>
                                   			</div>
                                       </div>
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                    	<div class="ui form">
                                        		<div class="field">
                                        		
                                        		
                                        		 <label  class="fields">Fax</label>
                                                 <form:input name="first-name" placeholder="Fax" type="text" path="fax" id="txtFax"/>
                                                
                                        		
												</div>
                                                
                                     	</div>
                                   </div>
                                 
                                       
                                       
                                       
                                    </div>
                                  
                                 
                            
                    
                    
                            
                            
                                    <div class="row" >
                                 
                             
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field">
                                              
                                                <label  class="fields">Location Map Url</label>
                                                 <form:input name="first-name" placeholder="Google Location Map Url " type="text" path="siteImage" />
                                                
                                              </div>
                                            </div>
                                       </div>
                                       
                                 
                                
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field">
                                              
                                                <label  class="fields">Customer Reference</label>
                                                 <form:input name="first-name" placeholder="Customer Reference" type="text" path="customerReference" id="txtCustomerReference"/>
                                                
                                              </div>
                                            </div>
                                       </div>
                            
                                 
                                 
                                       
                                       
                                       <div class="col-lg-4 mt-15">
                                       		<div class="ui form">
                                              <div class="field">
                                              
                                                <label  class="fields">Customer Naming</label>
                                                 <form:input name="first-name" placeholder="Customer Naming" type="text" path="customerNaming" id="txtCustomerNaming"/>
                                                
                                              </div>
                                            </div>
                                       </div>
                                       
                                       
                                    </div>
                                  
                                 
                             
                                    <div class="row" >
                                 
                                 
                                   <div class="col-lg-4 mt-15">
                                    	<div class="form-group">
                                        		<div class="field required">
                                        		
                                        		<label  class="fields">Site Status</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width" id="ddlActiveFlag" path="activeFlag">
   													<form:option value="-1" label="--- Select ---"/>
   													<form:options items="${activeStatusList}" />
												</form:select>
                                        		
												</div>
                                                
                                     	</div>
                                   </div>
                                 
                            
                                    <div class="col-lg-4 mt-15">
                                       		<div class="form-group">
                                       		  	<div class="field">
                                              	
                                                
                                            	</div>
                                   			</div>
                                       </div>
                                 
                                       
                                       
                                       <div class="col-lg-4 mt-15">
                                       		<div class="form-group">
                                       		  	<div class="field">
                                              	
                                                
                                            	</div>
                                   			</div>
                                       </div>
                                            
                                       
                                       
                                    </div>
                                  
                                  
                            
                    
                            
                                               
                                  
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>





                     <div class="card-block sortable">
                        <div class="row">
                           <div class="col-lg-12">
                                  <div class="card card-default bg-default" data-pages="card">
                                 <div class="card-block">
                                 
                                 
                                    <div class="row">
                                  
                                  
                                       <div class="col-lg-4 mt-15">
                                       		<div class="form-group">
                                       		  	<div class="field">
                                              	<label  class="fields">Operation Mode</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width" id="ddlOperationMode" path="operationMode">
   													<form:option value="-1" label="--- Select ---"/>
   													<form:options items="${operationmodeList}" />
												</form:select>
                                                
                                            	</div>
                                   			</div>
                                       </div>
                                       
                                       <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                    <label  class="fields">Local FTP Directory</label>
                                                 	<form:input name="first-name" placeholder="Local FTP Directory" type="text" path="localFtpDirectory" id="txtLocalFtpDirectory" />
                   		      					</div>
                                             </div>
                                          </div>
                                          
                                          
                                          
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label  class="fields">Local FTP Directory Path</label>
                                                 	<form:input name="first-name" placeholder="Local FTP Directory Path" type="text" path="localFtpDirectoryPath" id="txtLocalFtpDirectoryPath"/>
                   								</div>
                                             </div>
                                          </div>
                                          
                                 
                                  
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="row">
                                  
                                  
                                      <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                    <label  class="fields">Local FTP Home Directory</label>
                                                 	<form:input name="first-name" placeholder="Local FTP Home Directory" type="text" path="localFtpHomeDirectory" id="txtLocalFtpHomeDirectory"/>
                   		      					</div>
                                             </div>
                                          </div>
                                          
                                          
                                       <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                    <label  class="fields">Local FTP User Name</label>
                                                 	<form:input name="first-name" placeholder="Local FTP User Name" type="text" path="localFtpUserName" id="txtLocalFtpUserName"/>
                   		      					</div>
                                             </div>
                                          </div>
                                          
                                          
                                          
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label  class="fields">Local FTP Password</label>
                                                 	<form:input name="first-name" placeholder="Local FTP Password" type="text" path="localFtpPassword" id="txtLocalFtpPassword"/>
                   								</div>
                                             </div>
                                          </div>
                                          
                                 
                                  
                                    </div>
                                
                                
                                          <div class="row">
                                  
                                  		<div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                    <label  class="fields">Data Logger Id</label>
                                                 	<form:input name="first-name" placeholder="Enter Data Logger Id" type="text" path="dataLoggerID" />
                   		      					</div>
                                             </div>
                                          </div>
                                        
                                           
                                       <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                    <label  class="fields">Service Code</label>
                                                 	<form:input name="first-name" placeholder="Enter Service Code" type="text" path="serviceCode" />
                   		      					</div>
                                             </div>
                                          </div>
                                          
                                          
                                            <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                    <label  class="fields">Installation Capacity</label>
                                                 	<form:input name="first-name" placeholder="Installation Capacity" type="text" path="installationCapacity" id="txtCapacity"/>
                   		      					</div>
                                             </div>
                                          </div>
                                 
                                  
                                    </div>
                                    
                           
                           
                                
                                    
                                    <div id="fetchdata">
                                       
                                        <div class="row">
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label class="fields">Remote FTP Server</label>
                                                   <form:input name="first-name" placeholder="Remote FTP Server" type="text" path="remoteFtpServer" id="txtRemoteFtpServer"/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label class="fields">Remote FTP User Name</label>
                                                   <form:input name="first-name" placeholder="Remote FTP User Name" type="text" path="remoteFtpUserName" id="txtRemoteFtpUserName"/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label class="fields">Remote FTP Password</label>
                                                   <form:input name="first-name" placeholder="Remote FTP Passowrd" type="text" path="remoteFtpPassword" id="txtRemoteFtpPassword"/>
                                                </div>
                                            </div>
                                          </div>
                                       </div>
                                       
                                       
                                       
                                       
                                       
                                        <div class="row">
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label class="fields">Remote FTP Server Port</label>
                                                   <form:input name="first-name" placeholder="Remote FTP Server Port" type="text" path="remoteFtpServerPort" id="txtRemoteFtpServerPort"/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label class="fields">Remote FTP Directory Path</label>
                                                   <form:input name="first-name" placeholder="Remote FTP Directory Path" type="text" path="remoteFtpDirectoryPath" id="txtRemoteFtpDirectoryPath"/>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div class="row">
                                          <div class="col-lg-4 mt-15">
                                              <input type="checkbox" id="checker">&nbsp;&nbsp;&nbsp;<span class="fields">Filename Start with</span>
                                                   <div id="dependent-box">
                                                      <div class="ui form">
                                                         <div class="field required">
                                                            <form:input name="first-name" placeholder="Filename Start with" type="text" path="filePrefix" id="txtFileNameStartWith"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                          </div>
                                          <div class="col-lg-4 mt-15">
                                              <input type="checkbox" id="startto">&nbsp;&nbsp;&nbsp;<span class="fields">Filename Ends with</span>
                                                   <div id="dependents">
                                                      <div class="ui form">
                                                         <div class="field required">
                                                            <form:input name="first-name" placeholder="Filename End with" type="text" path="fileSuffix" id="txtFileNameEndsWith"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                          </div>
                                          <div class="col-lg-4 mt-15">
                                              <input type="checkbox" id="any">&nbsp;&nbsp;&nbsp;<span class="fields">Filename be like</span>
                                                   <div id="anys">
                                                      <div class="ui form">
                                                         <div class="field required">
                                                            <form:input name="first-name" placeholder="Filename be like" type="text" path="fileSubstring" id="txtFileNameBeLike"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                          </div>
                                       </div>
                                        <div class="row">
                                          <div class="col-lg-4 mt-15">
                                              <input type="checkbox" id="subfol">&nbsp;&nbsp;&nbsp;<span class="fields">Subfolder</span>
                                                   <div id="subfolder">
                                                      <div class="ui form">
                                                         <div class="field required">
                                                            <form:input name="first-name" placeholder="Subfolder" type="text" path="subFolder" id="txtSubFolder"/>
                                                         </div>
                                                      </div>
                                                   </div>
                                          </div>
                                       </div>
                                    </div>
                                    
                                    
                                    <div id="receivedata">
                                    
                                    </div>
                                    
                                    
                                    
                                       <div id="apidata">
                                            <div class="row">
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label class="fields">API Url</label>
                                                   <form:input name="first-name" placeholder="Api url" type="text" path="apiUrl" id="txtApiUrl"/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field required">
                                                   <label class="fields">API Key</label>
                                                   <form:input name="first-name" placeholder="Api key" type="text" path="apiKey" id="txtApiKey"/>
                                                </div>
                                             </div>
                                          </div>
                                          <div class="col-lg-4  mt-15">
                                             <div class="ui form">
                                                <div class="field">
                                              
                                               </div>
                                            </div>
                                          </div>
                                       </div>
                                       
                                   
                                    </div>
                                    
                                    
                                    
                                    <div  class="row mt-15">
                                       <div class="col-lg-12 center mb-20">
                                        <input class="btn btn-primary center" type="submit" value="Save" />
                                        <input class="btn btn-default center" type="button" value="Clear" />
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              </div>
                           </div>
                        </div>
                     
             
             	</form:form>
             
             
             
             
     					<c:if test="${not empty siteList}">
                     		<!-- Table Start-->
                     		<div id="configurationlist" class="card-block sortable">
                    			
                    			         <div class="row">
                           <div class="card-header ">
                                    <div class="card-title">Site Details</div>
                                 </div>
                           <div class="col-lg-12" id="sitedetails">
                              <div class="card card-default bg-default" data-pages="card">
                                 <div class="card-block table-responsive">
                                    <table id="example" class="table table-striped table-bordered" width="100%" cellspacing="0">
                                    <thead>
                                      <tr>
                                            <th>ID</th>
                                            <th>Site Code</th>
                                            <th>Site Type</th>
                                            <th>Site Name</th>
                                            <th>Customer</th>
                                            <th>State</th>
                                            <th>Country</th>
                                            <th>Site Status</th>
                                            <th>Edit</th>
                                            <th>De-activate</th>
                                            
                                      </tr>
                                    </thead>
                                    <tbody>
                                     
                                     	<c:forEach items="${siteList}" var="site">
        								<tr>
            								<td>${site.siteId}</td>
            								<td>${site.siteCode}</td>
            								<td>${site.siteTypeName}</td>
            								<td>${site.siteName}</td>
            								<td>${site.customerName}</td>
            								<td>${site.stateName}</td>
            								<td>${site.countryName}</td>
            								
            									
            								<c:if test="${site.activeFlag=='-1'}"><td>Not Mentioned</td></c:if>
            								<c:if test="${site.activeFlag=='0'}"><td>In-active</td></c:if>
            								<c:if test="${site.activeFlag=='1'}"><td>Active</td></c:if>
            								
            								
            								<td><a href="<c:url value='editsite${site.siteId}' />" >Edit</a></td>
            								<td><a href="<c:url value='removesite${site.siteId}' />" >De-activate</a></td>
        								</tr>
    									</c:forEach>
                                        
                                        
                                        
                                    </tbody>
                                  </table>
                                 </div>
                              </div>
                           </div>
                        </div>
               
                    			
                     		</div>
                     		<!-- Table End-->
            			</c:if> 
             
					</div>
				</div>
         
        
            </div>
         
         
            <div class="container-fixed-lg footer">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span class="font-montserrat">Inspire Clean Energy</span>.
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-right sm-pull-reset">
                     <span class="hint-text">Powered by</span>
                     <span class="font-montserrat">MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
 
      <!-- Side Bar Content Start-->

      
       <!--   <div id="quickview" class="quickview-wrapper" data-pages="quickview">
         <ul class="nav nav-tabs">
            <li class data-target="#quickview-notes" data-toggle="tab">
               <a href="#">Events</a>
            </li>
            <li data-target="#quickview-alerts" data-toggle="tab">
               <a href="#">Tickets</a>
            </li>
            <li class="active" data-target="#quickview-chat" data-toggle="tab">
               <a href="#">Chat</a>
            </li>
         </ul>
         <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
         <div class="tab-content">
            <div class="tab-pane no-padding" id="quickview-notes">
               <div class="view-port clearfix quickview-notes">
                  <div class="view list" id="quick-note-list">
                     <ul>
                        <li class="tkt_drop">
                           <div class="dropdown">
                             <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <p class="one-of-two">
                                       <span class="text-warning fs-12">#31</span>
                                    </p>
                                    <p class="two-of-two p-l-10 fs-12">
                                       <span class="text-master">Webdysun </span>
                                       <span class="text-warning pull-right">20-07-2017</span>
                                       <br>
                                       <span class="text-masters">Site Name</span>
                                       <span class="text-warning pull-right">15:15</span>
                                       <br>
                                       <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                    </p>
                             </a>
                             <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#myCommentModal" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#myShareModal" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                           </div>
                        </li>
                        <li>
                           <div class="dropdown">
                             <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <p class="one-of-two">
                                       <span class="text-warning fs-12">#31</span>
                                    </p>
                                    <p class="two-of-two p-l-10 fs-12">
                                       <span class="text-master">Webdysun </span>
                                       <span class="text-warning pull-right">20-07-2017</span>
                                       <br>
                                       <span class="text-masters">Site Name</span>
                                       <span class="text-warning pull-right">15:15</span>
                                       <br>
                                       <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                    </p>
                             </a>
                             <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#address" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#comment" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#share" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                           </div>
                        </li>
                        <li>
                           <div class="dropdown">
                             <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <p class="one-of-two">
                                       <span class="text-warning fs-12">#31</span>
                                    </p>
                                    <p class="two-of-two p-l-10 fs-12">
                                       <span class="text-master">Webdysun </span>
                                       <span class="text-warning pull-right">20-07-2017</span>
                                       <br>
                                       <span class="text-masters">Site Name</span>
                                       <span class="text-warning pull-right">15:15</span>
                                       <br>
                                       <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                    </p>
                             </a>
                             <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#address" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#comment" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#share" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                           </div>
                        </li>
                        <li data-toggle="modal" data-target="#myModal">
                           <p class="one-of-two">
                              <span class="text-warning fs-12">#31</span>
                           </p>
                           <p class="two-of-two p-l-10 fs-12">
                              <span class="text-master">Webdysun </span>
                              <span class="text-warning pull-right">20-07-2017</span>
                              <br>
                              <span class="text-masters">Site Name</span>
                              <span class="text-warning pull-right">15:15</span>
                              <br>
                              <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                           </p>
                           <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#address" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#comment" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#share" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                        </li>
                     </ul>
                  </div>
                  <div class="view note" id="quick-note">
                     <div>
                        <ul class="toolbar">
                           <li><a href="#" class="close-note-link"><i class="pg-arrow_left"></i></a></li>
                           <li><a href="#" data-action="Bold" class="fs-12"><i class="fa fa-bold"></i></a></li>
                           <li><a href="#" data-action="Italic" class="fs-12"><i class="fa fa-italic"></i></a></li>
                           <li><a href="#" class="fs-12"><i class="fa fa-link"></i></a></li>
                        </ul>
                        <div class="body">
                           <div>
                              <div class="top">
                                 <span>21st april 2014 2:13am</span>
                              </div>
                              <div class="content">
                                 <div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane no-padding" id="quickview-alerts">
               <div class="view-port clearfix" id="alerts">
                  <div class="view bg-white">
                     <div class="navbar navbar-default navbar-sm">
                        <div class="navbar-inner">
                           <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                           <i class="pg-more"></i>
                           </a>
                           <div class="view-heading">
                              Events
                           </div>
                           <a href="#" class="inline action p-r-10 pull-right link text-master">
                           <i class="pg-search"></i>
                           </a>
                        </div>
                     </div>
                     <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              Calendar
                           </div>
                           <ul>
                              <li class="alert-list">
                                 <a href="javascript:;" class="align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="p-l-10 overflow-ellipsis fs-12">
                                       <span class="text-master"></span>
                                    </p>
                                    <p class="p-r-10 ml-auto fs-12 text-right">
                                       <span class="text-warning"><br></span>
                                       <span class="text-master"></span>
                                    </p>
                                 </a>
                              </li>
                             
                               </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              Pending
                           </div>
                           <ul>
                              <li class="alert-list">
                                 <a href="javascript:;" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="col overflow-ellipsis fs-12 p-l-10">
                                       <span class="text-master link"><br></span>
                                       <span class="text-master"></span>
                                    </p>
                                 </a>
                              </li>
                              <li class="alert-list">
                                 <a href="javascript:;" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="col overflow-ellipsis fs-12 p-l-10">
                                       <span class="text-master link"><br></span>
                                       <span class="text-master"></span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              Ticket Status
                           </div>
                           <ul>
                              <li class="alert-list">
                                 <a href="#" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-danger fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="col overflow-ellipsis fs-12 p-l-10">
                                       <span class="text-master link"><br></span>
                                       <span class="text-master"></span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane active no-padding" id="quickview-chat">
               <div class="view-port clearfix" id="chat">
                  <div class="view bg-white">
                     <div class="navbar navbar-default">
                        <div class="navbar-inner">
                           <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                           <i class="pg-plus"></i>
                           </a>
                           <div class="view-heading">
                              Chat List
                              <div class="fs-11">Show All</div>
                           </div>
                           <a href="#" class="inline action p-r-10 pull-right link text-master">
                           <i class="pg-more"></i>
                           </a>
                        </div>
                     </div>
                     <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              a
                           </div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">ava flores	</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">b</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">bella mccoy</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">bob stephens</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">c</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view"  href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">carole roberts</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">christopher perez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">d</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">danielle fletcher</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">david sutton</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">e</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8x.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">earl hamilton</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9x.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">elaine lawrence</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">ellen grant</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">erik taylor</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">everett wagner</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">f</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">freddie gomez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">g</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">glen jensen</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">gwendolyn walker</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">j</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">janet romero</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">k</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">kim martinez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">l</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">lawrence white</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">leroy bell</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">letitia carr</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">lucy castro</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">m</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">mae hayes</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marilyn owens</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marlene cole</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marsha warren</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8x.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marsha dean</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">mia diaz</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">n</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">noah elliott</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">p</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">phyllis hamilton</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">r</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">raul rodriquez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">rhonda barnett</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">roberta king</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">s</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">scott armstrong</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">sebastian austin</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8x.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">sofia davis</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">t</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9x.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">terrance young</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">theodore woods</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">todd wood</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">tommy jenkins</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">w</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">wilma hicks</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="view chat-view bg-white clearfix">
                     <div class="navbar navbar-default">
                        <div class="navbar-inner">
                           <a href="javascript:;" class="link text-master inline action p-l-10 p-r-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                           <i class="pg-arrow_left"></i>
                           </a>
                           <div class="view-heading">
                              John Smith
                              <div class="fs-11 hint-text">Online</div>
                           </div>
                           <a href="#" class="link text-master inline action p-r-10 pull-right ">
                           <i class="pg-more"></i>
                           </a>
                        </div>
                     </div>
                     <div class="chat-inner" id="my-conversation">
                        <div class="message clearfix">
                           <div class="chat-bubble from-me">
                              Hello there
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="profile-img-wrapper m-t-5 inline">
                              <img class="col-top" width="30" height="30" src="resources/img/profiles/avatar_small.jpg" alt data-src="resources/img/profiles/avatar_small.jpg" data-src-retina="resources/img/profiles/avatar_small2x.jpg">
                           </div>
                           <div class="chat-bubble from-them">
                              Hey
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="chat-bubble from-me">
                              Did you check out Pages framework ?
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="chat-bubble from-me">
                              Its an awesome chat
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="profile-img-wrapper m-t-5 inline">
                              <img class="col-top" width="30" height="30" src="resources/img//avatar_small.jpg" alt data-src="resources/img//avatar_small.jpg" data-src-retina="resources/img/profiles/avatar_small2x.jpg">
                           </div>
                           <div class="chat-bubble from-them">
                              Yea
                           </div>
                        </div>
                     </div>
                     <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                        <div class="row">
                           <div class="col-1 p-t-15">
                              <a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
                           </div>
                           <div class="col-8 no-padding">
                              <input type="text" class="form-control chat-input" data-chat-input data-chat-conversation="#my-conversation" placeholder="Say something">
                           </div>
                           <div class="col-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top">
                              <a href="#" class="link text-master"><i class="pg-camera"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      <!-- Side Bar Content End-->
    <script>
      var map;
      function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 2,
          center: new google.maps.LatLng(22.91722, 80.23064),
          mapTypeId: 'roadmap'
        });

        var iconBase = 'https://maps.google.com/mapfiles/kml/shapes/';
        var icons = {
          active: {
            icon:'resources/img/img01.png',
            content:'<h1> New Site </h1>'
          },
          warning: {
            icon: 'resources/img/img01.png',
             content:'<h1> New Site </h1>'
          },
          down: {
            icon:'resources/img/img02.png',
             content:'<h1> New Site </h1>'
          },
          offline: {
            icon: 'resources/img/img01.png',
             content:'<h1> New Site </h1>'
          }
        };

        var features = [
          {
            position: new google.maps.LatLng(29.1732234, 76.9003276),
            type: 'active',
            url:'site.html'
          }, {
            position: new google.maps.LatLng(13.3524101, 74.7906418),
            type: 'warning',
            url:'equipmentmaster.html'
          }, {
            position: new google.maps.LatLng(8.4548082, 76.9062494),
            type: 'down',
            url:'customerview.html'
          }, {
            position: new google.maps.LatLng(23.4548082, 88.9062494),
            type: 'offline',
            url:'statemaster.html'
          }
        ];

        // Create markers.
        features.forEach(function(feature) {
          var marker = new google.maps.Marker({
            position: feature.position,
            icon: icons[feature.type].icon,
            map: map,
            url:feature.url
          });
          google.maps.event.addListener(marker, 'click', function() {
      window.location.href = marker.url;
    });
        });
      }
    </script>
      <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-30 p-r-30 ">
            <a class="builder-close quickview-toggle pg-close" data-toggle="quickview" data-toggle-element="#builder" href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary m-t-10" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#newconfiguration">Configuration Entry</a></li>
                              <li><a href="#configurationlist">Configuration List</a></li>                              
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
   
   
   
   
   
   
		  <script src="resources/js/pace.min.js" type="text/javascript"></script>
		  <script src="resources/js/jquery.gotop.js"></script>
		  <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
		  <script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
		  <script src="resources/js/tether.min.js" type="text/javascript"></script>
		 <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
		  <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
		  <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
		  <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
		  <script src="resources/js/jquery.actual.min.js"></script>
		  <script src="resources/js/jquery.scrollbar.min.js"></script>
	      <script src="resources/js/select2.full.min.js" type="text/javascript" ></script>
          <script src="resources/js/classie.js" type="text/javascript" ></script>
   	  	  <script src="resources/js/switchery.min.js" type="text/javascript"></script>
		  <script src="resources/js/pages.min.js"></script>
		  <script src="resources/js/card.js" type="text/javascript"></script>
		  <script src="resources/js/scripts.js" type="text/javascript"></script>
		  <script src="resources/js/demo.js" type="text/javascript"></script>
		  <script src="resources/js/jquery.easing.min.js"></script>
		  <script src="resources/js/jquery.fadethis.js"></script>
		  
			<script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
			<script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      		<script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      		<script src="resources/js/calendar.min.js" type="text/javascript" ></script>
      		<script src="resources/js/jquery.selectlistactions.js"></script>
	  		<script type="text/javascript" src="resources/js/jasny-bootstrap.js"></script>
		
		  
	  <script src="https:/www.amcharts.com/lib/3/amcharts.js"></script>
      <script src="https://www.amcharts.com/lib/3/serial.js"></script>
      <script src="https://www.amcharts.com/lib/3/plugins/export/export.min.js"></script>
      <script src="https://www.amcharts.com/lib/3/themes/light.js"></script>
      <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAM0gP_9pEey9sNSRgSt8Lh0cL3bkhp1Eo&callback=initMap"></script>



    <div id="gotop"></div>




<!-- Address Popup Start !-->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Address Notification Bar</h4>
      </div>
      <div class="modal-body popup-hei">
            <div class="col-md-3 add-list">
               <ul>
                  <li class="recent">Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
               </ul>
            </div>
            <div class="col-md-9">
              <div class="data-cont">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Initial Comment</p>
                  <div class="row">
                     <div class="col-md-3"><p class="add-text">Recommeneded<br> Action</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Recommeneded Action" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Assign To</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Assign To" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Date</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui calendar width" id="example2">
                                 <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="Date" class="width">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            <div class="col-md-12 data-deta">
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Equipement Id</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Equipement Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Site Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Site Code</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Customer</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Status</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Event Description</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Date / Time</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">23-10-2017 / 1:00</p></div>
               </div>
            </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Save</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Address Popup End !-->  



<!-- Comment Popup Start !-->
      <!-- Modal -->
<div id="myCommentModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comment Bar</h4>
      </div>
      <div class="modal-body popup-hei">
            <div class="col-md-4">
               <div class="cmt-list">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Events</p>
                  <ul>
                  <li>
                     <p class="one-of-two">
                                    <span class="text-warning fs-12">#31</span>
                                 </p>
                                 <p class="two-of-two p-l-10 fs-12">
                                    <span class="text-master">Webdysun </span>
                                    <span class="text-warning pull-right">20-07-2017</span>
                                    <br>
                                    <span class="text-masters">Site Name</span>
                                    <span class="text-warning pull-right">15:15</span>
                                    <br>
                                    <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                 </p>
                  </li>
                 
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>

                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
               </ul>
               </div>
               
            </div>
            <div class="col-md-8">
              <div class="data-cont">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Events Comment</p>
                  <div class="conte">
                     <div class="row border">
                        <div class="col-lg-3 col-md-3 col-xs-3 center">
                           <i class="fa fa-user fa-4x"></i>
                        </div>
                        <div class="col-md-9 col-xs-9 col-lg-9">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                              <div class="cmtcontent">
                                 <p><strong>Initial Comment :</strong><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                              </div>
                        </div>
                     </div>
                     <div class="hr-line"></div>
                     <div class="row border">
                        <div class="col-md-11 col-xs-11 col-lg-11">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left ml">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right ml"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                           <div class="padd">
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           </div>
                        </div>
                        <div class="col-md-1 col-xs-1 col-lg-1 center">
                           <i class="fa fa-user fa-3x m-t-15"></i>
                        </div>
                     </div>
                     <div class="hr-line"></div>
                     <div class="row border">
                        <div class="col-lg-2 col-md-2 col-xs-2 center">
                           <i class="fa fa-user fa-3x m-t-15"></i>
                        </div>
                        <div class="col-lg-10 col-md-10 col-xs-10">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left ml">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right ml"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                           <div class="padd">
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           </div>
                        </div>
                     </div>
                  </div>
                     <div class="hr-line"></div>
                     <div class="row">
                        <div class="col-md-9 col-lg-9 col-xs-7">
                           <form class="ui form mt-15 m-l-20">
                              <div class="field">
                                 <input name="first-name" placeholder="Customer Name" type="text">
                              </div>
                           </form>
                        </div>
                        <div class="col-md-1 col-lg-1 col-xs-1 m-t-15">
                           <input type="button" class="btn btn-success" value="Send" name="">
                        </div>
                     </div>

               </div>
            </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Save</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Comment Popup End !--> 


<!-- Share Popup Start !-->
<!-- Modal -->
<div id="myShareModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Share</h4>
      </div>
      <div class="modal-body popup-hei">
         <form class="ui form">
            <div class="field">
               <label class="fields">Subject</label>
               <input name="first-name" placeholder="Subject" type="text" class="txt-padd">
            </div>
         </form>
         <form class="ui form mt-15">
            <div class="field">
               <label class="fields">Additional Notes</label>
               <input name="first-name" placeholder="Additional Notes" type="text" class="txt-padd">
            </div>
         </form>
         <div class="row style-select">
         <div class="col-md-12 mt-15">
            <div class="subject-info-box-1">
               <!-- <label class="fields">Available Superheroes</label> -->
               <select multiple class="form-control" id="lstBox1">
                  <option value="123">Superman</option>
                  <option value="456">Batman</option>
                  <option value="789">Spiderman</option>
                  <option value="654">Captain America</option>
               </select>
            </div>

            <div class="subject-info-arrows text-center">
               <br /><br />
               <input type='button' id='btnAllRight' value="">>" class="btn btn-default" /><br />
               <input type='button' id='btnRight' value=">" class="btn btn-default" /><br />
               <input type='button' id='btnLeft' value="/<" class="btn btn-default" /><br />
               <input type='button' id='btnAllLeft' value="/<<" class="btn btn-default" />
            </div>

            <div class="subject-info-box-2">
               <!-- <label class="fields">Superheroes You Have Selected</label> -->
               <select multiple class="form-control" id="lstBox2">
                  <option value="765">Nick Fury</option>
                  <option value="698">The Hulk</option>
                  <option value="856">Iron Man</option>
               </select>
            </div>

            <div class="clearfix"></div>
         </div>
      </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Send</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Share Popup End !--> 


      <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      <script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>
       
       
       <script type='text/javascript'>
	window.onload=function(){
	$('.dropdown').on('show.bs.dropdown', function () {
	   var length = parseInt($('.dropdown-menu').css('width'), 10) * -1;
	   $('.dropdown-menu').css('left', length);
	})
	}
	</script>
	

<script>
        $('#btnRight').click(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });
        $('#lstBox1').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnAllRight').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnLeft').click(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });

         $('#lstBox2').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
        $('#btnAllLeft').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
    </script>
 
 
 
 
  </body>
</html>

