


package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.UnitOfMeasurementDAO; 
import com.mestech.eampm.model.UnitOfMeasurement;
 
@Service

public class UnitOfMeasurementServiceImpl  implements UnitOfMeasurementService {
	
	@Autowired
    private UnitOfMeasurementDAO unitofmeasurementDAO;
 
    public void setunitofmeasurementDAO(UnitOfMeasurementDAO unitofmeasurementDAO) {
        this.unitofmeasurementDAO = unitofmeasurementDAO;
    }
 
    //@Override
    @Transactional
    public void addUnitOfMeasurement(UnitOfMeasurement unitofmeasurement) {
    	unitofmeasurementDAO.addUnitOfMeasurement(unitofmeasurement);
    }
 
    //@Override
    @Transactional
    public void updateUnitOfMeasurement(UnitOfMeasurement unitofmeasurement) {
        unitofmeasurementDAO.updateUnitOfMeasurement(unitofmeasurement);
    }
 
    //@Override
    @Transactional
    public List<UnitOfMeasurement> listUnitOfMeasurements() {
        return this.unitofmeasurementDAO.listUnitOfMeasurements();
    }
 
    //@Override
    @Transactional
    public UnitOfMeasurement getUnitOfMeasurementById(int id) {
        return unitofmeasurementDAO.getUnitOfMeasurementById(id);
    }
 
    //@Override
    @Transactional
    public void removeUnitOfMeasurement(int id) {
        unitofmeasurementDAO.removeUnitOfMeasurement(id);
    }
    
}
