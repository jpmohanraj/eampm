package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EquipmentCategoryDAO; 
import com.mestech.eampm.model.EquipmentCategory;
 
@Service

public class EquipmentCategoryServiceImpl  implements EquipmentCategoryService {
	
	@Autowired
    private EquipmentCategoryDAO equipmentcategoryDAO;
 
    public void setequipmentcategoryDAO(EquipmentCategoryDAO equipmentcategoryDAO) {
        this.equipmentcategoryDAO = equipmentcategoryDAO;
    }
 
    //@Override
    @Transactional
    public void addEquipmentCategory(EquipmentCategory equipmentcategory) {
    	equipmentcategoryDAO.addEquipmentCategory(equipmentcategory);
    }
 
    //@Override
    @Transactional
    public void updateEquipmentCategory(EquipmentCategory equipmentcategory) {
        equipmentcategoryDAO.updateEquipmentCategory(equipmentcategory);
    }
 
    //@Override
    @Transactional
    public List<EquipmentCategory> listEquipmentCategories() {
        return this.equipmentcategoryDAO.listEquipmentCategories();
    }
 
    //@Override
    @Transactional
    public EquipmentCategory getEquipmentCategoryById(int id) {
        return equipmentcategoryDAO.getEquipmentCategoryById(id);
    }
 
    //@Override
    @Transactional
    public void removeEquipmentCategory(int id) {
        equipmentcategoryDAO.removeEquipmentCategory(id);
    }
    
}
