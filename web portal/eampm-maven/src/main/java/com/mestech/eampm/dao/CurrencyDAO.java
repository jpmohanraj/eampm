package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.Currency;

public interface CurrencyDAO {
	 
	public void addCurrency(Currency currency);
	    
	public void updateCurrency(Currency currency);
	    
	public Currency getCurrencyById(int id);
	    
	public void removeCurrency(int id);
	    
	public List<Currency> listCurrencies();
}
