
package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketTransaction;

@Repository
public class TicketTransactionDAOImpl implements TicketTransactionDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addTicketTransaction(TicketTransaction ticketdetail) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(ticketdetail);
       
   }

   //@Override
   public void updateTicketTransaction(TicketTransaction ticketdetail) {
       Session session = sessionFactory.getCurrentSession();
       session.update(ticketdetail);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<TicketTransaction> listTicketTransactions() {
       Session session = sessionFactory.getCurrentSession();
       List<TicketTransaction> TicketTransactionList = session.createQuery("from TicketTransaction").list();
       
       
       
       return TicketTransactionList;
   }

   //@Override
   public TicketTransaction getTicketTransactionById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       TicketTransaction ticketdetail = (TicketTransaction) session.get(TicketTransaction.class, new Integer(id));
       return ticketdetail;
   }

   
   public TicketTransaction getTicketTransactionByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       TicketTransaction ticketdetail = null;
       
       try
       {
    	   ticketdetail = (TicketTransaction) session.createQuery("from TicketTransaction Order By " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {
    	   
       }
       catch(Exception ex)
       {
    	   
       }
       
       return ticketdetail;
   }
   
   
   
   
   public List<TicketTransaction> getTicketTransactionListByUserId(int userId)
	{
		  Session session = sessionFactory.getCurrentSession();
	       List<TicketTransaction> TicketTransactionList = session.createQuery("from TicketTransaction where  ActiveFlag='1' and SiteID in (Select SiteID from Site where CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "'))").list();
	       
	       return TicketTransactionList;
	}
	
   
   
   public List<TicketTransaction> getTicketTransactionListBySiteId(int siteId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<TicketTransaction> TicketTransactionList = session.createQuery("from TicketTransaction where ActiveFlag='1' and SiteID='" + siteId + "'").list();
       
       return TicketTransactionList;
   }

   public List<TicketTransaction> getTicketTransactionListByTicketId(int ticketId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<TicketTransaction> TicketTransactionList = session.createQuery("from TicketTransaction where ActiveFlag='1' and TicketID='" + ticketId + "' order by TransactionId").list();
       
       return TicketTransactionList;
   }
	
   
   
   //@Override
   public void removeActivity(int id) {
       Session session = sessionFactory.getCurrentSession();
       TicketTransaction ticketdetail = (TicketTransaction) session.get(TicketTransaction.class, new Integer(id));
       
       //De-activate the flag
       ticketdetail.setActiveFlag(0);
       
       if(null != ticketdetail){
    	   session.update(ticketdetail);
           //session.delete(activity);
       }
   }

@Override
public void removeTicketTransaction(int id) {
	// TODO Auto-generated method stub
	
}
}
