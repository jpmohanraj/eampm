

package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.ParameterListDAO; 
import com.mestech.eampm.model.ParameterList;
 
@Service

public class ParameterListServiceImpl  implements ParameterListService {
	
	@Autowired
    private ParameterListDAO parameterlistDAO;
 
    public void setparameterlistDAO(ParameterListDAO parameterlistDAO) {
        this.parameterlistDAO = parameterlistDAO;
    }
 
    //@Override
    @Transactional
    public void addParameterList(ParameterList parameterlist) {
    	parameterlistDAO.addParameterList(parameterlist);
    }
 
    //@Override
    @Transactional
    public void updateParameterList(ParameterList parameterlist) {
        parameterlistDAO.updateParameterList(parameterlist);
    }
 
    //@Override
    @Transactional
    public List<ParameterList> listParameterLists() {
        return this.parameterlistDAO.listParameterLists();
    }
 
    //@Override
    @Transactional
    public ParameterList getParameterListById(int id) {
        return parameterlistDAO.getParameterListById(id);
    }
 
    //@Override
    @Transactional
    public void removeParameterList(int id) {
        parameterlistDAO.removeParameterList(id);
    }
    
}

