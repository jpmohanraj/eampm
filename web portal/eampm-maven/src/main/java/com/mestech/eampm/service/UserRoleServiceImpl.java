

package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.UserRoleDAO; 
import com.mestech.eampm.model.UserRole;
 
@Service

public class UserRoleServiceImpl  implements UserRoleService {
	
	@Autowired
    private UserRoleDAO userroleDAO;
 
    public void setuserroleDAO(UserRoleDAO userroleDAO) {
        this.userroleDAO = userroleDAO;
    }
 
    //@Override
    @Transactional
    public void addUserRole(UserRole userrole) {
    	userroleDAO.addUserRole(userrole);
    }
 
    //@Override
    @Transactional
    public void updateUserRole(UserRole userrole) {
        userroleDAO.updateUserRole(userrole);
    }
 
    //@Override
    @Transactional
    public List<UserRole> listUserRoles() {
        return this.userroleDAO.listUserRoles();
    }
 
    //@Override
    @Transactional
    public UserRole getUserRoleById(int id) {
        return userroleDAO.getUserRoleById(id);
    }
 
    //@Override
    @Transactional
    public void removeUserRole(int id) {
        userroleDAO.removeUserRole(id);
    }
    
}

