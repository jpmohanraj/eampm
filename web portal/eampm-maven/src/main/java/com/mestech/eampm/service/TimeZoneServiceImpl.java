


package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.TimeZoneDAO; 
import com.mestech.eampm.model.TimeZone;
 
@Service

public class TimeZoneServiceImpl  implements TimeZoneService {
	
	@Autowired
    private TimeZoneDAO timezoneDAO;
 
    public void settimezoneDAO(TimeZoneDAO timezoneDAO) {
        this.timezoneDAO = timezoneDAO;
    }
 
    //@Override
    @Transactional
    public void addTimeZone(TimeZone timezone) {
    	timezoneDAO.addTimeZone(timezone);
    }
 
    //@Override
    @Transactional
    public void updateTimeZone(TimeZone timezone) {
        timezoneDAO.updateTimeZone(timezone);
    }
 
    //@Override
    @Transactional
    public List<TimeZone> listTimeZones() {
        return this.timezoneDAO.listTimeZones();
    }
 
    //@Override
    @Transactional
    public TimeZone getTimeZoneById(int id) {
        return timezoneDAO.getTimeZoneById(id);
    }
 
    //@Override
    @Transactional
    public void removeTimeZone(int id) {
        timezoneDAO.removeTimeZone(id);
    }
    
}

