package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="mSite")
public class Site implements Serializable{

	 private static final long serialVersionUID = -723583058586873479L;
	 

	@Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "SiteID")
	 private Integer SiteId;
	 
	 @Column(name="CustomerID")
	 private Integer CustomerID;
	 
	 @Transient
	 private String CustomerName;	
		
	 
	 @Column(name="SiteTypeID")
	 private Integer SiteTypeID;
	 
	 @Transient
	 private String SiteTypeName;	
		
	 
	 @Column(name="SiteCode")
	 private String SiteCode;
	 
	 @Column(name="SiteName")
	 private String SiteName;
	 
	 @Column(name="SiteDescription")
	 private String SiteDescription;
	 
	 @Column(name="ContactPerson")
	 private String ContactPerson;
	 
	 @Column(name="Address")
	 private String Address;
	 
	 @Column(name="City")
	 private String City;
	 
	 @Column(name="StateID")
	 private Integer StateID;
	 

	 @Transient
	 private String StateName;	
		
	 
	 @Column(name="CountryID")
	 private Integer CountryID;
	 

	 @Transient
	 private String CountryName;	
		
	 
	 @Column(name="PostalCode")
	 private String PostalCode;
	 
	 @Column(name="Longitude")
	 private String Longitude;
	 
	 @Column(name="Latitude")
	 private String Latitude;
	 
	 @Column(name="Altitude")
	 private String Altitude;
	 
	 @Column(name="SiteImage")
	 private String SiteImage;
	 
	 
	 @Column(name="InstallationCapacity")
	 private double InstallationCapacity;
	 
	 
	 
	 @Column(name="SitePONumber")
	 private String SitePONumber;
	 
	 /*@Column(name="SitePODate")
	 private Date SitePODate;
	 
	 @Column(name="SiteHandoverDate")
	 private Date SiteHandoverDate;
 
	 
	 @Column(name="SiteCommisioningDate")
	 private Date SiteCommisioningDate;
 
	 
	 @Column(name="InstalledOn")
	 private Date InstalledOn;
*/ 
	 
	 @Column(name="SiteOperator")
	 private String SiteOperator;
 
	 
	 @Column(name="SiteManafacturer")
	 private String SiteManafacturer;
 
	 
	 @Column(name="ModuleName")
	 private String ModuleName;
 
	 
	 @Column(name="CommunicationType")
	 private String CommunicationType;
 
	 
	 @Column(name="CollectionType")
	 private String CollectionType;
 
	 
	 @Column(name="FileType")
	 private String FileType;
 
	 
	 
	 @Column(name="CustomerReference")
	 private String CustomerReference;
 

	 @Column(name="CustomerNaming")
	 private String CustomerNaming;
 
	 
/*	 @Column(name="Income")
	 private float Income;
*/	 
	 
	 @Column(name="CurrencyID")
	 private Integer CurrencyID;
	 
	 @Transient
	 private String CurrencyName;	
	 
	 
	 @Column(name="EmailID")
	 private String EmailID;
	 
	 
	 @Column(name="Mobile")
	 private String Mobile;
	
	 @Column(name="Telephone")
	 private String Telephone;
	 
	 @Column(name="Fax")
	 private String Fax;
	 
	  @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="CreatedBy")
	 private Integer CreatedBy;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 @Column(name="LastUpdatedBy")
	 private Integer LastUpdatedBy;

	 @Column(name="DataLoggerID")
	 private Integer DataLoggerID;
	 

	 @Column(name="OperationMode")
	 private Integer OperationMode;

	 @Column(name="RemoteFtpServer")
	 private String RemoteFtpServer;

	 @Column(name="RemoteFtpUserName")
	 private String RemoteFtpUserName;

	 @Column(name="RemoteFtpPassword")
	 private String RemoteFtpPassword;

	
	@Column(name="RemoteFtpServerPort")
	 private String RemoteFtpServerPort;

	 @Column(name="RemoteFtpDirectoryPath")
	 private String RemoteFtpDirectoryPath;

	 @Column(name="FilePrefix")
	 private String FilePrefix;

	 @Column(name="FileSuffix")
	 private String FileSuffix;

	 @Column(name="FileSubstring")
	 private String FileSubstring;


	 @Column(name="SubFolder")
	 private Integer SubFolder;

	 @Column(name="LocalFtpDirectory")
	 private String LocalFtpDirectory;

	 @Column(name="LocalFtpDirectoryPath")
	 private String LocalFtpDirectoryPath;


	 @Column(name="LocalFtpHomeDirectory")
	 private String LocalFtpHomeDirectory;
	 

	 @Column(name="LocalFtpUserName")
	 private String LocalFtpUserName;
	 

	 @Column(name="LocalFtpPassword")
	 private String LocalFtpPassword;
	 

	 @Column(name="ApiUrl")
	 private String ApiUrl;
	 

	 @Column(name="ApiKey")
	 private String ApiKey;
	 

	 
	 @Column(name="ServiceCode")
	 private String ServiceCode;
	 
	/* @Transient 
	private SiteStatus siteStatus;
	 
	 
	 @OneToOne(fetch = FetchType.LAZY, mappedBy = "mSite", cascade = CascadeType.ALL)
	 public SiteStatus getSiteStatus() {
			return this.siteStatus;
		}

		public void setSiteStatus(SiteStatus siteStatus) {
			this.siteStatus = siteStatus;
		}*/
		
		
	/* @Transient
	 private Set<SiteStatus> siteStatus = new HashSet<SiteStatus>(0);

	 @OneToOne(fetch = FetchType.LAZY, mappedBy = "mSite")
	 public Set<SiteStatus> getSiteStatus() {
			return siteStatus;
		}

		public void setSiteStatus(Set<SiteStatus> siteStatus) {
			siteStatus = siteStatus;
		}*/
		
	 
		
	public Integer getSiteId() {
		return SiteId;
	}

	public void setSiteId(Integer siteId) {
		SiteId = siteId;
	}

	public Integer getCustomerID() {
		return CustomerID;
	}

	public void setCustomerID(Integer customerID) {
		CustomerID = customerID;
	}

	public Integer getSiteTypeID() {
		return SiteTypeID;
	}

	public void setSiteTypeID(Integer siteTypeID) {
		SiteTypeID = siteTypeID;
	}

	public String getSiteCode() {
		return SiteCode;
	}

	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}

	public String getSiteName() {
		return SiteName;
	}

	public void setSiteName(String siteName) {
		SiteName = siteName;
	}

	public String getSiteDescription() {
		return SiteDescription;
	}

	public void setSiteDescription(String siteDescription) {
		SiteDescription = siteDescription;
	}

	public String getContactPerson() {
		return ContactPerson;
	}

	public void setContactPerson(String contactPerson) {
		ContactPerson = contactPerson;
	}

	public String getAddress() {
		return Address;
	}

	public void setAddress(String address) {
		Address = address;
	}

	public String getCity() {
		return City;
	}

	public void setCity(String city) {
		City = city;
	}

	public Integer getStateID() {
		return StateID;
	}

	public void setStateID(Integer stateID) {
		StateID = stateID;
	}

	public Integer getCountryID() {
		return CountryID;
	}

	public void setCountryID(Integer countryID) {
		CountryID = countryID;
	}

	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String postalCode) {
		PostalCode = postalCode;
	}

	public String getLongitude() {
		return Longitude;
	}

	public void setLongitude(String longitude) {
		Longitude = longitude;
	}

	public String getLatitude() {
		return Latitude;
	}

	public void setLatitude(String latitude) {
		Latitude = latitude;
	}

	public String getAltitude() {
		return Altitude;
	}

	public void setAltitude(String altitude) {
		Altitude = altitude;
	}

	public String getSiteImage() {
		return SiteImage;
	}

	public void setSiteImage(String siteImage) {
		SiteImage = siteImage;
	}

	public double getInstallationCapacity() {
		return InstallationCapacity;
	}

	public void setInstallationCapacity(double installationCapacity) {
		InstallationCapacity = installationCapacity;
	}

	public String getSitePONumber() {
		return SitePONumber;
	}

	public void setSitePONumber(String sitePONumber) {
		SitePONumber = sitePONumber;
	}
/*
	public Date getSitePODate() {
		return SitePODate;
	}

	public void setSitePODate(Date sitePODate) {
		SitePODate = sitePODate;
	}

	public Date getSiteHandoverDate() {
		return SiteHandoverDate;
	}

	public void setSiteHandoverDate(Date siteHandoverDate) {
		SiteHandoverDate = siteHandoverDate;
	}

	public Date getSiteCommisioningDate() {
		return SiteCommisioningDate;
	}

	public void setSiteCommisioningDate(Date siteCommisioningDate) {
		SiteCommisioningDate = siteCommisioningDate;
	}

	public Date getInstalledOn() {
		return InstalledOn;
	}

	public void setInstalledOn(Date installedOn) {
		InstalledOn = installedOn;
	}
*/
	public String getSiteOperator() {
		return SiteOperator;
	}

	public void setSiteOperator(String siteOperator) {
		SiteOperator = siteOperator;
	}

	public String getSiteManafacturer() {
		return SiteManafacturer;
	}

	public void setSiteManafacturer(String siteManafacturer) {
		SiteManafacturer = siteManafacturer;
	}

	public String getModuleName() {
		return ModuleName;
	}

	public void setModuleName(String moduleName) {
		ModuleName = moduleName;
	}

	public String getCommunicationType() {
		return CommunicationType;
	}

	public void setCommunicationType(String communicationType) {
		CommunicationType = communicationType;
	}

	public String getCollectionType() {
		return CollectionType;
	}

	public void setCollectionType(String collectionType) {
		CollectionType = collectionType;
	}

	public String getFileType() {
		return FileType;
	}

	public void setFileType(String fileType) {
		FileType = fileType;
	}

/*	public float getIncome() {
		return Income;
	}

	public void setIncome(float income) {
		Income = income;
	}
*/
	public Integer getCurrencyID() {
		return CurrencyID;
	}

	public void setCurrencyID(Integer currencyID) {
		CurrencyID = currencyID;
	}

	public String getEmailID() {
		return EmailID;
	}

	public void setEmailID(String emailID) {
		EmailID = emailID;
	}

	public String getMobile() {
		return Mobile;
	}

	public void setMobile(String mobile) {
		Mobile = mobile;
	}

	public String getTelephone() {
		return Telephone;
	}

	public void setTelephone(String telephone) {
		Telephone = telephone;
	}

	public String getFax() {
		return Fax;
	}

	public void setFax(String fax) {
		Fax = fax;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	 public String getCustomerName() {
		return CustomerName;
	}

	public void setCustomerName(String customerName) {
		CustomerName = customerName;
	}

	public String getSiteTypeName() {
		return SiteTypeName;
	}

	public void setSiteTypeName(String siteTypeName) {
		SiteTypeName = siteTypeName;
	}

	public String getStateName() {
		return StateName;
	}

	public void setStateName(String stateName) {
		StateName = stateName;
	}

	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String countryName) {
		CountryName = countryName;
	}

	public String getCurrencyName() {
		return CurrencyName;
	}

	public void setCurrencyName(String currencyName) {
		CurrencyName = currencyName;
	}

	 public Integer getDataLoggerID() {
			return DataLoggerID;
		}

		public void setDataLoggerID(Integer dataLoggerID) {
			DataLoggerID = dataLoggerID;
		}

		public Integer getOperationMode() {
			return OperationMode;
		}

		public void setOperationMode(Integer operationMode) {
			OperationMode = operationMode;
		}

		
		public String getFilePrefix() {
			return FilePrefix;
		}

		public void setFilePrefix(String filePrefix) {
			FilePrefix = filePrefix;
		}

		public String getFileSuffix() {
			return FileSuffix;
		}

		public void setFileSuffix(String fileSuffix) {
			FileSuffix = fileSuffix;
		}

		public String getFileSubstring() {
			return FileSubstring;
		}

		public void setFileSubstring(String fileSubstring) {
			FileSubstring = fileSubstring;
		}

		public Integer getSubFolder() {
			return SubFolder;
		}

		public void setSubFolder(Integer subFolder) {
			SubFolder = subFolder;
		}

		public String getCustomerReference() {
			return CustomerReference;
		}

		public void setCustomerReference(String customerReference) {
			CustomerReference = customerReference;
		}

		public String getCustomerNaming() {
			return CustomerNaming;
		}

		public void setCustomerNaming(String customerNaming) {
			CustomerNaming = customerNaming;
		}

		public String getRemoteFtpServer() {
			return RemoteFtpServer;
		}

		public void setRemoteFtpServer(String remoteFtpServer) {
			RemoteFtpServer = remoteFtpServer;
		}

		public String getRemoteFtpUserName() {
			return RemoteFtpUserName;
		}

		public void setRemoteFtpUserName(String remoteFtpUserName) {
			RemoteFtpUserName = remoteFtpUserName;
		}

		public String getRemoteFtpPassword() {
			return RemoteFtpPassword;
		}

		public void setRemoteFtpPassword(String remoteFtpPassword) {
			RemoteFtpPassword = remoteFtpPassword;
		}

		public String getRemoteFtpServerPort() {
			return RemoteFtpServerPort;
		}

		public void setRemoteFtpServerPort(String remoteFtpServerPort) {
			RemoteFtpServerPort = remoteFtpServerPort;
		}

		public String getRemoteFtpDirectoryPath() {
			return RemoteFtpDirectoryPath;
		}

		public void setRemoteFtpDirectoryPath(String remoteFtpDirectoryPath) {
			RemoteFtpDirectoryPath = remoteFtpDirectoryPath;
		}

		public String getLocalFtpDirectory() {
			return LocalFtpDirectory;
		}

		public void setLocalFtpDirectory(String localFtpDirectory) {
			LocalFtpDirectory = localFtpDirectory;
		}

		public String getLocalFtpDirectoryPath() {
			return LocalFtpDirectoryPath;
		}

		public void setLocalFtpDirectoryPath(String localFtpDirectoryPath) {
			LocalFtpDirectoryPath = localFtpDirectoryPath;
		}

		public String getLocalFtpHomeDirectory() {
			return LocalFtpHomeDirectory;
		}

		public void setLocalFtpHomeDirectory(String localFtpHomeDirectory) {
			LocalFtpHomeDirectory = localFtpHomeDirectory;
		}

		public String getLocalFtpUserName() {
			return LocalFtpUserName;
		}

		public void setLocalFtpUserName(String localFtpUserName) {
			LocalFtpUserName = localFtpUserName;
		}

		public String getLocalFtpPassword() {
			return LocalFtpPassword;
		}

		public void setLocalFtpPassword(String localFtpPassword) {
			LocalFtpPassword = localFtpPassword;
		}

		public String getApiUrl() {
			return ApiUrl;
		}

		public void setApiUrl(String apiUrl) {
			ApiUrl = apiUrl;
		}

		public String getApiKey() {
			return ApiKey;
		}

		public void setApiKey(String apiKey) {
			ApiKey = apiKey;
		}

		public String getServiceCode() {
			return ServiceCode;
		}

		public void setServiceCode(String serviceCode) {
			ServiceCode = serviceCode;
		}

		
}
