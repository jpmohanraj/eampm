package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.Site;

public interface SiteService {

	public void addSite(Site site);
    
    public void updateSite(Site site);
    
    public Site getSiteById(int id);
    
    public Site getSiteByName(String siteName);

    public List<Site> getSiteListByCustomerId(int customerId);

    public List<Site> getSiteListByUserId(int userId);
    
    public List<Site> listSiteById(int id);
    
	public Site getSiteByMax(String MaxColumnName);
	
    public void removeSite(int id);
    
    public List<Site> listSites();	
}

