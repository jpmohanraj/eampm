package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EquipmentViewController {
	
	 @Autowired
	    private SiteService siteService;

	 @Autowired
	    private DataTransactionService dataTransactionService;

	 @Autowired
	    private EquipmentService equipmentService;
	 
	 
	 @Autowired
	    private CustomerService customerService;
	 
	 
	 @Autowired
	    private UserService userService;
	 

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 
	 
 @RequestMapping(value = "/equipmentview{id}&ref={refid}", method = RequestMethod.GET)
 public String listCustomerViews(@PathVariable("id") int id,@PathVariable("refid") int refid, Model model,HttpSession session) {
 	
	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	 {
		 return "redirect:/login";
	 }
	 
 	
 	EquipmentListBean objEquipmentListBean = new EquipmentListBean();
 	
 	Equipment objEquipment = equipmentService.getEquipmentById(id);
	List<DataTransaction> lstDataTransaction = dataTransactionService.getDataTransactionListByEquipmentId(id);
	
	
	 String ChartJsonData1 = "";
	 String ChartJsonData1Parameter = "";
	 String ChartJsonData1GraphValue = "";
	 

	 SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	 //SimpleDateFormat sdfchart = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
			
	 String varEquipmentRef ="";
	 String varSiteID ="";
	 String varEquipmentID ="";
	 String varEquipmentCode ="";
	 String varEquipmentName ="";
	 String varEquipmentType ="";
	 
	 
	 if(objEquipment.getCustomerReference()!=null)
	 {
		 varEquipmentRef = objEquipment.getCustomerNaming(); 
	 }
	 
	 if(objEquipment.getSiteID()!=null)
	 {
		 varSiteID = objEquipment.getSiteID().toString(); 
	 }
	 
	 if(objEquipment.getEquipmentId()!=null)
	 {
		 varEquipmentID = objEquipment.getEquipmentId().toString(); 
	 }
	 
	 if(objEquipment.getEquipmentCode()!=null)
	 {
		 varEquipmentCode = objEquipment.getEquipmentCode(); 
	 }
	 
	 if(objEquipment.getDisplayName()!=null)
	 {
		 varEquipmentName = objEquipment.getDisplayName(); 
	 }
	
	 if(objEquipment.getEquipmentTypeName()!=null)
	 {
		 varEquipmentType = objEquipment.getEquipmentTypeName(); 
	 }
	 
	 objEquipmentListBean.setSiteID(varSiteID);
     objEquipmentListBean.setEquipmentID(varEquipmentID);
     objEquipmentListBean.setEquipmentCode(varEquipmentCode);
     objEquipmentListBean.setEquipmentReference(varEquipmentRef);
     objEquipmentListBean.setEquipmentName(varEquipmentName);
     objEquipmentListBean.setEquipmentType(varEquipmentType);
    
    
	 
     for(int a=0;a<lstDataTransaction.size();a++)
     {
    	 String varChartDate = sdfchart.format(lstDataTransaction.get(a).getTimestamp()).toString();
    	 
    	 Double dbTotalEnergy = lstDataTransaction.get(a).getTotalEnergy();
    	 Double dbTotalEnergyPrevious = lstDataTransaction.get(a).getTotalEnergy();
    	 
    	 Double dbTotalEnergyPresent = 0.0;
    	 if(a!=0)
    	 {
    		 if(sdfdate.format(lstDataTransaction.get(a).getTimestamp()).toString().equals(sdfdate.format(lstDataTransaction.get(a-1).getTimestamp()).toString()))
    		 {
    			 if(a==1)
        		 {
        			 dbTotalEnergyPrevious = lstDataTransaction.get(a-1).getTotalEnergy();
            		 dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
        		 }
    			 else if(lstDataTransaction.get(a-1).getTotalEnergy() >= lstDataTransaction.get(a-2).getTotalEnergy())
        		 {
        			 dbTotalEnergyPrevious = lstDataTransaction.get(a-1).getTotalEnergy();
            		 dbTotalEnergyPresent = dbTotalEnergy - dbTotalEnergyPrevious;
        		 }
        		 
    		 }
    		 
    	 }
    	 
    	 
    	 String varChartValue = String.format("%.2f", dbTotalEnergyPresent * 1000 );
    	
    	 
    	 if(a==0)
    	 {
    		 ChartJsonData1="{\"date\": \"" + varChartDate + "\",\"value1\":" + varChartValue + "}";
    		 //ChartJsonData1="{\"date\": \"" + varChartDate + "\",\"value1\":\"" + varChartValue + "\"}";
    		 //ChartJsonData1="{\"date\": \"" + varChartDate + "\",\"value1\":\"" + varChartValue + "\"}";
    	 }
    	 else
    	 {
    		 ChartJsonData1 = ChartJsonData1 + ",{\"date\": \"" + varChartDate + "\",\"value1\":" + varChartValue + "}";
    		 //ChartJsonData1 = ChartJsonData1 + ",{\"date\": \"" + varChartDate + "\",\"value1\":\"" + varChartValue + "\"}";
    		 //ChartJsonData1 = ChartJsonData1 + ",{\"date\": \"" + varChartDate + "\",\"value1\":\"" + varChartValue + "\"}";
    		 	
    	 }
        		
     }
     

     ChartJsonData1Parameter = "{\"fromField\": \"value1\",\"toField\": \"value1\"}";
	 ChartJsonData1GraphValue = "{\"id\": \"g1\",\"valueField\": \"value1\",\"comparable\": true,\"lineThickness\": 2,\"title\":\"Equipment (" + varEquipmentRef + ") \",\"type\": \"smoothedLine\",\"useDataSetColors\": false,\"balloonText\": \"Equipment (" + varEquipmentRef + "):<b>[[value1]]</b>\",\"compareGraphBalloonText\": \"Equipment (" + varEquipmentRef + "):<b>[[value1]]</b>\"}";
	 //ChartJsonData1GraphValue = "{\"id\": \"g1\",\"valueField\": \"value1\",\"comparable\": true,\"lineThickness\": 2,\"title\":\"Equipment (" + varEquipmentRef + ") \",\"type\": \"smoothedLine\",\"useDataSetColors\": false,\"balloonText\": \"Equipment (" + varEquipmentRef + "):[[value1]]\",\"compareGraphBalloonText\": \"Equipment (" + varEquipmentRef + "):[[value1]]\"}";


     ChartJsonData1 = "[" + ChartJsonData1 + "]";
     ChartJsonData1Parameter = "[" + ChartJsonData1Parameter + "]";
     ChartJsonData1GraphValue = "[" + ChartJsonData1GraphValue + "]";
 	
    
     
     objEquipmentListBean.setChartJsonData1(ChartJsonData1);
     objEquipmentListBean.setChartJsonData1Parameter(ChartJsonData1Parameter);
     objEquipmentListBean.setChartJsonData1GraphValue(ChartJsonData1GraphValue);
     
     
     System.out.println(ChartJsonData1);
     
     
     
     
	 AccessListBean objAccessListBean = new AccessListBean();
	   
	    try
	    {
	    User objUser =  userService.getUserById(refid);
		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	 	
	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	   
	    objAccessListBean.setUserID(refid);
	    objAccessListBean.setUserName(objUser.getUserName());
	    
	    
	    
	    
	    Site objSite = siteService.getSiteById(objEquipment.getSiteID()); 
	    
	    if(objSite!=null)
	    {
	    	objAccessListBean.setMySiteID(objSite.getSiteId());
	  	    objAccessListBean.setMySiteName(objSite.getSiteName());
	  	    objAccessListBean.setMyCustomerID(objSite.getCustomerID());
	    	
	  	    Customer objCustomer = customerService.getCustomerById(objSite.getCustomerID()); 
	  	    if(objCustomer!=null)
	  	    {
	  	    	objAccessListBean.setMyCustomerName(objCustomer.getCustomerName());
	  	    }
	  	    else
	  	    {
	  	    	objAccessListBean.setMyCustomerName("Customer View");
	  	    }
	  	    
	    }
	    else
	    {
	    	objAccessListBean.setMySiteName("Site View");
	    }
	    
	    
	    
	    
	  
	    
	    
	    
	    if(objUser.getRoleID()!=4)
		{
			objAccessListBean.setCustomerListView("visible");
		}
	    
	    
	    for(int l=0;l<lstRoleActivity.size();l++)
	    {
	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setDashboard("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setOverview("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setSystemMonitoring("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setVisualization("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setAnalytics("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setPortfolioManagement("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setTicketing("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setForcasting("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setConfiguration("visible");
	    	}
	    }
	    
	    }
	    catch(Exception e)
	    {
	    	System.out.println(e.getMessage());
	    }
	    
	     model.addAttribute("access",objAccessListBean);
	     model.addAttribute("siteList", getDropdownList("Site",refid));
 	    model.addAttribute("userList", getDropdownList("User",refid));
 	   model.addAttribute("ticketcreation", new TicketDetail());
	     model.addAttribute("equipmentview", objEquipmentListBean);
    
     return "equipmentview";
 }
 
 public  Map<String,String> getDropdownList(String DropdownName , int refid) 
	{
    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
    	
    	if(DropdownName == "Site")
    	{
    		List<Site> ddlList = siteService.getSiteListByUserId(refid);
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
    		}
    		
    	}
    	else if(DropdownName == "User")
    	{
    		List<User> ddlList = userService.listFieldUsers();
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
    		}
    		
    	}
    	
		return ddlMap;
	}
 
}
