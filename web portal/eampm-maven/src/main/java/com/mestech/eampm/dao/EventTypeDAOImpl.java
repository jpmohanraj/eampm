

package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.EventType;

@Repository
public class EventTypeDAOImpl implements EventTypeDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addEventType(EventType eventtype) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(eventtype);
       
       System.out.println("DAO Impl Completed");
   }

   //@Override
   public void updateEventType(EventType eventtype) {
       Session session = sessionFactory.getCurrentSession();
       session.update(eventtype);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<EventType> listEventTypes() {
       Session session = sessionFactory.getCurrentSession();
       List<EventType> EventTypesList = session.createQuery("from EventType").list();
       
       return EventTypesList;
   }

   //@Override
   public EventType getEventTypeById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       EventType eventtype = (EventType) session.get(EventType.class, new Integer(id));
       return eventtype;
   }

   //@Override
   public void removeEventType(int id) {
       Session session = sessionFactory.getCurrentSession();
       EventType eventtype = (EventType) session.get(EventType.class, new Integer(id));
       if(null != eventtype){
           session.delete(eventtype);
       }
   }
}



