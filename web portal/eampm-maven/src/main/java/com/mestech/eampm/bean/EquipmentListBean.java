package com.mestech.eampm.bean;

public class EquipmentListBean {


	 private String SiteID;
	 private String EquipmentID;
	 private String EquipmentCode;
	 private String EquipmentReference;
	 private String EquipmentName;
	 private String EquipmentType;
	 
	 private String ChartJsonData1;
	 private String ChartJsonData1Parameter ;
	 private String ChartJsonData1GraphValue;
     
     
	 public String getSiteID() {
		return SiteID;
	}
	public void setSiteID(String siteID) {
		SiteID = siteID;
	}
	public String getEquipmentID() {
		return EquipmentID;
	}
	public void setEquipmentID(String equipmentID) {
		EquipmentID = equipmentID;
	}
	public String getEquipmentCode() {
		return EquipmentCode;
	}
	public void setEquipmentCode(String equipmentCode) {
		EquipmentCode = equipmentCode;
	}
	public String getEquipmentReference() {
		return EquipmentReference;
	}
	public void setEquipmentReference(String equipmentReference) {
		EquipmentReference = equipmentReference;
	}
	public String getEquipmentName() {
		return EquipmentName;
	}
	public void setEquipmentName(String equipmentName) {
		EquipmentName = equipmentName;
	}
	public String getEquipmentType() {
		return EquipmentType;
	}
	public void setEquipmentType(String equipmentType) {
		EquipmentType = equipmentType;
	}
	public String getNetworkStatus() {
		return NetworkStatus;
	}
	public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}
	public String getTodayEnergy() {
		return TodayEnergy;
	}
	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}
	public String getTotalEnergy() {
		return TotalEnergy;
	}
	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}
	public String getPerformanceRatio() {
		return PerformanceRatio;
	}
	public void setPerformanceRatio(String performanceRatio) {
		PerformanceRatio = performanceRatio;
	}
	public String getLastUpdate() {
		return LastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		LastUpdate = lastUpdate;
	}
	private String NetworkStatus;
		
	 private String TodayEnergy;
		 private String TotalEnergy;
		 private String PerformanceRatio;
		 private String LastUpdate;


		public String getChartJsonData1() {
			return ChartJsonData1;
		}
		public void setChartJsonData1(String chartJsonData1) {
			ChartJsonData1 = chartJsonData1;
		}
		public String getChartJsonData1Parameter() {
			return ChartJsonData1Parameter;
		}
		public void setChartJsonData1Parameter(String chartJsonData1Parameter) {
			ChartJsonData1Parameter = chartJsonData1Parameter;
		}
		public String getChartJsonData1GraphValue() {
			return ChartJsonData1GraphValue;
		}
		public void setChartJsonData1GraphValue(String chartJsonData1GraphValue) {
			ChartJsonData1GraphValue = chartJsonData1GraphValue;
		}
}
