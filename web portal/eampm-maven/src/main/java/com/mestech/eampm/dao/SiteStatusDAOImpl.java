package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;

@Repository
public class SiteStatusDAOImpl implements SiteStatusDAO {
	
	@Autowired
    private SessionFactory sessionFactory;
   
	 @SuppressWarnings("unchecked")
	   //@Override
	   public List<SiteStatus> listSiteStatus() {
	       Session session = sessionFactory.getCurrentSession();
	       List<SiteStatus> SiteStatusList = session.createQuery("from SiteStatus").list();
	       
	       return SiteStatusList;
	   }
	 
	 
	 
	 public List<SiteStatus> getSiteStatusListByCustomerId(int customerId)
	   {
		   Session session = sessionFactory.getCurrentSession();
	       List<SiteStatus> SiteStatusList = session.createQuery("from SiteStatus where  ActiveFlag='1' and CustomerID='" + customerId + "'").list();
	       
	       return SiteStatusList;
	   }
	 
	 
	 public List<SiteStatus> getSiteStatusListByUserId(int userId)
	   {
		   Session session = sessionFactory.getCurrentSession();
	       List<SiteStatus> SiteStatusList = session.createQuery("from SiteStatus where  ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "')").list();
	       
	       return SiteStatusList;
	   }
	 

		public SiteStatus getSiteStatusBySiteId(int siteId)
		 {
			 Session session = sessionFactory.getCurrentSession();       
		       SiteStatus siteStatus = (SiteStatus) session.get(SiteStatus.class, new Integer(siteId));
		       return siteStatus;
		 }
		
		
}
