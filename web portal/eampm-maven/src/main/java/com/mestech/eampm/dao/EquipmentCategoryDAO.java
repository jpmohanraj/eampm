package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.EquipmentCategory;

public interface EquipmentCategoryDAO {
	 
	public void addEquipmentCategory(EquipmentCategory equipmentcategory);
	    
	public void updateEquipmentCategory(EquipmentCategory equipmentcategory);
	    
	public EquipmentCategory getEquipmentCategoryById(int id);
	    
	public void removeEquipmentCategory(int id);
	    
	public List<EquipmentCategory> listEquipmentCategories();
}
