package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.CustomerTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class CustomerController {
	
	 @Autowired
	    private CustomerService customerService;
	 @Autowired
	    private CustomerTypeService customertypeService;
	 @Autowired
	    private CountryService countryService;
	 @Autowired
	    private StateService stateService;
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	 
	 public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	    

		 public  Map<String,String> getDropdownList(String DropdownName) 
			{
		    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
		    	
		    	if(DropdownName == "Customer")
		    	{
		    		List<Customer> ddlList = customerService.listCustomers();
		    		
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
		    		}
		    		
		    	}
		    	else if(DropdownName == "CustomerType")
		    	{
		    		List<CustomerType> ddlList = customertypeService.listCustomerTypes();
		    		
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getCustomerTypeId().toString(), ddlList.get(i).getCustomerType());
		    		}
		    		
		    	}
		    	else if(DropdownName == "CustomerGroup")
		    	{
		    		ddlMap.put("Primary", "Primary");
					ddlMap.put("Secondary", "Secondary");
		    	
		    	}
		    	else if(DropdownName == "Country")
		    	{
		    		List<Country> ddlList = countryService.listCountries();
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getCountryId().toString(), ddlList.get(i).getCountryName());
		    		}
		    	}
		    	else if(DropdownName == "State")
		    	{
		    		List<State> ddlList = stateService.listStates();
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getStateId().toString(), ddlList.get(i).getStateName());
		    		}
		    	}
		    	
				return ddlMap;
			}
		 
		 
		 public List<Customer> getUpdatedCustomerList()
		 {
			 List<Customer> ddlCustomersList = customerService.listCustomers();
			 
			 
			 List<Customer> ddlPrimaryCustomersList = customerService.listCustomers();
			 List<CustomerType> ddlCustomerTypeList = customertypeService.listCustomerTypes();
			 List<Country> ddlCountryList = countryService.listCountries();
			 List<State> ddlStateList = stateService.listStates();
			 
			 
			 
		        for(int i=0;i<ddlCustomersList.size();i++)
		        {
		        	List<Customer> lstPrimaryCustomerList = new ArrayList<Customer>();
					List<CustomerType> lstCustomerTypeList = new ArrayList<CustomerType>();
					List<Country> lstCountryList = new ArrayList<Country>();
					List<State> lstStateList = new ArrayList<State>();
					 
		        	
		        	int pcid = ddlCustomersList.get(i).getCustomerId();
					int ctype = ddlCustomersList.get(i).getCustomerTypeID();
					int countryid = ddlCustomersList.get(i).getCountryID();
					int stateid = ddlCustomersList.get(i).getStateID();
					int cid =0;
										
					if(ddlCustomersList.get(i).getPrimaryCustomer() != null)
		        	{ pcid = ddlCustomersList.get(i).getPrimaryCustomer().intValue();}
		        	
					 
					lstPrimaryCustomerList = ddlPrimaryCustomersList.stream().filter(p -> p.getCustomerId() == cid).collect(Collectors.toList());
					lstCustomerTypeList = ddlCustomerTypeList.stream().filter(p -> p.getCustomerTypeId() == ctype ).collect(Collectors.toList());
					lstCountryList = ddlCountryList.stream().filter(p -> p.getCountryId() == countryid).collect(Collectors.toList());
					lstStateList = ddlStateList.stream().filter(p -> p.getStateId() == stateid ).collect(Collectors.toList());
					
		        	if(lstPrimaryCustomerList.size() >0)
		        	{
		        		ddlCustomersList.get(i).setPrimaryCustomerName(lstPrimaryCustomerList.get(0).getPrimaryCustomerName());
		        	}
		        	
		        	if(lstCustomerTypeList.size() >0)
		        	{
		        		ddlCustomersList.get(i).setCustomerTypeName(lstCustomerTypeList.get(0).getCustomerType());
		        	}
		        	
		        	if(lstCountryList.size() >0)
		        	{
		        		ddlCustomersList.get(i).setCountryName(lstCountryList.get(0).getCountryName());
		        	}
		        	
		        	
		        	if(lstStateList.size() >0)
		        	{
		        		ddlCustomersList.get(i).setStateName(lstStateList.get(0).getStateName());
		        	}
		        	
		        	
		        	
		        	
		        	
		        	
		        	
		        	
		        }
		        
		        return ddlCustomersList;
		 }
		
		 

		 public String getCustomerCode()
		 {
			 Customer objCust =new Customer();
			 objCust =customerService.getCustomerByMax("CustomerCode");
			 
			 String CR = "";
			 
			 if(objCust != null)
			 {
				 CR = objCust.getCustomerCode();
			 }
			 
			
		    	if(CR ==null || CR =="")
		    	{
		    		CR ="CR00001";
		    	}
		    	else
		    	{
		    		try{
		    			int NextIndex = Integer.parseInt(CR.replaceAll("CR", "")) + 1;
		    			
		    			if(NextIndex >= 10000) {CR =  String.valueOf(NextIndex);}
		    			else if(NextIndex >= 1000) {CR = "0" + String.valueOf(NextIndex);}
		    			else if(NextIndex >= 100) {CR = "00" +  String.valueOf(NextIndex);}
		    			else if(NextIndex >= 10) {CR = "000" +  String.valueOf(NextIndex);}
		    			else {CR = "0000" +  String.valueOf(NextIndex);}
		    			
		    			CR = "CR" + CR;
		    		}
		    		catch(Exception e)
		    		{
		    			CR ="CR00001";
		    		}
		    	}
		    	
		    	return CR;
		 }
		 
		 
		 
		    @RequestMapping(value = "/customers", method = RequestMethod.GET)
		    public String listCustomers(Model model,HttpSession session) {
		    	
		    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
		    	 {
		    		 return "redirect:/login";
		    	 }
		    	 
		    	 
		    	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
		    	 
		    	 AccessListBean objAccessListBean = new AccessListBean();
		    	   
		    	    try
		    	    {
		    	    User objUser =  userService.getUserById(id);
		    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
		    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
		    	 	
		    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
		    	   
		    	    objAccessListBean.setUserID(id);
		    	    objAccessListBean.setUserName(objUser.getUserName());
		    	    
		    	    
		    	    
		    	    if(objUser.getRoleID()!=4)
		    		{
		    			objAccessListBean.setCustomerListView("visible");
		    		}
		    	    
		    	    
		    	    for(int l=0;l<lstRoleActivity.size();l++)
		    	    {
		    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setDashboard("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setOverview("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setSystemMonitoring("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setVisualization("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setAnalytics("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setPortfolioManagement("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setTicketing("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setForcasting("visible");
		    	    	}
		    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
		    	    	{
		    	    		objAccessListBean.setConfiguration("visible");
		    	    	}
		    	    }
		    	    
		    	    }
		    	    catch(Exception e)
		    	    {
		    	    	System.out.println(e.getMessage());
		    	    }
		    	    
		    	     model.addAttribute("access",objAccessListBean);
		    	    
		
		    	     
		        model.addAttribute("customer", new Customer());
		        
		        
		        
		        model.addAttribute("customerList", getUpdatedCustomerList());
		        model.addAttribute("customertypeList", getDropdownList("CustomerType"));
		        model.addAttribute("primarycustomerList", getDropdownList("Customer"));
		        model.addAttribute("customergroupList", getDropdownList("CustomerGroup"));
		        model.addAttribute("countryList", getDropdownList("Country"));
		        model.addAttribute("stateList", getDropdownList("State"));
		        model.addAttribute("activeStatusList", getStatusDropdownList());
		    	   	
		    	
		        
		        return "customer";
		    }
		 
		    
		    
	    
	    // Same method For both Add and Update Customer
	   // @RequestMapping(value = "/customer/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewcustomer", method = RequestMethod.POST)
	    public String addcustomer(@ModelAttribute("customer") Customer customer,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	
	    	try
	    	{
	    		if(customer.getCustomerPODate()!=null)
	    		{
	    			
	    		}
	    	}
	    	catch(Exception e)
	    	{
	    		
	    	}
	    	
	    	Date date = new Date();
	        if (customer.getCustomerId()==null || customer.getCustomerId() == 0) {
	            // new customer, add it
	        	customer.setCustomerCode(getCustomerCode());
	        	customer.setCreationDate(date);
	            customerService.addCustomer(customer);
	        } else {
	            // existing customer, call update
	        	customer.setLastUpdatedDate(date);
	            customerService.updateCustomer(customer);
	        }
	 
	        return "redirect:/customers";
	 
	    }
	 
	    //@RequestMapping("/customer/remove/{id}")
	    @RequestMapping("/removecustomer{id}")
	    public String removecustomer(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	 
	        customerService.removeCustomer(id);
	        return "redirect:/customers";
	    }
	 
	    //@RequestMapping("/customer/edit/{id}")
	    @RequestMapping("/editcustomer{id}")
	    public String editcustomer(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	        model.addAttribute("customer", customerService.getCustomerById(id));
	        
	        

	        model.addAttribute("customerList", getUpdatedCustomerList());
	        model.addAttribute("customertypeList", getDropdownList("CustomerType"));
	        model.addAttribute("primarycustomerList", getDropdownList("Customer"));
	        model.addAttribute("customergroupList", getDropdownList("CustomerGroup"));
	        model.addAttribute("countryList", getDropdownList("Country"));
	        model.addAttribute("stateList", getDropdownList("State"));
	        
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	   	
	        return "customer";
	    }
}
