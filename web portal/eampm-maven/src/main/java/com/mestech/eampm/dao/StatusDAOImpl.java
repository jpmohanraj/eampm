
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.Status;

@Repository
public class StatusDAOImpl implements StatusDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addStatus(Status status) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(status);
       
   }

   //@Override
   public void updateStatus(Status status) {
       Session session = sessionFactory.getCurrentSession();
       session.update(status);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<Status> listStatuses() {
       Session session = sessionFactory.getCurrentSession();
       List<Status> StatusesList = session.createQuery("from Status").list();
       
       return StatusesList;
   }

   //@Override
   public Status getStatusById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       Status status = (Status) session.get(Status.class, new Integer(id));
       return status;
   }

   //@Override
   public void removeStatus(int id) {
       Session session = sessionFactory.getCurrentSession();
       Status status = (Status) session.get(Status.class, new Integer(id));
       if(null != status){
           session.delete(status);
       }
   }
}
