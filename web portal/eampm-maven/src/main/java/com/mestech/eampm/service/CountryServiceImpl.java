package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CountryDAO; 
import com.mestech.eampm.model.Country;
 
@Service

public class CountryServiceImpl  implements CountryService {
	
	@Autowired
    private CountryDAO countryDAO;
 
    public void setcountryDAO(CountryDAO countryDAO) {
        this.countryDAO = countryDAO;
    }
 
    //@Override
    @Transactional
    public void addCountry(Country country) {
    	countryDAO.addCountry(country);
    }
 
    //@Override
    @Transactional
    public void updateCountry(Country country) {
        countryDAO.updateCountry(country);
    }
 
    //@Override
    @Transactional
    public List<Country> listCountries() {
        return this.countryDAO.listCountries();
    }
 
    //@Override
    @Transactional
    public Country getCountryById(int id) {
        return countryDAO.getCountryById(id);
    }
 
    //@Override
    @Transactional
    public void removeCountry(int id) {
        countryDAO.removeCountry(id);
    }
    
}
