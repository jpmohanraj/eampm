package com.mestech.eampm.service;


import java.util.List;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;

public interface EquipmentService {

	public void addEquipment(Equipment equipment);
    
    public void updateEquipment(Equipment equipment);
    
    public Equipment getEquipmentById(int id);

    public Equipment getEquipmentByMax(String MaxColumnName);
    
    public void removeEquipment(int id);
    
    public List<Equipment> listEquipments();

	public List<Equipment> listEquipmentsBySiteId(int siteId);

	public List<Equipment> listEquipmentsByUserId(int userId);	
	
	public List<Equipment> listInverters();

	public List<Equipment> listInvertersBySiteId(int siteId);

	public List<Equipment> listInvertersByUserId(int userId);
}
