

package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.TimeZone;

public interface TimeZoneDAO {
	 
	public void addTimeZone(TimeZone timezone);
	    
	public void updateTimeZone(TimeZone timezone);
	    
	public TimeZone getTimeZoneById(int id);
	    
	public void removeTimeZone(int id);
	    
	public List<TimeZone> listTimeZones();
}

