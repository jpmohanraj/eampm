package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="mEquipmentParameter")
public class EquipmentParameter implements Serializable{

	 private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "ParameterID")
	 private Integer ParameterId;
	 
	 @Column(name="ParameterName")
	 private String ParameterName;
	 
	 @Column(name="EquipmentTypeID")
	 private Integer EquipmentTypeID;
	 
	 @Column(name="Sequence")
	 private Integer Sequence;
	 
	 @Column(name="ListID")
	 private Integer ListID;
	 
	 @Column(name="UOMID")
	 private Integer UOMID;
	 
	 @Column(name="SourceName")
	 private String SourceName;
	 
	 @Column(name="StandardName")
	 private String StandardName;
	
 
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="CreatedBy")
	 private Integer CreatedBy;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 @Column(name="LastUpdatedBy")
	 private Integer LastUpdatedBy;

	public Integer getParameterId() {
		return ParameterId;
	}

	public void setParameterID(Integer parameterId) {
		ParameterId = parameterId;
	}

	public String getParameterName() {
		return ParameterName;
	}

	public void setParameterName(String parameterName) {
		ParameterName = parameterName;
	}

	public Integer getEquipmentTypeID() {
		return EquipmentTypeID;
	}

	public void setEquipmentTypeID(Integer equipmentTypeID) {
		EquipmentTypeID = equipmentTypeID;
	}

	public Integer getSequence() {
		return Sequence;
	}

	public void setSequence(Integer sequence) {
		Sequence = sequence;
	}

	public Integer getListID() {
		return ListID;
	}

	public void setListID(Integer listID) {
		ListID = listID;
	}

	public Integer getUOMID() {
		return UOMID;
	}

	public void setUOMID(Integer uOMID) {
		UOMID = uOMID;
	}

	public String getSourceName() {
		return SourceName;
	}

	public void setSourceName(String sourceName) {
		SourceName = sourceName;
	}

	public String getStandardName() {
		return StandardName;
	}

	public void setStandardName(String standardName) {
		StandardName = standardName;
	}

	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	
	
		
	
}
