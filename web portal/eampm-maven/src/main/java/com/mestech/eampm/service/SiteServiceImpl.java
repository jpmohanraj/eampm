
package com.mestech.eampm.service;

import java.util.List;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.SiteDAO;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Site;
 
@Service
public class SiteServiceImpl  implements SiteService {
	
	@Autowired
    private SiteDAO siteDAO;
 
    public void setsiteDAO(SiteDAO siteDAO) {
        this.siteDAO = siteDAO;
    }
 
    //@Override
    @Transactional
    public void addSite(Site site) {
    	siteDAO.addSite(site);
    }
 
    //@Override
    @Transactional
    public void updateSite(Site site) {
        siteDAO.updateSite(site);
    }
 
    //@Override
    @Transactional
    public List<Site> listSites() {
        return this.siteDAO.listSites();
    }
 
    //@Override
    @Transactional
    public Site getSiteById(int id) {
        return siteDAO.getSiteById(id);
    }
    
    
    @Transactional
    public Site getSiteByName(String siteName) {
        return siteDAO.getSiteByName(siteName);
    }
    
    
    @Transactional
    public List<Site> getSiteListByCustomerId(int customerId)
    {
    	return siteDAO.getSiteListByCustomerId(customerId);
    }
    
    @Transactional
    public List<Site> getSiteListByUserId(int userId)
    {
    	return siteDAO.getSiteListByUserId(userId);
    }
    

    @Transactional
    public Site getSiteByMax(String MaxColumnName) {
        return siteDAO.getSiteByMax(MaxColumnName);
    }
 
    
    //@Override
    @Transactional
    public void removeSite(int id) {
        siteDAO.removeSite(id);
    }
    
    //@Override
    @Transactional
    public List<Site> listSiteById(int id) {
    	return siteDAO.listSiteById(id);
    }
    
    
}



