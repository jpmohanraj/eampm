package com.mestech.eampm.controllers;

import java.util.Date;
import java.util.Map;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.UserService;


@Controller
public class LoginController {

	 @Autowired
	    private UserService userService;
	
	 
	   @RequestMapping(value = "/login", method = RequestMethod.GET)
	    public String login(Model model,HttpSession session) {
		   
	        model.addAttribute("login", new User());
 			
	     
	        session.setAttribute("eampmuserid", null);
 			
	        if(session.getAttribute("eampmloginsubmit")==null)
 			{
 				session.setAttribute("eampmerrormsg", "");
 			}
	        
 		    session.setAttribute("eampmloginsubmit", null);
 			
 			
 			
	        return "login";
	    }
	   
	   @RequestMapping(value = "/loginauthendication", method = RequestMethod.POST)
	    public String loginAuthendication(@ModelAttribute("user") User user, Model model,HttpSession session) {
	 

		   session.setAttribute("eampmloginsubmit", "1");
			
			
		   String strUserName = user.getUserName();
		   String strPassword = user.getPassword();
		   
		   User objUser =  userService.getUserByName(strUserName);
		   
		  
	 		if(objUser.getUserName() !=null && objUser.getPassword()!=null)
	 		{
	 			
	 			
	 			if(objUser.getUserName().toUpperCase().equals(strUserName.toUpperCase()) && objUser.getPassword().equals(strPassword))
		 		{
		 			String UserId =objUser.getUserId().toString();
		 			String UserCode =objUser.getUserCode();
		 			
		 			session.setAttribute("eampmuserid", UserId);
		 			session.setAttribute("eampmerrormsg", "");
		 			
		 			return "redirect:/dashboard";
		 		}
		 		else
		 		{

		 			user.setErrorMessage("Invalid Username / Password!");
		 			session.setAttribute("eampmuserid", null);
		 			session.setAttribute("eampmerrormsg", "Invalid Username / Password!");
		 			
		 		       
		 			return "redirect:/login";
		 		}
	 		}
	 		else
	 		{
	 			user.setErrorMessage("Invalid Username / Password!");
	 			session.setAttribute("eampmuserid", null);
	 			session.setAttribute("eampmerrormsg", "Invalid Username / Password!");
	 			
	 			return "redirect:/login";
	 		}
	 		
	 	 	
	    }
	 

	    
}
