package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.DataSourceDAO; 
import com.mestech.eampm.model.DataSource;
 
@Service
public class DataSourceServiceImpl  implements DataSourceService {
	
	@Autowired
    private DataSourceDAO dataSourceDAO;
 
    public void setdataSourceDAO(DataSourceDAO dataSourceDAO) {
        this.dataSourceDAO = dataSourceDAO;
    }
 
    //@Override
    @Transactional
    public void addDataSource(DataSource dataSource) {
    	dataSourceDAO.addDataSource(dataSource);
    }
 
    //@Override
    @Transactional
    public void updateDataSource(DataSource dataSource) {
        dataSourceDAO.updateDataSource(dataSource);
    }
 
    //@Override
    @Transactional
    public List<DataSource> listDataSources() {
        return this.dataSourceDAO.listDataSources();
    }
    

    @Transactional
	public List<DataSource> getDataSourceListByUserId(int userId) {
        return this.dataSourceDAO.getDataSourceListByUserId(userId);
    }

    @Transactional
	public List<DataSource> getDataSourceListByCustomerId(int customerId) {
        return this.dataSourceDAO.getDataSourceListByCustomerId(customerId);
    }

    @Transactional
	public List<DataSource> getDataSourceListBySiteId(int siteId) {
        return this.dataSourceDAO.getDataSourceListBySiteId(siteId);
    }
 
    @Transactional
	public List<DataSource> getDataSourceListByEquipmentId(int equipmentId) {
        return this.dataSourceDAO.getDataSourceListByEquipmentId(equipmentId);
    }
 
    
    //@Override
    @Transactional
    public DataSource getDataSourceById(int id) {
        return dataSourceDAO.getDataSourceById(id);
    }
 
    
    @Transactional
    public DataSource getDataSourceByMax(String MaxColumnName) {
        return dataSourceDAO.getDataSourceByMax(MaxColumnName);
    }
    
    
    //@Override
    @Transactional
    public void removeDataSource(int id) {
        dataSourceDAO.removeDataSource(id);
    }
    
    
    
    
    
    //@Override
      @Transactional
      public List<DataSource> listDataSourceForInverters() {
          return this.dataSourceDAO.listDataSourceForInverters();
      }
      

      @Transactional
                  public List<DataSource> getDataSourceForInvertersListByUserId(int userId) {
          return this.dataSourceDAO.getDataSourceForInvertersListByUserId( userId);
      }

      @Transactional
                  public List<DataSource> getDataSourceForInvertersListByCustomerId(int customerId) {
          return this.dataSourceDAO.getDataSourceForInvertersListByCustomerId(customerId);
      }

      @Transactional
                  public List<DataSource> getDataSourceForInvertersListBySiteId(int siteId) {
          return this.dataSourceDAO.getDataSourceForInvertersListBySiteId(siteId);
      }

      @Transactional
                  public List<DataSource> getDataSourceForInvertersListByEquipmentId(int equipmentId) {
          return this.dataSourceDAO.getDataSourceForInvertersListByEquipmentId(equipmentId);
      }
      
      
}
