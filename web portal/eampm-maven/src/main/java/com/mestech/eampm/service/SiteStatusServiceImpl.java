package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.SiteStatusDAO;
import com.mestech.eampm.model.SiteStatus;

@Service
public class SiteStatusServiceImpl  implements SiteStatusService{
	@Autowired
    private SiteStatusDAO siteStatusDAO;
	
	
	 @Transactional
	    public List<SiteStatus> listSiteStatus() {
	        return this.siteStatusDAO.listSiteStatus();
	    }
	 
	    
	    @Transactional
	    public List<SiteStatus> getSiteStatusListByCustomerId(int customerId)
	    {
	    	return siteStatusDAO.getSiteStatusListByCustomerId(customerId);
	    }


	    @Transactional
	    public List<SiteStatus> getSiteStatusListByUserId(int userId)
	    {
	    	return siteStatusDAO.getSiteStatusListByUserId(userId);
	    }
		
	    @Transactional
		public SiteStatus getSiteStatusBySiteId(int siteId)
		{
	    	return siteStatusDAO.getSiteStatusBySiteId(siteId);
	    }
}