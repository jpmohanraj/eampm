package com.mestech.eampm.utility;


import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.service.CountryRegionService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.StateService;

public class UtilityDB {
	 private CountryService countryService; 	 
	 private CountryRegionService countryRegionService;
	 private StateService stateService;
	    

	    public  Map<String,String> getDropdownList(String DropdownName) 
		{
	    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
	    	
	    	if(DropdownName == "CountryRegion")
	    	{
	    		List<CountryRegion> ddlList = countryRegionService.listCountryRegions();
	    		
	    		for(int i=0;i< ddlList.size();i++)
	    		{
	    			ddlMap.put(ddlList.get(i).getCountryRegionId().toString(), ddlList.get(i).getCountryRegionName());
	    				
	    		}
	    		
	    	}
	    	
			
			
			return ddlMap;
		}
	    
	    
	    
}
