

package com.mestech.eampm.service;


import java.util.List;
import com.mestech.eampm.model.TicketTransaction;

public interface TicketTransactionService {
	
	
	
	public void addTicketTransaction(TicketTransaction ticketdetail);
    
	public void updateTicketTransaction(TicketTransaction ticketdetail);
	    
	public TicketTransaction getTicketTransactionById(int id);
	    
	public List<TicketTransaction> getTicketTransactionListByUserId(int userId);
	
	public List<TicketTransaction> getTicketTransactionListBySiteId(int siteId);	
	

	public List<TicketTransaction> getTicketTransactionListByTicketId(int ticketId);
	
	public TicketTransaction getTicketTransactionByMax(String MaxColumnName);
	
	public void removeTicketTransaction(int id);
	    
	public List<TicketTransaction> listTicketTransactions();

	
}


