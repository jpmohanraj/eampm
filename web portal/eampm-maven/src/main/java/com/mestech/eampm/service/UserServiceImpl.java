package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.UserDAO; 
import com.mestech.eampm.model.User;
 
@Service

public class UserServiceImpl  implements UserService {
	
	@Autowired
    private UserDAO userDAO;
 
    public void setuserDAO(UserDAO userDAO) {
        this.userDAO = userDAO;
    }
 
    //@Override
    @Transactional
    public void addUser(User user) {
    	userDAO.addUser(user);
    }
 
    //@Override
    @Transactional
    public void updateUser(User user) {
        userDAO.updateUser(user);
    }
 
    //@Override
    @Transactional
    public List<User> listUsers() {
        return this.userDAO.listUsers();
    }
 
    
    //@Override
    @Transactional
    public List<User> listFieldUsers() {
        return this.userDAO.listFieldUsers();
    }
    

	
	
    //@Override
    @Transactional
    public User getUserById(int id) {
        return userDAO.getUserById(id);
    }
 

    //@Override
    @Transactional
    public User getUserByName(String userName){
        return userDAO.getUserByName(userName);
    }

   
    
    
    //@Override
    @Transactional
    public void removeUser(int id) {
        userDAO.removeUser(id);
    }
    
}

