package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.TicketDetailDAO; 
import com.mestech.eampm.model.TicketDetail;
 
@Service

public class TicketDetailServiceImpl  implements TicketDetailService {
	
	@Autowired
    private TicketDetailDAO ticketDetailDAO;
 
    public void setticketDetailDAO(TicketDetailDAO ticketDetailDAO) {
        this.ticketDetailDAO = ticketDetailDAO;
    }
 
    //@Override
    @Transactional
    public void addTicketDetail(TicketDetail ticketDetail) {
    	ticketDetailDAO.addTicketDetail(ticketDetail);
    }
 
    //@Override
    @Transactional
    public void updateTicketDetail(TicketDetail ticketDetail) {
        ticketDetailDAO.updateTicketDetail(ticketDetail);
    }
 
    //@Override
    @Transactional
    public List<TicketDetail> listTicketDetails() {
        return this.ticketDetailDAO.listTicketDetails();
    }
 
    
	
  //@Override
    @Transactional
    public TicketDetail getTicketDetailByMax(String MaxColumnName) {
        return ticketDetailDAO.getTicketDetailByMax(MaxColumnName);
    }
    
    
    //@Override
    @Transactional
    public TicketDetail getTicketDetailByTicketCode(String TicketCode) {
        return ticketDetailDAO.getTicketDetailByTicketCode(TicketCode);
    }
    
    
    
    //@Override
    @Transactional
    public TicketDetail getTicketDetailById(int id) {
        return ticketDetailDAO.getTicketDetailById(id);
    }
 
    //@Override
    @Transactional
    public List<TicketDetail> getTicketDetailListByUserId(int userId) {
        return this.ticketDetailDAO.getTicketDetailListByUserId(userId);
    }
    
    @Transactional
    public List<TicketDetail> listTicketDetailsForDisplay(int siteId, String fromDate,String toDate,String category, String type, String priority ) {
        return this.ticketDetailDAO.listTicketDetailsForDisplay(siteId, fromDate,toDate,category, type, priority );
    }

	
		  
	
	
    //@Override
    @Transactional
    public List<TicketDetail> getTicketDetailListBySiteId(int siteId) {
        return this.ticketDetailDAO.getTicketDetailListBySiteId( siteId);
    }
    

  //@Override
      @Transactional
      public List<TicketDetail> getTicketDetailListByCustomerId(int customerId) {
          return this.ticketDetailDAO.getTicketDetailListByCustomerId(customerId);
      }

    
    
    
    //@Override
    @Transactional
    public void removeTicketDetail(int id) {
        ticketDetailDAO.removeTicketDetail(id);
    }
    
}
