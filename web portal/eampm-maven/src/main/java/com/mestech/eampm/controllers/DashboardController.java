package com.mestech.eampm.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteMapListBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;
import com.mestech.eampm.model.User;
import com.mestech.eampm.model.UserRole;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.TicketTransactionService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class DashboardController {

	 @Autowired
	    private CustomerService customerService;
	 
	 @Autowired
	    private SiteStatusService siteStatusService;
	 
	 @Autowired
	    private DataSourceService dataSourceService;

	 @Autowired
	    private SiteService siteService;
	 
	 @Autowired
	    private UserService userService;
	 

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 
	 

	 @Autowired
	    private TicketDetailService ticketDetailService;
	 
	 @Autowired
	    private TicketTransactionService ticketTransactionService;
	 
	 
	 
 @RequestMapping(value = "/dashboard", method = RequestMethod.GET)
 public String listCustomerViews(Model model,HttpSession session) {
 	
	 //Double dblAverageRadiation = 1000.0;
	 
	 
	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	 {
		 return "redirect:/login";
	 }
	
	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	 	
 	CustomerViewBean objCustomerViewBean = new CustomerViewBean();
 	
 	List<Site> lstSite = siteService.getSiteListByUserId(id);
	List<SiteStatus> lstSiteStatus = siteStatusService.getSiteStatusListByUserId(id);
	
	
	List<TicketDetail> lstTicketDetail = ticketDetailService.getTicketDetailListByUserId(id);
	
	
	
	List<SiteListBean> lstSiteList = new ArrayList<SiteListBean>();
	List<SiteMapListBean> lstSiteMapList = new ArrayList<SiteMapListBean>();
	
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
	
	Double dblCVYesterdayTotalEnergy = 0.0;
	Double dblCVTotalEnergy = 0.0;
	Double dblCVTodayEnergy = 0.0;
	Double dblCVTodayHoursOn = 0.0;
	 
    Date LstCVUpdatedDate = null;
    Date LstCVEventDate = null;
    Date LstCVLastDownTime = null;

    
    
   try
    {
    	
  
	
	for(int i=0; i< lstSite.size();i++)
	{
		SiteListBean objSiteListBean = new SiteListBean();
		objSiteListBean.setSiteID(lstSite.get(i).getSiteId().toString());
		objSiteListBean.setSiteCode(lstSite.get(i).getSiteCode());
		objSiteListBean.setSiteReference(lstSite.get(i).getCustomerReference());
		objSiteListBean.setSiteName(lstSite.get(i).getSiteName());
		
		
		SiteMapListBean objSiteMapListBean = new SiteMapListBean();
		objSiteMapListBean.setSiteID(lstSite.get(i).getSiteId().toString());
		objSiteMapListBean.setSiteCode(lstSite.get(i).getSiteCode());
		objSiteMapListBean.setSiteReference(lstSite.get(i).getCustomerReference());
		objSiteMapListBean.setSiteName(lstSite.get(i).getSiteName());
		
		objSiteMapListBean.setLat(lstSite.get(i).getLatitude());
		objSiteMapListBean.setLng(lstSite.get(i).getLongitude());
		objSiteMapListBean.setType(lstSite.get(i).getSiteName());

		objSiteMapListBean.setAddress(lstSite.get(i).getAddress());
		objSiteMapListBean.setCity(lstSite.get(i).getCity());
		
		
		
		objSiteMapListBean.setState(lstSite.get(i).getStateName());
		objSiteMapListBean.setCountry(lstSite.get(i).getCountryName());
		
		
		
		objSiteMapListBean.setPinCode(lstSite.get(i).getPostalCode());
		
		 
		 
		
		Integer SiteId= lstSite.get(i).getSiteId();
		
		SiteStatus objSiteStatus = new SiteStatus();
		
		if(lstSiteStatus.stream().filter(p -> p.getSiteId() == SiteId ).collect(Collectors.toList()).size() >=1)
		{
			objSiteStatus = lstSiteStatus.stream().filter(p -> p.getSiteId() == SiteId ).collect(Collectors.toList()).get(0);
		
			String varSiteStatus = "";
			
			
			
			if(objSiteStatus.getSiteStatus()!=null)
			{
				varSiteStatus = objSiteStatus.getSiteStatus();
			}
			
			if(varSiteStatus.trim() == "") {varSiteStatus ="0";}
			
			objSiteListBean.setNetworkStatus(varSiteStatus);
			objSiteMapListBean.setNetworkStatus(varSiteStatus);
			
			if(varSiteStatus.equals("1"))
			{
				objSiteMapListBean.setType("active");
			}
			else if(varSiteStatus.equals("2"))
			{
				objSiteMapListBean.setType("warning");
			}
			else if(varSiteStatus.equals("3"))
			{
				objSiteMapListBean.setType("down");
			}
			else
			{
				objSiteMapListBean.setType("offline");
			}
			
		}
		
		
		
		
		

		List<DataSource> lstDataSource = dataSourceService.getDataSourceForInvertersListByUserId(id);
		List<DataSource> lstEquipmentDataSource = lstDataSource.stream().filter(p -> p.getSiteID() == SiteId ).collect(Collectors.toList());
		
		Double dblYesterdayTotalEnergy = 0.0;
		Double dblTotalEnergy = 0.0;
		Double dblTodayEnergy = 0.0;
		Double dblTodayHoursOn =0.0;
		
		 
		Date LstUpdatedDate = null;
		Date LstEventDate  = null;
		
		Integer ActiveInverterCount =0;
	    
		Integer InverterCount = lstEquipmentDataSource.size();
		for(int j=0; j< InverterCount;j++)
		{
			if(lstEquipmentDataSource.get(j).getStatus() != null)
			{
				if(lstEquipmentDataSource.get(j).getStatus()==1)
				{
					ActiveInverterCount +=1;
				}
			}
			
			
			
			if(lstEquipmentDataSource.get(j).getTotalEnergy()!= null)
			{
				dblTotalEnergy += lstEquipmentDataSource.get(j).getTotalEnergy();
				
			}
			
			if(lstEquipmentDataSource.get(j).getYesterdayTotalEnergy()!= null)
			{
				dblYesterdayTotalEnergy += lstEquipmentDataSource.get(j).getYesterdayTotalEnergy();
				
			}
			
			
			if(lstEquipmentDataSource.get(j).getTodayHoursOn()!= null)
			{
				dblTodayHoursOn = lstEquipmentDataSource.get(j).getTodayHoursOn();
				
				if(dblCVTodayHoursOn < dblTodayHoursOn)
				{
					dblCVTodayHoursOn = dblTodayHoursOn;
				}
				
				
			}
			
			
			
			if(lstEquipmentDataSource.get(j).getLastDownTime() != null)
			{	
				if(LstCVLastDownTime == null)
				{
					LstCVLastDownTime = lstEquipmentDataSource.get(j).getLastDownTime();
					
				}
			    else if(LstCVLastDownTime.compareTo(lstEquipmentDataSource.get(j).getLastDownTime())  < 0)
				{
			    	LstCVLastDownTime = lstEquipmentDataSource.get(j).getLastDownTime();
					
				}
			}
			
			
			
			if(lstEquipmentDataSource.get(j).getTimestamp()!= null)
			{
				if(LstUpdatedDate == null)
				{
					LstUpdatedDate = lstEquipmentDataSource.get(j).getTimestamp();
					
				}
			    else if(LstUpdatedDate.compareTo(lstEquipmentDataSource.get(j).getTimestamp())  < 0)
				{
					LstUpdatedDate = lstEquipmentDataSource.get(j).getTimestamp();
				}
				
				
				
				if(LstCVUpdatedDate == null)
				{
					LstCVUpdatedDate = lstEquipmentDataSource.get(j).getTimestamp();
					
					if(lstEquipmentDataSource.get(j).getLastUpdatedDate() != null)
					{
						LstCVEventDate = lstEquipmentDataSource.get(j).getLastUpdatedDate();
						
					}
				}
			    else if(LstCVUpdatedDate.compareTo(lstEquipmentDataSource.get(j).getTimestamp())  < 0)
				{
					LstCVUpdatedDate = lstEquipmentDataSource.get(j).getTimestamp();
					
					if(lstEquipmentDataSource.get(j).getLastUpdatedDate() != null)
					{
						LstCVEventDate = lstEquipmentDataSource.get(j).getLastUpdatedDate();
						
					}
				}
			}
			
		}
			
		dblCVTotalEnergy += dblTotalEnergy;
		dblCVYesterdayTotalEnergy += dblYesterdayTotalEnergy;
		
		dblTodayEnergy = dblTotalEnergy - dblYesterdayTotalEnergy;		
		dblTotalEnergy = dblTotalEnergy/1000;
		
		
		
		objSiteListBean.setTodayEnergy(String.format("%.2f", dblTodayEnergy));
		objSiteListBean.setTotalEnergy(String.format("%.2f", dblTotalEnergy));
		objSiteListBean.setInverters(ActiveInverterCount.toString() + " / " + InverterCount.toString());
		
		
		/*if(dblCVTodayHoursOn <=0)
		{
			objSiteListBean.setPerformanceRatio("-");
		}
		else
		{
			Double dblPR = (((dblTodayEnergy/dblCVTodayHoursOn)/((dblAverageRadiation/1000) * lstSite.get(i).getInstallationCapacity())) * 100);
			if(dblPR > 100)
			{
				objSiteListBean.setPerformanceRatio(String.format("%.2f", dblPR));
			}
			else
			{
				objSiteListBean.setPerformanceRatio("-");
			}
			
			
		}*/
		
		Double dblSY = (((dblTodayEnergy)/(lstSite.get(i).getInstallationCapacity())));
		
		if(dblSY >= 7.0)
		{
			objSiteListBean.setPerformanceRatio("-");
		}
		else
		{
			objSiteListBean.setPerformanceRatio(String.format("%.2f", dblSY));
		}
		
		
		
		
		
		if(LstUpdatedDate == null)
		{
			objSiteListBean.setLastUpdate("-");
		}
		else
		{
			objSiteListBean.setLastUpdate(sdf.format(LstUpdatedDate));
		}
	
			
		lstSiteList.add(objSiteListBean);
		lstSiteMapList.add(objSiteMapListBean);
		
	}
		
	
 	Integer varIntTotalSiteCount = lstSite.size();
 	Integer varIntRooftopSiteCount = lstSite.stream().filter(p -> p.getSiteTypeID() == 1).collect(Collectors.toList()).size();
 	Integer varIntUtilitySiteCount = lstSite.stream().filter(p -> p.getSiteTypeID() == 2).collect(Collectors.toList()).size();
 	Integer varIntRooftopUtilitySiteCount = lstSite.stream().filter(p -> p.getSiteTypeID() == 3).collect(Collectors.toList()).size();
 	
 	Integer varIntOfflineSiteCount = lstSiteStatus.stream().filter(p -> p.getSiteStatus().equals("0")).collect(Collectors.toList()).size();
 	Integer varIntActiveSiteCount = lstSiteStatus.stream().filter(p -> p.getSiteStatus().trim().equals("1")).collect(Collectors.toList()).size();
 	Integer varIntWarningSiteCount = lstSiteStatus.stream().filter(p -> p.getSiteStatus().trim().equals("2")).collect(Collectors.toList()).size();
 	Integer varIntDownSiteCount = lstSiteStatus.stream().filter(p -> p.getSiteStatus().trim().equals("3")).collect(Collectors.toList()).size();
 	
 	Integer varIntTotalOpenTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(1)).collect(Collectors.toList()).size();
    Integer varIntTotalClosedTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(2)).collect(Collectors.toList()).size();
    Integer varIntTotalHoldTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(3)).collect(Collectors.toList()).size();
    //Integer varIntTotalCompletedTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(4)).collect(Collectors.toList()).size();
    
    
 	
 	
    objCustomerViewBean.setTotalSiteCount(varIntTotalSiteCount.toString());
    objCustomerViewBean.setRooftopCount(varIntRooftopSiteCount.toString());
    objCustomerViewBean.setUtilityCount(varIntUtilitySiteCount.toString());
    objCustomerViewBean.setRooftopUtilityCount(varIntRooftopUtilitySiteCount.toString());
     
    objCustomerViewBean.setActiveSiteCount(varIntActiveSiteCount.toString());
    objCustomerViewBean.setWarningSiteCount(varIntWarningSiteCount.toString());
    objCustomerViewBean.setDownSiteCount(varIntDownSiteCount.toString());
    objCustomerViewBean.setOfflineSiteCount(varIntOfflineSiteCount.toString());    
    
  
    objCustomerViewBean.setTotalDownTime("-");
         
    
    if(LstCVLastDownTime == null)
	{
    	objCustomerViewBean.setLastDownTime("-");
	}
	else
	{
		objCustomerViewBean.setLastDownTime(sdf.format(LstCVLastDownTime));
	}
    
    
    
    
   
    dblCVTodayEnergy = dblCVTotalEnergy - dblCVYesterdayTotalEnergy;		
    dblCVTotalEnergy = dblCVTotalEnergy/1000;
	
	
    
    
    if(dblCVTodayEnergy >= 1000)
	{
    	dblCVTodayEnergy = dblCVTodayEnergy/1000;
    	objCustomerViewBean.setTodayEnergy(String.format("%.2f", dblCVTodayEnergy) + " MWh");			//Ex: 96.1 MWh
        	
	}
	else
	{
		objCustomerViewBean.setTodayEnergy(String.format("%.2f", dblCVTodayEnergy) + " kWh");	
	}
	
    
    
    objCustomerViewBean.setTotalEnergy(String.format("%.2f", dblCVTotalEnergy) + " MWh");			//Ex: 28.8 GWh
    objCustomerViewBean.setLastUpdatedTime(sdf.format(LstCVUpdatedDate));		//Ex: 2 Mins ago..
    objCustomerViewBean.setProductionDate(sdfdate.format(LstCVUpdatedDate));			//Ex: 25-07-17
    objCustomerViewBean.setEventTime(sdf.format(LstCVEventDate));
    
    
    
   
    objCustomerViewBean.setTodayCo2Avoided("-");		//	
    objCustomerViewBean.setTotalCo2Avoided("-");		//
  

    objCustomerViewBean.setOpenTicketCount(varIntTotalOpenTicket.toString());  
    objCustomerViewBean.setYettoStartCount(varIntTotalOpenTicket.toString());                 
    objCustomerViewBean.setCompletedTicketCount(varIntTotalClosedTicket.toString());       
    objCustomerViewBean.setTicketMessages("-"); //Ex: Today 6 more scheduled jobs are there at 3 sites.
    objCustomerViewBean.setInprocessCount("-");
    
    
     
     objCustomerViewBean.setTodayEventCount("-");		//
     objCustomerViewBean.setTotalEventCount("-");		//
     
     
     objCustomerViewBean.setTotalProductionYield("-");		//
     objCustomerViewBean.setTodayProductionYield("-");		//
     objCustomerViewBean.setTotalPerformanceRatio("-");		//
     objCustomerViewBean.setTodayPerformanceRatio("-");		//
    
     String MapJsonData = "";
     for(int a=0;a<lstSiteMapList.size();a++)
     {
    	 String varZIndex ="99997";
    	 if(lstSiteMapList.get(a).getType() =="active")
    	 {
    		 varZIndex ="99996";
    	 }
    	 else if(lstSiteMapList.get(a).getType() =="warning")
    	 {
    		 varZIndex ="99998";
    	 }
    	 else if(lstSiteMapList.get(a).getType() =="down")
    	 {
    		 varZIndex ="99999";
    	 }
    	 else 
    	 {
    		 varZIndex ="99997";
    	 }
    	 
    	 if(a==0)
    	 {
    		 MapJsonData ="['<b>" + lstSiteMapList.get(a).getSiteName() +" - " + lstSiteMapList.get(a).getSiteReference() + "</b><p>" + lstSiteMapList.get(a).getAddress() + "</p><p>" + lstSiteMapList.get(a).getCity() + " - " +lstSiteMapList.get(a).getPinCode() + ".</p>'," + lstSiteMapList.get(a).getLat() +", " + lstSiteMapList.get(a).getLng() +",'" + lstSiteMapList.get(a).getSiteName() +"'," + lstSiteMapList.get(a).getType() + "," + varZIndex + ",'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "&ref=" + id + "']";
     	  	// MapJsonData ="{'title':'" + lstSiteMapList.get(a).getSiteName() +"' ,'lat':'" + lstSiteMapList.get(a).getLat() +"', 'lng':'" + lstSiteMapList.get(a).getLng() +"', 'description':'<b>" + lstSiteMapList.get(a).getSiteName() +" - " + lstSiteMapList.get(a).getSiteReference() + "</b><p>" + lstSiteMapList.get(a).getAddress() + "</p><p>" + lstSiteMapList.get(a).getCity() + " - " +lstSiteMapList.get(a).getPinCode() + ".</p>',icon:" + lstSiteMapList.get(a).getType() + ",zIndex:" + varZIndex + ",url:'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "'}";
     		//MapJsonData ="{'title':'" + lstSiteMapList.get(a).getSiteName() +"' ,'lat':'" + lstSiteMapList.get(a).getLat() +"', 'lng':'" + lstSiteMapList.get(a).getLng() +"', 'description':'<b>" + lstSiteMapList.get(a).getSiteName() +" - " + lstSiteMapList.get(a).getSiteReference() + "</b><p>" + lstSiteMapList.get(a).getAddress() + "</p><p>" + lstSiteMapList.get(a).getCity() + " - " +lstSiteMapList.get(a).getPinCode() + ",</p><p>" + lstSiteMapList.get(a).getState() + ", " + lstSiteMapList.get(a).getCountry()+ ".</p>',icon:" + lstSiteMapList.get(a).getType() + ",zIndex:" + varZIndex + ",url:'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "'}";
    		 //MapJsonData ="{position: new google.maps.LatLng(" + lstSiteMapList.get(a).getLat() +", " + lstSiteMapList.get(a).getLng() + "),type: '" + lstSiteMapList.get(a).getType() + "',url:'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "',title:'" + lstSiteMapList.get(a).getSiteName() + "'}";
    	 }
    	 else
    	 { 
    		 MapJsonData = MapJsonData + ",['<b>" + lstSiteMapList.get(a).getSiteName() +" - " + lstSiteMapList.get(a).getSiteReference() + "</b><p>" + lstSiteMapList.get(a).getAddress() + "</p><p>" + lstSiteMapList.get(a).getCity() + " - " +lstSiteMapList.get(a).getPinCode() + ".</p>'," + lstSiteMapList.get(a).getLat() +", " + lstSiteMapList.get(a).getLng() +",'" + lstSiteMapList.get(a).getSiteName() +"'," + lstSiteMapList.get(a).getType() + "," + varZIndex + ",'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "&ref=" + id + "']";
 	  		//MapJsonData =MapJsonData + ",{'title':'" + lstSiteMapList.get(a).getSiteName() +"' ,'lat':'" + lstSiteMapList.get(a).getLat() +"', 'lng':'" + lstSiteMapList.get(a).getLng() +"', 'description':'<b>" + lstSiteMapList.get(a).getSiteName() +" - " + lstSiteMapList.get(a).getSiteReference() + "</b><p>" + lstSiteMapList.get(a).getAddress() + "</p><p>" + lstSiteMapList.get(a).getCity() + " - " +lstSiteMapList.get(a).getPinCode() + ".</p>',icon:" + lstSiteMapList.get(a).getType() + ",zIndex:" + varZIndex + ",url:'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "'}";
    		// MapJsonData =MapJsonData + ",{'title':'" + lstSiteMapList.get(a).getSiteName() +"' ,'lat':'" + lstSiteMapList.get(a).getLat() +"', 'lng':'" + lstSiteMapList.get(a).getLng() +"', 'description':'<b>" + lstSiteMapList.get(a).getSiteName() +" - " + lstSiteMapList.get(a).getSiteReference() + "</b><p>" + lstSiteMapList.get(a).getAddress() + "</p><p>" + lstSiteMapList.get(a).getCity() + " - " +lstSiteMapList.get(a).getPinCode() + ",</p><p>" + lstSiteMapList.get(a).getState() + ", " + lstSiteMapList.get(a).getCountry()+ ".</p>',icon:" + lstSiteMapList.get(a).getType() + ",zIndex:" + varZIndex + ",url:'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "'}";
     		// MapJsonData =MapJsonData + ",{position: new google.maps.LatLng(" + lstSiteMapList.get(a).getLat() +", " + lstSiteMapList.get(a).getLng() + "),type: '" + lstSiteMapList.get(a).getType() + "',url:'/eampm-maven/siteview" + lstSiteMapList.get(a).getSiteID() + "',title:'" + lstSiteMapList.get(a).getSiteName() + "'}";
    	 }
    	 
    	
    	 
    	      
         
    	 
    	 
    	 
        		
     }
     
     MapJsonData = "[" + MapJsonData + "]";
     
     objCustomerViewBean.setMapJsonData(MapJsonData);     
     objCustomerViewBean.setUserID(id);
     
     
     
    }
   catch(NumberFormatException ex)
   {	   
	   return "redirect:/login";
   }
    catch(IndexOutOfBoundsException ex)
    {
    	return "redirect:/login";
    }
    catch(Exception ex)
    {
    	return "redirect:/login";
    }
    
    
   
    AccessListBean objAccessListBean = new AccessListBean();
   
    try
    {
    User objUser =  userService.getUserById(id);
	//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
 	
    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
   
    objAccessListBean.setUserID(id);
    objAccessListBean.setUserName(objUser.getUserName());
    
    
    
    if(objUser.getRoleID()!=4)
	{
		objAccessListBean.setCustomerListView("visible");
	}
    
    
    for(int l=0;l<lstRoleActivity.size();l++)
    {
    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setDashboard("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setOverview("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setSystemMonitoring("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setVisualization("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setAnalytics("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setPortfolioManagement("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setTicketing("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setForcasting("visible");
    	}
    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
    	{
    		objAccessListBean.setConfiguration("visible");
    	}
    }
    
    }
    catch(Exception e)
    {
    	System.out.println(e.getMessage());
    }
    

    model.addAttribute("siteList", getDropdownList("Site",id));
    model.addAttribute("userList", getDropdownList("User",id));
    
     model.addAttribute("access",objAccessListBean);
     model.addAttribute("dashboard", objCustomerViewBean);
     model.addAttribute("ticketcreation", new TicketDetail());
     model.addAttribute("dashboardSiteList", lstSiteList);
    
     return "dashboard";
 }
 
 
 

 public  Map<String,String> getDropdownList(String DropdownName , int refid) 
	{
    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
    	
    	if(DropdownName == "Site")
    	{
    		List<Site> ddlList = siteService.getSiteListByUserId(refid);
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
    		}
    		
    	}
    	else if(DropdownName == "User")
    	{
    		List<User> ddlList = userService.listFieldUsers();
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
    		}
    		
    	}
    	
		return ddlMap;
	}
 

 
 
 public String getTicketCode()
 {
	 TicketDetail objTicketDetail =new TicketDetail();
	 objTicketDetail =ticketDetailService.getTicketDetailByMax("TicketCode");
	 
	 String TC = "";
	 
	 if(objTicketDetail != null)
	 {
		 TC = objTicketDetail.getTicketCode();
	 }
	 
	
    	if(TC ==null || TC =="")
    	{
    		TC ="TC00001";
    	}
    	else
    	{
    		try{
    			int NextIndex = Integer.parseInt(TC.replaceAll("TC", "")) + 1;
    			
    			if(NextIndex >= 10000) {TC =  String.valueOf(NextIndex);}
    			else if(NextIndex >= 1000) {TC = "0" + String.valueOf(NextIndex);}
    			else if(NextIndex >= 100) {TC = "00" +  String.valueOf(NextIndex);}
    			else if(NextIndex >= 10) {TC = "000" +  String.valueOf(NextIndex);}
    			else {TC = "0000" +  String.valueOf(NextIndex);}
    			
    			TC = "TC" + TC;
    		}
    		catch(Exception e)
    		{
    			TC ="TC00001";
    		}
    	}
    	
    	return TC;
 }
 
 
 
 
 
 @RequestMapping(value = "/addnewticket", method = RequestMethod.POST)
 public String listTicketDetails(@ModelAttribute("ticketcreation") TicketDetail ticketdetail,HttpSession session) {
 	
 	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
 	 {
 		 return "redirect:/login";
 	 }
 	 
 	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
 	//ticketdetail
 	Integer TicketID=0;
 	Date date = new Date();
 	 try
 	 {
 		String TicketCode = getTicketCode();
 		ticketdetail.setTicketCode(TicketCode);
 	 	ticketdetail.setTicketMode("Manual Ticket");
 	 	ticketdetail.setNotificationStatus("0");
 	 	ticketdetail.setActiveFlag(1);
 	 	ticketdetail.setTicketStatus(1);
 	 	ticketdetail.setState(1);
 	 	ticketdetail.setDayCycle(1);
 	 	ticketdetail.setRefTicketID(0);
 	 	ticketdetail.setCreatedBy(id);
 	 	ticketdetail.setCreationDate(date);
 	 	//ticketdetail.setSeverity(1);
 	 	
 	 	
 	 	TicketTransaction tickettransaction = new TicketTransaction ();
 	 	
 	 	tickettransaction.setTicketStatus(1);
 	 	//tickettransaction.setRemarks(ticketdetail.getDescription());
 	 	tickettransaction.setDescription("Ticket has been created");
 	 	tickettransaction.setCreatedBy(id);
 	 	//tickettransaction.setAssignedTo(ticketdetail.getAssignedTo());
 	 	tickettransaction.setActiveFlag(1);
 	 	tickettransaction.setTimeStamp(date);
 	 	tickettransaction.setCreationDate(date);
 	 	tickettransaction.setDayCycle(1);
 	 	
 	 	
 	 	System.out.println(ticketdetail.getScheduledOn());
 	 	
 	 	
 	 	ticketDetailService.addTicketDetail(ticketdetail);
 	 	
 	 	if(ticketDetailService.getTicketDetailByTicketCode(TicketCode) != null)
 	 	{
 	 		TicketID = ticketDetailService.getTicketDetailByTicketCode(TicketCode).getTicketID();
 	 	 	tickettransaction.setTicketID(TicketID);
 	 	 	ticketTransactionService.addTicketTransaction(tickettransaction);
 	 		
 	 	}
 	 	
 	 	return "redirect:/ticketviews"+TicketID;
 	 	
 	 	
 	 }
 	catch(Exception ex)
 	 {
 		return "redirect:/dashboard";
 	 }
 	
 	

 	
	 
 	
 }

 
 
 
 
 
 
}
