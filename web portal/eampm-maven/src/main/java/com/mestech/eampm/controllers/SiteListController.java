package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class SiteListController {

	 @Autowired
	    private CustomerService customerService;
	 
	 @Autowired
	    private SiteStatusService siteStatusService;
	 
	 @Autowired
	    private DataSourceService dataSourceService;

	 
	 @Autowired
	    private SiteService siteService;
	 
	 @Autowired
	    private UserService userService;
	 

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 
	
  @RequestMapping(value = "/sitelist", method = RequestMethod.GET)
  public String listCustomerLists( Model model,HttpSession session) {
  	
 	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
 	 {
 		 return "redirect:/login";
 	 }
 	 
 	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	 
  	
	    List<SiteViewBean> lstSiteViewBean = new ArrayList<SiteViewBean>();
       AccessListBean objAccessListBean = new AccessListBean();

   	List<Site> lstSite = siteService.getSiteListByUserId(id);
   	
   	
   /*	if(lstSite.size()==1)
	{
		return "redirect:/siteview" + lstSite.get(0).getSiteId() + "&ref=" + id;
	}
   	
   	*/
   	for(int i=0;i<lstSite.size();i++)
   	{
   		SiteViewBean objSiteViewBean = new SiteViewBean();
   	        
   		objSiteViewBean.setSiteCode(lstSite.get(i).getSiteCode());
   		objSiteViewBean.setSiteName(lstSite.get(i).getSiteName());
   		objSiteViewBean.setSiteID(lstSite.get(i).getSiteId().toString());
   		objSiteViewBean.setSiteAddress(lstSite.get(i).getAddress());
   		objSiteViewBean.setSiteMobileNo(lstSite.get(i).getMobile());
   		objSiteViewBean.setSiteTelephoneNo(lstSite.get(i).getTelephone());
   		objSiteViewBean.setSiteLocation(lstSite.get(i).getSiteImage());
   	
   		objSiteViewBean.setDataSearch(lstSite.get(i).getSiteCode().toLowerCase() + " "+ lstSite.get(i).getSiteName().toLowerCase());
		

   		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
   		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
   		
   		List<DataSource> lstDataSource = dataSourceService.getDataSourceForInvertersListBySiteId(lstSite.get(i).getSiteId());
   		
   		Double dblYesterdayTotalEnergy = 0.0;
   		Double dblTotalEnergy = 0.0;
   		Double dblTodayEnergy = 0.0;
   		 
   		Date LstUpdatedDate = null;
   		
   		
   	    
   		Integer InverterCount = lstDataSource.size();
   		objSiteViewBean.setTotalEquipment(InverterCount.toString());
   		
   		
   		for(int j=0; j< InverterCount;j++)
   		{
   			
   			
   			if(lstDataSource.get(j).getTotalEnergy()!= null)
   			{
   				dblTotalEnergy += lstDataSource.get(j).getTotalEnergy();
   			}
   			
   			if(lstDataSource.get(j).getYesterdayTotalEnergy()!= null)
   			{
   				dblYesterdayTotalEnergy += lstDataSource.get(j).getYesterdayTotalEnergy();
   			}
   			
   			if(lstDataSource.get(j).getTimestamp()!= null)
   			{
   				if(LstUpdatedDate == null)
   				{
   					LstUpdatedDate = lstDataSource.get(j).getTimestamp();
   					
   				}
   			    else if(LstUpdatedDate.compareTo(lstDataSource.get(j).getTimestamp())  < 0)
   				{
   					LstUpdatedDate = lstDataSource.get(j).getTimestamp();
   				}
   				
   				
   			}
   			
   		}
   			
   			dblTodayEnergy = dblTotalEnergy - dblYesterdayTotalEnergy;	
   			dblTotalEnergy = dblTotalEnergy/1000;
   			dblYesterdayTotalEnergy = dblYesterdayTotalEnergy/1000;
   		
   	 	
   			if(dblTodayEnergy >=1000)
   			{
   				dblTodayEnergy = dblTodayEnergy /1000;
   				objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTodayEnergy) + " MWh");
   			}
   			else
   			{
   				objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTodayEnergy) + " kWh");	
   			}
   		    
   		    
   		    
   			
   			objSiteViewBean.setTotalEnergy(String.format("%.2f", dblTotalEnergy) + " MWh");
   			
   		 if(LstUpdatedDate == null)
 		{
   			objSiteViewBean.setEnergyLastUpdate("-");
 		}
 		else
 		{
 			objSiteViewBean.setEnergyLastUpdate(sdf.format(LstUpdatedDate));
 		}
   		 
   	
   		
   		lstSiteViewBean.add(objSiteViewBean);
   	}
   	
   	
   	
	   
	    try
	    {
	    User objUser =  userService.getUserById(id);
		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	 	
	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	   
	    objAccessListBean.setUserID(id);
	    objAccessListBean.setUserName(objUser.getUserName());
	    
	    
	    
	    if(objUser.getRoleID()!=4)
		{
			objAccessListBean.setCustomerListView("visible");
		}
	    
	    
	    for(int l=0;l<lstRoleActivity.size();l++)
	    {
	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setDashboard("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setOverview("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setSystemMonitoring("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setVisualization("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setAnalytics("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setPortfolioManagement("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setTicketing("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setForcasting("visible");
	    	}
	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	{
	    		objAccessListBean.setConfiguration("visible");
	    	}
	    }
	    
	    }
	    catch(Exception e)
	    {
	    	System.out.println(e.getMessage());
	    }
	    
	     model.addAttribute("access",objAccessListBean);

 	   model.addAttribute("siteList", getDropdownList("Site",id));
 	   model.addAttribute("userList", getDropdownList("User",id));
 	  model.addAttribute("ticketcreation", new TicketDetail());
      model.addAttribute("sitelist", lstSiteViewBean);
     
      return "sitelist";
  }

  public  Map<String,String> getDropdownList(String DropdownName , int refid) 
 	{
     	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
     	
     	if(DropdownName == "Site")
     	{
     		List<Site> ddlList = siteService.getSiteListByUserId(refid);
     		
     		for(int i=0;i< ddlList.size();i++)
     		{
     			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
     		}
     		
     	}
     	else if(DropdownName == "User")
     	{
     		List<User> ddlList = userService.listFieldUsers();
     		
     		for(int i=0;i< ddlList.size();i++)
     		{
     			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
     		}
     		
     	}
     	
 		return ddlMap;
 	}

}
