package com.mestech.eampm.controllers;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.CustomerViewBean;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteMapListBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class CustomerListController {

	 @Autowired
	    private CustomerService customerService;
	 
	 @Autowired
	    private SiteStatusService siteStatusService;
	 
	
	 
	 @Autowired
	    private DataSourceService dataSourceService;


	 @Autowired
	    private DataTransactionService dataTransactionService;
	 
	 @Autowired
	    private SiteService siteService;
	 
	 @Autowired
	    private EquipmentService equipmentService;
	 
	 
	 @Autowired
	    private UserService userService;
	 

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 
	
   @SuppressWarnings("deprecation")
@RequestMapping(value = "/customerlist", method = RequestMethod.GET)
   public String listCustomerLists( Model model,HttpSession session) {
   	
  	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
  	 {
  		 return "redirect:/login";
  	 }
  	 
  	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	 
  	 
  	 
	    List<CustomerViewBean> lstCustomerViewBean = new ArrayList<CustomerViewBean>();
        AccessListBean objAccessListBean = new AccessListBean();

    	List<Customer> lstCustomer = customerService.getCustomerListByUserId(id);
    	
    	
    	/*if(lstCustomer.size()==1)
    	{
    		return "redirect:/customerview" + lstCustomer.get(0).getCustomerId() + "&ref=" + id;
    	}*/
    	
    	
    	
    	
    	String ChartData = "";
    	
    	for(int i=0;i<lstCustomer.size();i++)
    	{
    		CustomerViewBean objCustomerViewBean = new CustomerViewBean();
    	    
    		objCustomerViewBean.setDivID(i);
    		objCustomerViewBean.setCustomerCode(lstCustomer.get(i).getCustomerCode());
    		objCustomerViewBean.setCustomerName(lstCustomer.get(i).getCustomerName());
    		objCustomerViewBean.setCustomerID(lstCustomer.get(i).getCustomerId().toString());
    		objCustomerViewBean.setCustomerAddress(lstCustomer.get(i).getAddress());
    		objCustomerViewBean.setContactPerson(lstCustomer.get(i).getContactPerson());
        	
    		objCustomerViewBean.setDataSearch(lstCustomer.get(i).getCustomerCode().toLowerCase() + " "+ lstCustomer.get(i).getCustomerName().toLowerCase());
    		
    		
    		
    		
    		List<Site> lstSite = siteService.getSiteListByCustomerId(lstCustomer.get(i).getCustomerId());
    		Integer varIntTotalSiteCount = lstSite.size();
    		
    		objCustomerViewBean.setCustomerMobileNo(lstCustomer.get(i).getMobile());
    		objCustomerViewBean.setCustomerTelephoneNo(lstCustomer.get(i).getTelephone());
    		objCustomerViewBean.setTotalSiteCount(varIntTotalSiteCount.toString());
    		objCustomerViewBean.setOpenTicketCount("-");
    		objCustomerViewBean.setTotalEventCount("-");
    		
    		/*
    		String CustomerwiseChart ="";
    		
    		for(int l=1;l>=0;l--)
        	{
    			Double dblCustomerTotalEnergy = 0.0;
    			Date dtCurrentDate;
    			
    			for(int j=0;j<lstSite.size();j++)
        		{
        			List<Equipment> lstEquipment = equipmentService.listEquipmentsBySiteId(lstSite.get(j).getSiteId());
            		
        			
    				for(int k=0;k<lstEquipment.size();k++)
            		{
            			List<DataTransaction> lstDataTransaction = new ArrayList<DataTransaction>();
        	    		
        				lstDataTransaction = dataTransactionService.listDataTransactionsByFilter1( lstEquipment.get(k).getEquipmentId(), lstEquipment.get(k).getSiteID(), l);
        	    		
        	    		if(lstDataTransaction.size() >= 1)
        	    		{
        	    			dblCustomerTotalEnergy += lstDataTransaction.get(0).getTotalEnergy();
        	    			dtCurrentDate =  lstDataTransaction.get(0).getTimestamp();
        	    			
        	    		}
            		}
    				
    				
    				
        		}
    			
    			String day = "day-" + (l+1);
				dblCustomerTotalEnergy = dblCustomerTotalEnergy/1000;
				
				if(l==5)
				{
					CustomerwiseChart = "{\"sites\":\"" + day +"\",\"totalenergy\":" + String.format("%.2f", dblCustomerTotalEnergy) + ",\"color\": \"#5B835B\"}";
				}
				else
				{
					CustomerwiseChart += ",{\"sites\":\"" + day +"\",\"totalenergy\":" + String.format("%.2f", dblCustomerTotalEnergy) + ",\"color\": \"#5B835B\"}";
				}
				
    			
    	    		
        	}
    		
    		CustomerwiseChart = "[" +  CustomerwiseChart +  "]";
    		
    		
    		if(i==0)
			{
    			ChartData = CustomerwiseChart;
			}
			else
			{
				ChartData += "||" + CustomerwiseChart;
			}

    		//System.out.println(ChartData);
        	
        	*/
    		
    		
    		lstCustomerViewBean.add(objCustomerViewBean);
        	
    		
    		
    		
    	
    	}
    	
    	
    	
        
        
    	    try
    	    {
    	    User objUser =  userService.getUserById(id);
    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
    	 	
    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
    	   
    	    objAccessListBean.setUserID(id);
    	    objAccessListBean.setUserName(objUser.getUserName());
    	    
    	    
    	    
    	    if(objUser.getRoleID()!=4)
    		{
    			objAccessListBean.setCustomerListView("visible");
    		}
    	    
    	    
    	    for(int l=0;l<lstRoleActivity.size();l++)
    	    {
    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setDashboard("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setOverview("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setSystemMonitoring("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setVisualization("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setAnalytics("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setPortfolioManagement("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setTicketing("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setForcasting("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setConfiguration("visible");
    	    	}
    	    }
    	    
    	    }
    	   
    	    catch(Exception e)
    	    {
    	    	System.out.println(e.getMessage());
    	    }
    	    
    	     model.addAttribute("access",objAccessListBean);
    	     model.addAttribute("siteList", getDropdownList("Site",id));
     	    model.addAttribute("userList", getDropdownList("User",id));

    	     model.addAttribute("chart",ChartData);        
    	     model.addAttribute("customerlist", lstCustomerViewBean);
    	     model.addAttribute("ticketcreation", new TicketDetail());
       return "customerlist";
  }

   
   
   public  Map<String,String> getDropdownList(String DropdownName , int refid) 
 	{
     	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
     	
     	if(DropdownName == "Site")
     	{
     		List<Site> ddlList = siteService.getSiteListByUserId(refid);
     		
     		for(int i=0;i< ddlList.size();i++)
     		{
     			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
     		}
     		
     	}
     	else if(DropdownName == "User")
     	{
     		List<User> ddlList = userService.listFieldUsers();
     		
     		for(int i=0;i< ddlList.size();i++)
     		{
     			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
     		}
     		
     	}
     	
 		return ddlMap;
 	}

}
