package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CountryRegionDAO; 
import com.mestech.eampm.model.CountryRegion;
 
@Service

public class CountryRegionServiceImpl  implements CountryRegionService {
	
	@Autowired
    private CountryRegionDAO countryregionDAO;
 
    public void setcountryregionDAO(CountryRegionDAO countryregionDAO) {
        this.countryregionDAO = countryregionDAO;
    }
 
    //@Override
    @Transactional
    public void addCountryRegion(CountryRegion countryregion) {
    	countryregionDAO.addCountryRegion(countryregion);
    }
 
    //@Override
    @Transactional
    public void updateCountryRegion(CountryRegion countryregion) {
        countryregionDAO.updateCountryRegion(countryregion);
    }
 
    //@Override
    @Transactional
    public List<CountryRegion> listCountryRegions() {
        return this.countryregionDAO.listCountryRegions();
    }
 
    //@Override
    @Transactional
    public CountryRegion getCountryRegionById(int id) {
        return countryregionDAO.getCountryRegionById(id);
    }
 
    //@Override
    @Transactional
    public void removeCountryRegion(int id) {
        countryregionDAO.removeCountryRegion(id);
    }
    
}
