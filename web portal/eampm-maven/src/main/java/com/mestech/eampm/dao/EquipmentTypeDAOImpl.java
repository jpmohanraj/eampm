package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.EquipmentType;

@Repository
public class EquipmentTypeDAOImpl implements EquipmentTypeDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addEquipmentType(EquipmentType equipmenttype) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(equipmenttype);
       
   }

   //@Override
   public void updateEquipmentType(EquipmentType equipmenttype) {
       Session session = sessionFactory.getCurrentSession();
       session.update(equipmenttype);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<EquipmentType> listEquipmentTypes() {
       Session session = sessionFactory.getCurrentSession();
       List<EquipmentType> EquipmentTypesList = session.createQuery("from EquipmentType").list();
       
       return EquipmentTypesList;
   }

   //@Override
   public EquipmentType getEquipmentTypeById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       EquipmentType equipmenttype = (EquipmentType) session.get(EquipmentType.class, new Integer(id));
       return equipmenttype;
   }

   public EquipmentType getEquipmentTypeByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       EquipmentType equipmenttype = (EquipmentType) session.createQuery("from EquipmentType ORDER BY " + MaxColumnName +  " DESC")
   		    .setMaxResults(1).getSingleResult();
       return equipmenttype;
   }
   
   
   
   //@Override
   public void removeEquipmentType(int id) {
       Session session = sessionFactory.getCurrentSession();
       EquipmentType equipmenttype = (EquipmentType) session.get(EquipmentType.class, new Integer(id));
       
       //De-activate the flag
       equipmenttype.setActiveFlag(0);
      
       if(null != equipmenttype){
           // session.delete(equipmenttype);    	   
    	   session.update(equipmenttype);
       }
   }
}
