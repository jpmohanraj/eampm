
package com.mestech.eampm.controllers;


import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpSession;

import org.dom4j.DocumentException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.TicketHistory;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.TicketTransaction;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Currency;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.TicketTransactionService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

@Controller
public class TicketViewController {

	@Autowired
	private TicketDetailService ticketdetailService;
	
	@Autowired
	private TicketTransactionService ticketTransactionService;

	@Autowired
	private SiteService siteService;

	@Autowired
	private UserService userService;

	@Autowired
	private UserRoleService userRoleService;

	@Autowired
	private RoleActivityService roleActivityService;



	public Map<String,String> getStatusDropdownList() 
	{
		Map<String,String> ddlMap = new LinkedHashMap<String,String>();
		ddlMap.put("1", "Active");
		ddlMap.put("0", "In-active");
		return ddlMap;
	}

	public List<Site> getUpdatedSiteList()
	{
		List<Site> ddlSitesList = siteService.listSites();






		for(int i=0;i<ddlSitesList.size();i++)
		{

			List<State> lstStateList = new ArrayList<State>();



			int cid = ddlSitesList.get(i).getCustomerID();
			int stateid = ddlSitesList.get(i).getStateID();
			int currid = ddlSitesList.get(i).getCurrencyID();


		}
		return ddlSitesList;
	}

	
	public  Map<String,String> getDropdownList(String DropdownName , int refid) 
	{
    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
    	
    	if(DropdownName == "Site")
    	{
    		List<Site> ddlList = siteService.getSiteListByUserId(refid);
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
    		}

    	    
    	}
    	else if(DropdownName == "FieldUser")
    	{
    		List<User> ddlList = userService.listFieldUsers();
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
    		}
    		
    	}
    	else if(DropdownName == "User")
       	{
       		List<User> ddlList = userService.listFieldUsers();
       		
       		for(int i=0;i< ddlList.size();i++)
       		{
       			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
       		}
       		
       	}
    	
		return ddlMap;
	}
 
	

	@RequestMapping(value = "/ticketviews{id}", method = RequestMethod.GET)
	public String listTicketDetails(@PathVariable("id") int refid,Model model,HttpSession session) throws ParseException {

		if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
		{
			return "redirect:/login";
		}

		int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());


		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
		
		AccessListBean objAccessListBean = new AccessListBean();
		
		List<User> lstUsers = userService.listUsers();
		List<Site> lstSites = siteService.listSites();
		TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid);
		
		
		List<TicketTransaction> lstTicketTransactions =ticketTransactionService.getTicketTransactionListByTicketId(refid);
		List<TicketHistory> lstTicketHistories =new ArrayList<TicketHistory>();
		
		/*for(int j=0;j<lstTicketTransactions.size();j++)
		{
			System.out.println(lstTicketTransactions.get(j).getRemarks() + " " + lstTicketTransactions.get(j).getTicketID() + " " + lstTicketTransactions.get(j).getTicketStatus() + " " +  lstTicketTransactions.get(j).getTimeStamp());
		}
		*/
		
		if(lstTicketTransactions != null)
		{
			for(int i=0;i< lstTicketTransactions.size();i++)
			{
				TicketHistory objTicketHistory = new TicketHistory();
				
				if(lstTicketTransactions.get(i).getTicketStatus() !=null)
				{
					if(lstTicketTransactions.get(i).getTicketStatus() == 0)
					{

								
					}
					
					//objTicketHistory.setUserName("");
					
					
					objTicketHistory.setTicketTransaction(lstTicketTransactions.get(i).getDescription());
					
					objTicketHistory.setTicketID(lstTicketTransactions.get(i).getTicketID().toString());
					
					if(lstTicketTransactions.get(i).getTimeStamp() !=null)
					{
						objTicketHistory.setTicketDate(sdf.format(lstTicketTransactions.get(i).getTimeStamp()));
					}
					else
					{
						objTicketHistory.setTicketDate("");
					}
				
					if(lstTicketTransactions.get(i).getRemarks() !=null)
					{
						objTicketHistory.setRemarks(lstTicketTransactions.get(i).getRemarks());
					}
					
					else
					{
						objTicketHistory.setRemarks("");
					}
			
					objTicketHistory.setSerialNo(String.valueOf(i+1));
					
			
				}
				
				
				lstTicketHistories.add(objTicketHistory);
			}
			
		}
		
		
		
		
		
		
		if(objTicketdetail!=null)
		{
			
			
			
			if(objTicketdetail.getDayCycle()!=null)
			{
				objTicketdetail.setDayCycle(objTicketdetail.getDayCycle()-1);
			}
			
			if(objTicketdetail.getCreationDate()!=null)
			{
				String StrCreatedDate = sdf.format(objTicketdetail.getCreationDate());
				
				objTicketdetail.setCreatedDateText(StrCreatedDate);
			}
			
			
			if(objTicketdetail.getStartedTimeStamp()!=null)
			{
				String StrStartedDate = sdf.format(objTicketdetail.getStartedTimeStamp());
				
				objTicketdetail.setStartedDateText(StrStartedDate);
			}
			
			if(objTicketdetail.getScheduledOn()!=null)
			{
				String StrScheduledDate = sdfdate.format(objTicketdetail.getScheduledOn());
				
				objTicketdetail.setScheduledDateText(StrScheduledDate);
			}
			
			
			if(objTicketdetail.getClosedTimeStamp()!=null)
			{
				String StrClosedDate = sdf.format(objTicketdetail.getClosedTimeStamp());
				
				objTicketdetail.setClosedDateText(StrClosedDate);
			}
			
			
			
			
			if(lstUsers.stream().filter(p -> p.getUserId()==objTicketdetail.getAssignedTo()).collect(Collectors.toList()).size()>0)
			{
				objTicketdetail.setAssignedToWhom(lstUsers.stream().filter(p -> p.getUserId()==objTicketdetail.getAssignedTo()).collect(Collectors.toList()).get(0).getUserName());
			}
			
			if(lstUsers.stream().filter(p -> p.getUserId()==objTicketdetail.getCreatedBy()).collect(Collectors.toList()).size()>0)
			{
				objTicketdetail.setCreatedByName(lstUsers.stream().filter(p -> p.getUserId()==objTicketdetail.getCreatedBy()).collect(Collectors.toList()).get(0).getUserName());
			}
			
			if(lstUsers.stream().filter(p -> p.getUserId()==objTicketdetail.getClosedBy()).collect(Collectors.toList()).size()>0)
			{
				objTicketdetail.setClosedByName(lstUsers.stream().filter(p -> p.getUserId()==objTicketdetail.getClosedBy()).collect(Collectors.toList()).get(0).getUserName());
			}
			if(lstSites.stream().filter(p -> p.getSiteId()==objTicketdetail.getSiteID()).collect(Collectors.toList()).size()>0)
			{
				objTicketdetail.setSiteName(lstSites.stream().filter(p -> p.getSiteId()==objTicketdetail.getSiteID()).collect(Collectors.toList()).get(0).getSiteName());
			}
			
			
			if(objTicketdetail.getPriority() != null)
			{
				if(objTicketdetail.getPriority()==2)
				{
					objTicketdetail.setPriorityText("Medium");
				}
				else if(objTicketdetail.getPriority()==3)
				{
					objTicketdetail.setPriorityText("High");
				}
				else 
				{
					objTicketdetail.setPriorityText("Low");
				}
			}
			else
			{
				objTicketdetail.setPriorityText("Low");
			}
			
			
			
			
		}
		
		
		

		try
		{
			User objUser =  userService.getUserById(id);
			//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
			List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();

			lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());

			objAccessListBean.setUserID(id);
			objAccessListBean.setUserName(objUser.getUserName());
			
			



			if(objUser.getRoleID() != 4)
			{
				objAccessListBean.setCustomerListView("visible");
			}


			for(int l=0;l<lstRoleActivity.size();l++)
			{
				if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setDashboard("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setOverview("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setSystemMonitoring("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setVisualization("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setAnalytics("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setPortfolioManagement("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setTicketing("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setForcasting("visible");
				}
				else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
				{
					objAccessListBean.setConfiguration("visible");
				}
			}

		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}

		model.addAttribute("access",objAccessListBean);

		objTicketdetail.setHoldRemarks("");
		objTicketdetail.setClosedRemarks("");
		objTicketdetail.setReassignRemarks("");
		objTicketdetail.setRemarks("");
		

		objTicketdetail.setScheduledOn(null);
		objTicketdetail.setAssignedTo(null);
		
		 model.addAttribute("siteList", getDropdownList("Site",id));
		// model.addAttribute("userList", getDropdownList("User",id));
		 
		 
		 List<User> ddlFieldUserList = userService.listFieldUsers();
		model.addAttribute("userList",ddlFieldUserList);
   	    
   	 model.addAttribute("ticketcreation", new TicketDetail());
		//model.addAttribute("userList", getDropdownList("FieldUser",id));
		model.addAttribute("ticketview", objTicketdetail);
		model.addAttribute("tickethistorylist", lstTicketHistories);
		model.addAttribute("activeStatusList", getStatusDropdownList());


		return "ticketview";
	}
	
	
	
	
	
	
	
	 
	 
	 

	 @RequestMapping(value = "/assignticket{id}", method = RequestMethod.POST)
	 public String listTicketDetailsAssign(@PathVariable("id") int refid, @ModelAttribute("ticketcreation") TicketDetail ticketdetail,HttpSession session) {
	 	
	 	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	 	 {
	 		 return "redirect:/login";
	 	 }
	 	 
	 	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	 	
	 	Date date = new Date();
	 	 try
	 	 {
	 		 
	 		Date dateScheduled=new SimpleDateFormat("dd/MM/yyyy").parse(ticketdetail.getScheduledOnText());  
	 		

	 		TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid);
	 		objTicketdetail.setState(1);
	 		objTicketdetail.setTicketStatus(2);
	 		objTicketdetail.setNotificationStatus("0");
	 		objTicketdetail.setAssignedTo(ticketdetail.getAssignedTo());
	 		objTicketdetail.setRemarks(ticketdetail.getRemarks());
	 		objTicketdetail.setScheduledOn(dateScheduled);
	 		objTicketdetail.setLastUpdatedBy(id);
	 		objTicketdetail.setLastUpdatedDate(date);
	 		ticketdetailService.updateTicketDetail(objTicketdetail);
	 		
	 		System.out.println("mohan reached ");
		 	 
	 		
	 		TicketTransaction tickettransaction = new TicketTransaction ();
	 		
	 		tickettransaction.setTicketID(refid);
	 	 	tickettransaction.setTicketStatus(2);
	 	 	tickettransaction.setRemarks(ticketdetail.getRemarks());
	 	 	tickettransaction.setDescription("Ticket has been assigned");
	 	 	tickettransaction.setAssignedTo(ticketdetail.getAssignedTo());	 	 	
	 	 	tickettransaction.setActiveFlag(1);
	 	 	tickettransaction.setTimeStamp(date);
	 	 	tickettransaction.setCreatedBy(id);
	 	 	tickettransaction.setCreationDate(date);
	 	 	tickettransaction.setDayCycle(ticketdetail.getDayCycle());
	 		ticketTransactionService.addTicketTransaction(tickettransaction);
	 	 	
	 	 	
	 		
	 		
	 	 	
	 	 }
	 	catch(Exception ex)
	 	 {
	 		
	 	 }
	 	
		 
	 	return "redirect:/ticketviews" + refid;
	 }

	 
	 

	 @RequestMapping(value = "/reassignticket{id}", method = RequestMethod.POST)
	 public String listTicketDetailsReassign(@PathVariable("id") int refid, @ModelAttribute("ticketcreation") TicketDetail ticketdetail,HttpSession session) {
	 	
	 	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	 	 {
	 		 return "redirect:/login";
	 	 }
	 	 
	 	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	 	//ticketdetail
	 	 
	 	Date date = new Date();
	 	 try
	 	 {
	 		Date dateRescheduled=new SimpleDateFormat("dd/MM/yyyy").parse(ticketdetail.getRescheduledOnText());  
	 		
	 	 	
	 		TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid);
	 		objTicketdetail.setTicketStatus(2);
	 		objTicketdetail.setState(1);
	 		objTicketdetail.setNotificationStatus("0");
	 		objTicketdetail.setRemarks(ticketdetail.getReassignRemarks());
	 		objTicketdetail.setLastUpdatedBy(id);
	 		objTicketdetail.setAssignedTo(ticketdetail.getReassignedTo());
	 		objTicketdetail.setScheduledOn(dateRescheduled);
	 		objTicketdetail.setLastUpdatedDate(date);
	 		ticketdetailService.updateTicketDetail(objTicketdetail);
	 		
	 		
	 		TicketTransaction tickettransaction = new TicketTransaction ();
	 		tickettransaction.setTicketID(refid);
	 	 	tickettransaction.setTicketStatus(2);
	 	 	tickettransaction.setRemarks(ticketdetail.getReassignRemarks());
	 	 	tickettransaction.setDescription("Ticket has been reassigned");
	 	 	tickettransaction.setCreatedBy(id);
	 	 	tickettransaction.setAssignedTo(ticketdetail.getReassignedTo());
	 	 	tickettransaction.setActiveFlag(1);
	 	 	tickettransaction.setTimeStamp(date);
	 	 	tickettransaction.setCreationDate(date);
	 	 	tickettransaction.setDayCycle(ticketdetail.getDayCycle());
	 	 	ticketTransactionService.addTicketTransaction(tickettransaction);
	 	 	
	 	 	
	 		
	 	 	
	 	 }
	 	catch(Exception ex)
	 	 {
	 		System.out.println("Exception" + ex.getMessage());
	 	 }
	 	
		 
	 	return "redirect:/ticketviews" + refid;
	 }

	 
	 
	 
	 
	 
	
	 
	 
	 

	 @RequestMapping(value = "/holdticket{id}", method = RequestMethod.POST)
	 public String listTicketDetailsHold(@PathVariable("id") int refid, @ModelAttribute("ticketcreation") TicketDetail ticketdetail,HttpSession session) {
	 	
	 	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	 	 {
	 		 return "redirect:/login";
	 	 }
	 	 
	 	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	 	//ticketdetail
	 	 
	 	Date date = new Date();
	 	 try
	 	 {
	 		

	 		TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid);
	 		
	 		if(objTicketdetail.getTicketStatus() !=4 && objTicketdetail.getTicketStatus() !=5)
	 		{
	 			objTicketdetail.setTicketStatus(7);
	 		}
	 		
	 		
	 		
	 		objTicketdetail.setState(3);
	 		objTicketdetail.setRemarks(ticketdetail.getHoldRemarks());
	 		//objTicketdetail.setClosedBy(id);
	 		objTicketdetail.setLastUpdatedBy(id);
	 		objTicketdetail.setLastUpdatedDate(date);
	 		ticketdetailService.updateTicketDetail(objTicketdetail);
	 		
	 		
	 		TicketTransaction tickettransaction = new TicketTransaction ();
	 		tickettransaction.setTicketID(refid);
	 	 	tickettransaction.setTicketStatus(7);
	 	 	tickettransaction.setRemarks(ticketdetail.getHoldRemarks());
	 	 	tickettransaction.setDescription("Ticket has been hold");
	 	 	tickettransaction.setCreatedBy(id);	 	 	
	 	 	tickettransaction.setActiveFlag(1);
	 	 	tickettransaction.setTimeStamp(date);
	 	 	tickettransaction.setCreationDate(date);
	 	 	tickettransaction.setDayCycle(ticketdetail.getDayCycle());
	 		ticketTransactionService.addTicketTransaction(tickettransaction);
	 	 	
	 	 	
	 		

	 		//download_pdf("343");
	 	 	
	 	 }
	 	catch(Exception ex)
	 	 {

	 		System.out.println("Exception at Service Report : " + ex.getMessage());
	 	 }
	 	
		 
	 	return "redirect:/ticketviews" + refid;
	 }
	
	 
	 

	 @RequestMapping(value = "/closeticket{id}", method = RequestMethod.POST)
	 public String listTicketDetails(@PathVariable("id") int refid, @ModelAttribute("ticketcreation") TicketDetail ticketdetail,HttpSession session) {
	 	
	 	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	 	 {
	 		 return "redirect:/login";
	 	 }
	 	 
	 	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	 	//ticketdetail
	 	 
	 	Date date = new Date();
	 	 try
	 	 {
	 		

	 		TicketDetail objTicketdetail = ticketdetailService.getTicketDetailById(refid);
	 		
	 		if(objTicketdetail.getTicketStatus() !=4 && objTicketdetail.getTicketStatus() !=5)
	 		{
	 			objTicketdetail.setTicketStatus(6);
	 		}
	 		
	 		
	 		objTicketdetail.setState(2);
	 		objTicketdetail.setRemarks(ticketdetail.getClosedRemarks());
	 		objTicketdetail.setClosedBy(id);
	 		objTicketdetail.setClosedTimeStamp(date);
	 		ticketdetailService.updateTicketDetail(objTicketdetail);
	 		
	 		
	 		TicketTransaction tickettransaction = new TicketTransaction ();
	 		tickettransaction.setTicketID(refid);
	 	 	tickettransaction.setTicketStatus(6);
	 	 	tickettransaction.setRemarks(ticketdetail.getClosedRemarks());
	 	 	tickettransaction.setDescription("Ticket has been closed");
	 	 	tickettransaction.setCreatedBy(id);
	 	 	tickettransaction.setActiveFlag(1);
	 	 	tickettransaction.setTimeStamp(date);
	 	 	tickettransaction.setCreationDate(date);
	 	 	tickettransaction.setDayCycle(ticketdetail.getDayCycle());
	 		ticketTransactionService.addTicketTransaction(tickettransaction);
	 	 	
	 	 	
	 	 	
	 	 }
	 	catch(Exception ex)
	 	 {
	 		
	 	 }
	 	
		 
	 	return "redirect:/ticketviews" + refid;
	 }


	 
	 
	 private Map map;
		private Connection con;
		 static ServletContext sc;
		 public String download_pdf(String ticketid) throws IOException, DocumentException, JRException
		    {
			/* ServletContext context = getContext();
				String fullPath1 = context.getRealPath("/WEB-INF/ireport/report.jrxml");*/
			//	String path = "D:/Workspace/test/WebContent/WEB-INF/ireport/report.jrxml";
	 		File path = new File("D:/Report Template/ServiceReport.jrxml");
	 		System.out.println(path);
				String op="";
				if(ticketid!=null && ticketid!=""){
				 JasperPrint jasperPrint = null;
			        try { 
			        	 Class.forName("oracle.jdbc.driver.OracleDriver");
			        	 con = DriverManager.getConnection("jdbc:oracle:thin:@192.168.31.207:1521:clouddb","eampm_qa","eampm_qa");
			        	 JasperReport jasperReport = JasperCompileManager.compileReport(path.toString());
			        	 map = new HashMap();
				         map.put("ticketid",Integer.parseInt(ticketid));
				          /*  map.put("todate",to_date);*/
				         jasperPrint = JasperFillManager.fillReport(jasperReport,map,  con);
				         /*OutputStream output = new FileOutputStream(new File("/var/www/html/mps_appstore/sites/default/files/pdf_download/selectedlist.pdf"));*/
				         OutputStream output = new FileOutputStream(new File("D:/Reports/PDF/ServiceReport.pdf"));
						 JasperExportManager.exportReportToPdfStream(jasperPrint, output);
						 
						 con.close();	          
			 
			        } catch (Exception e) { 
			        	System.out.println("e "+e.getMessage());
			        } 
			        op = "<mestech><response name='pdf' value='http://192.168.31.72/pdf/ServiceReport.pdf'/></mes>";
				}
					return op;
			    } 
		 
		 
		 
	

}









