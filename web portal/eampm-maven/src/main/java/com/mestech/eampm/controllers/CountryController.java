package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CountryRegionService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class CountryController {
	
	 @Autowired
	    private CountryService countryService;

	 @Autowired
	 private CountryRegionService countryRegionService;
	 
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	 
	 
	 public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	 
	 public  Map<String,String> getDropdownList(String DropdownName) 
		{
	    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
	    	
	    	if(DropdownName == "CountryRegion")
	    	{
	    		List<CountryRegion> ddlList = countryRegionService.listCountryRegions();
	    		
	    		for(int i=0;i< ddlList.size();i++)
	    		{
	    			if(ddlList.get(i).getCountryRegionId()!=null)
	    			{
	    				ddlMap.put(ddlList.get(i).getCountryRegionId().toString(), ddlList.get(i).getCountryRegionName());
	    			}
	    			
	    				
	    		}
	    		
	    	}
	    	
			return ddlMap;
		}
	 
	 
	 public List<Country> getUpdatedCountryList()
	 {
		 List<Country> ddlCountriesList = countryService.listCountries();
		 //List<CountryRegion> ddlCountryRegionList = countryRegionService.listCountryRegions();
	        
	        for(int i=0;i<ddlCountriesList.size();i++)
	        {
	        	//String RegionName = countryRegionService.getCountryRegionById(ddlCountriesList.get(i).getRegionID()).getCountryRegionName();
	        	String RegionName = "";
	        	if(ddlCountriesList.get(i).getCountryRegion()!= null)
	        	{
	        		RegionName = ddlCountriesList.get(i).getCountryRegion().getCountryRegionName();
	        	}
	        	
	        	ddlCountriesList.get(i).setRegionName(RegionName);
	        }
	        
	        return ddlCountriesList;
	 }
	 
	 
	    @RequestMapping(value = "/countries", method = RequestMethod.GET)
	    public String listCountries(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID()!=4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	    
	
	    	     model.addAttribute("country", new Country());
	        
	        model.addAttribute("countryList", getUpdatedCountryList());
	        model.addAttribute("countryRegionList", getDropdownList("CountryRegion"));
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	        return "country";
	    }
	 
	    

	    
	    // Same method For both Add and Update Country
	   // @RequestMapping(value = "/country/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewcountry", method = RequestMethod.POST)
	    public String addcountry(@ModelAttribute("country") Country country,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	    	Date date = new Date();
	        if (country.getCountryId()==null || country.getCountryId() == 0) {
	            // new country, add it
	        	country.setCreationDate(date);
	            countryService.addCountry(country);
	        } else {
	            // existing country, call update
	        	country.setLastUpdatedDate(date);
	            countryService.updateCountry(country);
	        }
	 
	        return "redirect:/countries";
	 
	    }
	 
	    //@RequestMapping("/country/remove/{id}")
	    @RequestMapping("/removecountry{id}")
	    public String removecountry(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	 
	        countryService.removeCountry(id);
	        return "redirect:/countries";
	    }
	 
	    //@RequestMapping("/country/edit/{id}")
	    @RequestMapping("/editcountry{id}")
	    public String editcountry(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	        model.addAttribute("country", countryService.getCountryById(id));
	       
	        
	        model.addAttribute("countryList", getUpdatedCountryList());	        
	        model.addAttribute("countryRegionList", getDropdownList("CountryRegion"));
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	        return "country";
	    }
}
