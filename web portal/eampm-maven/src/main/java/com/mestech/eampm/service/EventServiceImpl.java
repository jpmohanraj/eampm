package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EventDAO; 
import com.mestech.eampm.model.Event;
 
@Service

public class EventServiceImpl  implements EventService {
	
	@Autowired
    private EventDAO eventDAO;
 
    public void seteventDAO(EventDAO eventDAO) {
        this.eventDAO = eventDAO;
    }
 
    //@Override
    @Transactional
    public void addEvent(Event event) {
    	eventDAO.addEvent(event);
    }
 
    //@Override
    @Transactional
    public void updateEvent(Event event) {
        eventDAO.updateEvent(event);
    }
 
    //@Override
    @Transactional
    public List<Event> listEvents() {
        return this.eventDAO.listEvents();
    }
 
    //@Override
    @Transactional
    public Event getEventById(int id) {
        return eventDAO.getEventById(id);
    }
 
    //@Override
    @Transactional
    public void removeEvent(int id) {
        eventDAO.removeEvent(id);
    }
    
}
