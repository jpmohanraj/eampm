package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.Filter;

public interface FilterDAO {
	 
	public void addFilter(Filter filter);
	    
	public void updateFilter(Filter filter);
	    
	public Filter getFilterById(int id);
	    
	public void removeFilter(int id);
	    
	public List<Filter> listFilters();
}
