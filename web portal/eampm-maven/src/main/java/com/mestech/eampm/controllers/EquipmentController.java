package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EquipmentController {
	
	 @Autowired
	    private EquipmentService equipmentService;

	 @Autowired
	    private EquipmentTypeService equipmenttypeService;
	 

	 @Autowired
	    private SiteService siteService;
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	 
	 
	 public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	    
	 
	 
	 

	 public  Map<String,String> getDropdownList(String DropdownName) 
		{
	    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
	    	
	    	if(DropdownName == "EquipmentType")
	    	{
	    		List<EquipmentType> ddlList = equipmenttypeService.listEquipmentTypes();
	    		
	    		for(int i=0;i< ddlList.size();i++)
	    		{
	    			ddlMap.put(ddlList.get(i).getEquipmentTypeId().toString(), ddlList.get(i).getEquipmentType());
	    				
	    		}
	    		
	    	}
	    	else if(DropdownName == "Site")
	    	{
	    		List<Site> ddlList = siteService.listSites();
	    		
	    		for(int i=0;i< ddlList.size();i++)
	    		{
	    			String SiteName = "";
	    			
	    			if(ddlList.get(i).getSiteName() != null)
	    			{
	    				SiteName = ddlList.get(i).getSiteName();
	    			}
	    			
	    			if(ddlList.get(i).getCustomerReference() != null)
	    			{
	    				SiteName = SiteName + " - " + ddlList.get(i).getCustomerReference();
	    			}
	    			
	    			ddlMap.put(ddlList.get(i).getSiteId().toString(), SiteName );
	    				
	    		}
	    		
	    	}
	    	
	    	
			return ddlMap;
		}
	 
	 
	 public List<Equipment> getUpdatedEquipmentList()
	 {
		 List<Equipment> ddlEquipmentsList = equipmentService.listEquipments();
		 List<EquipmentType> ddlEquipmentTypeList = equipmenttypeService.listEquipmentTypes();
		   
		 
	        for(int i=0;i<ddlEquipmentsList.size();i++)
	        {
	        	List<EquipmentType> lstEquipmentTypeList = new ArrayList<EquipmentType>();
	        	int etype = ddlEquipmentsList.get(i).getEquipmentTypeID();
	        	lstEquipmentTypeList = ddlEquipmentTypeList.stream().filter(p -> p.getEquipmentTypeId() == etype).collect(Collectors.toList());
				
	        	if(lstEquipmentTypeList.size() >0)
	        	{
	        		ddlEquipmentsList.get(i).setEquipmentTypeName(lstEquipmentTypeList.get(0).getEquipmentType());
	        	}
	        	
	        }
	        
	        return ddlEquipmentsList;
	 }
	
	 
	 

	 public String getEquipmentCode()
	 {
		 
		 Equipment objEquipment =new Equipment();
		 objEquipment = equipmentService.getEquipmentByMax("EquipmentCode");
		 
		 
		 String EQ = "";
		 
		 if(objEquipment != null)
		 {
			 EQ = objEquipment.getEquipmentCode();
		 }
		 
		 
		 
		 
	    	if(EQ ==null || EQ =="")
	    	{
	    		EQ ="EQ00001";
	    	}
	    	else
	    	{
	    		try{
	    			int NextIndex = Integer.parseInt(EQ.replaceAll("EQ", "")) + 1;
	    			
	    			if(NextIndex >= 10000) {EQ =  String.valueOf(NextIndex);}
	    			else if(NextIndex >= 1000) {EQ = "0" + String.valueOf(NextIndex);}
	    			else if(NextIndex >= 100) {EQ = "00" +  String.valueOf(NextIndex);}
	    			else if(NextIndex >= 10) {EQ = "000" +  String.valueOf(NextIndex);}
	    			else {EQ = "0000" +  String.valueOf(NextIndex);}
	    			
	    			EQ = "EQ" + EQ;
	    		}
	    		catch(Exception e)
	    		{
	    			EQ ="EQ00001";
	    		}
	    	}
	    	
	    	return EQ;
	 }
	 

	 
	 
	    @RequestMapping(value = "/equipments", method = RequestMethod.GET)
	    public String listEquipments(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID()!=4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	    
	
	    	     
	        model.addAttribute("equipment", new Equipment());
	        
	        model.addAttribute("equipmentList", getUpdatedEquipmentList());
	        model.addAttribute("siteList", getDropdownList("Site"));
	        model.addAttribute("equipmenttypeList", getDropdownList("EquipmentType"));
	        
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	    	
	        
	        return "equipment";
	    }
	 
	    // Same method For both Add and Update Equipment
	   // @RequestMapping(value = "/equipment/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewequipment", method = RequestMethod.POST)
	    public String addequipment(@ModelAttribute("equipment") Equipment equipment,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	 
	    	Date date = new Date();
	        if (equipment.getEquipmentId()==null || equipment.getEquipmentId() == 0) {
	            // new equipment, add it
	        	equipment.setEquipmentCode(getEquipmentCode());
	        	equipment.setCreationDate(date);
	            equipmentService.addEquipment(equipment);
	        } else {
	            // existing equipment, call update
	        	equipment.setLastUpdatedDate(date);
	            equipmentService.updateEquipment(equipment);
	        }
	 
	      //  return "redirect:/equipments";
	       
	        
	        if (equipment.getCapacity()==null || equipment.getCapacity().isEmpty()) 
	        {
	            // new equipment, add it
	        	equipment.setEquipmentCode(getEquipmentCode());
	        	equipment.setCreationDate(date);
	            equipmentService.addEquipment(equipment);
	        } else {
	            // existing equipment, call update
	        	equipment.setLastUpdatedDate(date);
	            equipmentService.updateEquipment(equipment);
	        }
	        return "redirect:/equipments";
	    }
	 
	    //@RequestMapping("/equipment/remove/{id}")
	    @RequestMapping("/removeequipment{id}")
	    public String removeequipment(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	 
	        equipmentService.removeEquipment(id);
	        return "redirect:/equipments";
	    }
	 
	    //@RequestMapping("/equipment/edit/{id}")
	    @RequestMapping("/editequipment{id}")
	    public String editequipment(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	        model.addAttribute("equipment", equipmentService.getEquipmentById(id));


	        model.addAttribute("equipmentList", getUpdatedEquipmentList());
	        model.addAttribute("siteList", getDropdownList("Site"));
	        model.addAttribute("equipmenttypeList", getDropdownList("EquipmentType"));
	        
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	    	
	    	
	        return "equipment";
	    }
}
