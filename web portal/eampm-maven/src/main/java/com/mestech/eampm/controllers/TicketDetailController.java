package com.mestech.eampm.controllers;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.DataExport;
import com.mestech.eampm.bean.TicketFilter;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Currency;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class TicketDetailController {
	
	 @Autowired
	    private TicketDetailService ticketdetailService;
	 
	 @Autowired
	    private SiteService siteService;

	 @Autowired
	    private CustomerService customerService;
	 @Autowired
	    private SiteTypeService sitetypeService;
	 @Autowired
	    private CountryService countryService;
	 @Autowired
	    private StateService stateService;
	 @Autowired
	    private CurrencyService currencyService;
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	

	 

		public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
		
		
	   
	    @RequestMapping(value = "/ticketdetails", method = RequestMethod.GET)
	    public String listTicketDetails(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 //List<TicketDetail> lstTicketDetail = ticketdetailService.listTicketDetails();
	    	 List<TicketDetail> lstTicketDetail = ticketdetailService.listTicketDetailsForDisplay(-1, "","","", "", "" );
		     List<Site> lstSite = siteService.getSiteListByUserId(id);
	    	 List<User> lstUser = userService.listUsers();  
	    	 
	    	// System.out.println("Mohan : " +lstTicketDetail.size());
	    	 
	    	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
	 	 	 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
	 	 	
	 	 	
	 	 	TicketFilter objTicketFilter = new TicketFilter();
	 		 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	   
	    	    	for(int i=0; i< lstTicketDetail.size(); i++)
	    	    	{
	    	    		Integer intSiteID =  lstTicketDetail.get(i).getSiteID();
	    	    		Integer intAssignedID =  lstTicketDetail.get(i).getAssignedTo();
	    	    		
	    	    		if(lstSite.stream().filter(p -> p.getSiteId() == intSiteID ).collect(Collectors.toList()).size() >=1)
	    	    		{
	    	    			Site objSite = new Site();
	    	    			objSite = lstSite.stream().filter(p -> p.getSiteId() == intSiteID ).collect(Collectors.toList()).get(0);
	    	    			
	    	    			
	    	    			lstTicketDetail.get(i).setSiteName(objSite.getSiteName());
	    	    		}
	    	    		
	    	    		
	    	    		if(lstUser.stream().filter(p -> p.getUserId() == intAssignedID ).collect(Collectors.toList()).size() >=1)
	    	    		{
	    	    			User objUser = new User();
	    	    			objUser = lstUser.stream().filter(p -> p.getUserId() == intAssignedID ).collect(Collectors.toList()).get(0);
	    	    			
	    	    			
	    	    			lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());
	    	    		
	    	    		}
	    	    		
	    	    		
	    	    		if(lstTicketDetail.get(i).getCreationDate()!=null)
    	    			{
    	    				lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
    	    				lstTicketDetail.get(i).setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));
    	    				
    	    				//System.out.println( " Mohanraj ::: " + sdf.format(lstTicketDetail.get(i).getCreationDate()));
    	    			}
    	    			
    	    			
    	    			if(lstTicketDetail.get(i).getScheduledOn()!=null)
    	    			{
    	    				lstTicketDetail.get(i).setScheduledOn(lstTicketDetail.get(i).getScheduledOn());
    	    				lstTicketDetail.get(i).setScheduledDateText(sdfdate.format(lstTicketDetail.get(i).getScheduledOn()));
    	    				
    	    				
    	    			}
    	    			
    	    			
    	    			
	    	    	}
	    	    
	    	    
	    	    
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID() != 4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);	    	     
	    	model.addAttribute("siteList", getDropdownList("Site",id));
	    	model.addAttribute("ticketfilter", objTicketFilter);
	        model.addAttribute("ticketdetaillist", lstTicketDetail);	
	        model.addAttribute("siteList", getDropdownList("Site",id));
	   	    model.addAttribute("userList", getDropdownList("User",id));
	   	    
	   	    model.addAttribute("ticketcreation", new TicketDetail());
	    	
			
	        return "ticketdetail";
	    }
	 
	    
	    

	    @RequestMapping(value = "/ticketdetails", method = RequestMethod.POST)
	    public String listTicketDetailList(Model model, @ModelAttribute("ticketfilter") TicketFilter ticketfilter,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 //List<TicketDetail> lstTicketDetail = ticketdetailService.listTicketDetails();
	    	 int varSiteID = 0;
	    	 String varFromDate ="";
	    	 String varToDate ="";
	    	 String varTicketCategory ="";
	    	 String varTicketType ="";
	    	 String varPriority ="";
	    	 String varState ="";
	    	 
	    	 
	    	 if(ticketfilter!=null)
	    	 {
	    		 if(ticketfilter.getSiteID() != null) {varSiteID = ticketfilter.getSiteID();}
		    	 if(ticketfilter.getFromDate() != null) {varFromDate = ticketfilter.getFromDate();}
		    	 if(ticketfilter.getToDate() != null) {varToDate = ticketfilter.getToDate();}
		    	 if(ticketfilter.getTicketCategory() != null) {varTicketCategory = ticketfilter.getTicketCategory();}
		    	 if(ticketfilter.getTicketType() != null) {varTicketType = ticketfilter.getTicketType();}
		    	 if(ticketfilter.getPriority() != null) {varPriority = ticketfilter.getPriority();}
		    	 if(ticketfilter.getState() != null) {varState = ticketfilter.getState();}
		    	 

		    	 
	    	 }
	    	
	    	
	    	 
	    	 List<TicketDetail> lstTicketDetail = ticketdetailService.listTicketDetailsForDisplay(varSiteID,varFromDate,varToDate,varTicketCategory,varState,varPriority);
	    	 List<Site> lstSite = siteService.getSiteListByUserId(id);
	    	 List<User> lstUser = userService.listUsers();  
	    	 
	    	 SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	 	 	 SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
	 	 	
	 	 	
	 		 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	   
	    	    	for(int i=0; i< lstTicketDetail.size(); i++)
	    	    	{
	    	    		Integer intSiteID =  lstTicketDetail.get(i).getSiteID();
	    	    		Integer intAssignedID =  lstTicketDetail.get(i).getAssignedTo();
	    	    		
	    	    		if(lstSite.stream().filter(p -> p.getSiteId() == intSiteID ).collect(Collectors.toList()).size() >=1)
	    	    		{
	    	    			Site objSite = new Site();
	    	    			objSite = lstSite.stream().filter(p -> p.getSiteId() == intSiteID ).collect(Collectors.toList()).get(0);
	    	    			
	    	    			
	    	    			lstTicketDetail.get(i).setSiteName(objSite.getSiteName() + " - " + objSite.getCustomerReference());
	    	    		}
	    	    		
	    	    		
	    	    		if(lstUser.stream().filter(p -> p.getUserId() == intAssignedID ).collect(Collectors.toList()).size() >=1)
	    	    		{
	    	    			User objUser = new User();
	    	    			objUser = lstUser.stream().filter(p -> p.getUserId() == intAssignedID ).collect(Collectors.toList()).get(0);
	    	    			
	    	    			
	    	    			lstTicketDetail.get(i).setAssignedToWhom(objUser.getUserName());
	    	    			
	    	    			if(lstTicketDetail.get(i).getCreationDate()!=null)
	    	    			{
	    	    				lstTicketDetail.get(i).setCreationDate(lstTicketDetail.get(i).getCreationDate());
	    	    				lstTicketDetail.get(i).setCreatedDateText(sdf.format(lstTicketDetail.get(i).getCreationDate()));
	    	    				
	    	    				
	    	    			}
	    	    			
	    	    			
	    	    			
	    	    		}
	    	    		
	    	    		
	    	    		
	    	    	}
	    	    
	    	    
	    	    
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID() != 4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    
	    	    
	    	    
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	    
	    	    TicketFilter objTicketFilter = new TicketFilter();
	    		 
	    	    objTicketFilter.setSiteID(ticketfilter.getSiteID());
	    	    objTicketFilter.setFromDate(ticketfilter.getFromDate());
	    	    objTicketFilter.setToDate(ticketfilter.getToDate());
	    	    objTicketFilter.setTicketCategory(ticketfilter.getTicketCategory());
	    	    objTicketFilter.setState(ticketfilter.getState());
	    	    objTicketFilter.setTicketType(ticketfilter.getTicketType());
	    		objTicketFilter.setPriority(ticketfilter.getPriority());
	    	 	
	    		
	    		model.addAttribute("access",objAccessListBean);	    	     
	   	    	model.addAttribute("siteList", getDropdownList("Site",id));
	   	    	model.addAttribute("ticketfilter", objTicketFilter);
	   	        model.addAttribute("ticketdetaillist", lstTicketDetail);	
	   	        model.addAttribute("siteList", getDropdownList("Site",id));
	   	   	    model.addAttribute("userList", getDropdownList("User",id));
		   	    model.addAttribute("ticketcreation", new TicketDetail());
		   	    
		   	    
		   	    

	    	//model.addAttribute("access",objAccessListBean);
	    	//model.addAttribute("siteList", getDropdownList("Site",id));
	    	//model.addAttribute("ticketfilter", objTicketFilter);
	        //model.addAttribute("ticketdetaillist", lstTicketDetail);	
	    	//model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
			
	        return "ticketdetail";
	    }
	 
	    
	    

	    public  Map<String,String> getDropdownList(String DropdownName , int refid) 
	   	{
	       	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
	       	
	       	if(DropdownName == "Site")
	       	{
	       		List<Site> ddlList = siteService.getSiteListByUserId(refid);
	       		
	       		for(int i=0;i< ddlList.size();i++)
	       		{
	       			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
	       		}
	       		
	       	}
	       	else if(DropdownName == "User")
	       	{
	       		List<User> ddlList = userService.listFieldUsers();
	       		
	       		for(int i=0;i< ddlList.size();i++)
	       		{
	       			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
	       		}
	       		
	       	}
	       	
	   		return ddlMap;
	   	}
	    

}
