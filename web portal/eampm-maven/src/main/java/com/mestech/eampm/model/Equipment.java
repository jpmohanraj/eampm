package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="mEquipment")
public class Equipment implements Serializable{

	 private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "EquipmentID")
	 private Integer EquipmentId;
	 
	 public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	@Column(name="EquipmentTypeID")
	 private Integer EquipmentTypeID;
	 
	 
	 @Column(name="SiteID")
	 private Integer SiteID;
	 
	 

		public String getEquipmentTypeName() {
		return EquipmentTypeName;
	}

	public void setEquipmentTypeName(String equipmentTypeName) {
		EquipmentTypeName = equipmentTypeName;
	}

		@Transient
		private String EquipmentTypeName;	
		
		
	 @Column(name="EquipmentCode")
	 private String EquipmentCode;
	 
	 @Column(name="PrimarySerialNumber")
	 private String PrimarySerialNumber;
	 
	 @Column(name="Description")
	 private String Description;
	 
	 @Column(name="DisplayName")
	 private String DisplayName;
	 /*
	 @Column(name="DLConfigurationID")
	 private Integer DLConfigurationID;*/
	 
	 @Column(name="Remarks")
	 private String Remarks;
	 
	 @Column(name="CustomerReference")
	 private String CustomerReference;
	 
	 @Column(name="CustomerNaming")
	 private String CustomerNaming;
	 
	 
	@Column(name="Capacity",nullable = false)
	 private String Capacity;
	 
	 /*
	 @Column(name="UOMID")
	 private Integer UOMID;
	 
	 @Column(name="PurchaseDate")
	 private Date PurchaseDate;
	 
	 @Column(name="InstallationDate")
	 private Date InstallationDate;
	 
	 @Column(name="WarrantyDate")
	 private Date WarrantyDate;
	 */
	 
	 
	 @Column(name="WarrantyStatus")
	 private String WarrantyStatus;
	 
	 @Column(name="WarrantyPeriod")
	 private Integer WarrantyPeriod;
	 
	 /*@Column(name="EquipmentStatus")
	 private Integer EquipmentStatus;
	 
	 @Column(name="ErrorLogID")
	 private Integer ErrorLogID;
	 
	 @Column(name="Severity")
	 private Integer Severity;
 */
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="CreatedBy")
	 private Integer CreatedBy;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 @Column(name="LastUpdatedBy")
	 private Integer LastUpdatedBy;

	public Integer getEquipmentId() {
		return EquipmentId;
	}

	public void setEquipmentId(Integer equipmentId) {
		EquipmentId = equipmentId;
	}

	public Integer getEquipmentTypeID() {
		return EquipmentTypeID;
	}

	public void setEquipmentTypeID(Integer equipmentTypeID) {
		EquipmentTypeID = equipmentTypeID;
	}

	public String getEquipmentCode() {
		return EquipmentCode;
	}

	public void setEquipmentCode(String equipmentCode) {
		EquipmentCode = equipmentCode;
	}

	public String getPrimarySerialNumber() {
		return PrimarySerialNumber;
	}

	public void setPrimarySerialNumber(String primarySerialNumber) {
		PrimarySerialNumber = primarySerialNumber;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public String getDisplayName() {
		return DisplayName;
	}

	public void setDisplayName(String displayName) {
		DisplayName = displayName;
	}

	/*public Integer getDLConfigurationID() {
		return DLConfigurationID;
	}

	public void setDLConfigurationID(Integer dLConfigurationID) {
		DLConfigurationID = dLConfigurationID;
	}*/

	public String getRemarks() {
		return Remarks;
	}

	public void setRemarks(String remarks) {
		Remarks = remarks;
	}

	public String getCustomerReference() {
		return CustomerReference;
	}

	public void setCustomerReference(String customerReference) {
		CustomerReference = customerReference;
	}

	public String getCustomerNaming() {
		return CustomerNaming;
	}

	public void setCustomerNaming(String customerNaming) {
		CustomerNaming = customerNaming;
	}

	public String getCapacity() {
		return Capacity;
	}

	public void setCapacity(String capacity) {
		Capacity = capacity;
	}

	/*public Integer getUOMID() {
		return UOMID;
	}

	public void setUOMID(Integer uOMID) {
		UOMID = uOMID;
	}

	public Date getPurchaseDate() {
		return PurchaseDate;
	}

	public void setPurchaseDate(Date purchaseDate) {
		PurchaseDate = purchaseDate;
	}

	public Date getInstallationDate() {
		return InstallationDate;
	}

	public void setInstallationDate(Date installationDate) {
		InstallationDate = installationDate;
	}

	public Date getWarrantyDate() {
		return WarrantyDate;
	}

	public void setWarrantyDate(Date warrantyDate) {
		WarrantyDate = warrantyDate;
	}*/

	public String getWarrantyStatus() {
		return WarrantyStatus;
	}

	public void setWarrantyStatus(String warrantyStatus) {
		WarrantyStatus = warrantyStatus;
	}

	public Integer getWarrantyPeriod() {
		return WarrantyPeriod;
	}

	public void setWarrantyPeriod(Integer warrantyPeriod) {
		WarrantyPeriod = warrantyPeriod;
	}

	/*public Integer getEquipmentStatus() {
		return EquipmentStatus;
	}

	public void setEquipmentStatus(Integer equipmentStatus) {
		EquipmentStatus = equipmentStatus;
	}

	public Integer getErrorLogID() {
		return ErrorLogID;
	}

	public void setErrorLogID(Integer errorLogID) {
		ErrorLogID = errorLogID;
	}

	public Integer getSeverity() {
		return Severity;
	}

	public void setSeverity(Integer severity) {
		Severity = severity;
	}
*/
	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	
	
		
	
}
