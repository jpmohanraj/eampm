package com.mestech.eampm.controllers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.io.Reader;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.bean.EquipmentListBean;
import com.mestech.eampm.bean.SiteListBean;
import com.mestech.eampm.bean.SiteViewBean;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataSource;
import com.mestech.eampm.model.TicketDetail;
import com.mestech.eampm.service.TicketDetailService;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.EquipmentStatus;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.model.SiteSummary;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.DataSourceService;
import com.mestech.eampm.service.DataTransactionService;
import com.mestech.eampm.service.EquipmentService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteStatusService;
import com.mestech.eampm.service.SiteSummaryService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class SiteViewController {

	
	 @Autowired
	    private CustomerService customerService;
	 
	 @Autowired
	    private SiteStatusService siteStatusService;
	 
	 @Autowired
	    private DataSourceService dataSourceService;


	 @Autowired
	    private DataTransactionService dataTransactionService;
	 
	 @Autowired
	    private SiteService siteService;
	 
	
	 @Autowired
	    private SiteSummaryService siteSummaryService;
	
	 
	 
	 @Autowired
	    private EquipmentService equipmentService;
	 
	 @Autowired
	    private TicketDetailService ticketDetailService;
	 
	 
	 @Autowired
	    private UserService userService;
	 

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 
	
 @RequestMapping(value = "/siteview{id}&ref={refid}", method = RequestMethod.GET)
 public String listCustomerViews(@PathVariable("id") int id, @PathVariable("refid") int refid, Model model,HttpSession session) {
 	
	 //Double dblAverageRadiation =1000.0;
	 
	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	 {
		 return "redirect:/login";
	 }
	 
	 
	 List<TicketDetail> lstTicketDetail = ticketDetailService.getTicketDetailListBySiteId(id);
	 
 	SiteViewBean objSiteViewBean = new SiteViewBean();
 	 	
	Site objSite = siteService.getSiteById(id);
	SiteStatus objSiteStatus = siteStatusService.getSiteStatusBySiteId(id);
	
	
	
	
	SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
	SimpleDateFormat sdfdate = new SimpleDateFormat("dd-MM-yyyy");
	
	List<DataSource> lstDataSource = dataSourceService.getDataSourceForInvertersListBySiteId(id);
	
	//List<DataTransaction> lstDataTransaction = dataTransactionService.listDataTransactionsByFilter2(id,2);
	
	List<SiteSummary> lstSiteSummary = siteSummaryService.getSiteSummaryListBySiteId(id);
	
	
	
	
	Double dblYesterdayTotalEnergy = 0.0;
	Double dblTotalEnergy = 0.0;
	Double dblTodayEnergy = 0.0;
	Double dblTodayHoursOn = 0.0;
	Double dblCVTodayHoursOn = 0.0;
	Date LstUpdatedDate = null;
	
	

	Integer InverterCount = lstDataSource.size();
	for(int j=0; j< InverterCount;j++)
	{
		
		if(lstDataSource.get(j).getTotalEnergy()!= null)
		{
			dblTotalEnergy += lstDataSource.get(j).getTotalEnergy();
		}
		
		if(lstDataSource.get(j).getYesterdayTotalEnergy()!= null)
		{
			dblYesterdayTotalEnergy += lstDataSource.get(j).getYesterdayTotalEnergy();
		}
		
		
		if(lstDataSource.get(j).getTodayHoursOn()!= null)
		{
			dblTodayHoursOn = lstDataSource.get(j).getTodayHoursOn();
			
			if(dblCVTodayHoursOn < dblTodayHoursOn)
			{
				dblCVTodayHoursOn = dblTodayHoursOn;
			}
			
		}
		
		
		
		if(lstDataSource.get(j).getTimestamp()!= null)
		{
			if(LstUpdatedDate == null)
			{
				LstUpdatedDate = lstDataSource.get(j).getTimestamp();
				
			}
		    else if(LstUpdatedDate.compareTo(lstDataSource.get(j).getTimestamp())  < 0)
			{
				LstUpdatedDate = lstDataSource.get(j).getTimestamp();
			}
			
			
		}
		
	}
		
		dblTodayEnergy = dblTotalEnergy - dblYesterdayTotalEnergy;	
		dblTotalEnergy = dblTotalEnergy/1000;
		dblYesterdayTotalEnergy = dblYesterdayTotalEnergy/1000;
	
 	
 	 	objSiteViewBean.setSiteCode(objSite.getCustomerReference());//HAUT1021
	    
 	 	
 	 	if(objSite.getInstallationCapacity() >= 1000)
 	 	{
 	 		objSiteViewBean.setCapacity(String.format("%.2f", objSite.getInstallationCapacity()/1000) + " MW");
 	 	}
 	 	else
 	 	{
 	 		objSiteViewBean.setCapacity(String.format("%.2f", objSite.getInstallationCapacity()) + " kW");
 	 	}
 	 	
 	 	
 	 	
 	 	
	    
	    
	    objSiteViewBean.setCommisionningDate("-");//20-07-2017
		objSiteViewBean.setSiteStatus(objSiteStatus.getSiteStatus());//Active
		
		
		if(dblTodayEnergy >= 1000)
	    {
	    	Double dblTemp0 = dblTodayEnergy / 1000;
	    	objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTemp0) + " MWh");
	    }
	    else
	    {
	    	objSiteViewBean.setTodayEnergy(String.format("%.2f", dblTodayEnergy) + " kWh");
	    }
		
		
		
		objSiteViewBean.setTotalEnergy(String.format("%.2f", dblTotalEnergy) + " MWh");
	    
	    
	    
	    

	    if(dblTodayEnergy >= 1000)
	    {
	    	Double dblTemp = dblTodayEnergy / 1000;
	    	objSiteViewBean.setTodayProductionYeild(String.format("%.2f",dblTemp ) + " MWh");//725.3 kWh
	    }
	    else
	    {
	    	objSiteViewBean.setTodayProductionYeild(String.format("%.2f", dblTodayEnergy) + " kWh");//725.3 kWh
	    }
	    
	    Integer varIntTotalOpenTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(1)).collect(Collectors.toList()).size();
        Integer varIntTotalClosedTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(2)).collect(Collectors.toList()).size();
        Integer varIntTotalHoldTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(3)).collect(Collectors.toList()).size();
        //Integer varIntTotalCompletedTicket = lstTicketDetail.stream().filter(p -> p.getState().equals(4)).collect(Collectors.toList()).size();
        

	    
	    
	    objSiteViewBean.setTotalProductionYeild(String.format("%.2f", dblTotalEnergy) + " MWh");//857 kWh
	    objSiteViewBean.setYesterdayProductionYeild(String.format("%.2f", dblYesterdayTotalEnergy) + " MWh");// 185.5 kWh
	    
	    
	    
	    
	    
	    
	    if(LstUpdatedDate!=null)
	    {
	    	objSiteViewBean.setProductionDate(sdfdate.format(LstUpdatedDate));//Production Date : 17-07-2017
	    	
	    	Date CurrentDate = new Date();
	    	long duration = CurrentDate.getTime() - LstUpdatedDate.getTime();
	    	
	    	long diffInSeconds = TimeUnit.MILLISECONDS.toSeconds(duration);
	    	long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);
	    	long diffInHours = TimeUnit.MILLISECONDS.toHours(duration);
	    	long diffInDays = TimeUnit.MILLISECONDS.toDays(duration);
	    	
	    	String EnergyLastUpdate ="";
	    	if(diffInSeconds < 60)
	    	{
	    		EnergyLastUpdate = diffInSeconds + " Secs ago..";
	    	}
	    	else if(diffInMinutes < 60)
	    	{
	    		EnergyLastUpdate = diffInMinutes + " Mins ago..";
	    	}
	    	else if(diffInHours < 24)
	    	{
	    		EnergyLastUpdate = diffInHours + " Hrs ago..";
	    	}
	    	else if(diffInDays < 1)
	    	{
	    		EnergyLastUpdate = diffInDays + " Days ago..";
	    	}
	    	
	    	objSiteViewBean.setEnergyLastUpdate(EnergyLastUpdate);//12 Mins ago..
	    }
	    
	    
	    
	   /* System.out.println(dblCVTodayHoursOn);
	    
		if(dblCVTodayHoursOn <=0)
		{
			objSiteViewBean.setTodayPerformanceRatio("-");
		}
		else
		{
			Double dblPR = (((dblTodayEnergy/dblCVTodayHoursOn)/((dblAverageRadiation/1000) * objSite.getInstallationCapacity())) * 100);
			if(dblPR > 100)
			{
				objSiteViewBean.setTodayPerformanceRatio(String.format("%.2f", dblPR) + " %");
			}
			else
			{
				objSiteViewBean.setTodayPerformanceRatio("-");
			}
			
			
			
		}*/
		
		Double dblSY = (((dblTodayEnergy)/(objSite.getInstallationCapacity())));
		;
	    
		
		if(dblSY >= 7.0)
		{
			objSiteViewBean.setTodayPerformanceRatio("-");
		}
		else
		{
			objSiteViewBean.setTodayPerformanceRatio(String.format("%.2f", dblSY) + "");
		}
		
		
		
	    
	    
	    objSiteViewBean.setTotalPerformanceRatio("-");//98.3 %
	    objSiteViewBean.setYesterdayPerformanceRatio("-");// 83.5%
	    

	    
	    
	    objSiteViewBean.setLocationUrl(objSite.getSiteImage());//https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d446095.8251204708!2d76.76081242324295!3d29.13154359029048!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x390dc621d575b2af%3A0x8997e32432dcf12d!2sNestle+Samalkha+Factory!5e0!3m2!1sen!2sin!4v1500908406317
	    
	    if(objSite.getSiteName().toString().length() > 30)
	    {
	    	objSiteViewBean.setLocationName(objSite.getSiteName().substring(0, 30));//SiteName
	    }
	    else
	    {
	    	objSiteViewBean.setLocationName(objSite.getSiteName());//SiteName
	    }
	    
	    
	    objSiteViewBean.setWeatherStatus("-");//sunny
	    objSiteViewBean.setWeatherTodayTemp(""); //37�C
	    objSiteViewBean.setWeatherYesterdayTemp("-");//37�C
	    objSiteViewBean.setWindSpeed("-");//12 kmph
	    
	    String varIrradiation = "";
	    
	    if(objSiteStatus.getIrradiation() == null)
	    {
	    	objSiteViewBean.setIrradiation("-");//
	    }
	    else if(objSiteStatus.getIrradiation() == "")
	    {
	    	objSiteViewBean.setIrradiation("-");//
	    }
	    else
	    {
	    	varIrradiation = String.format("%.2f", Double.valueOf(objSiteStatus.getIrradiation()));
	    	objSiteViewBean.setIrradiation(varIrradiation);//
	    }
	    

	    
	    objSiteViewBean.setIrradiationLastUpdate("-");//6 days ago...
	    objSiteViewBean.setModuleTemp("-");//31�C
	    
	    objSiteViewBean.setOpenTickets(varIntTotalOpenTicket.toString());//8
	    objSiteViewBean.setCompletedTickets(varIntTotalClosedTicket.toString());//5
	    
	    
	    
        
        
	    
	    objSiteViewBean.setTodayScheduledCleaningJobs("-");//5
	    objSiteViewBean.setTodayScheduledPMJobs("-");//1
	    objSiteViewBean.setTodayScheduledJobs("-");//4
	    objSiteViewBean.setTotalScheduledJobs("-");//1185
	    
	    
	    objSiteViewBean.setTodayEvents("-");//4
	    objSiteViewBean.setTotalEvents("-");//23
	    
	    
	    Integer ErrorInverter = lstDataSource.stream().filter(p -> p.getStatus() == 3).collect(Collectors.toList()).size();
	    //Integer ActiveInverter = InverterCount - ErrorInverter;
	    Integer ActiveInverter = lstDataSource.stream().filter(p -> p.getStatus() == 1).collect(Collectors.toList()).size();
	    
	    
	    
	    objSiteViewBean.setTotalInverters(InverterCount.toString());//8	-	7/8
	    objSiteViewBean.setActiveInverters(ActiveInverter.toString());// 7
	    objSiteViewBean.setErrorInverters(ErrorInverter.toString());//1	
	    objSiteViewBean.setModules("-");//589
	    
	    
	    objSiteViewBean.setTodayCo2Avoided("-");//121.4 kg
	    objSiteViewBean.setTotalCo2("-");//97.3 t
	    
	    
	    
	    List<EquipmentListBean> lstEquipmentList = new ArrayList<EquipmentListBean>();
        
	    List<Equipment> lstEquipmentAll = equipmentService.listInverters();
	    List<Equipment> lstEquipment = lstEquipmentAll.stream().filter(p -> p.getSiteID() == id ).collect(Collectors.toList());
	    
	    
	    for(int k=0;k < lstEquipment.size();k++)
	    {
	    	EquipmentListBean objEquipmentListBean = new EquipmentListBean();
	    
	    	if(lstEquipment.get(k).getSiteID() !=null)
	    	{
	    		objEquipmentListBean.setSiteID(lstEquipment.get(k).getSiteID().toString());
	    		
	    	}
	    	
	    	if(lstEquipment.get(k).getEquipmentId() !=null)
	    	{
	    		objEquipmentListBean.setEquipmentID(lstEquipment.get(k).getEquipmentId().toString());
	    		
	    		Integer CurrentEquipmentId=lstEquipment.get(k).getEquipmentId();
	    		
	    		List<DataSource> myLstDataSource = lstDataSource.stream().filter(p -> p.getEquipmentID() == CurrentEquipmentId ).collect(Collectors.toList());
	    		
	    		
		    	if(myLstDataSource.size()!=0)
		    	{
		    		DataSource myObjDataSource = myLstDataSource.get(0);
		    	
		    		Double dbYesterdayTotalEnergy = 0.0;
		    		Double dbTotalEnergy = 0.0;
		    		Double dbTodayEnergy = 0.0;
		    		Double dbTodayHoursOn = 0.0;
	    		 
		    		Date LastUpdatedDate = null;
	    		
	    		
	    		
	    		if(myObjDataSource.getTotalEnergy()!= null)
    			{
    				dbTotalEnergy += myObjDataSource.getTotalEnergy();
    				
    			}
    			
    			if(myObjDataSource.getYesterdayTotalEnergy()!= null)
    			{
    				dbYesterdayTotalEnergy += myObjDataSource.getYesterdayTotalEnergy();
    				
    			}
    			
    			if(myObjDataSource.getTodayHoursOn()!= null)
    			{
    				dbTodayHoursOn += myObjDataSource.getTodayHoursOn();
    				
    			}
    			
    			
    			
    			
    			if(myObjDataSource.getTimestamp()!= null)
    			{
    				LastUpdatedDate = myObjDataSource.getTimestamp();
					
    			}
    			
    			if(myObjDataSource.getStatus()!=null)
    			{
    				objEquipmentListBean.setNetworkStatus(myObjDataSource.getStatus().toString());
    			}
    			
    			
    		dbTodayEnergy = dbTotalEnergy - dbYesterdayTotalEnergy;
    		
    		dbTotalEnergy = dbTotalEnergy/1000;
    		

    		objEquipmentListBean.setTodayEnergy(String.format("%.2f", dbTodayEnergy));
    		objEquipmentListBean.setTotalEnergy(String.format("%.2f", dbTotalEnergy));
    		
    		
    		if(lstEquipment.get(k).getCapacity() !=null)
    		{
    			
    			/*if(dbTodayHoursOn <=0)
    			{
    				objEquipmentListBean.setPerformanceRatio("-");
    			}
    			else
    			{
    				objEquipmentListBean.setPerformanceRatio(String.format("%.2f", ((dbTodayEnergy/dbTodayHoursOn)/(Double.parseDouble(lstEquipment.get(k).getCapacity()))) * 100));
    			}
    			*/
    			
    			
    			if((dbTodayEnergy)/(Double.parseDouble(lstEquipment.get(k).getCapacity())) >= 7.0)
    			{
    				objEquipmentListBean.setPerformanceRatio("-");
    			}
    			else
    			{
    				objEquipmentListBean.setPerformanceRatio(String.format("%.2f", ((dbTodayEnergy)/(Double.parseDouble(lstEquipment.get(k).getCapacity())))));
            		
    			}
    			
    			
    			
    			
    		}
    		else
    		{
    			objEquipmentListBean.setPerformanceRatio("-");    			
    		}
    		
    		if(LastUpdatedDate == null)
    		{
    			objEquipmentListBean.setLastUpdate("-");
    		}
    		else
    		{
    			objEquipmentListBean.setLastUpdate(sdf.format(LastUpdatedDate));
    		}
    	
    		
    		
    		
		   }
    		
    		
		    	
	    	}
	    	
	    	
	    	if(lstEquipment.get(k).getEquipmentCode() !=null)
	    	{
	    		objEquipmentListBean.setEquipmentCode(lstEquipment.get(k).getEquipmentCode());
	    	}
	    	
	    	
	    	if(lstEquipment.get(k).getCustomerReference() !=null)
	    	{
	    		objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getCustomerReference());
	    		//objEquipmentListBean.setEquipmentReference(lstEquipment.get(k).getPrimarySerialNumber());
	    	}
	    	
	    	
	    	if(lstEquipment.get(k).getDisplayName() !=null)
	    	{
	    		objEquipmentListBean.setEquipmentName(lstEquipment.get(k).getDisplayName());
	    	}
	    	
	    	if(lstEquipment.get(k).getEquipmentTypeName() !=null)
	    	{
	    		objEquipmentListBean.setEquipmentType(lstEquipment.get(k).getEquipmentTypeName());
	    	}
	    	
	    	
	    	
	    	lstEquipmentList.add(objEquipmentListBean);
	    	
	    
	    }
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    

		SimpleDateFormat sdfchart = new SimpleDateFormat("yyyy-MM-dd");
		String ChartJsonData1 ="";
		String ChartJsonData1Parameter ="";
	    String ChartJsonData1GraphValue ="";
	     
	     
	     for(int a=0;a<lstSiteSummary.size();a++)
	     {
	    	 
	    	 String varChartDate = sdfchart.format(lstSiteSummary.get(a).getTimestamp()).toString();
	    	 
	    	 Double dbTodayEnergy = lstSiteSummary.get(a).getTodayEnergy();
	    	 String varChartValue ="";
	    	 
//	    	if(lstSiteSummary.get(a).getSiteID()==5)
//	    	{
//	    		 varChartValue = String.format("%.2f", dbTodayEnergy/1000);
//	    	}
//	    	else
//	    	{
//	    		 varChartValue = String.format("%.2f", dbTodayEnergy);
//	    	}
	    	 
	    	 varChartValue = String.format("%.2f", dbTodayEnergy);
	    	 
	    	
	    	 //String varChartValue = String.format("%.2f", dbTodayEnergy * 1000 * 1000);
	    	
	    	 
	    	 if(a==0)
	    	 {
	    		 //ChartJsonData1="{\"date\": \"" + varChartDate + "\",\"value1\":\"" + varChartValue + "\"}";
	    		 ChartJsonData1="{\"date\": \"" + varChartDate + "\",\"value1\":" + varChartValue + "}";
	    	 }
	    	 else
	    	 {
	    		 ChartJsonData1 = ChartJsonData1 + ",{\"date\": \"" + varChartDate + "\",\"value1\":" + varChartValue + "}";
	    		 //ChartJsonData1 = ChartJsonData1 + ",{\"date\": \"" + varChartDate + "\",\"value1\":\"" + varChartValue + "\"}";	
	    	 }
	        		
	     }
	     

	     
	     
	     
	     ChartJsonData1Parameter = "{\"fromField\": \"value1\",\"toField\": \"value1\"}";
		 //ChartJsonData1GraphValue = "{\"id\": \"g1\",\"valueField\": \"value1\",\"comparable\": true,\"lineThickness\": 2,\"title\":\"Daywise Today Energy (kWh)  \",\"type\": \"smoothedLine\",\"useDataSetColors\": false,\"balloonText\": \"Today Energy:<b>[[value1]]</b>\",\"compareGraphBalloonText\": \"Total Energy:<b>[[value1]]</b>\"}";
		 ChartJsonData1GraphValue = "{\"id\": \"g1\",\"valueField\": \"value1\",\"comparable\": true,\"lineThickness\": 2,\"title\":\"Daywise Today Energy (kWh)  \",\"type\": \"column\",\"useDataSetColors\": false,\"balloonText\": \"Today Energy:<b>[[value1]]</b>\",\"compareGraphBalloonText\": \"Total Energy:<b>[[value1]]</b>\"}";
		 

	     ChartJsonData1 = "[" + ChartJsonData1 + "]";
	     ChartJsonData1Parameter = "[" + ChartJsonData1Parameter + "]";
	     ChartJsonData1GraphValue = "[" + ChartJsonData1GraphValue + "]";
	 	
	    System.out.println(ChartJsonData1);
	    System.out.println(ChartJsonData1Parameter);
	    System.out.println(ChartJsonData1GraphValue);
	     
	     objSiteViewBean.setChartJsonData1(ChartJsonData1);
	     objSiteViewBean.setChartJsonData1Parameter(ChartJsonData1Parameter);
	     objSiteViewBean.setChartJsonData1GraphValue(ChartJsonData1GraphValue);
	     
	     

	    
	     
	     
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
	     
	    
	    
	    
	    
	    
	    
	    
	    
	    
	    
		//List<EquipmentStatus> lstEquipmentStatus = equipmentStatusService.getSiteStatusListByUserId(id);
		
    	 AccessListBean objAccessListBean = new AccessListBean();
    	   
    	    try
    	    {
    	    User objUser =  userService.getUserById(refid);
    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
    	 	
    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
    	   
    	    objAccessListBean.setUserID(refid);
    	    objAccessListBean.setUserName(objUser.getUserName());
    	    //MyCustomerID = objSite.getCustomerID();
    	    objAccessListBean.setMyCustomerID(objSite.getCustomerID());
    	    
    	    Customer objCustomer = customerService.getCustomerById(objSite.getCustomerID()); 
    	    if(objCustomer!=null)
    	    {
    	    	objAccessListBean.setMyCustomerName(objCustomer.getCustomerName());
    	    }
    	    else
    	    {
    	    	objAccessListBean.setMyCustomerName("Customer View");
    	    }
    	    
    	    
    	    
    	    if(objUser.getRoleID()!=4)
    		{
    			objAccessListBean.setCustomerListView("visible");
    		}
    	    
    	    
    	    for(int l=0;l<lstRoleActivity.size();l++)
    	    {
    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setDashboard("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setOverview("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setSystemMonitoring("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setVisualization("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setAnalytics("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setPortfolioManagement("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setTicketing("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setForcasting("visible");
    	    	}
    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
    	    	{
    	    		objAccessListBean.setConfiguration("visible");
    	    	}
    	    }
    	    
    	    }
    	    catch(Exception e)
    	    {
    	    	System.out.println(e.getMessage());
    	    }
    	    

    	    objSiteViewBean.setSiteID(String.valueOf(id));
    	    
	    //model.addAttribute("datatransaction",lstDataTransaction);
    	    model.addAttribute("siteList", getDropdownList("Site",refid));
    	    model.addAttribute("userList", getDropdownList("User",refid));
	    model.addAttribute("access",objAccessListBean);
	    model.addAttribute("siteview", objSiteViewBean);
	    model.addAttribute("siteviewEquipmentList", lstEquipmentList);
	    model.addAttribute("ticketcreation", new TicketDetail());
    
     return "siteview";
 }
 public  Map<String,String> getDropdownList(String DropdownName , int refid) 
	{
    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
    	
    	if(DropdownName == "Site")
    	{
    		List<Site> ddlList = siteService.getSiteListByUserId(refid);
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getSiteId().toString(), ddlList.get(i).getSiteName());
    		}
    		
    	}
    	else if(DropdownName == "User")
    	{
    		List<User> ddlList = userService.listFieldUsers();
    		
    		for(int i=0;i< ddlList.size();i++)
    		{
    			ddlMap.put(ddlList.get(i).getUserId().toString(), ddlList.get(i).getUserName());
    		}
    		
    	}
    	
		return ddlMap;
	}
 
 private static String readAll(Reader rd) throws IOException {
	    StringBuilder sb = new StringBuilder();
	    int cp;
	    while ((cp = rd.read()) != -1) {
	      sb.append((char) cp);
	    }
	    return sb.toString();
	  }
 public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
	    InputStream is = new URL(url).openStream();
	    try {
	      BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
	      String jsonText = readAll(rd);
	      JSONObject json = new JSONObject(jsonText);
	      return json;
	    } finally {
	      is.close();
	    }
	  }
 public static void callfunction() throws IOException, JSONException {
	    JSONObject json = readJsonFromUrl("http://api.openweathermap.org/data/2.5/weather?lat=19.111976&lon=72.911129&appid=6a977327fea2cb3bf2aae26c2c0474cb");
	   // System.out.println(json.toString());
	    System.out.println(json.get("main"));
	    System.out.println(json.get("weather"));
	    System.out.println(json.get("wind"));
	    String name ="main";
	    String Temp ="";
	    String Weather ="";
	    String Windspeed ="";
		if(json.get("main").toString().contains("\"temp\":")== true)
		{
			Temp = json.getJSONObject("main").getString("temp");
		}
		
		 if(json.get("wind").toString().contains("\"speed\":")== true)
		{
			Windspeed = json.getJSONObject("wind").getString("speed");
		}
		 if(json.get("weather").toString().contains("\"main\":")== true)
			{
				Weather = json.getJSONArray("weather").getJSONObject(0).getString("main");
			}
		System.out.println(Temp);
		System.out.println(Weather);
		System.out.println(Windspeed);
		
	  }
 
}
