<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To eAMPM ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
      <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

     <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >      
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
      
      
      <!-- <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script> -->
		<script type="text/javascript" src="https://rawgit.com/Semantic-Org/Semantic-UI/next/dist/semantic.js"></script>

      
      <script type="text/javascript">
         $(document).ready(function() {
            $('#example').DataTable();
            
           
			
            
             $('.close').click(function(){
					$('.clear').click();
				});
  $('.clear').click(function(){
            	$('.search').val("");
            });

$('body').click(function(){
                $('#builder').removeClass('open'); 
              });


            $("#searchSelect").change(function() {
                var value = $('#searchSelect option:selected').val();
                var uid =  $('#hdneampmuserid').val();
             	redirectbysearch(value,uid);
     	    });
            
           

              

            
            $('.dataTables_filter input[type="search"]').attr('placeholder','search');
            
         } );
      </script>
      
     <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown();
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 



             $('.validation-form')
   .form({
     on: 'blur',
     onFailure: function(formErrors, fields) {
       $.each(fields, function(e) {
         var ele = $('#' + e);
         var fail = (ele.val() === false || ele.val().length === 0);
         if (fail) {
           console.log(ele, fail);
           ele.focus();
           $('html,body').animate({
             scrollTop: ele.offset().top - ($(window).height() - ele.outerHeight(true)) / 2
           }, 200);
           return false;
         }
       });
       return false;
     },
     fields: {
    	 txtSubject: {
         identifier: 'txtSubject',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
        ddlSite: {
         identifier: 'ddlSite',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
      ddlType: {
            identifier: 'ddlType',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
      ddlCategory: {
         identifier: 'ddlCategory',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
       ddlPriority: {
         identifier: 'ddlPriority',
         rules: [{
           type: 'empty',
           prompt: 'Please select a dropdown value'
         }]
       },
       description: {
         identifier: 'description',
         rules: [{
           type: 'empty',
           prompt: 'Please Enter Description'
         }]
       },
       
     }
   }); ('form').reset('clear');

         })
         
      </script>
      <script type="text/javascript">
        $(document).ready(function() {
        
        	 $("#ddlType").change(function() {
        		 $('#ddlCategory').val();
        
                 var value = $('#ddlType option:selected').val();
     
                 if(value == "Operations") {
                	 $('#ddlCategory').val();
                	 $('#category').show();

                 }
                 else{
                	 $('#ddlCategory').val();
                	 $('#category').show();

                    }
                 
              });

        	/* var $ddlType = $( '#ddlType' ),
    		$ddlCategory = $( '#ddlCategory' ),
        $options = $ddlCategory.find( 'option' );
        
    $ddlType.on( 'change', function() {
    	$ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
    } ).trigger( 'change' ); */
        });
      </script>
      <script type="text/javascript">
         $(document).ready(function() {
            $('#example').DataTable();
            $('#example1').DataTable();
            /* $('body').click(function(){
                $('#builder').removeClass('open'); 
              }); */
            $(".bars").click(function() {
               $(".sidebar-menu").invisible();
            })
             $('#example2').calendar({
            type: 'date'
            });
            
            var today = new Date();
            /* $('#example3').calendar({
              minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate())
            }); */
           
          $('#example3').calendar({
                monthFirst: false,
               type: 'date',
               minDate: 0,
               minDate: new Date(today.getFullYear(), today.getMonth(), today.getDate()),
               formatter: {
                 date: function (date, settings) {
                   if (!date) return '';
                   var day = date.getDate();
                   var month = date.getMonth() + 1;
                   var year = date.getFullYear();
                   return day + '/' + month + '/' + year;
                 }
               }
             });
           
            
            $("#searchSelect").change(function() {
                var value = $('#searchSelect option:selected').val();
                var uid =  $('#hdneampmuserid').val();
             	redirectbysearch(value,uid);
     	    });
          /*   $('#dateAndTime').hide(); */
            $("#DateTimes").change(function() {
            	 var value = $('#DateTimes option:selected').val();
            	 if(value == "Scheduled") {
            		 $('#dateAndTime').show();
                 }
            	 else if(value == "Immediate") {
            		 $('#dateAndTime').show();
                 }
            })
            
         
         
            $('.dataTables_filter input[type="search"]').attr('placeholder','search');
         } );
      </script>
    
    
       <script type='text/javascript'>//<![CDATA[
    $(window).load(function(){

    	   $('.ui.dropdown').dropdown({forceSelection:false});
    });
    </script>
       <script type="text/javascript">
        $(document).ready(function() {
            if($(window).width() < 767)
{
   
   $('.card').removeClass("slide-left");
   $('.card').removeClass("slide-right");
   $('.card').removeClass("slide-top");
   $('.card').removeClass("slide-bottom");
}
        });
      </script>
  <style>
  	#overviewInfor {
          display:none;
        }
        .oveallSearch i {
        	color:#FFF !important;
        }
        .ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
}

.calendar {
    height: 10%;
}

          .input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}

        div.dataTables_wrapper div.dataTables_length select {
    width: 50px;
    display: inline-block;
}
  </style>
  </head>
  <body  class="fixed-header">
  
  <input type="hidden" value="${access.userID}" id="hdneampmuserid">
  
  
         <nav class="page-sidebar" data-pages="sidebar">
        
         <div class="sidebar-header">
            <a href="./dashboard"><img src="resources/img/logo01.png" class="m-t-10" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management"></a>
            <i class="fa fa-bars bars"></i>
         </div>
         
         <div class="sidebar-menu">
          
          <c:if test="${not empty access}">
          
            <ul class="menu-items">
            
           <c:if test="${access.dashboard == 'visible'}">
          
               <li class="" style="margin-top:2px;">
                  <a href="./dashboard" class="detailed">
                  <span class="title">Dashboard</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.overview == 'visible'}">
               <li>
                  <a href="#"><span class="title">Overview</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-list-alt"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.systemMonitoring == 'visible'}">
              <li>
                  <a href="#"><span class="title">System Monitoring</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
              <c:if test="${access.visualization == 'visible'}">
           <li class="active">
                  <a href="javascript:;"><span class="title">Visualization</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li class="active">
                        	<a href="./customerlist">Customer View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
                     	</li>
                    </c:if>
           
                     <li>
                        <a href="./sitelist">Site View</a>
                        <span class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
                     </li>
                     <li>
                        <a href="./equipmentlist">Equipment View</a>
                        <span class="icon-thumbnail"><i class="fa fa-cog"></i></span>
                     </li>
                  </ul>
               </li>
              
           </c:if>

           
            <c:if test="${access.analytics == 'visible'}">
           <li>
                  <a href="#">
                  <span class="title">Analytics</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.portfolioManagement == 'visible'}">
          <li>
                  <a href="#">
                  <span class="title">Portfolio Manager</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
               </li>
           </c:if>
           
             <c:if test="${access.ticketing == 'visible'}">
            <li>
                    <a href="javascript:;"><span class="title">Operation & Maintenance</span>
                  <span class=" arrow"></span></a>
                   <span class="icon-thumbnail" style="padding-top:5px !important;"><img src="resources/img/maintance.png"></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./ticketdetails">Ticket View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-ticket"></i></span>
                     	</li>
                    </c:if>
           
                  </ul>
               </li>
           
           
           
           
           
           </c:if>
           
           
           
            <c:if test="${access.forcasting == 'visible'}">
          <li>
                  <a href="#"><span class="title">Forecasting</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                     <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>/
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li>
                     
                      <li>
                        <a href="#">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li>
                     
                      <li>
                        <a href="#">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>
        
        
              
              
               
              
               
              
             
          
            </ul>
         
         
          </c:if>
          
          
         
            <div class="clearfix"></div>
         </div>
    
    
    
      </nav>
  
  
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
               <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"></a>
                 <!--  <img src="resources/img/logo.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"> -->
               </div>
            </div>
            <div class="d-flex align-items-center">
            <div class="form-group required field searchbx" id="SearcSElect">
                           <select class="ui fluid search selection dropdown width oveallSearch" id="searchSelect">
                             <!--  <option value="-2"></option> -->
                              <option value="-1">search </option>
                              <option value="0">Dashboard</option>
                                
 								<c:if test="${not empty access}">
          							<c:if test="${access.customerListView == 'visible'}">
          								<option value="1">Customer View</option>
                    				</c:if>
                 				</c:if>   
                 				  
                              <option value="3">Site View</option>
                              <option value="5">Equipment View</option>
                              
                               <c:if test="${access.customerListView == 'visible'}">
          				 			<option value="6">Ticket View</option>
                    		   </c:if>
           
           
           
                           </select>
                        </div>
                 <%-- <div class="form-group required field searchbx" id="SearcSElect">
                            <select class="ui fluid search selection dropdown width oveallSearch" id="searchSelect">
                             <!--  <option value="-2"></option> -->
                              <option value="-1">search </option>
                              <option value="0">Dashboard</option>
                               
 								<c:if test="${not empty access}">
          							<c:if test="${access.customerListView == 'visible'}">
          								<option value="1">Customer View</option>
                    				</c:if>
                 				</c:if>   
                 				  
                 				  
                              <option value="3">Site View</option>
                              <option value="5">Equipment View</option>
                                   <c:if test="${access.customerListView == 'visible'}">
          				 			<option value="6">Ticket View</option>
                    		   </c:if>
                    		   
                    		   
                           </select>
                        </div> --%>
                        <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
               <c:if test="${access.customerListView == 'visible'}">
          				<div>
                  <p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
                  <p class="create-tkts">New Ticket</p>
               </div>
                    </c:if>
                    
                    
              
               <%-- <div class="dropdown pull-right hidden-md-down user-log">
                 <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                  <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
             <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span> --%>
               <!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
               <!-- <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
            </div>
         </div>
         <div class="page-content-wrapper ">
         
         <c:if test="${not empty customerview}">
         
            <div class="cv-cont content" id="QuickLinkWrapper1">
               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item"><a>Visualization</a></li>
                     <li class="breadcrumb-item"><a href="./customerlist">Customer View</a></li>
                     <li class="breadcrumb-item active">${customerview.customerName}</li>
                  </ol>
               </div>
               
               <div class=" container-fluid   container-fixed-lg">
                  <div class="card card-transparent">
                  
                  
                  
           <div class="card-block sortable padd-0">
              
                        <div class="col-md-12 padd-0">
                           <div class="col-lg-3">
                              <div class="card card-default bg-default slide-left" id="allsites" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">total sites</div>
                                    <div class="card-title pull-right"><i class="fa fa-globe font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${customerview.totalSiteCount}</strong></span>
                                    </h3>
                                     <div class="col-md-12 padd-0">
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<p>Rooftop</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-2 padd-0">
                                    			<p>-</p>
                                    		</div>
                                    		<div class="col-md-7 col-xs-6 padd-0">
                                    			<p>${customerview.rooftopCount}</p>
                                    		</div>
                                    	</div>
                                    	 <div class="col-md-12 padd-0 uitlity m-t-10">
                                    		<div class="col-md-3 col-xs-4 padd-0">
                                    			<p>Utility</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-2 padd-0">
                                    			<p>-</p>
                                    		</div>
                                    		<div class="col-md-7 col-xs-6 padd-0">
                                    			<p>${customerview.utilityCount}</p>
                                    		</div>
                                    	</div>
                                    <%-- <p>Rooftop - ${customerview.rooftopCount} </p>
                                    <p>Utility - ${customerview.utilityCount} </p> --%>
                                    <!-- <p>Rooftop & Utility - ${customerview.rooftopUtilityCount} </p> -->
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    <p>Last Down Time : ${customerview.lastDownTime}</p>
                                 </div>
                              </div>

                              <div class="card card-default bg-default slide-left" id="energy" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">today's energy</div>
                                    <div class="card-title pull-right"><i class="fa fa-bolt font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${customerview.todayEnergy}</strong></span>
                                    </h3>
                                    <div class="col-md-12 padd-0">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Event Time</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-7 col-xs-6 padd-0">
                                    			<p>${customerview.lastUpdatedTime}</p>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 padd-0 m-t-10 uitlity ">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>File Received</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-7 col-xs-6 padd-0">
                                    			<p>${customerview.eventTime}</p>
                                    		</div>
                                    	</div>
                                    	<div class="col-md-12 padd-0 m-t-10 uitlity ">
                                    		<div class="col-md-4 col-xs-4 padd-0">
                                    			<p>Total Energy</p>
                                    		</div>
                                    		<div class="col-md-1 col-xs-1 padd-0">
                                    			<p>:</p>
                                    		</div>
                                    		<div class="col-md-7 col-xs-6 padd-0">
                                    			<p>${customerview.totalEnergy}</p>
                                    		</div>
                                    	</div>
                                   <%--  <p>Event Time    : ${customerview.lastUpdatedTime}</p>
                                    <p>File Received : ${customerview.eventTime}</p>
                                    <p>Total Energy  : ${customerview.totalEnergy}</p> --%>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Production Date : ${customerview.productionDate}</p>
                                 </div>
                              </div>

                              <div class="card card-default bg-default slide-left" id="avoid" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">CO<sub>2</sub> Avoided</div>
                                    <div class="card-title pull-right"><img src="resources/img/ImageResize_01.png"></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${customerview.todayCo2Avoided}</strong></span>
                                    </h3>
                                    <p>Today Emission</p>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    <p>Total : ${customerview.totalCo2Avoided}</p>
                                 </div>
                              </div>

                           </div>
                           <div class="col-lg-6" id="maps">
                                  <div class="card card-default bg-default" data-pages="card">
                                 <div class="cmap card-block">
                                    <div id="dvMap" class="map"></div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3">
                              <div class="card card-default bg-default slide-right" id="sitestatus" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">site status</div>
                                    <div class="card-title pull-right"><i class="fa fa-sitemap font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                              
                                    <div class="col-md-12 col-xs-12 padd-0">
                                    	<div class="col-md-6 col-xs-7 padd-0">
                                    		<span class="normal"></span><span class="nortxt"> Active Sites </span>
                                    	</div>
                                    	<div class="col-md-1 col-xs-1 padd-0 m-t-10">
                                    		<p class="nortxt">-</p>
                                    	</div>
                                    	<div class="col-md-5 col-xs-4 padd-0 m-t-10">
                                    		<p class="nortxt">${customerview.activeSiteCount}</p>
                                    	</div>
                                    </div>
                                     <div class="col-md-12 col-xs-12 padd-0">
                                    	<div class="col-md-6 col-xs-7 padd-0">
                                    		<span class="warn"></span><span class="warntxt"> Warning Sites </span>
                                    	</div>
                                    	<div class="col-md-1 col-xs-1 padd-0 m-t-10">
                                    		<p class="nortxt">-</p>
                                    	</div>
                                    	<div class="col-md-5 col-xs-4 padd-0 m-t-10">
                                    		<p class="nortxt">${customerview.warningSiteCount}</p>
                                    	</div>
                                    </div>
                                     <div class="col-md-12 col-xs-12 padd-0">
                                    	<div class="col-md-6 col-xs-7 padd-0">
                                    		<span class="err"></span><span class="errtxt"> Down Sites </span>
                                    	</div>
                                    	<div class="col-md-1 col-xs-1 padd-0 m-t-10">
                                    		<p class="errtxt">-</p>
                                    	</div>
                                    	<div class="col-md-5 col-xs-4 padd-0 m-t-10">
                                    		<p class="errtxt">${customerview.downSiteCount}</p>
                                    	</div>
                                    </div>
                                     <div class="col-md-12 col-xs-12 padd-0">
                                    	<div class="col-md-6 col-xs-7 padd-0">
                                    		<span class="off"></span><span class="offtxt"> Offline Sites </span>
                                    	</div>
                                    	<div class="col-md-1 col-xs-1 padd-0 m-t-10">
                                    		<p class="offtxt">-</p>
                                    	</div>
                                    	<div class="col-md-5 col-xs-4 padd-0 m-t-10">
                                    		<p class="offtxt">${customerview.offlineSiteCount}</p>
                                    	</div>
                                    </div>
                                   <%--  <div class="row">
                                       <div  class="data ml-20">
                                          <span class="normal"></span><span class="nortxt"> Active Sites - ${customerview.activeSiteCount}</span>
                                       </div>
                                    </div> --%>
                                   <%--  <div class="row">
                                       <div  class="data ml-20">
                                          <span class="warn"></span><span class="warntxt"> Warning Sites - ${customerview.warningSiteCount}</span>
                                       </div>
                                    </div> --%>
                                 <%--    <div class="row">
                                       <div  class="data ml-20">
                                          <span class="err"></span><span class="errtxt"> Down Sites - ${customerview.downSiteCount}</span>
                                       </div>
                                    </div> --%>
                                   <%--  <div class="row">
                                       <div  class="data ml-20">
                                          <span class="off"></span><span class="offtxt"> Offline Sites - ${customerview.offlineSiteCount}</span>
                                       </div>
                                    </div> --%>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    <a href="./sitelist">View all sites</a>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="tickets" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">Open Tickets</div>
                                    <div class="card-title pull-right"><i class="fa fa-ticket font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                    <h3 class="m-t-10">
                                       <span class="semi-bold"><strong>${customerview.openTicketCount}</strong></span>
                                    </h3>
                                    <p><strong>Operations & Maintenance</strong></p>
                                   <p class="m-t-10"><a><strong><a href="./ticketdetails">Click here</a></strong></a> to view ticket details</p>
                                   <%--  <p>${customerview.ticketMessages}<a><strong><a href="./ticketdetails">Click here</a></strong></a> to view ticket details</p> --%>
                                  </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Closed Tickets : ${customerview.completedTicketCount}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-200">
                                 <div class="card-header header-top">
                                    <div class="card-title pull-left m-t-5">events</div>
                                    <div class="card-title pull-right"><i class="fa fa-calendar font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block m-t-10">
                                    <div class="row">
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <p>Today</p>
                                          <p>${customerview.todayEventCount}</p>
                                       </div>
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <p>Total</p>
                                          <p>${customerview.totalEventCount}</p>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="card-footer">
                                    <a>View all events</a>
                                 </div>
                              </div>
                           </div>
                        </div>
                
                  
                     </div>
                  
                     <!-- Table Start-->
                     <div class="card-block sortable">
                           <div class="row" id="sitedetails">                  
                                 
                              <div class="card card-default bg-default" data-pages="card" style="padding-bottom:10px;">
                              
                               <div class="card-header">
                                    <div class="card-title m-l-10 tbl-head">Site Details</div>
                                 </div>
                                 
                                 
                                 <div class="card-block table-responsive padd-0" >
                                    <table id="example" class="table table-striped table-bordered"  width="100%" cellspacing="0">
                                       <thead>
                                          <tr>
                                          
                                           <c:if test="${access.customerListView == 'visible'}">
                                             <th>Site Code</th>
          								</c:if>
                                             <th>Site Ref.</th>
                                             <th style="width:150px !important;">Site Name</th>
                                             <th>Status</th>
                                             <th>Today Energy (kWh)</th>
                                             <th>Total Energy (MWh)</th>
                                             <th>Specific Yield</th>
                                             <th>Inverters</th>
                                             
                                              <c:if test="${access.customerListView == 'visible'}">
                                           <th>Event Time</th>
          								</c:if>
          								
                                             
                                          </tr>
                                       </thead>
                                       <tbody>
                                       
                                       
                                       
                                       <c:forEach items="${customerviewSiteList}" var="customerviewlist">
                        <tr>
                            
                             <c:if test="${access.customerListView == 'visible'}">
                                       <td><a href="./siteview${customerviewlist.siteID}&ref=${access.userID}" class="newlink">${customerviewlist.siteCode}</a></td>
                           </c:if>
          								
          								
                             <td><a href="./siteview${customerviewlist.siteID}&ref=${access.userID}" class="newlink">${customerviewlist.siteReference}</a></td>
                            <td><a href="./siteview${customerviewlist.siteID}&ref=${access.userID}" class="newlink">${customerviewlist.siteName}</a></td>
                            
                              <c:choose>
                          <c:when test="${customerviewlist.networkStatus=='0'}">
                            <td><div class="offl"></div>&nbsp;Offline</td>
                          </c:when>
                          <c:when test="${customerviewlist.networkStatus=='1'}">
                            <td><div class="act"></div>&nbsp;Active</td>
                          </c:when>
                          <c:when test="${customerviewlist.networkStatus=='3'}">
                            <td><div class="erro"></div>&nbsp;Error</td>
                          </c:when>
                          <c:when test="${customerviewlist.networkStatus=='2'}">
                            <td><div class="warn"></div>Warning</td>
                          </c:when>
                          <c:otherwise>
                            <td><div class="offl"></div>Offline</td>
                          </c:otherwise>
                      </c:choose>
                      
                      
                            <td>${customerviewlist.todayEnergy}</td>
                            <td>${customerviewlist.totalEnergy}</td>
                            <td>${customerviewlist.performanceRatio}</td>
                            <td>${customerviewlist.inverters}</td>
                            
                            
                          
                           <c:if test="${access.customerListView == 'visible'}">
                                           <td>${customerviewlist.lastUpdate}</td>
          								</c:if>
          								
          								
                    </tr>
                      </c:forEach>
                      
                                       
                                        
                                         
                                        
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                     </div>
                     <!-- Table End-->
                     
                     <!-- Content Start-->
                        <div class="card-block sortable" id="overviewInfor" >
                           <div class="card-header ">
                                    <div class="card-title">Overview information and statistics</div>
                                 </div>
                           <div class="col-lg-12" id="overview">
                              <div class="card card-default bg-default slide-bottom" data-pages="card">
                                 <div class="h-300">
                                    <div class="card-header">
                                       <div class="row">
                                          <div class="col-lg-3 center">
                                             <div class="small">
                                                <i class="fa fa-bolt"></i> Production Yield
                                             </div>
                                          <div>
                                                <h1 class="font-extra-bold m-t-xl m-b-xs">${customerview.todayProductionYield}</h1>
                                                <small>Today</small>
                                          </div>
                                                <div class="small m-t-xl">Total Yield : ${customerview.totalProductionYield}</div>
                                          </div>
                                          <div class="col-lg-6">
                                                <div class="text-center small">
                                                   <i class="fa fa-laptop"></i>Production yield in current month
                                                </div>
                                                      <div id="chartdiv"></div>
                                          </div>
                                         <div class="col-lg-3 center">
                                             <div class="small">
                                                <i class="fa fa-line-chart" aria-hidden="true"></i> Performance Ratio
                                             </div>
                                             <div>
                                                <h1 class="font-extra-bold m-t-xl m-b-xs">${customerview.todayPerformanceRatio}</h1>
                                                <small>Today</small>
                                             </div>
                                             <div class="small m-t-xl">Yesterday : ${customerview.totalPerformanceRatio}</div>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                                 <div class="panel-footer">
                                       <a>View all events</a>
                                 </div>
                              </div>
                           </div>
                        
                     </div>
                     <!-- Content End-->
                  </div>
               </div>
            </div>
         
         </c:if>
         <div class="container-fixed-lg">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span>INSPIRE CLEAN ENERGY.</span>
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-rhs sm-pull-reset">
                     <span class="hint-text">Powered by</span>
                     <span>MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>
 
      <!-- Side Bar Content Start-->

      <!-- 
         <div id="quickview" class="quickview-wrapper" data-pages="quickview">
         <ul class="nav nav-tabs">
            <li class data-target="#quickview-notes" data-toggle="tab">
               <a href="#">Tickets</a>
            </li>
            <li data-target="#quickview-alerts" data-toggle="tab">
               <a href="#">Events</a>
            </li>
            <li class="active" data-target="#quickview-chat" data-toggle="tab">
               <a href="#">Chat</a>
            </li>
         </ul>
         <a class="btn-link quickview-toggle" data-toggle-element="#quickview" data-toggle="quickview"><i class="pg-close"></i></a>
         <div class="tab-content">
            <div class="tab-pane no-padding" id="quickview-notes">
               <div class="view-port clearfix quickview-notes">
                  <div class="view list" id="quick-note-list">
                     <ul>
                        <li class="tkt_drop">
                           <div class="dropdown">
                             <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <p class="one-of-two">
                                       <span class="text-warning fs-12">#31</span>
                                    </p>
                                    <p class="two-of-two p-l-10 fs-12">
                                       <span class="text-master">Webdysun </span>
                                       <span class="text-warning pull-right">20-07-2017</span>
                                       <br>
                                       <span class="text-masters">Site Name</span>
                                       <span class="text-warning pull-right">15:15</span>
                                       <br>
                                       <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                    </p>
                             </a>
                             <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#myModal" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#myCommentModal" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#myShareModal" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                           </div>
                        </li>
                        <li>
                           <div class="dropdown">
                             <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <p class="one-of-two">
                                       <span class="text-warning fs-12">#31</span>
                                    </p>
                                    <p class="two-of-two p-l-10 fs-12">
                                       <span class="text-master">Webdysun </span>
                                       <span class="text-warning pull-right">20-07-2017</span>
                                       <br>
                                       <span class="text-masters">Site Name</span>
                                       <span class="text-warning pull-right">15:15</span>
                                       <br>
                                       <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                    </p>
                             </a>
                             <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#address" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#comment" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#share" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                           </div>
                        </li>
                        <li>
                           <div class="dropdown">
                             <a class="dropdown-toggle" href="#" data-toggle="dropdown">
                                <p class="one-of-two">
                                       <span class="text-warning fs-12">#31</span>
                                    </p>
                                    <p class="two-of-two p-l-10 fs-12">
                                       <span class="text-master">Webdysun </span>
                                       <span class="text-warning pull-right">20-07-2017</span>
                                       <br>
                                       <span class="text-masters">Site Name</span>
                                       <span class="text-warning pull-right">15:15</span>
                                       <br>
                                       <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                    </p>
                             </a>
                             <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#address" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#comment" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#share" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                           </div>
                        </li>
                        <li data-toggle="modal" data-target="#myModal">
                           <p class="one-of-two">
                              <span class="text-warning fs-12">#31</span>
                           </p>
                           <p class="two-of-two p-l-10 fs-12">
                              <span class="text-master">Webdysun </span>
                              <span class="text-warning pull-right">20-07-2017</span>
                              <br>
                              <span class="text-masters">Site Name</span>
                              <span class="text-warning pull-right">15:15</span>
                              <br>
                              <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                           </p>
                           <ul class="dropdown-menu animated flipInX m-t-xs">
                                 <li><a href="#" data-toggle="modal" data-target="#address" data-backdrop="static" data-keyboard="false">Address</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#comment" data-backdrop="static" data-keyboard="false">Comment</a></li>
                                 <li><a href="#" data-toggle="modal" data-target="#share" data-backdrop="static" data-keyboard="false">Share</a></li>
                             </ul>
                        </li>
                     </ul>
                  </div>
                  <div class="view note" id="quick-note">
                     <div>
                        <ul class="toolbar">
                           <li><a href="#" class="close-note-link"><i class="pg-arrow_left"></i></a></li>
                           <li><a href="#" data-action="Bold" class="fs-12"><i class="fa fa-bold"></i></a></li>
                           <li><a href="#" data-action="Italic" class="fs-12"><i class="fa fa-italic"></i></a></li>
                           <li><a href="#" class="fs-12"><i class="fa fa-link"></i></a></li>
                        </ul>
                        <div class="body">
                           <div>
                              <div class="top">
                                 <span>21st april 2014 2:13am</span>
                              </div>
                              <div class="content">
                                 <div class="quick-note-editor full-width full-height js-input" contenteditable="true"></div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane no-padding" id="quickview-alerts">
               <div class="view-port clearfix" id="alerts">
                  <div class="view bg-white">
                     <div class="navbar navbar-default navbar-sm">
                        <div class="navbar-inner">
                           <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                           <i class="pg-more"></i>
                           </a>
                           <div class="view-heading">
                              Notications
                           </div>
                           <a href="#" class="inline action p-r-10 pull-right link text-master">
                           <i class="pg-search"></i>
                           </a>
                        </div>
                     </div>
                     <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              Calendar
                           </div>
                           <ul>
                              <li class="alert-list">
                                 <a href="javascript:;" class="align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="p-l-10 overflow-ellipsis fs-12">
                                       <span class="text-master">David Nester Birthday</span>
                                    </p>
                                    <p class="p-r-10 ml-auto fs-12 text-right">
                                       <span class="text-warning">Today <br></span>
                                       <span class="text-master">All Day</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="alert-list">
                                 <a href="#" class="align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-warning fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="p-l-10 overflow-ellipsis fs-12">
                                       <span class="text-master">Meeting at 2:30</span>
                                    </p>
                                    <p class="p-r-10 ml-auto fs-12 text-right">
                                       <span class="text-warning">Today</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              Social
                           </div>
                           <ul>
                              <li class="alert-list">
                                 <a href="javascript:;" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="col overflow-ellipsis fs-12 p-l-10">
                                       <span class="text-master link">Jame Smith commented on your status<br></span>
                                       <span class="text-master">“Perfection Simplified - Company Revox"</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="alert-list">
                                 <a href="javascript:;" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-complete fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="col overflow-ellipsis fs-12 p-l-10">
                                       <span class="text-master link">Jame Smith commented on your status<br></span>
                                       <span class="text-master">“Perfection Simplified - Company Revox"</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              Sever Status
                           </div>
                           <ul>
                              <li class="alert-list">
                                 <a href="#" class="p-t-10 p-b-10 align-items-center" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                                    <p class>
                                       <span class="text-danger fs-10"><i class="fa fa-circle"></i></span>
                                    </p>
                                    <p class="col overflow-ellipsis fs-12 p-l-10">
                                       <span class="text-master link">12:13AM GTM, 10230, ID:WR174s<br></span>
                                       <span class="text-master">Server Load Exceeted. Take action</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div class="tab-pane active no-padding" id="quickview-chat">
               <div class="view-port clearfix" id="chat">
                  <div class="view bg-white">
                     <div class="navbar navbar-default">
                        <div class="navbar-inner">
                           <a href="javascript:;" class="inline action p-l-10 link text-master" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                           <i class="pg-plus"></i>
                           </a>
                           <div class="view-heading">
                              Chat List
                              <div class="fs-11">Show All</div>
                           </div>
                           <a href="#" class="inline action p-r-10 pull-right link text-master">
                           <i class="pg-more"></i>
                           </a>
                        </div>
                     </div>
                     <div data-init-list-view="ioslist" class="list-view boreded no-top-border">
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">
                              a
                           </div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">ava flores</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">b</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">bella mccoy</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">bob stephens</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">c</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view"  href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">carole roberts</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">christopher perez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">d</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">danielle fletcher</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">david sutton</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">e</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8x.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">earl hamilton</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9x.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">elaine lawrence</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">ellen grant</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">erik taylor</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">everett wagner</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">f</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">freddie gomez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">g</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">glen jensen</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">gwendolyn walker</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">j</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">janet romero</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">k</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">kim martinez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">l</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">lawrence white</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">leroy bell</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">letitia carr</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">lucy castro</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">m</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">mae hayes</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marilyn owens</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marlene cole</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marsha warren</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8x.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">marsha dean</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">mia diaz</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">n</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">noah elliott</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">p</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">phyllis hamilton</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">r</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">raul rodriquez</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">rhonda barnett</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/5x.jpg" data-src="resources/img/profiles/5.jpg" src="resources/img/profiles/5x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">roberta king</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">s</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/6x.jpg" data-src="resources/img/profiles/6.jpg" src="resources/img/profiles/6x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">scott armstrong</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/7x.jpg" data-src="resources/img/profiles/7.jpg" src="resources/img/profiles/7x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">sebastian austin</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/8x.jpg" data-src="resources/img/profiles/8x.jpg" src="resources/img/profiles/8x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">sofia davis</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">t</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/9x.jpg" data-src="resources/img/profiles/9x.jpg" src="resources/img/profiles/9x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">terrance young</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/1x.jpg" data-src="resources/img/profiles/1.jpg" src="resources/img/profiles/1x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">theodore woods</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/2x.jpg" data-src="resources/img/profiles/2.jpg" src="resources/img/profiles/2x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">todd wood</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/3x.jpg" data-src="resources/img/profiles/3.jpg" src="resources/img/profiles/3x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">tommy jenkins</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                        <div class="list-view-group-container">
                           <div class="list-view-group-header text-uppercase">w</div>
                           <ul>
                              <li class="chat-user-list clearfix">
                                 <a data-view-animation="push-parrallax" data-view-port="#chat" data-navigate="view" class href="#">
                                    <span class="thumbnail-wrapper d32 circular bg-success">
                                    <img width="34" height="34" alt data-src-retina="resources/img/profiles/4x.jpg" data-src="resources/img/profiles/4.jpg" src="resources/img/profiles/4x.jpg" class="col-top">
                                    </span>
                                    <p class="p-l-10 ">
                                       <span class="text-master">wilma hicks</span>
                                       <span class="block text-master hint-text fs-12">Hello there</span>
                                    </p>
                                 </a>
                              </li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="view chat-view bg-white clearfix">
                     <div class="navbar navbar-default">
                        <div class="navbar-inner">
                           <a href="javascript:;" class="link text-master inline action p-l-10 p-r-10" data-navigate="view" data-view-port="#chat" data-view-animation="push-parrallax">
                           <i class="pg-arrow_left"></i>
                           </a>
                           <div class="view-heading">
                              John Smith
                              <div class="fs-11 hint-text">Online</div>
                           </div>
                           <a href="#" class="link text-master inline action p-r-10 pull-right ">
                           <i class="pg-more"></i>
                           </a>
                        </div>
                     </div>
                     <div class="chat-inner" id="my-conversation">
                        <div class="message clearfix">
                           <div class="chat-bubble from-me">
                              Hello there
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="profile-img-wrapper m-t-5 inline">
                              <img class="col-top" width="30" height="30" src="resources/img/profiles/avatar_small.jpg" alt data-src="resources/img/profiles/avatar_small.jpg" data-src-retina="resources/img/profiles/avatar_small2x.jpg">
                           </div>
                           <div class="chat-bubble from-them">
                              Hey
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="chat-bubble from-me">
                              Did you check out Pages framework ?
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="chat-bubble from-me">
                              Its an awesome chat
                           </div>
                        </div>
                        <div class="message clearfix">
                           <div class="profile-img-wrapper m-t-5 inline">
                              <img class="col-top" width="30" height="30" src="resources/img//avatar_small.jpg" alt data-src="resources/img//avatar_small.jpg" data-src-retina="resources/img/profiles/avatar_small2x.jpg">
                           </div>
                           <div class="chat-bubble from-them">
                              Yea
                           </div>
                        </div>
                     </div>
                     <div class="b-t b-grey bg-white clearfix p-l-10 p-r-10">
                        <div class="row">
                           <div class="col-1 p-t-15">
                              <a href="#" class="link text-master"><i class="fa fa-plus-circle"></i></a>
                           </div>
                           <div class="col-8 no-padding">
                              <input type="text" class="form-control chat-input" data-chat-input data-chat-conversation="#my-conversation" placeholder="Say something">
                           </div>
                           <div class="col-2 link text-master m-l-10 m-t-15 p-l-10 b-l b-grey col-top">
                              <a href="#" class="link text-master"><i class="pg-camera"></i></a>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div> -->
      
      
      <input type="hidden" value="${customerview.mapJsonData}" id="MapJsonId">
      <%-- <input type="hidden" value="${customerview.chartJsonData}" id="ChartJsonId"> --%>
      
      <!-- Side Bar Content End-->
   
   
      <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-10 ">
            <a class="builder-close quickview-toggle pg-close"  href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Total Sites</a></li>
                              <li><a href="#QuickLinkWrapper2">Energy</a></li>
                              <li><a href="#QuickLinkWrapper3">Co<sub>2</sub> Avoided</a></li>
                              <li><a href="#QuickLinkWrapper1">Map</a></li>
                              <li><a href="#QuickLinkWrapper1">Site Status</a></li>
                              <li><a href="#QuickLinkWrapper2">Tickets</a></li>
                              <li><a href="#QuickLinkWrapper3">Events</a></li>
                              <li><a href="#sitedetails">Site Details</a></li>
                             <!--  <li><a href="#overview">Overview Information</a></li> -->
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
  
         <script src="resources/js/pace.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.gotop.js"></script>
      <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
      <script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="resources/js/tether.min.js" type="text/javascript"></script>
      <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
      <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.actual.min.js"></script>
      <script src="resources/js/jquery.scrollbar.min.js"></script>
      <script type="text/javascript" src="resources/js/select2.full.min.js"></script>
      <script type="text/javascript" src="resources/js/classie.js"></script>
      <script src="resources/js/switchery.min.js" type="text/javascript"></script>
      <script src="resources/js/pages.min.js"></script>
      <script src="resources/js/card.js" type="text/javascript"></script>
      <script src="resources/js/scripts.js" type="text/javascript"></script>
      <script src="resources/js/demo.js" type="text/javascript"></script>
      <script src="resources/js/jquery.easing.min.js"></script>
      <script src="resources/js/jquery.fadethis.js"></script>

       
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      <script src="resources/js/calendar.min.js" type="text/javascript" ></script>
      <script src="resources/js/jquery.selectlistactions.js"></script>
  
  
    <div id="gotop"></div>
    
    
    
    
         <!-- Address Popup Start !-->
      <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Address Notification Bar</h4>
      </div>
      <div class="modal-body popup-hei">
            <div class="col-md-3 add-list">
               <ul>
                  <li class="recent">Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
               </ul>
            </div>
            <div class="col-md-9">
              <div class="data-cont">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Initial Comment</p>
                  <div class="row">
                     <div class="col-md-3"><p class="add-text">Recommeneded<br> Action</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Recommeneded Action" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Assign To</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Assign To" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Date</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui calendar width" id="example2">
                                 <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="Date" class="width">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            <div class="col-md-12 data-deta">
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Equipement Id</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Equipement Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Site Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Site Code</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Customer</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Status</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Event Description</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Date / Time</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">23-10-2017 / 1:00</p></div>
               </div>
            </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Save</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Address Popup End !-->  
<!-- Comment Popup Start !-->
      <!-- Modal -->
<div id="myCommentModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comment Bar</h4>
      </div>
      <div class="modal-body popup-hei">
            <div class="col-md-4">
               <div class="cmt-list">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Events</p>
                  <ul>
                  <li>
                     <p class="one-of-two">
                                    <span class="text-warning fs-12">#31</span>
                                 </p>
                                 <p class="two-of-two p-l-10 fs-12">
                                    <span class="text-master">Webdysun </span>
                                    <span class="text-warning pull-right">20-07-2017</span>
                                    <br>
                                    <span class="text-masters">Site Name</span>
                                    <span class="text-warning pull-right">15:15</span>
                                    <br>
                                    <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                 </p>
                  </li>
                 
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>

                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
               </ul>
               </div>
               
            </div>
            <div class="col-md-8">
              <div class="data-cont">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Events Comment</p>
                  <div class="conte">
                     <div class="row border">
                        <div class="col-lg-3 col-md-3 col-xs-3 center">
                           <i class="fa fa-user fa-4x"></i>
                        </div>
                        <div class="col-md-9 col-xs-9 col-lg-9">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                              <div class="cmtcontent">
                                 <p><strong>Initial Comment :</strong><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                              </div>
                        </div>
                     </div>
                     <div class="hr-line"></div>
                     <div class="row border">
                        <div class="col-md-11 col-xs-11 col-lg-11">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left ml">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right ml"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                           <div class="padd">
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           </div>
                        </div>
                        <div class="col-md-1 col-xs-1 col-lg-1 center">
                           <i class="fa fa-user fa-3x m-t-15"></i>
                        </div>
                     </div>
                     <div class="hr-line"></div>
                     <div class="row border">
                        <div class="col-lg-2 col-md-2 col-xs-2 center">
                           <i class="fa fa-user fa-3x m-t-15"></i>
                        </div>
                        <div class="col-lg-10 col-md-10 col-xs-10">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left ml">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right ml"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                           <div class="padd">
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           </div>
                        </div>
                     </div>
                  </div>
                     <div class="hr-line"></div>
                     <div class="row">
                        <div class="col-md-9 col-lg-9 col-xs-7">
                           <form class="ui form mt-15 m-l-20">
                              <div class="field">
                                 <input name="first-name" placeholder="Customer Name" type="text">
                              </div>
                           </form>
                        </div>
                        <div class="col-md-1 col-lg-1 col-xs-1 m-t-15">
                           <input type="button" class="btn btn-success" value="Send" name="">
                        </div>
                     </div>

               </div>
            </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Save</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Comment Popup End !--> 
<!-- Share Popup Start !-->
      <!-- Modal -->
<div id="myShareModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Share</h4>
      </div>
      <div class="modal-body popup-hei">
         <form class="ui form">
            <div class="field">
               <label class="fields">Subject</label>
               <input name="first-name" placeholder="Subject" type="text" class="txt-padd">
            </div>
         </form>
         <form class="ui form mt-15">
            <div class="field">
               <label class="fields">Additional Notes</label>
               <input name="first-name" placeholder="Additional Notes" type="text" class="txt-padd">
            </div>
         </form>
         <div class="row style-select">
         <div class="col-md-12 mt-15">
            <div class="subject-info-box-1">
               <!-- <label class="fields">Available Superheroes</label> -->
               <select multiple class="form-control" id="lstBox1">
                  <option value="123">Superman</option>
                  <option value="456">Batman</option>
                  <option value="789">Spiderman</option>
                  <option value="654">Captain America</option>
               </select>
            </div>

            <div class="subject-info-arrows text-center">
               <br /><br />
               <input type='button' id='btnAllRight' value="">>" class="btn btn-default" /><br />
               <input type='button' id='btnRight' value=">" class="btn btn-default" /><br />
               <input type='button' id='btnLeft' value="/<" class="btn btn-default" /><br />
               <input type='button' id='btnAllLeft' value="/<<" class="btn btn-default" />
            </div>

            <div class="subject-info-box-2">
               <!-- <label class="fields">Superheroes You Have Selected</label> -->
               <select multiple class="form-control" id="lstBox2">
                  <option value="765">Nick Fury</option>
                  <option value="698">The Hulk</option>
                  <option value="856">Iron Man</option>
               </select>
            </div>

            <div class="clearfix"></div>
         </div>
      </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Send</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>




<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        <form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form"> 
             
               <div class="col-md-12 field m-t-10">
                     <div class="col-md-4">
                      <label class="fields required m-t-10">Site</label>
                  </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 field">
                     <div class="col-md-4">
                      <label class="fields required m-t-10">Type</label>
                  </div>
                  <div class="col-md-8">
                     <form:select class="ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
               </div>
               <div class="col-md-12 field">
                     <div class="col-md-4">
                      <label class="fields required m-t-10">Category</label>
                  </div>
                  <div class="col-md-8">
                     <form:select class="ui fluid search selection dropdown width" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                    </form:select> 
                  </div>
               </div>
               <div class="col-md-12 field">
                  <div class="col-md-4"><label class="fields required m-t-10">Subject</label></div>
                  <div class="col-md-8">   <form:input  placeholder="Subject" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" /></div>
               </div>
              
               <div class="col-md-12 field">
                     <div class="col-md-4">
                      <label class="fields required m-t-10">Priority</label>
                  </div>
                  <div class="col-md-8">
                     <form:select class="ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
                  </div>
               </div>
               <div class="col-md-12 field">
                     <div class="col-md-4">
                      <label class="fields required m-t-10">Description</label>
                  </div>
                  <div class="col-md-8">
                    <textarea id="txtTktdescription" name="description" style="resize: none;" placeholder="Ticket Description"></textarea>
                  </div>
               </div>
               <div class="col-md-12 col-xs-12  m-t-15 center">
				       <div class="btn btn-success  submit">Create</div>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
  <!-- <div class="ui blue submit button">Submit</div>
  <div class="ui  clear button">Clear</div> -->
  <!-- <div class="ui error message"></div> -->
</form:form>
	  		</div>
	  </div>
    </div>
<!-- Address Popup End !--> 




      <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      <script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>
      
      
      
      
     <script type='text/javascript'>
         function initialize() {

        
        	 var active = "resources/img/active.png";
             var warning = "resources/img/warning.png";
             var down = "resources/img/down.png";
             var offline = "resources/img/offline.png";

            var a = $('#MapJsonId').val();
            a = eval('(' + a + ')');
			
            // locations=[];

            var locations = a;
            
            //var locations= [["<b>India Food Park - KART1003</b><p>null</p><p>null - null.</p>",13.4924793,77.0411002,"India Food Park","resources/img/active.png",99996,"/eampm-maven/siteview7&ref=7"],["<b>Kensington A - MHRT1009</b><p>null</p><p>null - null.</p>",19.111976,72.911129,"Kensington A","resources/img/offline.png",99997,"/eampm-maven/siteview5&ref=7"]]
            // var locations = [

            //     ['SITE 1', 13.3524101, 74.7906418, 'NEW DATA SET',Down,9999],
            //     // ['SITE 2',13.3694539,74.7367716,'Panel 01',Active],
            //     // ['<b>TATA Motors Pvt Ltd</b>' + '\n' + '<p>WD0086BF</p>' + '\n' + '<p>KG Halli, AshokeNagar,</p>'+ '\n' + '<p>Bengaluru - 560001</p>'+ '\n' + 'Karnataka, India', 29.1732234, 76.9003276, 'Site 03',Warning],
            //     // ['SITE 4',8.4548082,72.81203529999993,'Site 04',Off],
            //     // ['SITE 5',19.2147067,72.91062020000004,'Site 05',Down],
            //     // ['SITE 5',11.6680848,78.1321082,'Site 06',Active],
            // ];
            //        
            
            
            
            var myJsonString = JSON.stringify(locations);
            console.log(myJsonString);
            window.map = new google.maps.Map(document.getElementById('dvMap'), {
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });

            var infowindow = new google.maps.InfoWindow();

            var bounds = new google.maps.LatLngBounds();

            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    map: map,
                    Icon: locations[i][4],
                    title: locations[i][3],
                    zIndex: locations[i][5],
                });

                bounds.extend(marker.position);

                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                    }
                })(marker, i));
            }

            map.fitBounds(bounds);

            var listener = google.maps.event.addListener(map, "idle", function() {
                fnZoom();
                google.maps.event.removeListener(listener);
            });

            function fnZoom() {
                // alert('Work');
                if (locations.length == 1) {
                    map.setZoom(15);
                }
            }
        }

        function loadScript() {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDEbIbuKRR_b0iHzErE79mYJsqkh3LIb_g&sensor=false&' + 'callback=initialize';
            document.body.appendChild(script);
        }

        window.onload = loadScript;
        
    </script>
      
      
      
      
      
      
      
        <script type="text/javascript">
        $(document).ready(function(){
      
          var Data = $('#ChartJsonId').val();
          Data = eval('(' + Data + ')');
        
          
            var chart = AmCharts.makeChart( "chartdiv", {
              "type": "serial",
              "theme": "light",
              // "marginRight": 40,
              "marginLeft": 40,
              "autoMarginOffset": 20,
              "dataDateFormat": "YYYY-MM-DD",
              "valueAxes": [ {
                "id": "v1",
                "axisAlpha": 0,
                "position": "left",
                "ignoreAxisWidth": true
              } ],
              "balloon": {
                "borderThickness": 1,
                "shadowAlpha": 0
              },
              "graphs": [ {
                "id": "g1",
                "balloon": {
                  "drop": true,
                  "adjustBorderColor": false,
                  "color": "#ffffff",
                  "type": "smoothedLine"
                },
                "fillAlphas": 0.2,
                "bullet": "round",
                "bulletBorderAlpha": 1,
                "bulletColor": "#FFFFFF",
                "bulletSize": 5,
                "hideBulletsCount": 50,
                "lineThickness": 2,
                "title": "red line",
                "useLineColorForBulletBorder": true,
                "valueField": "value",
                "balloonText": "<span style='font-size:18px;'>[[value]]</span>"
              } ],
              "chartCursor": {
                "valueLineEnabled": true,
                "valueLineBalloonEnabled": true,
                "cursorAlpha": 0,
                "zoomable": false,
                "valueZoomable": true,
                "valueLineAlpha": 0.5
              },
              "valueScrollbar": {
                "autoGridCount": true,
                "color": "#000000",
                "scrollbarHeight": 50
              },
              "categoryField": "date",
              "categoryAxis": {
                "parseDates": true,
                "dashLength": 1,
                "minorGridEnabled": true
              },
              "export": {
                "enabled": true
              },
              "dataProvider": Data
            } );
        });
        
        
  
</script>
<script>
        $('#btnRight').click(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });
        $('#lstBox1').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnAllRight').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnLeft').click(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });

         $('#lstBox2').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
        $('#btnAllLeft').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
    </script>
  </body>
</html>

