<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>

<html>
<head>
	<title></title>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
      <meta name="robots" content="noindex, nofollow">
      <meta name="googlebot" content="noindex, nofollow">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="resources/css/pages.css" type="text/css" rel="stylesheet">
      <link href="resources/css/styles.css" type="text/css" rel="stylesheet">
	  <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      
      <style>
      
.bg {
	height: 100%;
	weight: 100%;
	/*background: #5cb85c;*/
	/*opacity: 0.8;*/
	background: url(resources/img/bg_02.jpg) no-repeat;
	/*opacity: 0.5;*/
	overflow: hidden;
	background-size: cover;
	background-position: center;
}

/* .wrapper {
	background: #FFF;
	height: auto;
	width: 80%;
	margin-bottom: 80px;
	margin-top: 240px;
	/*  opacity: 0.8; */
} */

.lbl-color {
	color: #000 !important;
}

.invalid {
	color: #DB2828;
	font-size: 12px;
	text-align: center;
	font-weight: bold;
}

.ui.segment {
	position: relative;
	background: #FFFFFF;
	opacity: 0.8 !important;
	box-shadow: 0px 1px 2px 0 rgba(34, 36, 38, 0.15);
	margin: 1rem 0em;
	padding: 1em 1em;
	border-radius: 0.28571429rem;
	border: 1px solid rgba(34, 36, 38, 0.15);
}

</style>
<script type="text/javascript">
	$(document).ready(function() {
		/* alert($(window).height());
		alert($(window).width()); */
		
		var marginTop = Math.round($(window).height() * 0.35);
		var marginBottom = Math.round($(window).height() * 0.35);
		
		$('.wrapper').css("margin-top",marginTop);
		$('.wrapper').css("margin-bottom",marginBottom);
	
	});
</script>
      
      <script type="text/javascript">
							$(document).ready(
									
									function() {
										if ($(window).width() < 767) {
											$('.ftexts').removeClass("footer");
											$('.cname').removeClass(
													"pull-right").addClass(
													"center");
										}

										$('.reset').click(function() {
											$('.invalid').hide();
										});

										$('#password').keypress(function() {
											$('.invalid').hide();
										})
										$('#username').keypress(function() {
											$('.invalid').hide();
										});

									});
						</script>
           
       <script type='text/javascript'>
         $(function() {
             $(".ui.dropdown").dropdown();
             $(".ui.checkbox").checkbox("uncheck");
             $('.ui.form')
                 .form({
                     inline: true,
                     on: "blur",
                     fields: {
                        
                         username: {
                             identifier: 'username',
                             rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter  username'
                             }]
                         },
                         password: {
                             identifier: 'password',
                             rules: [{
                                 type: 'empty',
                                 prompt: 'Please enter  password'
                             }]
                         }
                     }
                 });
             ('form').form('reset')
         });
      </script>
      
      
      
      
      
</head>

<body class="bg">

	<div class="row">
	
	
	
		<div class="col-md-12 col-lg-12 col-sm-12">
			
			
			<div class="col-sm-12 col-md-4  col-lg-4 col-xl-4  "></div>
			
			<div class="col-sm-12 col-md-4 col-lg-4  col-xl-12" style="">
					<div class="wrapper">
						<c:url var="loginValidation" value="/loginauthendication" ></c:url>                  
        				<form:form action="${loginValidation}" modelAttribute="login" class="ui form">   
            			<div class="ui form segment">
      						<div class="padd-top-20 required field ">
               					<label class="lbl-color">Username</label>
               					<form:input placeholder="Username" id="username" name="username" type="text" path="userName" />
            				</div>
            				<div class="required field">
					            <label class="lbl-color">Password</label>
					            <form:input placeholder="Password" id="password" name="password" type="password"  path="password" />
					        </div>
					        <p class="invalid"><c:out value="${sessionScope.eampmerrormsg}"></c:out></p>					        
					        <div class="row center">
					         	<div class="col-md-12 center">
					         	
					         	  <input class="btn btn-success ui submit" type="submit" value="Login" />
					         	  <input class="btn btn-primary ui reset" type="button" value="Reset" />
					         		
					         	</div>
					         </div>
      					 </div>
      					</form:form>
				</div>
			</div>
			
			<div class="col-md-4  col-lg-4 col-sm-12 col-xs-12"></div>
			
			
			
		</div>
		
		
		
		
	
    
    
    
	</div>
	
	<div class="row">
			
		 <div class="ftexts col-md-12 col-lg-12 col-xs-12 footer">
      <div class="col-md-6 col-lg-6 col-xs-12">
        <p class="ftext"><span class="copyrits">Copyrights &copy; 2017.</span><strong>Inspire Clean Energy.</strong> All Rights Reserved</p>
      </div>
      <div class="col-md-6 col-lg-6 col-xs-12 ">
        <p class="ftext cname pull-right">Powered By<a href='https://www.mestechservices.com/'><img src="resources/img/Comp_logo.png"></a></p>
      </div>
    </div>
    
	</div>
	
	
	<%-- <div class="row">
	
	
	
		<div class="col-md-12 col-lg-12 col-xs-12 col-sm-12">
			<div class="col-md-5"></div>
				<div class="col-md-3 col-lg-3 col-sm-12 col-xs-12">
					<div class="wrapper">
						<c:url var="loginValidation" value="/loginauthendication" ></c:url>                  
        				<form:form action="${loginValidation}" modelAttribute="login" class="ui form">   
            			<div class="ui form segment">
      						<div class="padd-top-20 required field ">
               					<label class="lbl-color">Username</label>
               					<form:input placeholder="Username" id="username" name="username" type="text" path="userName" />
            				</div>
            				<div class="required field">
					            <label class="lbl-color">Password</label>
					            <form:input placeholder="Password" id="password" name="password" type="password"  path="password" />
					        </div>
					        <p class="invalid"><c:out value="${sessionScope.eampmerrormsg}"></c:out></p>					        
					        <div class="row center">
					         	<div class="col-md-12 center">
					         	
					         	  <input class="btn btn-success ui submit" type="submit" value="Login" />
					         	  <input class="btn btn-primary ui reset" type="button" value="Reset" />
					         		
					         	</div>
					         </div>
      					 </div>
      					</form:form>
				</div>
			</div>
			<div class="col-md-3"></div>
		</div>
		
		
		
		
		
		 <div class="ftexts col-md-12 col-lg-12 col-xs-12 footer">
      <div class="col-md-6 col-lg-6 col-xs-12">
        <p class="ftext"><span class="copyrits">Copyrights &copy; 2017.</span><strong>Inspire Clean Energy.</strong> All Rights Reserved</p>
      </div>
      <div class="col-md-6 col-lg-6 col-xs-12 ">
        <p class="ftext cname pull-right">Powered By<a href='https://www.mestechservices.com/'><img src="resources/img/Comp_logo.png"></a></p>
      </div>
    </div>
    
    
    
    
	</div> --%>
</body>
</html>