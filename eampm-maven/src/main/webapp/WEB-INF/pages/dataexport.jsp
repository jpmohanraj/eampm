<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To eAMPM ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
      <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

      
      <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >
      <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <link rel="stylesheet" type="text/css" href="resources/css/jquery-ui.css">
      <link href="resources/css/formValidation.min.css" rel="stylesheet" type="text/css">
     <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css">
      
     
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script type="text/javascript" src="resources/js/semantic.min.js"></script>
      
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
      <script type="text/javascript" src="resources/js/jquery-ui.js"></script>
      <script type="text/javascript" src="resources/js/formValidation.min.js"></script>
      <script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>
     
        <script type="text/javascript">
      $(document).ready(function(){
    	 
    	  
    	  if($(window).width() < 767)
			{
			   $('.card').removeClass("slide-left");
			   $('.card').removeClass("slide-right");
			   $('.card').removeClass("slide-top");
			   $('.card').removeClass("slide-bottom");
			}
    	 /*  $('.dt-buttons').css("border","10px solid #ccc"); */
    	  
    	  $('.close').click(function(){
				$('.clear').click();
			});
			$('.clear').click(function(){
			      	$('.search').val("");
			      });
			
			$('body').click(function(){
			          $('#builder').removeClass('open'); 
			        });


    	  $('.clears').click(function(){
    		  $("#txtFromDate").val('');
    	      $("#txtToDate").val('');
    	      $(".text").empty();
    	      $(".text").addClass('default')
    	      $(".text").html("Select");
    	      $('#ddlSiteId option:selected').removeAttr('selected');
            	 $('#ddlEquipmentId option:selected').removeAttr('selected');
            	/*  window.location.href = './dataexport'; */
    	  });
    	 

      });
    </script>
  <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown();
         
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 



             $('.validation-form')
   .form({
     on: 'blur',
     onFailure: function(formErrors, fields) {
       $.each(fields, function(e) {
         var ele = $('#' + e);
         var fail = (ele.val() === false || ele.val().length === 0);
         if (fail) {
           console.log(ele, fail);
           ele.focus();
           $('html,body').animate({
             scrollTop: ele.offset().top - ($(window).height() - ele.outerHeight(true)) / 2
           }, 200);
           return false;
         }
       });
       return false;
     },
     fields: {
    	 txtSubject: {
         identifier: 'txtSubject',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
        ddlSite: {
         identifier: 'ddlSite',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
      ddlType: {
            identifier: 'ddlType',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
      ddlCategory: {
         identifier: 'ddlCategory',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
       ddlPriority: {
         identifier: 'ddlPriority',
         rules: [{
           type: 'empty',
           prompt: 'Please select a dropdown value'
         }]
       },
       description: {
         identifier: 'description',
         rules: [{
           type: 'empty',
           prompt: 'Please Enter Description'
         }]
       },
       
     }
   }); ('form').reset('clear');

         })
         
      </script>
      
         
           <style type="text/css">
           .dropdown-menu{
           	      left: -80px !important;
    margin-left: 0px !important;
           }
      .dt-button-collection {
          height: 150px;
    overflow: scroll;
          
        }  
        .ui.form textarea:not([rows]) {
		    height: 4em;
		    min-height: 4em;
		    max-height: 24em;
		}
        .has-error .input-group-addon {
		    color: #a94442;
		    background-color: #eee !important;
		    border-color: transparent;
		}


.dt-button-background {
       position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0 !important; 
}
.tbh-width {
width:150px;
	padding-right: 30px !important;
}.dt-buttons {
	margin-left:50px;
	
} 

.buttons-collection {
       height:28px;
           padding: 3px;
      /*  z-index:9999; */
}
.input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
.buttons-excel {
  background: rgba(36, 95, 68, 0.87);
  color: #FFF;
}
.buttons-pdf {
  background: rgb(98, 165, 96);
  color: #FFF;
}
.dataTables_filter {
  float: right;
}
.dataTables_length {
  float: left;
}
.larginleft {
	margin-left:150px;
}
    </style>
   
      
      <script type="text/javascript">
         $(document).ready(function() {
        	
        	 var today = new Date();
        	 var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
        	 var time = today.getHours() + "-" + today.getMinutes() + "-" + today.getSeconds();
        	 var dateTime = date+' '+time;
              $("#searchSelect").change(function() {
                             var value = $('#searchSelect option:selected').val();
                             var uid =  $('#hdneampmuserid').val();
                           redirectbysearch(value,uid);
                        });
			var table = $('#example').DataTable( {
            	 
          
           lengthChange: true,
           "scrollX" :true,
           /* buttons: ['excel', 'pdf', 'colvis' ] */
           
             buttons: [
                /*  {
                     extend: 'print',
                     exportOptions: {
                         columns: ':visible'
                     },
                 }, */
                 {
                     extend: 'excel',
                     text:      '<i class="fa fa-file-excel-o excel"></i><i class="fa fa-download ml-15" aria-hidden="true"></i>',
                     filename: 'Equipment Details'+dateTime,
                     titleAttr: 'Excel',
                     exportOptions: {
                         columns: ':visible'
                     },
                 },
                 

                 'colvis'
             ],

       } );
    
       table.buttons().container()
           .appendTo( '#example_wrapper .col-sm-6:eq(0)' );
       
       $('.dataTables_filter input[type="search"]').attr('placeholder','search');
         
   } );
        
      </script>
  
      <script type="text/javascript">
$(document).ready(function() {
$("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
$("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"});
    $('#profileForm')
        .bootstrapValidator({
            framework: 'bootstrap',
            fields: {
                fromDate: {
                    validators: {
                        notEmpty: {
                            message: 'Select From date'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            message: 'The is not valid date'
                        }
                    }
                },
                 toDate: {
                    validators: {
                        notEmpty: {
                            message: 'Select To Date'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            message: 'The is not valid date'
                        }
                    }
                }
            }
        })
        .find('[name="fromDate"]')
            .datepicker({
                onSelect: function(date, inst) {
                    $('#profileForm').formValidation('revalidateField', 'fromDate');
                }
            });


 $('#profileForm').bootstrapValidator({
            framework: 'bootstrap',
        })
            .find('[name="toDate"]')
            .datepicker({
                onSelect: function(date, inst) {
                    $('#profileForm').formValidation('revalidateField', 'toDate');
                }
            });


  });

</script>
 <script type='text/javascript'>//<![CDATA[
              $(window).load(function(){
              $('.ui.dropdown').dropdown();
              });
              </script>
                    
      
     </head>
  <body  class="fixed-header">
    
    <input type="hidden" value="${access.userID}" id="hdneampmuserid">
    
    
           <nav class="page-sidebar" data-pages="sidebar">
        
         <div class="sidebar-header">
            <a href="./dashboard"><img src="resources/img/logo01.png" class="m-t-10" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management"></a>
            <i class="fa fa-bars bars"></i>
         </div>
         
         <div class="sidebar-menu">
         
          
          <c:if test="${not empty access}">
          
            <ul class="menu-items">
            
            
            
           <c:if test="${access.dashboard == 'visible'}">
          
                     <li class="" style="margin-top:2px;">
                  <a href="./dashboard" class="detailed">
                  <span class="title">Dashboard</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.overview == 'visible'}">
                     <li>
                  <a href="#"><span class="title">Overview</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-list-alt"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.systemMonitoring == 'visible'}">
                    <li>
                  <a href="#"><span class="title">System Monitoring</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
                        <c:if test="${access.visualization == 'visible'}">
           <li  class="active">
                  <a href="javascript:;"><span class="title">Visualization</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
                                  <li>
                          <a href="./customerlist">Customer View</a>
                          <span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
                          </li>
                    </c:if>
           
           
           
                     <li  class="active">
                        <a href="./sitelist">Site View</a>
                        <span class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
                     </li>
                     <li>
                        <a href="./equipmentlist">Equipment View</a>
                        <span class="icon-thumbnail"><i class="fa fa-cog"></i></span>
                     </li>
                  </ul>
               </li>
              
           </c:if>

            <c:if test="${access.analytics == 'visible'}">
           <li>
                  <a href="#">
                  <span class="title">Analytics</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.portfolioManagement == 'visible'}">
          <li>
                  <a href="#">
                  <span class="title">Portfolio Manager</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
               </li>
           </c:if>
           
             <c:if test="${access.ticketing == 'visible'}">
            <li>
                    <a href="javascript:;"><span class="title">Operation & Maintenance</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-cogs"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./ticketdetails">Ticket View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-ticket"></i></span>
                     	</li>
                    </c:if>
           
                  </ul>
               </li>
           
           
           
           
           
           </c:if>
           
           
            <c:if test="${access.forcasting == 'visible'}">
          <li>
                  <a href="#"><span class="title">Forecasting</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                     <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li>
                     
                      <li>
                        <a href="#">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li>
                     
                      <li>
                        <a href="#">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>
        
        
              
              
               
              
               
              
             
          
            </ul>
         
         
          </c:if>
          
          
         
            <div class="clearfix"></div>
         </div>
    
    
    
      </nav>
  
    
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
                   <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"></a>	
               </div>
            </div>
            <div class="d-flex align-items-center">
             <div class="form-group required field searchbx" id="SearcSElect">
                           <select class="ui fluid search selection dropdown width oveallSearch" id="searchSelect">
                             <!--  <option value="-2"></option> -->
                              <option value="-1">search </option>
                              <option value="0">Dashboard</option>
                                
                        <c:if test="${not empty access}">
                              <c:if test="${access.customerListView == 'visible'}">
                                 <option value="1">Customer View</option>
                              </c:if>
                           </c:if>   
                             
                              <option value="3">Site View</option>
                              <option value="5">Equipment View</option>
                              
                               <c:if test="${access.customerListView == 'visible'}">
                              <option value="6">Ticket View</option>
                           </c:if>
           
           
           
                           </select>
                        </div>
               <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                  <!-- <p class="user-login">ICE</p>
                   -->
                  
                   <c:if test="${not empty access}">         
                           <p class="user-login">${access.userName}</p>
                  </c:if>
                  
                  
                  
                  </button>
                  <div class="dropdown-menu dropdown-menu-right profile-dropdown exportlog" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
               <c:if test="${access.customerListView == 'visible'}">
          				<div>
          	  <span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><p class="center m-t-5 tkts"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
          				<!-- <p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p> -->
                 <!--  <p class="center mb-1 tkts"><img src="resources/img/tkt.png"></p>
                  <p class="create-tkts">New Ticket</p> -->
               </div>
                    </c:if>
              <!--  <span ><p class="center m-t-5 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span> -->
               <!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a> -->
               <!-- <a href="#" class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
            </div>
         </div>
         <div class="page-content-wrapper ">
            <div class="content ">
               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item hover-idle"><a>Visualization</a></li>
                     
                     			<c:if test="${not empty access}">
          							<c:choose>
    									<c:when test="${access.customerListView == 'visible'}">
       									 	<li class="breadcrumb-item"><a href="./customerview${access.myCustomerID}&ref=${access.userID}">${access.myCustomerName}</a></li>
                     					</c:when>    
    									<c:otherwise>
       									 	<li class="breadcrumb-item"><a href="./dashboard">${access.myCustomerName}</a></li>
                     					</c:otherwise>
								 	</c:choose>

                 				</c:if> 
                 				
                     <li class="breadcrumb-item"><a href="./siteview${access.mySiteID}&ref=${access.userID}">${access.mySiteName}</a></li>
                     <li class="breadcrumb-item active">Download Equipment Details</li>
                  </ol>
               </div>
           
           
           
             <div id="QuickLinkWrpper1" class="" >
                           <div  class="card card-transparent mb-0">
                                                 <!-- Table Start-->
                     
                     <div class="card-block sortable mb-80" id="equipmentdata">
                        <div class="row">
                             
                           <div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
                           
                              <div class="card card-default bg-default" data-pages="card">
                              <div class="card-header">
                                    <div class="card-title tbl-head ml-15">Equipment Details</div>
                                 </div>
                        
                         <c:url var="viewAction" value="/dataexport${access.mySiteID}&ref=${access.userID}" ></c:url>                  
                 
    			<form:form action="${viewAction}" modelAttribute="dataexport" class="ui form form-horizontal" id="profileForm" method="post">   
                        
                               <div class="col-md-12 mt-30">
                               
                              
											    	<div class="col-lg-3">
                                              <div class="form-group padd-0  field">
                                                
 												<label  class="fields">Site</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width dataSite" name="siteid" id="ddlSiteId"  path="siteID">
   													<form:option value="" label="--- Select ---"/>
   													<form:options  items="${site1List}" />
												</form:select>
												
                                             </div>                                             
                             </div>
                             <div class="col-lg-3 mm-20 ">
                                              <div class="form-group padd-0  field">
                                               <label  class="fields">Equipment</label> 
                                                
                                                <form:select class="ui fluid search selection dropdown width dataSite" name="equipmentid" id="ddlEquipmentId" path="equipmentID">
   													<form:option site="-2" value="" label="--- Select ---"/>
   													<form:option site="-2" value="0" label="All"/>
   													<form:options site="7" items="${equipmentList}" />
												</form:select>
												
                                             
                                             
                                             
                                             </div>
                                          </div>
											<div class="col-lg-3 mm-20">
													<div class="txtwidth">
														<label class="fields ml-2">From Date <span class="errormess">*</span></label>
														<div class="controls">
															<div class="input-group">
																<form:input id="txtFromDate" type="text"
																	placeholder="From Date"
																	class="date-picker form-control" name="fromDate"
																	path="fromDate" required="required"/>
																<label for="txtFromDate" class="input-group-addon btn"><span
																	class="glyphicon glyphicon-calendar"></span> </label>
															</div>
														</div>
													</div>
												</div>


												<div class="col-lg-3 mm-20 ">
													<div class="required  txtwidth">
														<label class="fields ml-2">To Date <span class="errormess">*</span></label>
														<div class="controls">
															<div class="input-group">
																<form:input id="txtToDate" type="text"
																	placeholder="To Date" class="date-picker form-control"
																	name="toDate" path="toDate" required="required" />
																<label for="txtToDate" class="input-group-addon btn"><span
																	class="glyphicon glyphicon-calendar"></span> </label>
															</div>
														</div>
													</div>
												</div> 
												




											
											</div>
                                 
                                  <div class="col-md-12 col-lg-12 center">
                                  	 <!-- <button type="submit" id="profileForm" class="btn btn-success btn-width submit m-t-5"><b>View</b></button>
										<button  type="button"	class="btn btn-primary btn-width m-t-5 clr"><b>Clear</b></button> -->
                                          <input type="submit" class="btn btn-success btn-width m-t-20 profileForm" value="View">
                                          <input type="button" class="btn btn-primary btn-width clears  m-t-20 profileForm" value="Clear">
                                      
                                  </div>
                            
                        	</form:form>
     				       
     				       
     				       
                                 <div class="card-block table-responsive">
                                    <table id="example" class="table table-striped table-bordered table-fixed" style="width:100% !important;">
								        <thead class='bg-color'>
								            <tr>
								            <th>Equipment Serial No</th>
                                             <th>Time stamp</th>
                                             <th>Input Current-01 (A)</th>
                                             <th>Input Current-02 (A)</th>
                                             <th>Input Current-03 (A)</th>
                                             <th>Input Current-04 (A)</th>
                                             <th>Input Current-05 (A)</th>
                                             <th>Input Current-06 (A)</th>
                                             <th>Input Current-07 (A)</th>
                                             <th>Input Current-08 (A)</th>
                                             <th>Input Current-09 (A)</th>
                                             <th>Input Current-10 (A)</th>
                                             <th>Input Voltage-01 (V)</th>
                                             <th>Input Voltage-02 (V)</th>
                                             <th>Input Voltage-03 (V)</th>
                                             <th>Input Voltage-04 (V)</th>
                                             <th>Input Voltage-05 (V)</th>
                                             <th>Input Voltage-06 (V)</th>
                                             <th>Input Voltage-07 (V)</th>
                                             <th>Input Voltage-08 (V)</th>
                                             <th>Input Voltage-09 (V)</th>
                                             <th>Input Voltage-10 (V)</th>
                                             <th>Input Power-01 (W)</th>
                                             <th>Input Power-02 (W)</th>
                                             <th>Input Power-03 (W)</th>
                                             <th>Input Power-04 (W)</th>
                                             <th>Input Power-05 (W)</th>
                                             <th>Input Power-06 (W)</th>
                                             <th>Input Power-07 (W)</th>
                                             <th>Input Power-08 (W)</th>
                                             <th>Input Power-09 (W)</th>
                                             <th>Input Power-10 (W)</th>
                                             <th>Phase Current (A)</th>
                                             <th>Phase Current-L1 (A)</th>
                                             <th>Phase Current-L2 (A)</th>
                                             <th>Phase Current-L3 (A)</th>
                                             <th>Phase Voltage (V)</th>
                                             <th>Phase Voltage-L1 (V)</th>
                                             <th>Phase Voltage-L2 (V)</th>
                                             <th>Phase Voltage-L3 (V)</th>
                                             <th>Apparent Power (VA)</th>
                                             <th>Active Power (W)</th>
                                             <th>Re-active Power (VAr)</th>
                                             <th>Power Factor</th>
                                             <th>Total Energy (kWh)</th>
                                             <th>Today Energy (kWh)</th>
                                             <th>Isolation Resistance (Ohm)</th>
                                             <th>Output Frequency (Hz)</th>
                                             <th>Ambient Temperature (Â°C)</th>
                                             <th>Module Temperature (Â°C)</th>
                                             <th>Inverter Temperature (Â°C)</th>
                                             <th>Wind Speed (m/s)</th>
                                             <th>Rain Fall</th>
                                             <th>Total Hours On (h)</th>
                                             <th>Today Hours On (h)</th>
                                             <th>Phase Power Balancer</th>
                                             <th>Differential Current (mA)</th>
                                             <th>Status</th>
                                             <th>Error Code</th>
								           </tr>
                                       </thead>								      
								        <tbody> 
								         <c:forEach items="${datatransaction}" var="datalsit">
                                           <tr>                                                             
                                             <td>${datalsit.equipmentID}</td>
                                             <td>${datalsit.timestamp}</td>
                                             <td>${datalsit.inputCurrent_01}</td>
                                             <td>${datalsit.inputCurrent_02}</td>
                                             <td>${datalsit.inputCurrent_03}</td>
                                             <td>${datalsit.inputCurrent_04}</td>
                                             <td>${datalsit.inputCurrent_05}</td>
                                             <td>${datalsit.inputCurrent_06}</td>
                                             <td>${datalsit.inputCurrent_07}</td>
                                             <td>${datalsit.inputCurrent_08}</td>
                                             <td>${datalsit.inputCurrent_09}</td>
                                             <td>${datalsit.inputCurrent_10}</td>
                                             <td>${datalsit.inputVolatge_01}</td>
                                             <td>${datalsit.inputVolatge_02}</td>
                                             <td>${datalsit.inputVolatge_03}</td>
                                             <td>${datalsit.inputVolatge_04}</td>
                                             <td>${datalsit.inputVolatge_05}</td>
                                             <td>${datalsit.inputVolatge_06}</td>
                                             <td>${datalsit.inputVolatge_07}</td>
                                             <td>${datalsit.inputVolatge_08}</td>
                                             <td>${datalsit.inputVolatge_09}</td>
                                             <td>${datalsit.inputVolatge_10}</td>
                                             <td>${datalsit.inputPower_01}</td>
                                             <td>${datalsit.inputPower_02}</td>
                                             <td>${datalsit.inputPower_03}</td>
                                             <td>${datalsit.inputPower_04}</td>
                                             <td>${datalsit.inputPower_05}</td>
                                             <td>${datalsit.inputPower_06}</td>
                                             <td>${datalsit.inputPower_07}</td>
                                             <td>${datalsit.inputPower_08}</td>
                                             <td>${datalsit.inputPower_09}</td>
                                             <td>${datalsit.inputPower_10}</td>
                                             <td>${datalsit.phaseCurrent}</td>
                                             <td>${datalsit.phaseCurrent_L1}</td>
                                             <td>${datalsit.phaseCurrent_L2}</td>
                                             <td>${datalsit.phaseCurrent_L3}</td>
                                             <td>${datalsit.phaseVolatge}</td>
                                             <td>${datalsit.phaseVolatge_L1}</td>
                                             <td>${datalsit.phaseVolatge_L2}</td>
                                             <td>${datalsit.phaseVolatge_L3}</td>
                                             <td>${datalsit.apparentPower}</td>
                                             <td>${datalsit.activePower}</td>
                                             <td>${datalsit.reactivePower}</td>
                                             <td>${datalsit.powerFactor}</td>
                                             <td>${datalsit.totalEnergy}</td>
                                             <td>${datalsit.todayEnergy}</td>
                                             <td>${datalsit.isolationResistance}</td>
                                             <td>${datalsit.outputFrequency}</td>
                                             <td>${datalsit.ambientTemperature}</td>
                                             <td>${datalsit.moduleTemperature}</td>
                                             <td>${datalsit.inverterTemperature}</td>
                                             <td>${datalsit.windSpeed}</td>
                                             <td>${datalsit.rainfall}</td>
                                             <td>${datalsit.totalHoursOn}</td>
                                             <td>${datalsit.todayHoursOn}</td>
                                             <td>${datalsit.phasePowerBalancer}</td>
                                             <td>${datalsit.differentialCurrent}</td>
                                             <td>${datalsit.status}</td>
                                             <td>${datalsit.errorCode}</td>                                             
                                         </tr>                                                             
                                     </c:forEach>
                                    </tbody>
								    </table>
                                 </div>
                               </div>
                           </div>
                         </div>
                     </div>
                  
                                                          
                     <!-- Table End-->
                     </div>
                  </div>
         		</div>
         	 <div class="container-fixed-lg footer mb-0">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span>INSPIRE CLEAN ENERGY</span>.
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-rhs sm-pull-reset">
                     <span class="hint-text">Powered by</span>
                     <span>MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
         </div>
      </div>

      <!-- Side Bar Content Start-->

      
   
     <div class="quickview-wrapper  builder hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-10 ">
            <a class="builder-close quickview-toggle pg-close" ></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrpper1">Equipment Details</a></li>
                                                         
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
   
   

   
   
   
      <script src="resources/js/pace.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.gotop.js"></script>
      <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
      <script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="resources/js/tether.min.js" type="text/javascript"></script>
      <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
      <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.actual.min.js"></script>
      <script src="resources/js/jquery.scrollbar.min.js"></script>
      <script type="text/javascript" src="resources/js/select2.full.min.js"></script>
      <script type="text/javascript" src="resources/js/classie.js"></script>
      <script src="resources/js/switchery.min.js" type="text/javascript"></script>
      <script src="resources/js/pages.min.js"></script>
      <script src="resources/js/card.js" type="text/javascript"></script>
      <script src="resources/js/scripts.js" type="text/javascript"></script>
      <script src="resources/js/demo.js" type="text/javascript"></script>
      <script src="resources/js/jquery.easing.min.js"></script>
      <script src="resources/js/jquery.fadethis.js"></script>

       
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      <script src="resources/js/calendar.min.js" type="text/javascript" ></script>
      <script src="resources/js/jquery.selectlistactions.js"></script>
      
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script type="text/javascript" src="resources/js/dataTables.buttons.min.js"></script>
      <script type="text/javascript" src="resources/js/buttons.bootstrap.min.js"></script>
      <script type="text/javascript" src="resources/js/jszip.min.js"></script>
      <script type="text/javascript" src="resources/js/buttons.html5.min.js"></script>
      <script type="text/javascript" src="resources/js/buttons.colVis.min.js"></script>
            
              

    <div id="gotop"></div>




<!-- Address Popup Start !-->
<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Address Notification Bar</h4>
      </div>
      <div class="modal-body popup-hei">
            <div class="col-md-3 add-list">
               <ul>
                  <li class="recent">Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
               </ul>
            </div>
            <div class="col-md-9">
              <div class="data-cont">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Initial Comment</p>
                  <div class="row">
                     <div class="col-md-3"><p class="add-text">Recommeneded<br> Action</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Recommeneded Action" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Assign To</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Assign To" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Date</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui calendar width" id="example2">
                                 <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="Date" class="width">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            <div class="col-md-12 data-deta">
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Equipement Id</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Equipement Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Site Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Site Code</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Customer</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Status</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Event Description</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Date / Time</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">23-10-2017 / 1:00</p></div>
               </div>
            </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Save</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Address Popup End !-->  


<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        <form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form">   
            <div class="col-md-12 m-t-10 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields required m-t-10">Site</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
              </div>
            </div>
              <div class="col-md-12 m-t-15 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Type</label> </div>
                  <div class="col-md-8">
                     <form:select class="ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Category</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                    </form:select> 
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Subject</label> </div>
                  <div class="col-md-8">
                    <form:input  placeholder="Subject" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" />
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Priority</label> </div>
                  <div class="col-md-8">
                  <%-- <form:select class="ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="priority">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select> --%>
                   <form:select class="ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select> 
  					<!-- <div class="ui fluid search selection dropdown">
  					<input type="hidden" name="user" />
  					<i class="dropdown icon"></i>
  					<div class="default text">Select Priority</div>
  					<div class="menu" path="priority">
						    <div class="item" data-value="3" value="3"><span style="color:#f9152f; margin-left:15px;">High</span></div>
						    <div class="item" data-value="1" value="2"><span style="color:#FF6600; margin-left:15px;">Medium</span></div>
						    <div class="item" data-value="2" value="1"><span style="color:#f5bc0c; margin-left:15px;">Low</span></div>
						  </div>
				</div> -->
                </div>
              </div>
            </div>
			<%-- <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Assign To</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlAssign" name="ddlAssign" path="assignedTo">
                         <form:option value="">Select</form:option>
                        <form:options  items="${userList}" />
                    </form:select>
                  </div>
              </div>
            </div> --%>

         <%--    <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-3"> <label  class="fields m-t-10">Date/Time<span class="errormess">*</span></label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="DateTimes" name="DateTimes" path="processType">
                        <form:option value="">Select</form:option>
                        <form:option value="Scheduled">Scheduled</form:option>
                        <form:option value="Immediate">Immediate</form:option>
                    </form:select>
                  </div>
              </div>
            </div>
 --%>
           <%--  <div class="col-md-12 m-t-15" id="dateAndTime">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Assign Date</label> </div>
                  <div class="col-md-8">
                    <div class="ui calendar width" id="example3">
                        <div class="ui input left icon">
                          <i class="calendar icon"></i>
                          <form:input type="text" name="Datetime" placeholder="Assign Date" class="width" path="scheduledOn" />
                        </div>
                    </div>
                  </div>
              </div>
            </div> --%>
            <div class="col-md-12 m-t-15 ">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Description</label> </div>
                  <div class="col-md-8">
                    <form:textarea placeholder="Ticket Description" style="resize: none;" id="txtTktdescription" name="txtTktdescription" path="description" ></form:textarea>
                  <!--   <p class="errortext">(Maximum 150 characters are allowed)</p> -->
                  </div>
              </div>
            </div>
				<div class="col-md-12 col-xs-12  m-t-15 center">
				       <div class="btn btn-success  submit">Create</div>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
				</div>
				<!-- <div class="ui error message"></div> -->
				</form:form>
	  		</div>
	  </div>
    </div>
<!-- Address Popup End !--> 





      <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      <script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>
       
       
       <script type='text/javascript'>
       window.onload=function(){
       $('.dropdown').on('show.bs.dropdown', function () {
          var length = parseInt($('.dropdown-menu').css('width'), 10) * -1;
          $('.dropdown-menu').css('left', length);
       })
       }
       </script>
 
 
 
 
  </body>
</html>


