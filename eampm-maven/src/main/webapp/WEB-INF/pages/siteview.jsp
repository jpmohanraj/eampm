<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="true" %>


<html>
  <head>
    <meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
      <meta charset="utf-8"/>
      <title>:: Welcome To eAMPM ::</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no"/>
     <link rel="icon" type="image/x-icon" href="favicon.ico"/>
      <meta name="apple-mobile-web-app-capable" content="yes">
      <meta name="apple-touch-fullscreen" content="yes">
      <meta name="apple-mobile-web-app-status-bar-style" content="default">
      <meta content name="description"/>
      <meta content name="author"/>

      
      <link href="resources/css/pace-theme-flash.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/font-awesome.css" rel="stylesheet" type="text/css"/>
      <link href="resources/css/jquery.scrollbar.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/select2.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/switchery.min.css" rel="stylesheet" type="text/css" media="screen"/>
      <link href="resources/css/pages-icons.css" rel="stylesheet" type="text/css">
      <link href="resources/css/animate.css" rel="stylesheet"  />
      <link href="resources/css/pages.css" class="main-stylesheet"  rel="stylesheet" type="text/css"/>
      <link href="resources/css/styles.css" rel="stylesheet" type="text/css">
      <link href="resources/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
      <link href="resources/css/site.css" rel="stylesheet" >
     <!--  <link href="resources/css/semantic.min.css" rel="stylesheet" type="text/css"> -->
      <link href="resources/css/calendar.min.css" rel="stylesheet" type="text/css">
      <link href="resources/css/bootstrap.min.3.37.css" type="text/css" rel="stylesheet">
      <link href="resources/css/dataTables.bootstrap.min.css" type="text/css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="resources/css/semantic.min.css">
      <script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
      <script src="resources/js/searchselect.js" type="text/javascript"></script>
      
      <script type="text/javascript" src="http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
      <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>
	
	 <script type="text/javascript">
        $(document).ready(function() {
        	
        	 $('.ui.dropdown').dropdown({forceSelection:false});
            if($(window).width() < 767)
            {
   			   $('.card').removeClass("slide-left");
   			   $('.card').removeClass("slide-right");
			   $('.card').removeClass("slide-top");
			   $('.card').removeClass("slide-bottom");
			}
            
            $('.close').click(function(){
				$('.clear').click();
			});
$('.clear').click(function(){
        	$('.search').val("");
        });

$('body').click(function(){
            $('#builder').removeClass('open'); 
          });


        });
      </script>
      
        
      <script type="text/javascript">
         $(document).ready(function() {
        	
             
        	 $("#searchSelect").change(function() {
                 var value = $('#searchSelect option:selected').val();
                 var uid =  $('#hdneampmuserid').val();
              	redirectbysearch(value,uid);
      	    });
        	 
        
        	 $('#dateTime').hide();

        	  $("#ddlTemp").change(function() {
        	            	 var value = $('#ddlTemp option:selected').val();
        	            	 if(value == 0) {
        	            		 $('#dateTime').show();
        	                 }
        	            	 else if(value == 1) {
        	            		 $('#dateTime').show();
        	                 }
        	            })

        	    
        	 
        
          
        	 var table = $('#example2').DataTable();
           
        	 $('#example2_filter input[type="search"]').attr('placeholder','search');
      
         
   } );
        
      </script>
     <script type="text/javascript">
      $(document).ready(function(){
          $(document).ready(function(){
              $('.ui.form')
      .form({
        fields: {
          name: {
            identifier: 'name',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please enter your name'
              }
            ]
          },
          ddlSite: {
            identifier: 'ddlSite',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please select site'
              }
            ]
          },
          ddlType: {
            identifier: 'ddlType',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please select a Type'
              }
            ]
          },
          txtSubject : {
        	  identifier: 'txtSubject',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please select a Type'
                },
                {
                	 type:'maxLength[50]',
                     prompt: 'Subject should not exist more than 50 characters'
                  }
              ] 
          },
          ddlCategory: {
            identifier: 'ddlCategory',
            rules: [
              {
                type   : 'empty',
                prompt : 'Please Select Category'
              }
            ]
          },
          
          ddlAssign: {
              identifier: 'ddlAssign',
              rules: [
                {
                  type   : 'empty',
                  prompt : 'Please Select Category'
                }
              ]
            },
            ddlPriority: {
            	identifier: 'ddlPriority',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Please select Priority'
                  }
                ]
            },
            Datetime: {
            	identifier: 'Datetime',
                rules: [
                  {
                    type   : 'empty',
                    prompt : 'Please Select Date'
                  }
                ]
            },
      
          txtTktdescription : {
              identifier: 'txtTktdescription',
              rules: [{
                  type:'empty',
                  prompt:'Please Enter Ticket Description',
                },{
                type:'maxLength[150]',
                prompt: 'Description should not exist more than 150 characters'
              }]
            }
        }
      });

          });

      });
    </script>

      <script type="text/javascript">
        $(document).ready(function() {
        
        	 $("#ddlType").change(function() {
        		 $('#ddlCategory').val();
        
                 var value = $('#ddlType option:selected').val();
     
                 if(value == "Operations") {
                	 $('#ddlCategory').val();
                	 $('#category').show();

                 }
                 else{
                	 $('#ddlCategory').val();
                	 $('#category').show();

                    }
                 
              });

        	var $ddlType = $( '#ddlType' ),
    		$ddlCategory = $( '#ddlCategory' ),
        $options = $ddlCategory.find( 'option' );
        
    $ddlType.on( 'change', function() {
    	$ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
    } ).trigger( 'change' );
        });
      </script>

      
            <style type="text/css">
            
           @media (min-width: 0px) and (max-width: 480px) {
           	.dataTables_filter {
           		float:left !important;
           		margin-left:30px;
           	}
           }
        .dropdown-menu {
          margin-left: -85px !important;
          left:0px !important;
        }
     .ui.form .fields.error .field .ui.dropdown, .ui.form .field.error .ui.dropdown {
    border-color: #E0B4B4 !important;
    line-height: 17px !important;
}
.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
}

.amcharts-period-input {

    background: #FFF;
    border: 1px solid #CCC;
    font-size: 14px;
    padding: 5px;
    margin: 3px;

}
.amcharts-period-input-selected {

    background: #5B835B;
    border: 1px solid #CCC;
    font-size: 14px;
    padding: 5px;
    margin: 3px;
    color: #FFF;

}
        .dt-button-background {
       position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    z-index: 0 !important; 
}
.buttons-collection {
	height:29px;
	z-index:9999;
}
     .input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
.buttons-excel {
  background: rgba(36, 95, 68, 0.87);
  color: #FFF;
}
.buttons-pdf {
  background: rgb(98, 165, 96);
  color: #FFF;
}
.dataTables_filter {
  float: right;
}
.dataTables_length {
  float: left;
}
#example th {
    padding-top: 12px;
    padding-bottom: 12px;
    text-align: left;
    background-color: #818181;
}
 #chartdiv {
			 width : 100%;
			 height   : 250px;
		  }   
		  #overviewInfor {
                  display:none;
            }  
    </style>

  </head>
  <body  class="fixed-header">
  
    <input type="hidden" value="${access.userID}" id="hdneampmuserid">
    
           <nav class="page-sidebar" data-pages="sidebar">
        
         <div class="sidebar-header">
            <a href="./dashboard"><img src="resources/img/logo01.png" class="m-t-10" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management"></a>
            <i class="fa fa-bars bars"></i>
         </div>
         
         <div class="sidebar-menu">
         
          <c:if test="${not empty access}">
          
            <ul class="menu-items">
            
            
            
           <c:if test="${access.dashboard == 'visible'}">
          
                   <li class="active" style="margin-top:2px;">
                  <a href="./dashboard" class="detailed">
                  <span class="title">Dashboard</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.overview == 'visible'}">
                   <li>
                  <a href="#"><span class="title">Overview</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-list-alt"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.systemMonitoring == 'visible'}">
                  <li>
                  <a href="#"><span class="title">System Monitoring</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.visualization == 'visible'}">
           <li  class="active">
                  <a href="javascript:;"><span class="title">Visualization</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="fa fa-area-chart"></i></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./customerlist">Customer View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
                     	</li>
                    </c:if>
           
           
           
                     <li  class="active">
                        <a href="./sitelist">Site View</a>
                        <span class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
                     </li>
                     <li>
                        <a href="./equipmentlist">Equipment View</a>
                        <span class="icon-thumbnail"><i class="fa fa-cog"></i></span>
                     </li>
                  </ul>
               </li>
              
           </c:if>
           
            <c:if test="${access.analytics == 'visible'}">
           <li>
                  <a href="#">
                  <span class="title">Analytics</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.portfolioManagement == 'visible'}">
          <li>
                  <a href="#">
                  <span class="title">Portfolio Manager</span>
                  </a>
                  <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.ticketing == 'visible'}">
            <li>
                    <a href="javascript:;"><span class="title">Operation & Maintenance</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail" style="padding-top:5px !important;"><img src="resources/img/maintance.png"></span>
                  <ul class="sub-menu">
                       
                    <c:if test="${access.customerListView == 'visible'}">
          				<li>
                        	<a href="./ticketdetails">Ticket View</a>
                        	<span class="icon-thumbnail"><i class="fa fa-ticket"></i></span>
                     	</li>
                    </c:if>
           
                  </ul>
               </li>
           
           
           
           
           
           </c:if>
           
            <c:if test="${access.forcasting == 'visible'}">
          <li>
                  <a href="#"><span class="title">Forecasting</span></a>
                  <span class="icon-thumbnail"><i class="fa fa-desktop"></i></span>
               </li>
           </c:if>
           
            <c:if test="${access.configuration == 'visible'}">
            
               <li>
                  <a href="javascript:;"><span class="title">Configuration</span>
                  <span class=" arrow"></span></a>
                  <span class="icon-thumbnail"><i class="pg-tables"></i></span>
                  <ul class="sub-menu">
                     
                     <li>
                        <a href="./customers">Customer Config.</a>
                        <span class="icon-thumbnail">CU</span>
                     </li>
                     
                     <li>
                        <a href="./sites">Site Config.</a>
                        <span class="icon-thumbnail">SI</span>
                     </li>
                     
                     <li>
                        <a href="./equipments">Equipment Config.</a>
                        <span class="icon-thumbnail">EQ</span>
                     </li>
                     
                     <li>
                        <a href="#">Data Logger Config.</a>
                        <span class="icon-thumbnail">DL</span>
                     </li>
                     
                     <li>
                        <a href="#">Equ-Attrib Config.</a>
                        <span class="icon-thumbnail">EA</span>
                     </li>
                     
                      <li>
                        <a href="#">Configuration Loader</a>
                        <span class="icon-thumbnail">CL</span>
                     </li>
                     
                     <li>
                        <a href="./activities">Activity Config.</a>
                        <span class="icon-thumbnail">AC</span>
                     </li>
                     
                      <li>
                        <a href="./timezones">Timezone Config.</a>
                        <span class="icon-thumbnail">TZ</span>
                     </li>
                     
                      <li>
                        <a href="./currencies">Currency Config.</a>
                        <span class="icon-thumbnail">CY</span>
                     </li>
                     
                      <li>
                        <a href="./unitofmeasurements">Unit Measurement Config.</a>
                        <span class="icon-thumbnail">UM</span>
                     </li>
                     
                     
                     <li>
                        <a href="./countryregions">Country Region Config.</a>
                        <span class="icon-thumbnail">CR</span>
                     </li>
                     
                     <li>
                        <a href="./countries">Country Config.</a>
                        <span class="icon-thumbnail">CO</span>
                     </li>
                     
                     <li>
                        <a href="./states">State Config.</a>
                        <span class="icon-thumbnail">SE</span>
                     </li>
                     
                     <li>
                        <a href="./customertypes">Customer Type Config.</a>
                        <span class="icon-thumbnail">CT</span>
                     </li>
                     
                      <li>
                        <a href="./sitetypes">Site Type Config.</a>
                        <span class="icon-thumbnail">ST</span>
                     </li>
                     
                      <li>
                        <a href="./equipmentcategories">Equ Category Config.</a>
                        <span class="icon-thumbnail">EC</span>
                     </li>
                     
                      <li>
                        <a href="./equipmenttypes">Equ Type Config.</a>
                        <span class="icon-thumbnail">ET</span>
                     </li>
                     
                      <li>
                        <a href="#">Event Type Config.</a>
                        <span class="icon-thumbnail">EY</span>
                     </li>
                     
                     <li>
                        <a href="#">Event Config.</a>
                        <span class="icon-thumbnail">EV</span>
                     </li>
                     
                      <li>
                        <a href="#">Inspection Config.</a>
                        <span class="icon-thumbnail">IN</span>
                     </li>
                     
                      <li>
                        <a href="#">User Role Config.</a>
                        <span class="icon-thumbnail">UR</span>
                     </li>
                     
                      <li>
                        <a href="#">User Config.</a>
                        <span class="icon-thumbnail">US</span>
                     </li>
                     
                  </ul>
               </li>
          
           </c:if>

            </ul>
         
         
          </c:if>
          
          
          
          
            <div class="clearfix"></div>
         </div>
    
    
    
      </nav>
  
    
     <div class="page-container">
         <div class="header">
            <a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu" data-toggle="sidebar">
            </a>
            <div>
               <div class="brand inline">
                  <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo" title="Electronically Assisted Monitoring and Portfolio Management" class="logos"></a>
               </div>
            </div>
            <div class="d-flex align-items-center">
            <div class="form-group required field searchbx" id="SearcSElect">
                           <select class="ui fluid search selection dropdown width oveallSearch" id="searchSelect">
                             <!--  <option value="-2"></option> -->
                              <option value="-1">search </option>
                            <option value="0">Dashboard</option> 
                                
 								<c:if test="${not empty access}">
          							<c:if test="${access.customerListView == 'visible'}">
          								<option value="1">Customer View</option>
                    				</c:if>
                 				</c:if>   
                 				  
                              <option value="3">Site View</option>
                              <option value="5">Equipment View</option>
                              
                               <c:if test="${access.customerListView == 'visible'}">
          				 			<option value="6">Ticket View</option>
                    		   </c:if>
           
           
           
                           </select>
                        </div>
               <!-- <div class="pull-left p-t-8 fs-14 font-heading hidden-md-down">
                  <input type="text" placeholder="search..." class="searchbox">
               </div> -->
                <!-- <div class="m-l-15">
               		<p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
               		<p class="create-tkts">New Ticket</p>
               </div> -->
               <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
                <c:if test="${access.customerListView == 'visible'}">
          				 <div>
               				<p class="center mb-1 tkts" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt.png"></p>
               				<p class="create-tkts">New Ticket</p>
               			</div>
               
               	</c:if>
               <!-- <a href="#" class="header-icon  btn-link m-l-10 sm-no-margin d-inline-block" data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><i class="fa fa-ticket fa-2x"></i></a><br> -->
               <!-- <p class="user-login">Create Ticket</p> -->      
              <!--  <a href="#" class="header-icon pg pg-alt_menu btn-link  sm-no-margin d-inline-block" data-toggle="quickview" data-toggle-element="#quickview"></a> -->
            </div>
         </div>
             
         <div class="page-content-wrapper ">
           <c:if test="${not empty siteview}">
            <div class="sv-cont content" id="QuickLinkWrapper1">
               
               <div class="container-fixed-lg">
                  <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="./dashboard"><i class="fa fa-home mt-7"></i></a></li>
                     <li class="breadcrumb-item"><a>Visualization</a></li>
                     
                       
 								<c:if test="${not empty access}">
          							<c:choose>
    									<c:when test="${access.customerListView == 'visible'}">
       									 	<li class="breadcrumb-item"><a href="./customerview${access.myCustomerID}&ref=${access.userID}">${access.myCustomerName}</a></li>
                     					</c:when>    
    									<c:otherwise>
       									 	<li class="breadcrumb-item"><a href="./dashboard">${access.myCustomerName}</a></li>
                     					</c:otherwise>
								 	</c:choose>

                 				</c:if>   
                 				  
                 				  
                 				  


                     
                     <li class="breadcrumb-item active">${siteview.locationName}</li>
                  </ol>
               </div>
           
           
           
           
            	<div id="newconfiguration" class="container-fluid   container-fixed-lg">
     				<div  class="card card-transparent">
     				
     				<div class="card-block sortable">
                        <div class="row padd-0">
                           <div class="col-lg-3 col-sm-6 col-md-6">
                              <div class="card card-default bg-default slide-left" id="allsites" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">site details</div>
                                    <div class="card-title pull-right"><i class="fa fa-globe font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                <div class="card-block">
                                     
                                      <div class="row" style="margin-top: 15px;">
                                       <div class="col-lg-5 col-xs-5 col-md-5"><p><strong>Site Ref</strong></p></div>
                                       <div class="col-lg-1 col-xs-1 col-md-1"><p>:</p></div>
                                       <div class="col-lg-5 col-xs-5 col-md-5"><p>${siteview.siteCode}</p></div>
                                      </div>
                                      <div class="row">
                                      <div class="col-lg-5 col-xs-5 col-md-5"><p><strong>Capacity</strong></p></div>
                                       <div class="col-lg-1 col-xs-1 col-md-1"><p>:</p></div>
                                       <div class="col-lg-5 col-xs-5 col-md-5"><p>${siteview.capacity}</p></div>
                                      </div>
                                      <div class="row">
                                       <div class="col-lg-5 col-xs-5 col-md-5"><p><strong>COD</strong></p></div>
                                       <div class="col-lg-1 col-xs-1 col-md-1"><p>:</p></div>
                                       <div class="col-lg-5 col-xs-5 col-md-5"><p>${siteview.commisionningDate}</p></div>
                                      </div>
                                      
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper2">
                                    
                                    
                         <c:choose>
  							<c:when test="${siteview.siteStatus=='0'}">
    							<p>Status : <span class="siteStatusOffline">Offline</span></p>
  							</c:when>
  							
  							<c:when test="${siteview.siteStatus=='1'}">
    							<p>Status : <span class="siteStatusActive">Active</span></p>
  							</c:when>
  							
  							<c:when test="${siteview.siteStatus=='2'}">
    							<p>Status : <span class="siteStatusWarning">Warning</span></p>
  							</c:when>
  							
  							<c:when test="${siteview.siteStatus=='3'}">
    							<p>Status : <span class="siteStatusDown">Error</span></p>
  							</c:when>
  							
  							<c:otherwise>
    							<p>Status : <span class="offstxt">-</span></p>
  							</c:otherwise>
						</c:choose>
                                    
                                    
                                    
                                     
                                  
                                 </div>
                              </div>

                              <div class="card card-default bg-default slide-left" id="energy" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">location</div>
                                    <div class="card-title pull-right"><i class="fa fa-map-marker font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                  
                                  <iframe src="${siteview.locationUrl}" class="sitemap" style="border:0" allowfullscreen="" frameborder="0"></iframe> 
                                 </div>
                              </div>
                              <div class="card-footer" id="#QuickLinkWrapper3">
                                    <p>Name : <span class="">${siteview.locationName}</span></p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">scheduled jobs</div>
                                    <div class="card-title pull-right"><i class="fa fa-calendar font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                
                                    <div class="col-lg-12 col-md-12">
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <h3>
                                       <span class="semi-bold"><strong>${siteview.todayScheduledJobs}</strong></span>
                                    </h3>
                                          <p>Today</p>
                                       </div>
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <h3>
                                       <span class="semi-bold"><strong>${siteview.todayScheduledCleaningJobs} : ${siteview.todayScheduledPMJobs}</strong></span>
                                    </h3>
                                          <p>Ratio</p>
                                       </div>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper4">
                                   <p>Total : ${siteview.totalScheduledJobs}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3">
                                  <div class="card card-default bg-default slide-right" id="sitestatus" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">energy</div>
                                    <div class="card-title pull-right center"><i class="fa fa-bolt font-size icon-color" aria-hidden="true"></i><br><p>Today</p></div>
                                 </div>
                                 <div class="card-block">
                                    <h3>
                                       <span class="semi-bold"><strong>${siteview.todayEnergy}</strong></span>
                                    </h3>
                                    <p>${siteview.energyLastUpdate}</p>
                                 </div>
                              </div>
                              <div class="card-footer">
                                    <p>Production Date : ${siteview.productionDate}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="sitestatus" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">weather</div>
                                    <div class="card-title pull-right"><i class="fa fa-sun-o font-size icon-color"></i><br><p>${siteview.weatherStatus}</p></div>
                                 </div>
                                 <div class="card-block">
                                    <h3>
                                       <span class="semi-bold"><strong>${siteview.weatherTodayTemp}</strong></span>
                                    </h3>
                                    <p>Yesterday : ${siteview.weatherYesterdayTemp}</p>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Wind Speed : ${siteview.windSpeed}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header">
                                    <div class="card-title pull-left">events</div>
                                    <div class="card-title pull-right"><i class="fa fa-calendar-check-o font-size icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 
                                    <div class="col-lg-12 col-md-12">
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <h3>
                                       <span class="semi-bold"><strong>${siteview.todayEvents}</strong></span>
                                    </h3>
                                          <p>Today</p>
                                       </div>
                                       <div class="col-lg-6 col-xs-6 col-md-6">
                                          <h3>
                                       <span class="semi-bold"><strong>${siteview.totalEvents}</strong></span>
                                    </h3>
                                          <p>Total</p>
                                       </div>
                                    </div>
                                 
                              </div>
                              <div class="card-footer">
                                   <a>View all events</a>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3 col-sm-6">
                              <div class="card card-default bg-default slide-right" id="productionyield" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">production yield</div>
                                    <div class="card-title pull-right"><i class="fa fa-area-chart font-size yield icon-color"></i><br><p>Total</p></div>
                                 </div>
                                 <div class="card-block">
                                    <h3>
                                       <span class="semi-bold"><strong>${siteview.totalProductionYeild}</strong></span>
                                    </h3>
                                    <p>Yesterday : ${siteview.yesterdayProductionYeild}</p>
                                 </div>
                              </div>
                              <div class="card-footer" id="QuickLinkWrapper3">
                                    <p>Today Yield : ${siteview.todayProductionYeild}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">irradiation</div>
                                    <div class="card-title pull-right"><img src="resources/img/ImageResize_03.png"></div>
                                 </div>
                                 <div class="card-block">
                                     <h3>
                                     
                                     <c:if test="${not empty siteview.irradiation}">
          								<c:choose>
  												<c:when test="${siteview.irradiation==''}">
    											<span class="semi-bold"><strong></strong></span>
                    							</c:when>
  												<c:when test="${siteview.irradiation=='-'}">
    											<span class="semi-bold"><strong></strong></span>
                    							</c:when>
  											
  												<c:otherwise>
    											<span class="semi-bold"><strong>${siteview.irradiation} W/m<sup>2</sup></strong></span>
                    							</c:otherwise>
											</c:choose>
											
                 				</c:if>  
                 				
                 				
                 				
											
											
											
                                     
                                       
                                    </h3>
                                    <p>${siteview.irradiationLastUpdate}</p>
                                 </div>
                              </div>
                              <div class="card-footer">
                                    <p>Module Temperature : ${siteview.moduleTemp}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">inverter</div>
                                    <div class="card-title pull-right"><img src="resources/img/ImageResize_02.png"></div>
                                 </div>
                                 <div class="card-block">
                                   <h3>
                                       <span class="semi-bold"><strong>${siteview.activeInverters} / ${siteview.totalInverters}</strong></span>
                                    </h3>
                                    <p>Error Inverter : ${siteview.errorInverters}</p>
                                 </div>
                              </div>
                              <div class="card-footer">
                                    <p>Module : ${siteview.modules}</p>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-3 col-sm-6">
                              <div class="card card-default bg-default slide-right" id="specificyield" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">Specific yield</div>
                                    <div class="card-title pull-right"><img src="resources/img/ImageResize_01.png"></div>
                                 </div>
                                 <div class="card-block">
                                    <h3>
                                       <span class="semi-bold"><strong>${siteview.todayPerformanceRatio}</strong></span>
                                    </h3>
                                    <p>Today </p>
                                    <p>Yesterday : ${siteview.yesterdayPerformanceRatio}</p>
                                 </div>
                              </div>
                              <div class="card-footer">
                                    <p>Over all : ${siteview.totalPerformanceRatio}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">tickets</div>
                                    <div class="card-title pull-right"><i class="fa fa-ticket fa-3x icon-color" aria-hidden="true"></i></div>
                                 </div>
                                 <div class="card-block">
                                   <h3>
                                       <span class="semi-bold"><strong>${siteview.openTickets}</strong></span>
                                    </h3>
                                    <p>Open Tickets</p>
                                 </div>
                              </div>
                              <div class="card-footer">
                                    <p>Closed Tickets : ${siteview.completedTickets}</p>
                                 </div>
                              </div>
                              <div class="card card-default bg-default slide-right" id="events" data-pages="card">
                              <div class="h-150">
                                 <div class="card-header ">
                                    <div class="card-title pull-left">CO<sub>2</sub>  Avoided</div>
                                    <div class="card-title pull-right"><img src="resources/img/ImageResize_04.png"></div>
                                 </div>
                                 <div class="card-block">
                                   <h3>
                                       <span class="semi-bold"><strong>${siteview.todayCo2Avoided}</strong></span>
                                    </h3>
                                    <p>Today Emission</p>
                                 </div>
                              </div>
                              <div class="card-footer">
                                    <p>Total : ${siteview.totalCo2}</p>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div> 
                   
                   
            
            
            
                      <!-- Table Start-->
                     <div class="card-block sortable" id="equipmentDetails" style="padding-top:0px !important;padding-bottom:20px !important;">                        
                           <div class="row padding-15" id="QuickViewWrapper5">
                              <div class="card card-default bg-default" data-pages="card">                                
                           	      <div class="card-header" >
                                    <div class="card-title m-l-10 tbl-head">Equipment Details</div>
                                    <c:if test="${access.customerListView == 'visible'}">
          								<div class="pull-right m-r-5"><a class="dataExport" style="color:#000;margin-right:20px;"  href="./dataexport${siteview.siteID}&ref=${access.userID}"><i class="fa fa-download"></i> Equipment Details</a></div>
                    				</c:if>
                    			</div>                           
                                 <div class="card-block table-responsive" style="padding:0px !important;padding-bottom:20px !important;">
                                    <table id="example2" class="table table-striped table-bordered padd-0" width="100%" cellspacing="0" style="padding-left:0px !important;padding-right:0px !important;">
                                       <thead>
                                          <tr>                                          
                                             <c:if test="${access.customerListView == 'visible'}">
          										 <th>Equipment Code</th>
                   							 </c:if>            
                                             <th>Equipment ID</th>
                                             <th>Equipment Type</th>
                                             <th>Status</th>
                                             <th>Today Energy (kWh)</th>
                                             <th>Total Energy (MWh)</th>
                                             <th>Specific Yield</th>                                             
                                              <c:if test="${access.customerListView == 'visible'}">
          										  <th>Event Time</th>
                   							 </c:if>                                            
                                          </tr>
                                       </thead>
                                       <tbody>                                     
                                       
                                       
                                       <c:forEach items="${siteviewEquipmentList}" var="siteviewlist">
        								<tr>            								
            								<c:if test="${access.customerListView == 'visible'}">
          										<td><a href="./equipmentview${siteviewlist.equipmentID}&ref=${access.userID}" class="newlink">${siteviewlist.equipmentCode}</a></td>
            								</c:if>                 							 
                   							 
            								<td><a href="./equipmentview${siteviewlist.equipmentID}&ref=${access.userID}" class="newlink">${siteviewlist.equipmentName}</a></td>
            								
            								<td>Inverter</td>
            								
            								<c:choose>
  												<c:when test="${siteviewlist.networkStatus=='0'}">
    												<td><div class="offl"></div>&nbsp;Offline</td>
  												</c:when>
  												<c:when test="${siteviewlist.networkStatus=='1'}">
    												<td><div class="act"></div>&nbsp;Active</td>
  												</c:when>
  												<c:when test="${siteviewlist.networkStatus=='3'}">
    												<td><div class="erro"></div>&nbsp;Error</td>
  												</c:when>
  												<c:when test="${siteviewlist.networkStatus=='2'}">
    												<td><div class="warn"></div>Warning</td>
  												</c:when>
  												<c:otherwise>
    												<td><div class="offl"></div>Offline</td>
  												</c:otherwise>
											</c:choose>
											
											
            								<td>${siteviewlist.todayEnergy}</td>
            								<td>${siteviewlist.totalEnergy}</td>
            								<td>${siteviewlist.performanceRatio}</td>
            								
            								
            								<c:if test="${access.customerListView == 'visible'}">
          										<td>${siteviewlist.lastUpdate}</td>
            								</c:if>
            							
										</tr>
    									</c:forEach>
    									
                                       
                                        
                                         
                                        
                                       </tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                       
                     </div>
                     <!-- Table End-->
                    
                   
                   
                   
       <!--      <div class="card-header">
                                    <div class="card-title  tbl-head" style="margin-left:10px !important;">Daily Energy Generation</div>
                                 </div> -->
                                        
                     
     				 <div class="card-block sortable" style="padding-top:0px !important;">
         
          
         
         
              
               	
                           <div class="row chart-mob padding-15" id="dailyEnergy">
                           
                              <div class="card card-default bg-default site-chart-mob" id="sitestatus" data-pages="card">
                                   <div class="card-header">
                                    <div class="card-title  tbl-head" style="margin-left:10px !important;">Daily Energy Generation</div>
                                 </div>
                                 <div class="card-block site-chart-mob">
                                    <div id="chartdiv"></div>
                                    
              
      <input type="hidden" value='${siteview.chartJsonData1}' id="ChartData1">
      <input type="hidden" value='${siteview.chartJsonData1Parameter}' id="Parameter">
      <input type="hidden" value='${siteview.chartJsonData1GraphValue}' id="GraphValue">
                  
                                
                                 </div>
                              </div>
                           </div>
                       
                         
                    
                  
                     </div>
                   

					</div>
				</div>
         
        
            </div>
         
         
           </c:if>
           <div class="container-fixed-lg">
               <div class="container-fluid copyright sm-text-center">
                  <p class="small no-margin pull-left sm-pull-reset">
                     <span class="hint-text">Copyright &copy; 2017.</span>
                     <span>INSPIRE CLEAN ENERGY.</span>
                     <span class="hint-text">All rights reserved. </span>
                  </p>
                  <p class="small no-margin pull-rhs sm-pull-reset">
                     <span class="hint-text">Powered by</span>
                     <span>MESTECH SERVICES PVT LTD</span>.
                  </p>
                  <div class="clearfix"></div>
               </div>
            </div>
            </div>
      </div>

          
      <!-- Side Bar Content Start-->

    
      
      <input type="hidden" value="${dashboard.mapJsonData}" id="MapJsonId">
      
      <!-- Side Bar Content End-->
    
    
    
      <div class="quickview-wrapper  builder builder-site hidden-sm hidden-xs" id="builder">
         <div class="p-l-10 p-r-10 ">
            <a class="builder-close quickview-toggle pg-close"  href="#"></a>
            <a class="builder-toggle" data-toggle="quickview" data-toggle-element="#builder"><img src="resources/img/ImageResize_06.png"></a>
            <ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary" id="builderTabs" role="tablist">
               <li class="nav-item">
                  <a class="nav-link active" data-toggle="tab" href="#tabLayouts" role="tab" aria-controls="home"><span>Quick Links</span></a>
               </li>
            </ul>
            <div class="tab-content">
               <div class="tab-pane active " id="tabLayouts" role="tabcard">
                  <div class="scrollable">
                     <div class="p-r-50">
                        <div class="list">
                           <ul>
                              <li><a href="#QuickLinkWrapper1">Site Details</a></li>
                              <li><a href="#QuickLinkWrapper1" id="energy">Energy</a></li>
                              <li><a href="#QuickLinkWrapper1" id="productionyield">Production Yield</a></li>
                              <li><a href="#QuickLinkWrapper1" id="specificyield">Specific Yield</a></li>
                             
                              <li><a href="#QuickLinkWrapper2" id="maps">Location</a></li>
                              <li><a href="#QuickLinkWrapper2" id="sstatus">Weather</a></li>
                              <li><a href="#QuickLinkWrapper2" id="tkts">Irradiation</a></li>
                              <li><a href="#QuickLinkWrapper2" id="ents">Tickets</a></li>
                              
                              <li><a href="#QuickLinkWrapper3" id="maps">Scheduled Jobs</a></li>
                              <li><a href="#QuickLinkWrapper3" id="sstatus">Events</a></li>
                              <li><a href="#QuickLinkWrapper3" id="tkts">Inverter</a></li>
                              <li><a href="#QuickLinkWrapper3" id="ents">Co<sub>2</sub> Avoided</a></li>
                              
                              <li><a href="#QuickLinkWrapper4">Equipment Details</a></li>
                              <li><a href="#QuickLinkWrapper4">Daily Energy Generation</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
               
            </div>
         </div>
      </div>
      <script src="resources/js/pace.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.gotop.js"></script>
      <script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
      <script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
      <script src="resources/js/tether.min.js" type="text/javascript"></script>
      <script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery-easy.js" type="text/javascript"></script>
      <script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
      <script src="resources/js/jquery.actual.min.js"></script>
      <script src="resources/js/jquery.scrollbar.min.js"></script>
      <script type="text/javascript" src="resources/js/select2.full.min.js"></script>
      <script type="text/javascript" src="resources/js/classie.js"></script>
      <script src="resources/js/switchery.min.js" type="text/javascript"></script>
      <script src="resources/js/pages.min.js"></script>
      <script src="resources/js/card.js" type="text/javascript"></script>
      <script src="resources/js/scripts.js" type="text/javascript"></script>
      <script src="resources/js/demo.js" type="text/javascript"></script>
      <script src="resources/js/jquery.easing.min.js"></script>
      <script src="resources/js/jquery.fadethis.js"></script>

       
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/semantic.min.js" type="text/javascript" ></script>
      <script src="resources/js/calendar.min.js" type="text/javascript" ></script>
      <script src="resources/js/jquery.selectlistactions.js"></script>
      
      <script src="resources/js/jquery.dataTables.min.js" type="text/javascript" ></script>
      <script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript" ></script>
      <script src="resources/js/chart/amcharts.js"></script>
      <script src="resources/js/chart/serial.js"></script>
      <script src="resources/js/chart/amstock.js"></script>
      <script src="resources/js/chart/export.min.js"></script>
      		
      
      


    <div id="gotop"></div>
         <!-- Address Popup Start !-->
         
         <script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
      
      <!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Address Notification Bar</h4>
      </div>
      <div class="modal-body popup-hei">
            <div class="col-md-3 add-list">
               <ul>
                  <li class="recent">Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
                  <li>Recent Events</li>
               </ul>
            </div>
            <div class="col-md-9">
              <div class="data-cont">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Initial Comment</p>
                  <div class="row">
                     <div class="col-md-3"><p class="add-text">Recommeneded<br> Action</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Recommeneded Action" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Assign To</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui form">
                                 <div class="field txt-box">
                                    <textarea placeholder="Assign To" style="resize: none;"></textarea>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                     <div class="col-md-3"><p class="add-text">Date</p></div>
                        <div class="col-md-9">
                           <div class="form-group">
                              <div class="ui calendar width" id="example2">
                                 <div class="ui input left icon">
                                    <i class="calendar icon"></i>
                                    <input type="text" placeholder="Date" class="width">
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
               </div>
            </div>
            <div class="col-md-12 data-deta">
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Equipement Id</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Equipement Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Site Name</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Site Code</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Customer</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Status</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#38</p></div>
               </div>
               <div class="row mb-10">
                  <div class="col-md-2"><p class="add-text">Event Description</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">#31</p></div>
                  <div class="col-md-1"></div>
                  <div class="col-md-2"><p class="add-text">Event Date / Time</p></div>
                  <div class="col-md-1"><p class="add-text">:</p></div>
                  <div class="col-md-2"><p class="add-text">23-10-2017 / 1:00</p></div>
               </div>
            </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Save</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Address Popup End !-->  
<!-- Comment Popup Start !-->
      <!-- Modal -->
<div id="myCommentModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Comment Bar</h4>
      </div>
      <div class="modal-body popup-hei">
            <div class="col-md-4">
               <div class="cmt-list">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Events</p>
                  <ul>
                  <li>
                     <p class="one-of-two">
                                    <span class="text-warning fs-12">#31</span>
                                 </p>
                                 <p class="two-of-two p-l-10 fs-12">
                                    <span class="text-master">Webdysun </span>
                                    <span class="text-warning pull-right">20-07-2017</span>
                                    <br>
                                    <span class="text-masters">Site Name</span>
                                    <span class="text-warning pull-right">15:15</span>
                                    <br>
                                    <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                                 </p>
                  </li>
                 
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>

                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
                  <li>
                     <p class="one-of-two">
                        <span class="text-warning fs-12">#31</span>
                     </p>
                     <p class="two-of-two p-l-10 fs-12">
                        <span class="text-master">Webdysun </span>
                        <span class="text-warning pull-right">20-07-2017</span>
                        <br>
                        <span class="text-masters">Site Name</span>
                        <span class="text-warning pull-right">15:15</span>
                        <br>
                        <span class="text-masterse">Datalogger webdysun n'est plus alimente </span>
                     </p>
                  </li>
               </ul>
               </div>
               
            </div>
            <div class="col-md-8">
              <div class="data-cont">
                  <p class="add-head"><i class="fa fa-chat fa-2x"></i>Events Comment</p>
                  <div class="conte">
                     <div class="row border">
                        <div class="col-lg-3 col-md-3 col-xs-3 center">
                           <i class="fa fa-user fa-4x"></i>
                        </div>
                        <div class="col-md-9 col-xs-9 col-lg-9">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                              <div class="cmtcontent">
                                 <p><strong>Initial Comment :</strong><br>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                              </div>
                        </div>
                     </div>
                     <div class="hr-line"></div>
                     <div class="row border">
                        <div class="col-md-11 col-xs-11 col-lg-11">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left ml">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right ml"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                           <div class="padd">
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           </div>
                        </div>
                        <div class="col-md-1 col-xs-1 col-lg-1 center">
                           <i class="fa fa-user fa-3x m-t-15"></i>
                        </div>
                     </div>
                     <div class="hr-line"></div>
                     <div class="row border">
                        <div class="col-lg-2 col-md-2 col-xs-2 center">
                           <i class="fa fa-user fa-3x m-t-15"></i>
                        </div>
                        <div class="col-lg-10 col-md-10 col-xs-10">
                           <div class="row">
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-left ml">User Name</p>
                              </div>
                              <div class="col-md-6 col-xs-6 col-lg-6">
                                 <p class="pull-right ml"><i class="fa fa fa-clock-o"></i> 12 Mins ago..</p>
                              </div>
                           </div>
                           <div class="padd">
                              <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
                           </div>
                        </div>
                     </div>
                  </div>
                     <div class="hr-line"></div>
                     <div class="row">
                        <div class="col-md-9 col-lg-9 col-xs-7">
                           <form class="ui form mt-15 m-l-20">
                              <div class="field">
                                 <input name="first-name" placeholder="Customer Name" type="text">
                              </div>
                           </form>
                        </div>
                        <div class="col-md-1 col-lg-1 col-xs-1 m-t-15">
                           <input type="button" class="btn btn-success" value="Send" name="">
                        </div>
                     </div>

               </div>
            </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Save</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>
<!-- Comment Popup End !--> 
<!-- Share Popup Start !-->
      <!-- Modal -->
<div id="myShareModal" class="modal fade" role="dialog">
  <div class="modal-dialog modal-top modal-lg">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Share</h4>
      </div>
      <div class="modal-body popup-hei">
         <form class="ui form">
            <div class="field">
               <label class="fields">Subject</label>
               <input name="first-name" placeholder="Subject" type="text" class="txt-padd">
            </div>
         </form>
         <form class="ui form mt-15">
            <div class="field">
               <label class="fields">Additional Notes</label>
               <input name="first-name" placeholder="Additional Notes" type="text" class="txt-padd">
            </div>
         </form>
         <div class="row style-select">
         <div class="col-md-12 mt-15">
            <div class="subject-info-box-1">
               <!-- <label class="fields">Available Superheroes</label> -->
               <select multiple class="form-control" id="lstBox1">
                  <option value="123">Superman</option>
                  <option value="456">Batman</option>
                  <option value="789">Spiderman</option>
                  <option value="654">Captain America</option>
               </select>
            </div>

            <div class="subject-info-arrows text-center">
               <br /><br />
               <input type='button' id='btnAllRight' value="">>" class="btn btn-default" /><br />
               <input type='button' id='btnRight' value=">" class="btn btn-default" /><br />
               <input type='button' id='btnLeft' value="/<" class="btn btn-default" /><br />
               <input type='button' id='btnAllLeft' value="/<<" class="btn btn-default" />
            </div>

            <div class="subject-info-box-2">
               <!-- <label class="fields">Superheroes You Have Selected</label> -->
               <select multiple class="form-control" id="lstBox2">
                  <option value="765">Nick Fury</option>
                  <option value="698">The Hulk</option>
                  <option value="856">Iron Man</option>
               </select>
            </div>

            <div class="clearfix"></div>
         </div>
      </div>
      </div>
      <div class="modal-footer">
         <button type="button" class="btn btn-success">Send</button>
         <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
      </div>
    </div>

  </div>
</div>


<!-- Share Popup End !--> 
<!-- Modal -->
<div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        <form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form">   
            <div class="col-md-12 m-t-10 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields required m-t-10">Site</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
              </div>
            </div>
              <div class="col-md-12 m-t-15 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Type</label> </div>
                  <div class="col-md-8">
                     <form:select class="ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Category</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                    </form:select> 
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Subject</label> </div>
                  <div class="col-md-8">
                    <form:input  placeholder="Subject" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" />
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15 smb-50">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Priority</label> </div>
                  <div class="col-md-8">
                   <form:select class="ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select>
  					<!-- <div class="ui fluid search selection dropdown">
  					<input type="hidden" name="user" />
  					<i class="dropdown icon"></i>
  					<div class="default text">Select Priority</div>
  					<div class="menu" path="priority">
						    <div class="item" data-value="3" value="3"><span style="color:#f9152f; margin-left:15px;">High</span></div>
						    <div class="item" data-value="1" value="2"><span style="color:#FF6600; margin-left:15px;">Medium</span></div>
						    <div class="item" data-value="2" value="1"><span style="color:#f5bc0c; margin-left:15px;">Low</span></div>
						  </div>
				</div> -->
                </div>
              </div>
            </div>
			<%-- <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Assign To</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlAssign" name="ddlAssign" path="assignedTo">
                         <form:option value="">Select</form:option>
                        <form:options  items="${userList}" />
                    </form:select>
                  </div>
              </div>
            </div> --%>

         <%--    <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-3"> <label  class="fields m-t-10">Date/Time<span class="errormess">*</span></label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="DateTimes" name="DateTimes" path="processType">
                        <form:option value="">Select</form:option>
                        <form:option value="Scheduled">Scheduled</form:option>
                        <form:option value="Immediate">Immediate</form:option>
                    </form:select>
                  </div>
              </div>
            </div>
 --%>
           <%--  <div class="col-md-12 m-t-15" id="dateAndTime">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Assign Date</label> </div>
                  <div class="col-md-8">
                    <div class="ui calendar width" id="example3">
                        <div class="ui input left icon">
                          <i class="calendar icon"></i>
                          <form:input type="text" name="Datetime" placeholder="Assign Date" class="width" path="scheduledOn" />
                        </div>
                    </div>
                  </div>
              </div>
            </div> --%>
            <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Description</label> </div>
                  <div class="col-md-8">
                    <form:textarea placeholder="Ticket Description" style="resize: none;" id="txtTktdescription" name="txtTktdescription" path="description" ></form:textarea>
                  <!--   <p class="errortext">(Maximum 150 characters are allowed)</p> -->
                  </div>
              </div>
            </div>
				<div class="col-md-12 col-xs-12  m-t-15 center">
				       <div class="btn btn-success  submit">Create</div>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
				</div>
				<!-- <div class="ui error message"></div> -->
				</form:form>
	  		</div>
	  </div>
    </div>
<!-- Address Popup End !--> 

 <script type="text/javascript">

 var chartData1 = [];


 var Data1 = $('#ChartData1').val();
 Data1 = eval('(' + Data1 + ')');
 chartData1 = Data1;

 var chartData = [];
 
	  for ( var i = 0; i < chartData1.length; i++ ) {
	   var vdate =  chartData1[i]["date"];
	  	var newDate = new Date(vdate);  
	   var value =  chartData1[i]["value1"];

	   chartData.push( {
	      "date": newDate,
	      "value1": value
	    });
	  }
	  
	  

 var Field = $('#Parameter').val();
 Field = eval('(' + Field + ')');
 var FieldValue=Field;

 var GraphData = $('#GraphValue').val();
 GraphData = eval ('(' + GraphData +')');
 var GraphVal = GraphData;

 

 var chart = AmCharts.makeChart( "chartdiv", {
   "type": "stock",
   "theme": "light",

   "dataSets": [ {
     "color": "#b0de09",
     "fieldMappings": [ {
       "fromField": "value1",
       "toField": "value1"
     }],

     "dataProvider": chartData,
     "categoryField": "date"
   } ],
   "valueAxes": [
	    {
	      "title": "Axis title"
	    }
	  ],
   "panels": [ {
 	    
     "stockGraphs": [ {
       "id": "g1",
       "valueField": "value1",
       "type": "column",
       "fillAlphas": 1,
       "lineThickness": 2,
       //"bullet": "round",
       "balloonText": "Daily Energy:<b>[[value1]]MWh</b>",
       "compareGraphBalloonText": "Daily Energy:<b>[[value1]]MWh</b>"
     } ],
     
     "stockLegend": {
         "valueTextRegular": "",
         "markerType": "none"
       }
   
   } ],
   
  /* "periodSelector": {
     "position": "top",
     "dateFormat": "DD-MM-YYYY",
     "inputFieldWidth": 150,
     "periods": [  {
           "period": "hh",
           "count": 720,
           "selected": true,
           "label": "Current Month"
         }, {
       "period": "hh",
       "count": 720,
       "label": "Previ Month"
     }   ]
   },  */
   

   "chartCursorSettings": {
     "valueBalloonsEnabled": true
   }
 } );


 
 /* 
var chart = AmCharts.makeChart( "chartdiv", {
  "type": "stock",
  "theme": "light",

  "dataSets": [ {
    "color": "#b0de09",
    "fieldMappings": [ {
      "fromField": "value1",
      "toField": "value1"
    }],

    "dataProvider": chartData,
    "categoryField": "date"
  } ],

  "panels": [ {
	    
    "stockGraphs": [ {
      "id": "g1",
      "valueField": "value1",
      //"lineThickness": 2,
      "bullet": "round",
      "balloonText": "Daily Energy:<b>[[value1]]MWh</b>",
      "compareGraphBalloonText": "Daily Energy:<b>[[value1]]MWh</b>"
    } ],
    
    "stockLegend": {
        "valueTextRegular": "",
        "markerType": "none"
      }
  
  } ],

  

  "chartCursorSettings": {
    "valueBalloonsEnabled": true
  }
} );

 */
      </script>
<script>
        $('#btnRight').click(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });
        $('#lstBox1').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnAllRight').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnLeft').click(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });

         $('#lstBox2').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
        $('#btnAllLeft').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
    </script>
  </body>
</html>


