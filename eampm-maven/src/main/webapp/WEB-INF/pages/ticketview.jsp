<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To eAMPM ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">
<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/jquery-ui_cal.css" rel="stylesheet"
	type="text/css">

<!-- <link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css"> -->
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>
<script type="text/javascript">
	$(document).ready(function() {

		$("#ajaxbutton").click(function() {
			//alert($('#ajaxbutton').attr("name"));
			var SerUrl = $('#ajaxbutton').attr("name");
			$.ajax({
				url : SerUrl,
				success : function(result) {
					//alert(result);
					var pdfVal = result;
					//window.location.href= result;
					window.open(result, '_blank');
					/* $("#div1").html(result); */
				}
			});
		});
		$('#example').DataTable();
		$("#searchSelect").change(function() {
			var value = $('#searchSelect option:selected').val();
			var uid = $('#hdneampmuserid').val();
			redirectbysearch(value, uid);
		});

		$('.dataTables_filter input[type="search"]').attr(
				'placeholder', 'search');

		if ($(window).width() < 767) {

			$('.card').removeClass("slide-left");
			$('.card').removeClass("slide-right");
			$('.card').removeClass("slide-top");
			$('.card').removeClass("slide-bottom");
		}

		$('.close').click(function() {
			$('.clear').click();
		});
		$('.clear').click(function() {
			$('.search').val("");
		});

		$('body').click(function() {
			$('#builder').removeClass('open');
		});

	});
</script>
<script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown();
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 



             $('.validation-form')
   .form({
     on: 'blur',
     onFailure: function(formErrors, fields) {
       $.each(fields, function(e) {
         var ele = $('#' + e);
         var fail = (ele.val() === false || ele.val().length === 0);
         if (fail) {
           console.log(ele, fail);
           ele.focus();
           $('html,body').animate({
             scrollTop: ele.offset().top - ($(window).height() - ele.outerHeight(true)) / 2
           }, 200);
           return false;
         }
       });
       return false;
     },
     fields: {
    	 txtSubject: {
         identifier: 'txtSubject',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
        ddlSite: {
         identifier: 'ddlSite',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
      ddlType: {
            identifier: 'ddlType',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
      ddlCategory: {
         identifier: 'ddlCategory',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
       ddlPriority: {
         identifier: 'ddlPriority',
         rules: [{
           type: 'empty',
           prompt: 'Please select a dropdown value'
         }]
       },
       description: {
         identifier: 'description',
         rules: [{
           type: 'empty',
           prompt: 'Please Enter Description'
         }]
       },
       
     }
   }); ('form').reset('clear');
   
   // Assign Popup Validation
   	

         })
         
      </script>
 <script>
	$(document)
			.ready(
					function() {
						$('.ui.form ')
								.form(
										{
											fields : {
												remarks : {
													identifier : 'remarks',
													rules : [ {
														type : 'empty',
														prompt : 'Please enter remarks'
													} ]
												},

												ddlstate : {
													identifier : 'ddlstate',
													rules : [ {
														type : 'empty',
														prompt : 'Please select a gender'
													} ]
												},
												
												assignTo : {
													identifier : 'assignTo',
													rules : [ {
														type : 'empty',
														prompt : 'Please enter a statename'
													} ]
												},
												reassignTo: {
													identifier : 'reassignTo',
													rules : [ {
														type : 'empty',
														prompt : 'Please enter a statename'
													} ]
												},
												descr : {
													identifier : 'descr',
													rules : [ {
														type : 'empty',
														prompt : 'Please enter a descr'
													} ]
												},
												reassignDescr :{
													identifier : 'reassignDescr',
													rules : [ {
														type : 'empty',
														prompt : 'Please enter a descr'
													} ]
												},
												
												CloseDescr : {
													identifier : 'CloseDescr',
													rules : [ {
														type : 'empty',
														prompt : 'Please enter a descr'
													} ]
												}
												
											}
										});
						$('form').form('clear')
					})
</script>
<style type="text/css">
#popupwidth {
	width: 80%;
	margin-left: 10%;
}

.date-picker {
	/* z-index:9999 !important; */
	
}

.dropdown-menu-right {
	left: -70px !important;
}

#example td a {
	color: #337ab7;
	font-weight: bold;
}

.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}

.input-sm, .form-horizontal .form-group-sm .form-control {
	font-size: 13px;
	min-height: 25px;
	height: 32px;
	padding: 8px 9px;
}

.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.ui.form textarea:not ([rows] ) {
	height: 2em;
	min-height: 6em;
	max-height: 24em;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}

#txtCloseRemarks
 
#dateAndTime .ui.icon.input>i.icon:before, .ui.icon.input>i.icon:after {
	left: 0;
	position: absolute;
	text-align: center;
	top: 50%;
	width: 100%;
	margin-top: -1em;
}
.ui.form textarea:not([rows]) {
    height: 6em !important;
    min-height: 6em !important;
    max-height: 24em;
}
#txtReassignRemarks{}

.padd-15 {
	padding-left: 15px;
	padding-bottom: 15px;
}

.p-t-10 {
	padding-top: 10px;
}
</style>

<script type="text/javascript">
	window.onload = function() {
		var $ddlSiteId = $('#ddlSiteId'), $ddlEquipmentId = $('#ddlEquipmentId'), $options = $ddlEquipmentId
				.find('option');

		$ddlSiteId.on(
				'change',
				function() {
					$ddlEquipmentId.html($options.filter('[data-value="'
							+ this.value + '"]'));
				}).trigger('change');
	}
</script>

<script type='text/javascript'>
	//<![CDATA[
	$(window).load(function() {
		$('.ui.dropdown').dropdown({
			forceSelection : false
		});
	});
</script>
</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">


	<nav class="page-sidebar" data-pages="sidebar">

		<div class="sidebar-header">
			<a href="./dashboard"><img src="resources/img/logo01.png"
				class="m-t-10" alt="logo"
				title="Electronically Assisted Monitoring and Portfolio Management"></a>
			<i class="fa fa-bars bars"></i>
		</div>

		<div class="sidebar-menu">


			<c:if test="${not empty access}">

				<ul class="menu-items">



					<c:if test="${access.dashboard == 'visible'}">

						<li class="" style="margin-top: 2px;"><a href="./dashboard"
							class="detailed"> <span class="title">Dashboard</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
						</li>
					</c:if>

					<c:if test="${access.overview == 'visible'}">
						<li><a href="#"><span class="title">Overview</span></a> <span
							class="icon-thumbnail"><i class="fa fa-list-alt"></i></span></li>
					</c:if>

					<c:if test="${access.systemMonitoring == 'visible'}">
						<li><a href="#"><span class="title">System
									Monitoring</span></a> <span class="icon-thumbnail"><i
								class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.visualization == 'visible'}">
						<li><a href="javascript:;"><span class="title">Visualization</span>
								<span class=" arrow"></span></a> <span class="icon-thumbnail"><i
								class="fa fa-area-chart"></i></span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li><a href="./customerlist">Customer View</a> <span
										class="icon-thumbnail"><i class="fa fa-eye"></i></span></li>
								</c:if>



								<li><a href="./sitelist">Site View</a> <span
									class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
								</li>
								<li><a href="./equipmentlist">Equipment View</a> <span
									class="icon-thumbnail"><i class="fa fa-cog"></i></span></li>
							</ul></li>

					</c:if>

					<c:if test="${access.analytics == 'visible'}">
						<li><a href="#"> <span class="title">Analytics</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
						</li>
					</c:if>

					<c:if test="${access.portfolioManagement == 'visible'}">
						<li><a href="#"> <span class="title">Portfolio
									Manager</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
						</li>
					</c:if>

					<c:if test="${access.ticketing == 'visible'}">
						<li class="active"><a href="javascript:;"><span class="title">Operation
									& Maintenance</span> <span class=" arrow"></span></a> <span
							class="icon-thumbnail" style="padding-top: 5px !important;"><img
								src="resources/img/maintance.png"></span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li class="active"><a href="./ticketdetails">Ticket View</a> <span
										class="icon-thumbnail"><i class="fa fa-ticket"></i></span></li>
								</c:if>

							</ul></li>





					</c:if>




					<c:if test="${access.forcasting == 'visible'}">
						<li><a href="#"><span class="title">Forecasting</span></a> <span
							class="icon-thumbnail"><i class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.configuration == 'visible'}">

						<li><a href="javascript:;"><span class="title">Configuration</span>
								<span class=" arrow"></span></a> <span class="icon-thumbnail"><i
								class="pg-tables"></i></span>
							<ul class="sub-menu">

								<li><a href="./customers">Customer Config.</a> <span
									class="icon-thumbnail">CU</span></li>

								<li><a href="./sites">Site Config.</a> <span
									class="icon-thumbnail">SI</span></li>

								<li><a href="./equipments">Equipment Config.</a> <span
									class="icon-thumbnail">EQ</span></li>

								<li><a href="#">Data Logger Config.</a> <span
									class="icon-thumbnail">DL</span></li>

								<li><a href="#">Equ-Attrib Config.</a> <span
									class="icon-thumbnail">EA</span></li>

								<li><a href="#">Configuration Loader</a> <span
									class="icon-thumbnail">CL</span></li>

								<li><a href="./activities">Activity Config.</a> <span
									class="icon-thumbnail">AC</span></li>

								<li><a href="./timezones">Timezone Config.</a> <span
									class="icon-thumbnail">TZ</span></li>

								<li><a href="./currencies">Currency Config.</a> <span
									class="icon-thumbnail">CY</span></li>

								<li><a href="./unitofmeasurements">Unit Measurement
										Config.</a> <span class="icon-thumbnail">UM</span></li>


								<li><a href="./countryregions">Country Region Config.</a> <span
									class="icon-thumbnail">CR</span></li>

								<li><a href="./countries">Country Config.</a> <span
									class="icon-thumbnail">CO</span></li>

								<li><a href="./states">State Config.</a> <span
									class="icon-thumbnail">SE</span></li>

								<li><a href="./customertypes">Customer Type Config.</a> <span
									class="icon-thumbnail">CT</span></li>

								<li><a href="./sitetypes">Site Type Config.</a> <span
									class="icon-thumbnail">ST</span></li>

								<li><a href="./equipmentcategories">Equ Category
										Config.</a> <span class="icon-thumbnail">EC</span></li>

								<li><a href="./equipmenttypes">Equ Type Config.</a> <span
									class="icon-thumbnail">ET</span></li>

								<li><a href="#">Event Type Config.</a> <span
									class="icon-thumbnail">EY</span></li>

								<li><a href="#">Event Config.</a> <span
									class="icon-thumbnail">EV</span></li>

								<li><a href="#">Inspection Config.</a> <span
									class="icon-thumbnail">IN</span></li>

								<li><a href="#">User Role Config.</a> <span
									class="icon-thumbnail">UR</span></li>

								<li><a href="#">User Config.</a> <span
									class="icon-thumbnail">US</span></li>

							</ul></li>
					</c:if>
				</ul>
			</c:if>
			<div class="clearfix"></div>
		</div>



	</nav>


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					<a href="./dashboard"><img src="resources/img/logo01.png"
						alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx" id="SearcSElect">
					<select
						class="ui fluid search selection dropdown width oveallSearch"
						id="searchSelect">
						<!--  <option value="-2"></option> -->
						<option value="-1">search</option>
						<option value="0">Dashboard</option>

						<c:if test="${not empty access}">
							<c:if test="${access.customerListView == 'visible'}">
								<option value="1">Customer View</option>
							</c:if>
						</c:if>

						<option value="3">Site View</option>
						<option value="5">Equipment View</option>

						<c:if test="${access.customerListView == 'visible'}">
							<!--  <option value="6">Ticket View</option> -->
						</c:if>
						<c:if test="${access.customerListView == 'visible'}">
							<option value="6">Ticket View</option>
						</c:if>
					</select>


				</div>
				<div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>
					</button>
					<div class="dropdown-menu dropdown-menu-right profile-dropdown"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="login"
							class="dropdown-item color logout"><i class="pg-power"></i>
							Logout</a>
					</div>
				</div>

				<c:if test="${not empty access}">
					<c:if test="${access.customerListView == 'visible'}">
						<div>
							<p class="center mb-1 tkts" data-toggle="modal"
								data-target="#tktCreation" data-backdrop="static"
								data-keyboard="false">
								<img src="resources/img/tkt.png">
							</p>
							<p class="create-tkts">New Ticket</p>
						</div>


					</c:if>
				</c:if>




				<%-- <div class="dropdown pull-right hidden-md-down user-log">
					<button class="profile-dropdown-toggle" type="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<span class="thumbnail-wrapper d32 circular inline"> <img
							src="resources/img/user_01.png" alt
							data-src="resources/img/user_01.png"
							data-src-retina="resources/img/user_01.png" width="32"
							height="32">
						</span>

						<!-- <p class="user-login">ICE</p>
                   -->

						<c:if test="${not empty access}">
							<p class="user-login">${access.userName}</p>
						</c:if>



					</button>
					<div
						class="dropdown-menu dropdown-menu-right profile-dropdown exportlog"
						role="menu">
						<a class="dropdown-item disablecolor"><i
							class="pg-settings_small"></i> Settings</a> <a
							class="dropdown-item disablecolor"><i class="pg-outdent"></i>
							Feedback</a> <a class="dropdown-item disablecolor"><i
							class="pg-signals"></i> Help</a> <a href="login"
							class="dropdown-item color"><i class="pg-power"></i> Logout</a>
					</div>
				</div>
				<a href="#"
					class="header-icon pg pg-alt_menu btn-link m-l-10 sm-no-margin d-inline-block"
					data-toggle="quickview" data-toggle-element="#quickview"></a> --%>
			</div>
		</div>
		<div class="page-content-wrapper ">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item hover-idle"><a>Operation &
								Maintenance</a></li>
						<li class="breadcrumb-item"><a href="./ticketdetails">Ticket
								View</a></li>
						<li class="breadcrumb-item active">Ticket Information</li>
					</ol>
				</div>



				<div id="QuickLinkWrapper1" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="card-block sortable" id="equipmentdata"
							style="padding-top: 0px !important; margin-bottom: 30px;">
							<div class="row">
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card">
										<div class="card-header">
											<div class="card-title tbl-head ml-15">
												<i class="fa fa-ticket" aria-hidden="true"></i> Ticket
												Information
											</div>



											<c:if
												test="${ticketview.ticketType == 'Maintenance' || ticketview.ticketCategory == 'Modules Cleaning'}">

												<c:if
													test="${ticketview.ticketStatus == 4 || ticketview.ticketStatus == 5 || ticketview.state == 2 }">

													<div class="card-title tbl-head ml-15 pull-right">
														<!-- <button id="ajaxbutton" name="http://192.168.31.160:80/JsonAngularwebservices/countries/download/402/2" class="btn btn-primary ajaxbutton">Ajax Call</button> 
														<a id="ajaxbutton" name="http://192.168.31.160:80/JsonAngularwebservices/countries/download/402/2"><i class="fa fa-download" aria-hidden="true"></i> Download Reports</a>-->
														
														
														<a id="ajaxbutton"
															name="http://192.168.31.160:80/JsonAngularwebservices/countries/download/${ticketview.ticketID}/${ticketview.dayCycle}"><i
															class="fa fa-download" aria-hidden="true"></i> Download
															Reports</a> 
															
															
															<%-- <a id="ajaxbutton"
															name="http://www.inspirece.com/JsonAngularwebservices/pdf/download/${ticketview.ticketID}/${ticketview.dayCycle}"><i
															class="fa fa-download" aria-hidden="true"></i> Download
															Reports</a> --%>
															
															
													</div>

												</c:if>

											</c:if>



















										</div>
										<div class="card-block">
											<div class="col-md-12"
												style="border: 1px solid #ccc; padding: 10px; border-radius: 5px;">
												<div class="col-md-12 padd-0">
													<div class="col-md-4 col-xs-12">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">
																<b>Ticket No</b>
															</p>
														</div>
														<!-- <div class="col-md-1"><p class="tkttext">:</p></div> -->
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext m-l-15"><b>${ticketview.ticketCode}</b></span>
															</p>
															<%-- <span class="tkttext tkt-bg m-l-15"><b>${ticketview.ticketCode}</b></span> --%>

														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Priority</p>
														</div>
														<!-- <div class="col-md-1"><p class="tkttext">:</p></div> -->
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.priorityText}</span>
															</p>
															<%-- <p class="tkttext"><span class="tkttext  m-l-15">${ticketview.priorityText}</span></p> --%>
														</div>
													</div>

													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Scheduled On</p>
														</div>
														<!-- <div class="col-md-1"><p class="tkttext">:</p></div> -->
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.scheduledDateText}</span>
															</p>

														</div>
													</div>


												</div>
												<div class="col-md-12 m-t-15 padd-0">

													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Subject</p>
														</div>
														<!-- <div class="col-md-1"><p class="tkttext">:</p></div> -->
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext"
																style="white-space: nowrap; overflow: hidden; text-overflow: ellipsis;">
																: <span class="tkttext  m-l-15">${ticketview.ticketDetail}</span>
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Type</p>
														</div>
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.ticketType}</span>
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Started On</p>
														</div>
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.startedDateText}</span>
															</p>
														</div>
													</div>
												</div>
												<div class="col-md-12 m-t-15 padd-0">
													<!-- <div class="col-md-4">
                               					<div class="col-md-4"><p class="tkttext">Priority</p></div>
                               					<div class="col-md-8">
                               						<p class="tkttext">:<span class="tkttext  m-l-15">High</span></p>
                               					</div>
                               				</div> -->
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Created On</p>
														</div>
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.createdDateText}</span>
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12  mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Category</p>
														</div>
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.ticketCategory}</span>
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Closed by</p>
														</div>
														<div class="col-md-8  col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.closedByName}</span>
															</p>
														</div>
													</div>
													<!-- <div class="col-md-4">
                               					<div class="col-md-4"><p class="tkttext">Finished On</p></div>
                               					<div class="col-md-8">
                               						<p class="tkttext">:<span class="tkttext  m-l-15">21-01-2018 11:10 AM</span></p>
                               					</div>
                               				</div> -->
												</div>
												<div class="col-md-12 m-t-15 padd-0">
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Created by</p>
														</div>
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.createdByName}</span>
															</p>
														</div>
													</div>

													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Assign To</p>
														</div>
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.assignedToWhom}</span>
															</p>
														</div>
													</div>
													<div class="col-md-4 col-xs-12 mm-10">
														<div class="col-md-4 col-xs-5 padd-0">
															<p class="tkttext">Closed On</p>
														</div>
														<div class="col-md-8 col-xs-7 padd-0">
															<p class="tkttext">
																:<span class="tkttext  m-l-15">${ticketview.closedDateText}</span>
															</p>
														</div>
													</div>

												</div>
												<div class="col-md-12 m-t-15 padd-0  mm-275">

													<div class="col-md-8 padd-0">
														<div class="col-md-2 col-xs-5">
															<p class="tkttext">Description</p>
														</div>
														<div class="col-md-8 col-xs-7  padd-0 mml--3">
															<p class="tkttext">
																<span style="margin-left: 5px;">:</span><span
																	class="m-l-15">${ticketview.description}</span>
															</p>
														</div>
													</div>
												</div>

												<div class="col-md-12 m-t-15 center mm-10">
													<c:choose>
														<c:when test="${ticketview.state == 1}">
															<c:choose>
																<c:when test="${ticketview.ticketStatus == 1}">
																	<div class="col-md-3"></div>
																	<div class="col-md-3">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlAssign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt-assign.png"><span
																				class="m-l-10">Assign</span>
																		</button>
																	</div>
																	<div class="col-md-3">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	<!-- <div class="col-md-2"><button class="btn btn-primary btn-width" data-toggle="modal" data-target="#MdlClose" data-backdrop="static" data-keyboard="false"><img src="resources/img/btn-close.png"><span class="m-l-10">Close</span></button></div> -->
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 2}">
																	<div class="col-md-3"></div>
																	<div class="col-md-2">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																	</div>
																	<div class="col-md-2">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	<div class="col-md-2">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 3}">
																	<div class="col-md-3"></div>
																	<div class="col-md-3">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																	</div>
																	<div class="col-md-3">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	<!-- <div class="col-md-2"><button class="btn btn-primary btn-width" data-toggle="modal" data-target="#MdlClose" data-backdrop="static" data-keyboard="false"><img src="resources/img/btn-close.png"><span class="m-l-10">Close</span></button></div> -->
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 4}">
																	<div class="col-md-3"></div>
																	<div class="col-md-2">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																	</div>
																	<div class="col-md-2">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	<div class="col-md-2">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 5}">
																	<div class="col-md-3"></div>
																	<div class="col-md-2">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																	</div>
																	<div class="col-md-2">
																		<button class="btn btn-primary-hold btn-width"
																			data-toggle="modal" data-target="#MdlHold"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_Hold_06.png"><span
																				class="m-l-10">Hold</span>
																		</button>
																	</div>
																	<div class="col-md-2">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	<div class="col-md-3"></div>
																</c:when>
																<c:when test="${ticketview.ticketStatus == 6}">
																</c:when>
																<c:when test="${ticketview.ticketStatus == 7}">
																	<div class="col-md-3"></div>
																	<div class="col-md-3">
																		<button class="btn btn-primary-sub btn-width"
																			data-toggle="modal" data-target="#MdlReassign"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/tkt_reassign.png"><span
																				class="m-l-10">Reassign</span>
																		</button>
																	</div>
																	<div class="col-md-3">
																		<button class="btn btn-primary btn-width"
																			data-toggle="modal" data-target="#MdlClose"
																			data-backdrop="static" data-keyboard="false">
																			<img src="resources/img/btn-close.png"><span
																				class="m-l-10">Close</span>
																		</button>
																	</div>
																	<div class="col-md-3"></div>
																</c:when>
																<c:otherwise>
																</c:otherwise>
															</c:choose>


															<%-- <c:choose>
																<c:when test="${ticketview.assignedTo == null}">

																</c:when>
																
																<c:when test="${ticketview.assignedTo == ''}">

																</c:when>
																<c:otherwise>

																</c:otherwise>
															</c:choose> --%>





														</c:when>
														<c:when test="${ticketview.state == 2}">
														</c:when>
														<c:when test="${ticketview.state == 3}">
															<div class="col-md-3"></div>
															<div class="col-md-3">
																<button class="btn btn-primary-sub btn-width"
																	data-toggle="modal" data-target="#MdlReassign"
																	data-backdrop="static" data-keyboard="false">
																	<img src="resources/img/tkt_reassign.png"><span
																		class="m-l-10">Reassign</span>
																</button>
															</div>
															<div class="col-md-3">
																<button class="btn btn-primary btn-width"
																	data-toggle="modal" data-target="#MdlClose"
																	data-backdrop="static" data-keyboard="false">
																	<img src="resources/img/btn-close.png"><span
																		class="m-l-10">Close</span>
																</button>
															</div>
															<div class="col-md-3"></div>

														</c:when>
														<c:otherwise>

														</c:otherwise>
													</c:choose>





													<!--<div class="col-md-2"></div>
													 <div class="col-md-2"><button class="btn btn-primary btn-width" data-toggle="modal" data-target="#MdlClose" data-backdrop="static" data-keyboard="false"><img src="resources/img/btn-close.png"><span class="m-l-10">Close</span></button></div>
                               				<div class="col-md-2"><button class="btn btn-primary-sub btn-width" data-toggle="modal" data-target="#MdlAssign" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt-assign.png"><span class="m-l-10">Assign</span></button></div>
                               				<div class="col-md-2"><button class="btn btn-primary-sub btn-width" data-toggle="modal" data-target="#MdlReassign" data-backdrop="static" data-keyboard="false"><img src="resources/img/btn-reassign-icon.png"><span class="m-l-10">Reassign</span></button></div>
                               				<div class="col-md-2"><button class="btn btn-primary-hold btn-width" data-toggle="modal" data-target="#MdlHold" data-backdrop="static" data-keyboard="false"><img src="resources/img/tkt_Hold_06.png"><span class="m-l-10">Hold</span></button></div>
                               			 <div class="col-md-2"></div>
                               			 -->







												</div>
											</div>
										</div>
										<div class="card-block table-responsive">
											<table id="example"
												class="table table-striped table-bordered" width="100%"
												cellspacing="0">
												<thead>
													<tr>
														<th style="width: 10%;">S.No</th>
														<th style="width: 30%;">Date Time</th>
														<th style="width: 30%;">Ticket Transactions</th>
														<th style="width: 30%;">Remarks</th>
													</tr>
												</thead>
												<tbody>

													<c:forEach items="${tickethistorylist}" var="tickethistory">
														<tr>
														<td style="text-align: center;">${tickethistory.serialNo}</td>
															<td>${tickethistory.ticketDate}</td>
															<td>${tickethistory.ticketTransaction}</td>
															<td>${tickethistory.remarks}</td>
														</tr>
													</c:forEach>
												</tbody>
											</table>
										</div>
									</div>
								</div>
							</div>

						</div>
						<!-- Table End-->
					</div>
				</div>


			</div>
<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->

	


	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrapper1">Ticket Information</a></li>

								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>
	
	<div id="gotop"></div>

	<!--  Ticket Creation Popup Start -->
		<!-- Modal -->
	<div id="tktCreation" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header Popup-header">
					<button type="button" class="close" data-dismiss="modal">
						<img src="resources/img/close.png">
					</button>
					<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
					<p class="modal-title tkt-title">
						<img src="resources/img/tkt.png"><span class="tkt-title">New
							Ticket</span>
					</p>
				</div>
				<div class="modal-body">
					<c:url var="addAction" value="/addnewticket"></c:url>
					<div class="col-md-12">
						<p class="tkt-mandat">
							<span class="errormess">*</span> All fields are mandatory
						</p>
					</div>
					<form:form action="${addAction}" modelAttribute="ticketcreation"
						class="ui form validation-form">
						<div class="col-md-12 m-t-10">
							<div class="form-group required field">
								<div class="col-md-4">
									<label class="fields required m-t-10">Site</label>
								</div>
								<div class="col-md-8">
									<form:select class="ui fluid search selection dropdown width"
										id="ddlSite" name="ddlSite" path="siteID">
										<form:option value="">Select</form:option>
										<form:options items="${siteList}" />
									</form:select>
								</div>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="form-group required field">
								<div class="col-md-4">
									<label class="fields m-t-10">Type</label>
								</div>
								<div class="col-md-8">
									<form:select class="ui fluid search selection dropdown width"
										id="ddlType" name="ddlType" path="ticketType">
										<form:option value="">Select</form:option>
										<form:option value="Operation">Operation</form:option>
										<form:option value="Maintenance">Maintenance</form:option>
									</form:select>
								</div>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="form-group required field">
								<div class="col-md-4">
									<label class="fields m-t-10">Category</label>
								</div>
								<div class="col-md-8">
									<form:select class="ui fluid search selection dropdown width"
										id="ddlCategory" name="ddlCategory" path="ticketCategory">
										<form:option value="">Select </form:option>
										<form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
										<form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
										<form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
										<form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
										<form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
										<form:option data-value="Operation"
											value="Equipment Replacement">Equipment Replacement</form:option>
										<form:option data-value="Operation"
											value="Communication Issue">Communication Issue</form:option>
										<form:option data-value="Operation" value="String Down">String Down</form:option>
										<form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
										<form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
										<form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
										<form:option data-value="Maintenance"
											value="Inverter Cleaning">Inverter Cleaning</form:option>
										<form:option data-value="Maintenance"
											value="DataLogger Cleaning">DataLogger Cleaning</form:option>
										<form:option data-value="Maintenance"
											value="String Current Measurement">String Current Measurement</form:option>
										<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
										<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
										<form:option data-value="Maintenance"
											value="Visual Inspection">Visual Inspection</form:option>
										<form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
									</form:select>
								</div>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="form-group required field">
								<div class="col-md-4">
									<label class="fields m-t-10">Subject</label>
								</div>
								<div class="col-md-8">
									<form:input placeholder="Subject" name="Subject" type="text"
										id="txtSubject" path="ticketDetail" />
								</div>
							</div>
						</div>
						<div class="col-md-12 m-t-15">
							<div class="form-group required field">
								<div class="col-md-4">
									<label class="fields m-t-10">Priority</label>
								</div>
								<div class="col-md-8">
									<form:select class="ui fluid search selection dropdown width"
										id="ddlPriority" name="user" path="priority">
										<form:option value="">Select </form:option>
										<form:option data-value="3" value="3">High</form:option>
										<form:option data-value="2" value="2">Medium</form:option>
										<form:option data-value="1" value="1">Low</form:option>
									</form:select>
									</div>
							</div>
						</div>
						
						<div class="col-md-12 m-t-15">
							<div class="form-group required field">
								<div class="col-md-4">
									<label class="fields m-t-10">Description</label>
								</div>
								<div class="col-md-8">
									<form:textarea placeholder="Ticket Description"
										style="resize: none;" id="txtTktdescription"
										name="txtTktdescription" path="description"></form:textarea>
									<!--   <p class="errortext">(Maximum 150 characters are allowed)</p> -->
								</div>
							</div>
						</div>
						<div class="col-md-12 col-xs-12  m-t-15 center">
							<div class="btn btn-success  submit">Create</div>
							<div class="btn btn-primary clear m-l-15">Reset</div>
						</div>
				</div>
				<!-- <div class="ui error message"></div> -->
				</form:form>
			</div>
		</div>
	</div>
	
	<!--  Ticket Creation Popup End -->
	
	<!--  Close Ticket Popup Start -->
<c:url var="addAction1" value="/closeticket${ticketview.ticketID}"></c:url>

	<form:form action="${addAction1}" modelAttribute="ticketview"
		class="ui form" method="post">
		<form:hidden path="ticketID" />


		<!-- Close Popup Start -->
		<div id="MdlClose" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header Popup-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<h5 class="modal-title">
							<img class="mt--7" src="resources/img/tkt_close.png"><span
								class="popup-head">Close Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
					<form class="ui form">							
							<div class="col-md-12 m-t-15">
								<div class="field">
									<div class="col-lg-12">
										<form:textarea placeholder="Enter Remarks*"
											style="resize:none;" class="AssignDesc" name="CloseDescr"
											id="txtActivityDescription" path="closedRemarks"></form:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-12 center m-t-20">
							<button type="submit" class="btn btn-success submit btn-width">Confirm</button>
							<button type="button" class="btn btn-primary clear btn-width">Reset</button>
							 	 	<!-- <button type="submit" class="btn btn-success submit">Hold</button>
                                  	<button type="button" class="btn btn-primary clear" data-dismiss="modal">Cancel</button> -->
							</div>
						</form>
					</div>
					
				</div>

			</div>
		</div>
		<!--  Close Popup End -->
	</form:form>

<!--  Close Ticket Popup End -->
	
	<!--  Assign Ticket Popup Start -->
	<c:url var="addAction0" value="/assignticket${ticketview.ticketID}"></c:url>

	<form:form action="${addAction0}" modelAttribute="ticketview"
		class="ui form" method="post">
		<form:hidden path="ticketID" />

		<!-- Close Popup Start -->
		<div id="MdlAssign" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header Popup-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<h5 class="modal-title">
							<img  src="resources/img/tkt-assign.png"><span
								class="popup-head">Assign Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
						<form class="ui form assign-validation">
							<div class="col-md-12 col-xs-12 col-lg-12">
								<div class="field">
									<div class="col-lg-4 m-t-8">
										<label class="fields">Assign To<span class="errormess">*</span></label>
									</div>
									<div class="col-lg-8" style="z-index: 999999 !important;">
										<form:select
											class="ui fluid search selection dropdown width required"
											id="ddlAssign" name="descr" path="assignedTo" >
											<form:option value="" style="z-index: 999999 !important;">Select</form:option>
											<c:forEach items="${userList}" var="userlist">
												<form:option value="${userlist.userId}">${userlist.userName}</form:option>
											</c:forEach>
										</form:select>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-xs-12 col-lg-12 m-t-15">
								<div class="ui form">
									<div class="col-md-4 m-t-8">
										<label class="fields">Date<span
											class="errormess">*</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group required">
											<form:input id="AssignScheduledDate" type="text"
												placeholder="Scheduled On" class="date-picker form-control"
												name="Datetime" path="scheduledOnText" required="required"
												style="z-index:9999 !important;" />
											<label for="AssignScheduledDate"
												class="input-group-addon btn"
												style="z-index: 9999 !important;"><span
												class="glyphicon glyphicon-calendar"
												style="z-index: 9999 !important;"></span> </label>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 col-xs-12 col-lg-12  m-t-15">
								<div class="field">
									<div class="col-lg-12">
										<form:textarea placeholder="Enter Remarks*"
											style="resize:none;" class="AssignDesc" name="descr"
											id="txtActivityDescription" path="remarks"></form:textarea>
									</div>
								</div>
							</div>


							<div class="col-md-12 center m-t-15">
								<button type="submit" class="btn btn-success btn-width submit mm-10">Confirm</button>
								&nbsp;&nbsp;&nbsp;
								<button type="button" class="btn btn-primary btn-width clear mm-10">Reset</button>
							</div>
							<!-- <div class="ui blue submit button">Submit</div>
<div class="ui blue clear button">Clear</div> -->
							<!-- <div class="ui error message"></div>  -->
						</form>
					</div>

				</div>

			</div>
		</div>
</form:form>
	<!--  Assign Ticket Popup End -->
	
	<!--  Reassign Ticket Popup Start -->
	<c:url var="addAction2" value="/reassignticket${ticketview.ticketID}"></c:url>
	<form:form action="${addAction2}" modelAttribute="ticketview"
		class="ui form reassign" method="post">
		<!-- Close Popup Start -->
		<div id="MdlReassign" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header Popup-header">
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<h5 class="modal-title">
							<img  src="resources/img/tkt_reassign.png"><span
								class="popup-head">Reassign Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
						<form:hidden path="ticketID" />
						<form class="ui form assign-validation">
							<div class="col-md-12">
								<div class="field">
									<div class="col-lg-4 m-t-8">
										<label class="fields">Reassign To<span class="errormess">*</span></label>
									</div>
									<div class="col-lg-8" style="z-index: 999999 !important;">
										
										<form:select
										class="ui fluid search selection dropdown width required"
										id="ddlReassign" name="reassignTo" path="reassignedTo">
										<form:option value="">Select</form:option>
										<c:forEach items="${userList}" var="userlist">
												<form:option value="${userlist.userId}">${userlist.userName}</form:option>
											</c:forEach>
									</form:select>
									</div>
								</div>
							</div>
							<div class="col-md-12 m-t-15">
								<div class="ui form">
									<div class="col-md-4">
										<label class="fields m-t-8">Date<span
											class="errormess">*</span></label>
									</div>
									<div class="col-md-8">
										<div class="input-group required">
										<form:input id="ScheduledDate" type="text"
											placeholder="Scheduled On" class="date-picker form-control"
											name="Datetime" path="rescheduledOnText" required="required"
											style="z-index:9999 !important;" />
										<label for="ScheduledDate" class="input-group-addon btn"
											style="z-index: 9999 !important;"><span
											class="glyphicon glyphicon-calendar"
											style="z-index: 9999 !important;"></span> </label>
									</div>
									</div>
								</div>
							</div>
							<div class="col-md-12 m-t-15">
								<div class="field">
									<div class="col-lg-12">
										<form:textarea placeholder="Enter Remarks*"
											style="resize:none;" class="AssignDesc" name="reassignDescr"
											id="txtActivityDescription" path="reassignRemarks"></form:textarea>
									</div>
								</div>
							</div>


							<div class="col-md-12 center m-t-20">
								<button type="submit" class="btn btn-success btn-width submit">Confirm</button>
								&nbsp;&nbsp;&nbsp;
								<button type="button" class="btn btn-primary btn-width clear">Reset</button>
							</div>
						
						</form>
					</div>

				</div>

			</div>
		</div>
</form:form>
<!--  Reassign Popup End -->

<!-- Hold Ticket Popup Start -->
<c:url var="addAction3" value="/holdticket${ticketview.ticketID}"></c:url>
	<form:form action="${addAction3}" modelAttribute="ticketview"
		class="ui form" method="post">
		<form:hidden path="ticketID" />

		<!--  Hold Popup Start -->
		<div id="MdlHold" class="modal fade" role="dialog">
			<div class="modal-dialog modal-top modal-popup modal-md">
				<!-- Modal content-->
				<div class="modal-content" id="popupwidth">
					<div class="modal-header popup-header">
						<button type="button" class="close" data-dismiss="modal">
							<img src="resources/img/close.png">
						</button>
						<!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
						<h5 class="modal-title">
							<img  src="resources/img/tkt_Hold_06.png"><span
								class="popup-head">Hold Ticket</span>
						</h5>
					</div>
					<div class="modal-body" id="txtCloseRemarks">
						<!--  <h5 class="m-l-20">Do you want  hold this ticket?</h5> -->
						<form class="ui form">							
							<div class="col-md-12 m-t-15">
								<div class="field">
									<div class="col-lg-12">
										<form:textarea placeholder="Enter Remarks*"
											style="resize:none;" class="AssignDesc" name="descr"
											id="txtActivityDescription" path="holdRemarks"></form:textarea>
									</div>
								</div>
							</div>
							<div class="col-md-12 center m-t-20">
							 	 	<button type="submit" class="btn btn-success submit btn-width">Hold</button>
                                  	<button type="button" class="btn btn-primary clear btn-width">Reset</button>
							</div>
						</form>
			
			</div>
		</div>
		</div>
		
	</form:form>
<!-- Hold Ticket Popup End -->


	<script>
		$('#gotop').gotop({
			customHtml : '<i class="fa fa-angle-up fa-2x"></i>',
			bottom : '2em',
			right : '2em'
		});
	</script>
	<script>
		$(document).ready(function() {
			$("#ScheduledDate").datepicker({
				minDate : 0,
				dateFormat : "dd/mm/yy"
			});
			$("#AssignScheduledDate").datepicker({
				minDate : 0,
				dateFormat : "dd/mm/yy"
			});

			$(window).fadeThis({
				speed : 500,
			});
		});
	</script>


	<!-- Modal -->
	
</body>
</html>


