<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page session="true"%>


<html>
<head>
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<meta charset="utf-8" />
<title>:: Welcome To eAMPM ::</title>
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no, shrink-to-fit=no" />
<link rel="icon" type="image/x-icon" href="favicon.ico" />
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-touch-fullscreen" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="default">
<meta content name="description" />
<meta content name="author" />


<link href="resources/css/pace-theme-flash.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/bootstrap.min.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/font-awesome.css" rel="stylesheet"
	type="text/css" />
<link href="resources/css/jquery.scrollbar.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/select2.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/switchery.min.css" rel="stylesheet"
	type="text/css" media="screen" />
<link href="resources/css/pages-icons.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/animate.css" rel="stylesheet" />
<link href="resources/css/pages.css" class="main-stylesheet"
	rel="stylesheet" type="text/css" />
<link href="resources/css/styles.css" rel="stylesheet" type="text/css">
<link href="resources/css/jquerysctipttop.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/site.css" rel="stylesheet">
<link href="resources/css/semantic.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/calendar.min.css" rel="stylesheet"
	type="text/css">
<link href="resources/css/bootstrap.min.3.37.css" type="text/css"
	rel="stylesheet">
<link href="resources/css/dataTables.bootstrap.min.css" type="text/css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css"
	href="resources/css/semantic.min.css">
<link rel="stylesheet" type="text/css"
	href="resources/css/jquery-ui.css">
<link href="resources/css/formValidation.min.css" rel="stylesheet"
	type="text/css">
<!--  <link href="resources/css/jquery-ui_cal.css" rel="stylesheet" type="text/css"> -->

<link rel="stylesheet"
	href="http://code.jquery.com/ui/1.11.3/themes/smoothness/jquery-ui.css">
<script src="resources/js/jquery-1.12.4.js" type="text/javascript"></script>
<script type="text/javascript"
	src="https://cdnjs.cloudflare.com/ajax/libs/semantic-ui/2.2.7/semantic.js"></script>

<script src="resources/js/searchselect.js" type="text/javascript"></script>
<script type="text/javascript" src="resources/js/jquery-ui.js"></script>
<script type="text/javascript" src="resources/js/formValidation.min.js"></script>
<script type="text/javascript"
	src="http://formvalidation.io/vendor/formvalidation/js/framework/bootstrap.min.js"></script>
<script type="text/javascript" src="resources/js/bootstrap.min.val.js"></script>
<script type="text/javascript">
        $(document).ready(function() {
        	 //$('#example').DataTable();
        	 $('#example').DataTable( { "order": [[ 0, "desc" ]] } );
        	 $("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
       	     $("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"});
       	   
       	 $("#searchSelect").change(function() {
             var value = $('#searchSelect option:selected').val();
             var uid =  $('#hdneampmuserid').val();
           redirectbysearch(value,uid);
        }); $('.dataTables_filter input[type="search"]').attr('placeholder','search');


            if($(window).width() < 767)
				{
				   $('.card').removeClass("slide-left");
				   $('.card').removeClass("slide-right");
				   $('.card').removeClass("slide-top");
				   $('.card').removeClass("slide-bottom");
				}
            
           /*  $('.tkts').click(function(){
            	
            	 $("input").trigger("select");
            }) */
            
            $('.clr').click(function(){
           	 debugger;
           	 $("#txtFromDate").val('');
           	 $("#txtToDate").val('');
           	 $(".text").empty();
           	 $(".text").addClass('default')
           	 $(".text").html("Select");
           	 $('#ddlSite option:selected').removeAttr('selected');
           	 $('#ddlState option:selected').removeAttr('selected');
           	 $('#ddlCate option:selected').removeAttr('selected');
           	 $('#ddlPriority option:selected').removeAttr('selected'); 
           	window.location.href = './ticketdetails';
           	//location.reload();
           	   });
            
            
            $('.close').click(function(){
				$('.clear').click();
			});
			$('.clear').click(function(){
			    $('.search').val("");
			});

			$('body').click(function(){
            	$('#builder').removeClass('open'); 
         	 });

        });
      </script>

  
    
      <script type='text/javascript'>//<![CDATA[
         $(window).load(function(){
        	 $('.ui.dropdown').dropdown({forceSelection:false});
         $.fn.dropdown.settings.selectOnKeydown = false;
         $.fn.dropdown.settings.forceSelection = false;
         
         $('.ui.dropdown').dropdown();
         var $ddlType = $( '#ddlType' ),
            $ddlCategory = $( '#ddlCategory' ),
            $options = $ddlCategory.find( 'option' );     
            $ddlType.on( 'change', function() {
            $ddlCategory.html( $options.filter( '[data-value="' + this.value + '"]' ) );
         }).trigger( 'change' );//]]> 



             $('.validation-form')
   .form({
     on: 'blur',
     onFailure: function(formErrors, fields) {
       $.each(fields, function(e) {
         var ele = $('#' + e);
         var fail = (ele.val() === false || ele.val().length === 0);
         if (fail) {
           console.log(ele, fail);
           ele.focus();
           $('html,body').animate({
             scrollTop: ele.offset().top - ($(window).height() - ele.outerHeight(true)) / 2
           }, 200);
           return false;
         }
       });
       return false;
     },
     fields: {
    	 txtSubject: {
         identifier: 'txtSubject',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
        ddlSite: {
         identifier: 'ddlSite',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
       },
      ddlType: {
            identifier: 'ddlType',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
      ddlCategory: {
         identifier: 'ddlCategory',
         rules: [{
           type: 'empty',
           prompt: 'Please enter a value'
         }]
      },
       ddlPriority: {
         identifier: 'ddlPriority',
         rules: [{
           type: 'empty',
           prompt: 'Please select a dropdown value'
         }]
       },
       description: {
         identifier: 'description',
         rules: [{
           type: 'empty',
           prompt: 'Please Enter Description'
         }]
       },
       
     }
   }); 

         })
         
      </script>
<style type="text/css">
.dropdown-menu-right {
	left: -70px !important;
}
 	div.dataTables_wrapper div.dataTables_length select {
			    width: 50px;
			    display: inline-block;
			    margin-left: 3px;
			    margin-right: 4px;
			}
			
			.ui.form textarea:not([rows]) {
    height: 4em;
    min-height: 4em;
    max-height: 24em;
}
.ui.icon.input > i.icon:before, .ui.icon.input > i.icon:after {
    left: 0;
    position: absolute;
    text-align: center;
    top: 36%;
    width: 100%;
}


.input-sm, .form-horizontal .form-group-sm .form-control {
    font-size: 13px;
    min-height: 25px;
    height: 32px;
    padding: 8px 9px;
}
  
#example td a {
	color: #337ab7;
	font-weight: bold;
}
.dt-button-background {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	z-index: 0 !important;
}

.tbh-width {
	width: 150px;
	padding-right: 30px !important;
}

.dt-buttons {
	margin-left: 50px;
}

.buttons-collection {
	height: 28px;
	padding: 3px;
	/*  z-index:9999; */
}


.buttons-excel {
	background: rgba(36, 95, 68, 0.87);
	color: #FFF;
}

.buttons-pdf {
	background: rgb(98, 165, 96);
	color: #FFF;
}

.dataTables_filter {
	float: right;
}

.dataTables_length {
	float: left;
}


.ui.selection.dropdown {
    width: 100%;
    margin-top: 0.1em;
   /*  height: 2.3em; */
}

.ui.selection.dropdown .menu {
    width: 100%;
    white-space: normal !important;
}

.ui.selection.dropdown .menu .item:first-child {
    border-top: 1px solid #ddd !important;
    min-height: 2.8em;
}

.ui.selection.dropdown .text {
    overflow: hidden;
    text-overflow: ellipsis;
    white-space: nowrap;
    width: 90%;
}

.ui.selection.dropdown .icon {
    text-align: right;
    margin-left: 7px !important;
}







</style>
<script type="text/javascript">
$(document).ready(function() {
  $("#txtFromDate").datepicker({ dateFormat: "dd/mm/yy"});
$("#txtToDate").datepicker({ dateFormat: "dd/mm/yy"});


    $('#profileForm')
        .bootstrapValidator({
            framework: 'bootstrap',
            fields: {
                fromDate: {
                    validators: {
                        notEmpty: {
                            message: 'Select from date'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            message: 'The is not valid date'
                        }
                    }
                },
                 toDate: {
                    validators: {
                        notEmpty: {
                            message: 'Select From Date'
                        },
                        date: {
                            format: 'DD/MM/YYYY',
                            message: 'The is not valid date'
                        }
                    }
                }
            }
        })
        .find('[name="fromDate"]')
            .datepicker({
                onSelect: function(date, inst) {
                    $('#profileForm').formValidation('revalidateField', 'fromDate');
                }
            });


 $('#profileForm').bootstrapValidator({
            framework: 'bootstrap',
        })
            .find('[name="toDate"]')
            .datepicker({
                onSelect: function(date, inst) {
                    $('#profileForm').formValidation('revalidateField', 'toDate');
                }
            });

  });

</script>
</head>
<body class="fixed-header">

	<input type="hidden" value="${access.userID}" id="hdneampmuserid">


	<nav class="page-sidebar" data-pages="sidebar">

		<div class="sidebar-header">
			<a href="./dashboard"><img src="resources/img/logo01.png" class="m-t-10"
				alt="logo"
				title="Electronically Assisted Monitoring and Portfolio Management"></a>
			<i class="fa fa-bars bars"></i>
		</div>

		<div class="sidebar-menu">


			<c:if test="${not empty access}">

				<ul class="menu-items">



					<c:if test="${access.dashboard == 'visible'}">

						<li class="" style="margin-top: 2px;"><a
							href="./dashboard" class="detailed"> <span
								class="title">Dashboard</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-tachometer"></i></span>
						</li>
					</c:if>

					<c:if test="${access.overview == 'visible'}">
						<li><a href="#"><span class="title">Overview</span></a> <span
							class="icon-thumbnail"><i class="fa fa-list-alt"></i></span></li>
					</c:if>

					<c:if test="${access.systemMonitoring == 'visible'}">
						<li><a href="#"><span class="title">System
									Monitoring</span></a> <span class="icon-thumbnail"><i
								class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.visualization == 'visible'}">
						<li><a href="javascript:;"><span class="title">Visualization</span>
								<span class=" arrow"></span></a> <span class="icon-thumbnail"><i
								class="fa fa-area-chart"></i></span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li><a href="./customerlist">Customer View</a>
										<span class="icon-thumbnail"><i class="fa fa-eye"></i></span>
									</li>
								</c:if>



								<li><a href="./sitelist">Site View</a> <span
									class="icon-thumbnail"><i class="fa fa-map-marker"></i></span>
								</li>
								<li><a href="./equipmentlist">Equipment View</a>
									<span class="icon-thumbnail"><i class="fa fa-cog"></i></span></li>
							</ul></li>

					</c:if>

					<c:if test="${access.analytics == 'visible'}">
						<li><a href="#"> <span class="title">Analytics</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-pie-chart"></i></span>
						</li>
					</c:if>

					<c:if test="${access.portfolioManagement == 'visible'}">
						<li><a href="#"> <span class="title">Portfolio
									Manager</span>
						</a> <span class="icon-thumbnail"><i class="fa fa-briefcase"></i></span>
						</li>
					</c:if>

					<c:if test="${access.ticketing == 'visible'}">
						<li class="active"><a href="javascript:;"><span class="title">Operation
									& Maintenance</span> <span class=" arrow"></span></a> <span class="icon-thumbnail" style="padding-top:5px !important;"><img src="resources/img/maintance.png"></span>
							<ul class="sub-menu">

								<c:if test="${access.customerListView == 'visible'}">
									<li class="active"><a href="./ticketdetails">Ticket
											View</a> <span class="icon-thumbnail"><i
											class="fa fa-ticket"></i></span></li>
								</c:if>

							</ul></li>





					</c:if>




					<c:if test="${access.forcasting == 'visible'}">
						<li><a href="#"><span class="title">Forecasting</span></a> <span
							class="icon-thumbnail"><i class="fa fa-desktop"></i></span></li>
					</c:if>

					<c:if test="${access.configuration == 'visible'}">

						<li><a href="javascript:;"><span class="title">Configuration</span>
								<span class=" arrow"></span></a> <span class="icon-thumbnail"><i
								class="pg-tables"></i></span>
							<ul class="sub-menu">

								<li><a href="./customers">Customer Config.</a> <span
									class="icon-thumbnail">CU</span></li>

								<li><a href="./sites">Site Config.</a> <span
									class="icon-thumbnail">SI</span></li>

								<li><a href="./equipments">Equipment Config.</a>
									<span class="icon-thumbnail">EQ</span></li>

								<li><a href="#">Data Logger Config.</a> <span
									class="icon-thumbnail">DL</span></li>

								<li><a href="#">Equ-Attrib Config.</a> <span
									class="icon-thumbnail">EA</span></li>

								<li><a href="#">Configuration Loader</a> <span
									class="icon-thumbnail">CL</span></li>

								<li><a href="./activities">Activity Config.</a>
									<span class="icon-thumbnail">AC</span></li>

								<li><a href="./timezones">Timezone Config.</a> <span
									class="icon-thumbnail">TZ</span></li>

								<li><a href="./currencies">Currency Config.</a>
									<span class="icon-thumbnail">CY</span></li>

								<li><a href="./unitofmeasurements">Unit
										Measurement Config.</a> <span class="icon-thumbnail">UM</span></li>


								<li><a href="./countryregions">Country
										Region Config.</a> <span class="icon-thumbnail">CR</span></li>

								<li><a href="./countries">Country Config.</a> <span
									class="icon-thumbnail">CO</span></li>

								<li><a href="./states">State Config.</a> <span
									class="icon-thumbnail">SE</span></li>

								<li><a href="./customertypes">Customer Type
										Config.</a> <span class="icon-thumbnail">CT</span></li>

								<li><a href="./sitetypes">Site Type Config.</a>
									<span class="icon-thumbnail">ST</span></li>

								<li><a href="./equipmentcategories">Equ
										Category Config.</a> <span class="icon-thumbnail">EC</span></li>

								<li><a href="./equipmenttypes">Equ Type
										Config.</a> <span class="icon-thumbnail">ET</span></li>

								<li><a href="#">Event Type Config.</a> <span
									class="icon-thumbnail">EY</span></li>

								<li><a href="#">Event Config.</a> <span
									class="icon-thumbnail">EV</span></li>

								<li><a href="#">Inspection Config.</a> <span
									class="icon-thumbnail">IN</span></li>

								<li><a href="#">User Role Config.</a> <span
									class="icon-thumbnail">UR</span></li>

								<li><a href="#">User Config.</a> <span
									class="icon-thumbnail">US</span></li>

							</ul></li>

					</c:if>










				</ul>


			</c:if>



			<div class="clearfix"></div>
		</div>



	</nav>


	<div class="page-container">
		<div class="header">
			<a href="#" class="btn-link toggle-sidebar hidden-lg-up pg pg-menu"
				data-toggle="sidebar"> </a>
			<div>
				<div class="brand inline">
					 <a href="./dashboard"><img src="resources/img/logo01.png" alt="logo"
						title="Electronically Assisted Monitoring and Portfolio Management"
						class="logos"></a>
				</div>
			</div>
			<div class="d-flex align-items-center">
				<div class="form-group required field searchbx tktSearch" id="SearcSElect">
                           <select class="ui fluid search selection dropdown width oveallSearch" id="searchSelect" style="height:30px;"">
                             <!--  <option value="-2"></option> -->
                              <option value="-1">search </option>
                              <option value="0">Dashboard</option>
                                
                        <c:if test="${not empty access}">
                              <c:if test="${access.customerListView == 'visible'}">
                                 <option value="1">Customer View</option>
                              </c:if>
                           </c:if>   
                             
                              <option value="3">Site View</option>
                              <option value="5">Equipment View</option>
                              
                               <c:if test="${access.customerListView == 'visible'}">
                             <option value="6">Ticket View</option>
                           </c:if>
           
           
           
                           </select>
                        </div>
				
				 <div class="dropdown pull-right hidden-md-down user-log">
                  <button class="profile-dropdown-toggle" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                  <span class="thumbnail-wrapper d32 circular inline">
                  <img src="resources/img/user_01.png" alt data-src="resources/img/user_01.png" data-src-retina="resources/img/user_01.png" width="32" height="32">
                  </span>
                  
                   <c:if test="${not empty access}">         
                              <p class="user-login">${access.userName}</p>
                  </c:if>
                  </button>
                   <div class="dropdown-menu dropdown-menu-right profile-dropdown" role="menu">
                     <a class="dropdown-item disablecolor" ><i class="pg-settings_small"></i> Settings</a>
                     <a class="dropdown-item disablecolor"><i class="pg-outdent"></i> Feedback</a>
                     <a class="dropdown-item disablecolor"><i class="pg-signals"></i> Help</a>
                     <a href="login" class="dropdown-item color logout"><i class="pg-power"></i> Logout</a>
                  </div>
               </div>
               
               	<c:if test="${not empty access}">
							<c:if test="${access.customerListView == 'visible'}">
								<div>
								  <span data-toggle="modal" data-target="#tktCreation" data-backdrop="static" data-keyboard="false"><p class="center m-t-5 tkts"><img class="m-t-5" src="resources/img/tkt.png"><p class="create-tkts">New Ticket</p></p></span>
								
               </div>
               
							</c:if>
						</c:if>
						
						
						
              
			
				
			</div>
		</div>
		<div class="page-content-wrapper" id="QuickLinkWrapper1">

			<div class="content ">

				<div class="container-fixed-lg">
					<ol class="breadcrumb">
						<li class="breadcrumb-item"><a href="./dashboard"><i
								class="fa fa-home mt-7"></i></a></li>
						<li class="breadcrumb-item hover-idle"><a>Operation &
								Maintenance</a></li>

						<li class="breadcrumb-item active">Ticket View</li>
					</ol>
				</div>



				<div id="newconfiguration" class="">
					<div class="card card-transparent mb-0">
						<!-- Table Start-->
						<div class="card-block sortable" id="equipmentdata" style="padding-top:0px !important;padding-bottom:0px !important;">
							<div class="row">
								<!-- <div class="card-header">
                                    <div class="card-title tbl-head ml-15">Equipment Curing Data</div>
                                 </div> -->
								<div class="col-lg-12 col-xs-12 col-md-12"
									id="datadetails">

									<div class="card card-default bg-default" data-pages="card">
										<div class="card-header">
											<div class="card-title tbl-head ml-15">
												<i class="fa fa-filter" aria-hidden="true"></i> Ticket
												Filters
											</div>
										</div>

										<c:url var="viewAction" value="/ticketdetails"></c:url>

										<form:form action="${viewAction}"
											modelAttribute="ticketfilter" class="ui form form-horizontal"
											 method="post">
											<div class="col-md-12">
		<!-- 										 <div class="form-group">
        <label class="col-md-3 control-label">From Date</label>
        <div class="col-md-3">
         <input type="text" id="txtFromDate" placeholder="From Date" class="date-picker form-control" name="fromDate" />
            <label for="txtFromDate" class="input-group-addon btn">
            <span class="glyphicon glyphicon-calendar"></span> </label>
        </div>
       
        
    </div> -->

<!--     <div class="form-group">
        <label class="col-md-3 control-label">To Date</label>
        <div class="col-md-3">
            <input type="text" placeholder="To Date"" id="txtToDate" class="form-control" name="toDate" />
            <label for="txtToDate" class="input-group-addon btn">
            <span class="glyphicon glyphicon-calendar"></span> </label>
        </div>
    </div> -->

    <!-- <div class="form-group">
        <div class="col-md-3">
            <button type="submit" class="btn btn-default">Validate</button>
            <button type="submit" class="btn btn-Primary">Clear</button>
        </div>
    </div> -->
											</div>
											<div class="col-md-12 mt-30">
											
											<div class="col-lg-2">
													<div class="txtwidth">
														<label class="fields ml-2">From Date <span class="errormess">*</span></label>
														<div class="controls">
															<div class="input-group">
																<form:input id="txtFromDate" type="text"
																	placeholder="From Date"
																	class="date-picker form-control" name="fromDate"
																	path="fromDate" required="required"/>
																<label for="txtFromDate" class="input-group-addon btn"><span
																	class="glyphicon glyphicon-calendar"></span> </label>
															</div>
														</div>
													</div>
												</div>


												<div class="col-lg-2 uitlity ">
													<div class="required  txtwidth">
														<label class="fields ml-2">To Date <span class="errormess">*</span></label>
														<div class="controls">
															<div class="input-group">
																<form:input id="txtToDate" type="text"
																	placeholder="To Date" class="date-picker form-control"
																	name="toDate" path="toDate" required="required" />
																<label for="txtToDate" class="input-group-addon btn"><span
																	class="glyphicon glyphicon-calendar"></span> </label>
															</div>
														</div>
													</div>
												</div> 
												
												
												<div class="col-lg-2 uitlity">
													<div class="field">
														<label class="fields ml-2">Site</label>
														<form:select
															class="ui fluid search selection dropdown width ddLPtype"
															id="ddlSite" path="siteID">
															<form:option value="">Select</form:option>
															<form:options items="${siteList}"></form:options>
														</form:select>
													</div>
												</div>

												<div class="col-lg-2 mm-60">
													<div class="field">
														<label class="fields ml-2">State</label>
														<form:select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlState" id="ddlState" path="state">
															<form:option value="">Select </form:option>
															<form:option  value="1">Open</form:option>
															<form:option value="2">Closed</form:option>
															<form:option  value="3">Hold</form:option>
														</form:select>
													</div>
												</div>


												<div class="col-lg-2 mm-60 ">
													<div class="field">
														<label class="fields ml-2">Category</label>
														<form:select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="ddlCate" path="ticketCategory">
															<form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
														</form:select>
														<%-- <form:select
															class="ui fluid search selection dropdown width ddLPtype"
															name="ddlCategory" id="ddlCategory" path="ticketType">
															
															
															
															  <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                         
                         
                         
														</form:select> --%>
													</div>
												</div>



												<div class="col-lg-2 mm-60 ">
													<div class="field">
														<label class="fields ml-2">Priority</label>
														<form:select
															class="ui fluid search selection dropdown width ddLPtype"
															id="ddlPriority" name="ddlPriority" path="priority">
															<form:option value="">Select </form:option>
															<form:option value="3">High</form:option>
															<form:option value="2">Medium</form:option>
															<form:option value="1">Low</form:option>
														</form:select>

													</div>
												</div>
											</div>

											<div class="col-md-12 mm-60">
												<div class="m-t-20 center">
												
											
					<button type="submit" id="profileForm" class="btn btn-success btn-width submit m-t-5"><b>View</b></button>
										<button  type="button"	class="btn btn-primary btn-width m-t-5 clr"><b>Clear</b></button>
											
											<!-- <input type="button" value="Clear" class="btn btn-primary btn-width clear m-t-5 text-clr"> -->

												</div>
											</div>


								
										</form:form>
									</div>
								</div>


							</div>

						</div>


						<!-- Table End-->

						<!-- Table Start-->
						<div class="card-block sortable m-b-30" id="equipmentdata" style="padding-top:0px !important;padding-bottom:0px !important;">
							<div class="row">								
								<div class="col-lg-12 col-xs-12 col-md-12" id="datadetails">
									<div class="card card-default bg-default" data-pages="card">
										<div class="card-header">
											<div class="card-title tbl-head ml-15">
												 <i class="fa fa-list" aria-hidden="true"></i> List of Tickets
											</div>
										</div>
										<div class="card-block table-responsive">
                                        <table id="example" class="table table-striped table-bordered"
                                            width="100%" cellspacing="0">
                                            <thead>
                                                <tr>
                                                    <th style="width:9%;">Ticket No</th>
                                                    <th style="width:10%;">Site</th>
                                                    <th style="width:8%;">Priority</th>
                                                    <th style="width:14%;">Created Time</th>
                                                    <th style="width:12%;">Category</th>
                                                    <th style="width:10%;">Subject</th>
                                                    <th style="width:10%;">Assign To</th>
                                                    <th style="width:12%;">Scheduled On</th>
                                                    <th style="width:8%;">State</th>
                                                    <th style="width:10%;">Status</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${ticketdetaillist}" var="objticketdetail">
                                                    <tr>
                                                        <td><a
                                                            href=".\ticketviews${objticketdetail.ticketID}">${objticketdetail.ticketCode}</a></td>
                                                        <td>${objticketdetail.siteName}</td>


                                                        <c:choose>
                                                            <c:when test="${objticketdetail.priority == 1}">
                                                                <td>Low</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.priority == 2}">
                                                                <td>Medium</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.priority == 3}">
                                                                <td>High</td>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <td>Low</td>
                                                            </c:otherwise>
                                                        </c:choose>


                                                        <td>${objticketdetail.createdDateText}</td>

                                                        <td>${objticketdetail.ticketCategory}</td>
                                                        <td>${objticketdetail.ticketDetail}</td>

                                                        <td>${objticketdetail.assignedToWhom}</td>
                                                        <td>${objticketdetail.scheduledDateText}</td>



                                                        <c:choose>

                                                            <c:when test="${objticketdetail.state == 1}">
                                                                <td>Open</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.state == 2}">
                                                                <td>Closed</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.state == 3}">
                                                                <td>Hold</td>
                                                            </c:when>

                                                            <c:otherwise>
                                                                <td>-</td>
                                                            </c:otherwise>
                                                        </c:choose>





                                                        <c:choose>
                                                            <c:when test="${objticketdetail.ticketStatus == 1}">
                                                                <td>Created</td>
                                                            </c:when>
                                                            
                                                            <c:when test="${objticketdetail.ticketStatus == 2}">
                                                                <td>Assigned</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 3}">
                                                                <td>Inprogress</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 4}">
                                                                <td>Unfinished</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 5}">
                                                                <td>Finished</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 6}">
                                                                <td>Closed</td>
                                                            </c:when>
                                                            <c:when test="${objticketdetail.ticketStatus == 7}">
                                                                <td>Hold</td>
                                                            </c:when>

                                                            <c:otherwise>
                                                                <td>-</td>
                                                            </c:otherwise>
                                                        </c:choose>


                                                        <!--  <td class="center"><a data-toggle="modal" class="btn btn-primary btn-color" data-target="#Close" data-backdrop="static" data-keyboard="false">Close</a></td> -->
                                                        <!-- <td><i class="fa fa-times m-l-10" data-toggle="modal" data-target="#Close" aria-hidden="true"></i></td> -->
                                                    </tr>
                                                </c:forEach>



                                            </tbody>
                                        </table>
                                    </div>
										
									

										
									</div>
								</div>


							</div>

						</div>
						

						<!-- Table End-->
					</div>
				</div>


			</div>








































































































			<div class="container-fixed-lg footer mb-0">
				<div class="container-fluid copyright sm-text-center">
					<p class="small no-margin pull-left sm-pull-reset">
						<span class="hint-text">Copyright &copy; 2017.</span> <span>INSPIRE
							CLEAN ENERGY</span>. <span class="hint-text">All rights reserved.
						</span>
					</p>
					<p class="small no-margin pull-rhs sm-pull-reset">
						<span class="hint-text">Powered by</span> <span>MESTECH
							SERVICES PVT LTD</span>.
					</p>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
	</div>

	<!-- Side Bar Content Start-->

	


	<div class="quickview-wrapper  builder hidden-sm hidden-xs"
		id="builder">
		<div class="p-l-10 p-r-10 ">
			<a class="builder-close quickview-toggle pg-close"></a>
			<a class="builder-toggle" data-toggle="quickview"
				data-toggle-element="#builder"><img
				src="resources/img/ImageResize_06.png"></a>
			<ul class="nav nav-tabs nav-tabs-simple nav-tabs-primary"
				id="builderTabs" role="tablist">
				<li class="nav-item"><a class="nav-link active"
					data-toggle="tab" href="#tabLayouts" role="tab"
					aria-controls="home"><span>Quick Link</span></a></li>
			</ul>
			<div class="tab-content">
				<div class="tab-pane active " id="tabLayouts" role="tabcard">
					<div class="scrollable">
						<div class="p-r-50">
							<div class="list">
								<ul>
									<li><a href="#QuickLinkWrapper1">Ticket Filters</a></li>
									<li><a href="#QuickLinkWrapper2">List Of Tickets</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>






	<script src="resources/js/pace.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.gotop.js"></script>
	<script src="resources/js/modernizr.custom.js" type="text/javascript"></script>
	<script src="resources/js/jquery-ui.min.js" type="text/javascript"></script>
	<script src="resources/js/tether.min.js" type="text/javascript"></script>
	<script src="resources/js/bootstrap.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery-easy.js" type="text/javascript"></script>
	<script src="resources/js/jquery.unveil.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.ioslist.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.actual.min.js"></script>
	<script src="resources/js/jquery.scrollbar.min.js"></script>
	<script type="text/javascript" src="resources/js/select2.full.min.js"></script>
	<script type="text/javascript" src="resources/js/classie.js"></script>
	<script src="resources/js/switchery.min.js" type="text/javascript"></script>
	<script src="resources/js/pages.min.js"></script>
	<script src="resources/js/card.js" type="text/javascript"></script>
	<script src="resources/js/scripts.js" type="text/javascript"></script>
	<script src="resources/js/demo.js" type="text/javascript"></script>
	<script src="resources/js/jquery.easing.min.js"></script>
	<script src="resources/js/jquery.fadethis.js"></script>


	<script src="resources/js/jquery.dataTables.min.js"
		type="text/javascript"></script>
	<script src="resources/js/dataTables.bootstrap.min.js"
		type="text/javascript"></script>
	<script src="resources/js/semantic.min.js" type="text/javascript"></script>
	<script src="resources/js/calendar.min.js" type="text/javascript"></script>
	<script src="resources/js/jquery.selectlistactions.js"></script>









	<div id="gotop"></div>




	<!-- Address Popup Start !-->
	<!-- Modal -->
	<div id="myModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Address Notification Bar</h4>
				</div>
				<div class="modal-body popup-hei">
					<div class="col-md-3 add-list">
						<ul>
							<li class="recent">Recent Events</li>
							<li>Recent Events</li>
							<li>Recent Events</li>
							<li>Recent Events</li>
							<li>Recent Events</li>
						</ul>
					</div>
					<div class="col-md-9">
						<div class="data-cont">
							<p class="add-head">
								<i class="fa fa-chat fa-2x"></i>Initial Comment
							</p>
							<div class="row">
								<div class="col-md-3">
									<p class="add-text">
										Recommeneded<br> Action
									</p>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<div class="ui form">
											<div class="field txt-box">
												<textarea placeholder="Recommeneded Action"
													style="resize: none;"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<p class="add-text">Assign To</p>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<div class="ui form">
											<div class="field txt-box">
												<textarea placeholder="Assign To" style="resize: none;"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-md-3">
									<p class="add-text">Date</p>
								</div>
								<div class="col-md-9">
									<div class="form-group">
										<div class="ui calendar width" id="example2">
											<div class="ui input left icon">
												<i class="calendar icon"></i> <input type="text"
													placeholder="Date" class="width">
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-12 data-deta">
						<div class="row mb-10">
							<div class="col-md-2">
								<p class="add-text">Equipement Id</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">#31</p>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
								<p class="add-text">Equipement Name</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">#38</p>
							</div>
						</div>
						<div class="row mb-10">
							<div class="col-md-2">
								<p class="add-text">Site Name</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">#31</p>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
								<p class="add-text">Site Code</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">#38</p>
							</div>
						</div>
						<div class="row mb-10">
							<div class="col-md-2">
								<p class="add-text">Customer</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">#31</p>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
								<p class="add-text">Event Status</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">#38</p>
							</div>
						</div>
						<div class="row mb-10">
							<div class="col-md-2">
								<p class="add-text">Event Description</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">#31</p>
							</div>
							<div class="col-md-1"></div>
							<div class="col-md-2">
								<p class="add-text">Event Date / Time</p>
							</div>
							<div class="col-md-1">
								<p class="add-text">:</p>
							</div>
							<div class="col-md-2">
								<p class="add-text">23-10-2017 / 1:00</p>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>

		</div>
	</div>
	<!-- Address Popup End !-->



	<!-- Comment Popup Start !-->
	<!-- Modal -->
	<div id="myCommentModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Comment Bar</h4>
				</div>
				<div class="modal-body popup-hei">
					<div class="col-md-4">
						<div class="cmt-list">
							<p class="add-head">
								<i class="fa fa-chat fa-2x"></i>Events
							</p>
							<ul>
								<li>
									<p class="one-of-two">
										<span class="text-warning fs-12">#31</span>
									</p>
									<p class="two-of-two p-l-10 fs-12">
										<span class="text-master">Webdysun </span> <span
											class="text-warning pull-right">20-07-2017</span> <br> <span
											class="text-masters">Site Name</span> <span
											class="text-warning pull-right">15:15</span> <br> <span
											class="text-masterse">Datalogger webdysun n'est plus
											alimente </span>
									</p>
								</li>

								<li>
									<p class="one-of-two">
										<span class="text-warning fs-12">#31</span>
									</p>
									<p class="two-of-two p-l-10 fs-12">
										<span class="text-master">Webdysun </span> <span
											class="text-warning pull-right">20-07-2017</span> <br> <span
											class="text-masters">Site Name</span> <span
											class="text-warning pull-right">15:15</span> <br> <span
											class="text-masterse">Datalogger webdysun n'est plus
											alimente </span>
									</p>
								</li>

								<li>
									<p class="one-of-two">
										<span class="text-warning fs-12">#31</span>
									</p>
									<p class="two-of-two p-l-10 fs-12">
										<span class="text-master">Webdysun </span> <span
											class="text-warning pull-right">20-07-2017</span> <br> <span
											class="text-masters">Site Name</span> <span
											class="text-warning pull-right">15:15</span> <br> <span
											class="text-masterse">Datalogger webdysun n'est plus
											alimente </span>
									</p>
								</li>
								<li>
									<p class="one-of-two">
										<span class="text-warning fs-12">#31</span>
									</p>
									<p class="two-of-two p-l-10 fs-12">
										<span class="text-master">Webdysun </span> <span
											class="text-warning pull-right">20-07-2017</span> <br> <span
											class="text-masters">Site Name</span> <span
											class="text-warning pull-right">15:15</span> <br> <span
											class="text-masterse">Datalogger webdysun n'est plus
											alimente </span>
									</p>
								</li>
								<li>
									<p class="one-of-two">
										<span class="text-warning fs-12">#31</span>
									</p>
									<p class="two-of-two p-l-10 fs-12">
										<span class="text-master">Webdysun </span> <span
											class="text-warning pull-right">20-07-2017</span> <br> <span
											class="text-masters">Site Name</span> <span
											class="text-warning pull-right">15:15</span> <br> <span
											class="text-masterse">Datalogger webdysun n'est plus
											alimente </span>
									</p>
								</li>
								<li>
									<p class="one-of-two">
										<span class="text-warning fs-12">#31</span>
									</p>
									<p class="two-of-two p-l-10 fs-12">
										<span class="text-master">Webdysun </span> <span
											class="text-warning pull-right">20-07-2017</span> <br> <span
											class="text-masters">Site Name</span> <span
											class="text-warning pull-right">15:15</span> <br> <span
											class="text-masterse">Datalogger webdysun n'est plus
											alimente </span>
									</p>
								</li>
							</ul>
						</div>

					</div>
					<div class="col-md-8">
						<div class="data-cont">
							<p class="add-head">
								<i class="fa fa-chat fa-2x"></i>Events Comment
							</p>
							<div class="conte">
								<div class="row border">
									<div class="col-lg-3 col-md-3 col-xs-3 center">
										<i class="fa fa-user fa-4x"></i>
									</div>
									<div class="col-md-9 col-xs-9 col-lg-9">
										<div class="row">
											<div class="col-md-6 col-xs-6 col-lg-6">
												<p class="pull-left">User Name</p>
											</div>
											<div class="col-md-6 col-xs-6 col-lg-6">
												<p class="pull-right">
													<i class="fa fa fa-clock-o"></i> 12 Mins ago..
												</p>
											</div>
										</div>
										<div class="cmtcontent">
											<p>
												<strong>Initial Comment :</strong><br>Lorem Ipsum is
												simply dummy text of the printing and typesetting industry.
												Lorem Ipsum has been the industry's standard dummy text ever
												since the 1500s
											</p>
										</div>
									</div>
								</div>
								<div class="hr-line"></div>
								<div class="row border">
									<div class="col-md-11 col-xs-11 col-lg-11">
										<div class="row">
											<div class="col-md-6 col-xs-6 col-lg-6">
												<p class="pull-left ml">User Name</p>
											</div>
											<div class="col-md-6 col-xs-6 col-lg-6">
												<p class="pull-right ml">
													<i class="fa fa fa-clock-o"></i> 12 Mins ago..
												</p>
											</div>
										</div>
										<div class="padd">
											<p>Lorem Ipsum is simply dummy text of the printing and
												typesetting industry.</p>
										</div>
									</div>
									<div class="col-md-1 col-xs-1 col-lg-1 center">
										<i class="fa fa-user fa-3x m-t-15"></i>
									</div>
								</div>
								<div class="hr-line"></div>
								<div class="row border">
									<div class="col-lg-2 col-md-2 col-xs-2 center">
										<i class="fa fa-user fa-3x m-t-15"></i>
									</div>
									<div class="col-lg-10 col-md-10 col-xs-10">
										<div class="row">
											<div class="col-md-6 col-xs-6 col-lg-6">
												<p class="pull-left ml">User Name</p>
											</div>
											<div class="col-md-6 col-xs-6 col-lg-6">
												<p class="pull-right ml">
													<i class="fa fa fa-clock-o"></i> 12 Mins ago..
												</p>
											</div>
										</div>
										<div class="padd">
											<p>Lorem Ipsum is simply dummy text of the printing and
												typesetting industry.</p>
										</div>
									</div>
								</div>
							</div>
							<div class="hr-line"></div>
							<div class="row">
								<div class="col-md-9 col-lg-9 col-xs-7">
									<form class="ui form mt-15 m-l-20">
										<div class="field">
											<input name="first-name" placeholder="Customer Name"
												type="text">
										</div>
									</form>
								</div>
								<div class="col-md-1 col-lg-1 col-xs-1 m-t-15">
									<input type="button" class="btn btn-success" value="Send"
										name="">
								</div>
							</div>

						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success">Save</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>

		</div>
	</div>
	<!-- Comment Popup End !-->


	<!-- Share Popup Start !-->
	<!-- Modal -->
	<div id="myShareModal" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top modal-lg">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Share</h4>
				</div>
				<div class="modal-body popup-hei">
					<form class="ui form">
						<div class="field">
							<label class="fields">Subject</label> <input name="first-name"
								placeholder="Subject" type="text" class="txt-padd">
						</div>
					</form>
					<form class="ui form mt-15">
						<div class="field">
							<label class="fields">Additional Notes</label> <input
								name="first-name" placeholder="Additional Notes" type="text"
								class="txt-padd">
						</div>
					</form>
					<div class="row style-select">
						<div class="col-md-12 mt-15">
							<div class="subject-info-box-1">
								<!-- <label class="fields">Available Superheroes</label> -->
								<select multiple class="form-control" id="lstBox1">
									<option value="123">Superman</option>
									<option value="456">Batman</option>
									<option value="789">Spiderman</option>
									<option value="654">Captain America</option>
								</select>
							</div>

							<div class="subject-info-arrows text-center">
								<br />
								<br /> <input type='button' id='btnAllRight' value="">>"
								class="btn btn-default" /><br /> <input type='button'
									id='btnRight' value=">" class="btn btn-default" /><br /> <input
									type='button' id='btnLeft' value="/<" class=" btn btn-default" /><br />
								<input type='button' id='btnAllLeft' value="/<<" class=" btn btn-default" />
							</div>

							<div class="subject-info-box-2">
								<!-- <label class="fields">Superheroes You Have Selected</label> -->
								<select multiple class="form-control" id="lstBox2">
									<option value="765">Nick Fury</option>
									<option value="698">The Hulk</option>
									<option value="856">Iron Man</option>
								</select>
							</div>

							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-success">Send</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
				</div>
			</div>

		</div>
	</div>
	<!-- Share Popup End !-->

	<!-- Close Popup Start -->
	<div id="Close" class="modal fade" role="dialog">
		<div class="modal-dialog modal-top modal-popup">

			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">
						<span><i class="fa fa-info-circle info" aria-hidden="true"></i></span><span
							class="m-l-15"># Ticket No - </span>
					</h4>
				</div>
				<div class="modal-body">
					<p class="center">Are you sure want to close this ticket.</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary">Yes</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">No</button>
				</div>
			</div>

		</div>
	</div>
	<!--  Close Popup End -->
	<!-- Share Popup End !--> 
<!-- Modal -->
 <div id="tktCreation" class="modal fade" role="dialog"> 
  <div class="modal-dialog modal-top">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header Popup-header">
      <button type="button" class="close" data-dismiss="modal"><img src="resources/img/close.png"></button>
        <!-- <button type="button" class="close" data-dismiss="modal">&times;</button> -->
        <p class="modal-title tkt-title"><img src="resources/img/tkt.png"><span class="tkt-title">New Ticket</span></p>
      </div>
      		<div class="modal-body">
        	<c:url var="addAction" value="/addnewticket" ></c:url>      
        	<div class="col-md-12">
				 	<p class="tkt-mandat"><span class="errormess">*</span> All fields are mandatory</p>
				 </div>            
        <form:form action="${addAction}" modelAttribute="ticketcreation" class="ui form validation-form">   
            <div class="col-md-12 m-t-10">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields required m-t-10">Site</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlSite" name="ddlSite" path="siteID">
                        <form:option value="">Select</form:option>
                        <form:options  items="${siteList}" />
                    </form:select>
                  </div>
              </div>
            </div>
              <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Type</label> </div>
                  <div class="col-md-8">
                     <form:select class="ui fluid search selection dropdown width" id="ddlType" name="ddlType"  path="ticketType">
                        <form:option value="">Select</form:option>
                        <form:option value="Operation">Operation</form:option>
                        <form:option value="Maintenance">Maintenance</form:option>
                    </form:select>
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Category</label> </div>
                  <div class="col-md-8">
                    <form:select class="ui fluid search selection dropdown width" id="ddlCategory" name="ddlCategory" path="ticketCategory">
                        <form:option value="">Select </form:option>
                        <form:option data-value="Operation" value="Inverter Down">Inverter Down </form:option>
                        <form:option data-value="Operation" value="Plant Down">Plant Down</form:option>
                        <form:option data-value="Operation" value="Module Damages">Module Damages</form:option>
                        <form:option data-value="Operation" value="Plumbing Damages">Plumbing Damages</form:option>
                        <form:option data-value="Operation" value="Equipment Failure">Equipment Failure</form:option>
                        <form:option data-value="Operation" value="Equipment Replacement">Equipment Replacement</form:option>
                        <form:option data-value="Operation" value="Communication Issue">Communication Issue</form:option>
                        <form:option data-value="Operation" value="String Down">String Down</form:option>
                        <form:option data-value="Operation" value="Energy Meter Issue">Energy Meter Issue</form:option>
                        <form:option data-value="Operation" value="Plant Trip">Plant Trip</form:option>
                        <form:option data-value="Maintenance" value="Modules Cleaning">Modules Cleaning</form:option>
                        <form:option data-value="Maintenance" value="Inverter Cleaning">Inverter Cleaning</form:option>
						<form:option data-value="Maintenance" value="DataLogger Cleaning">DataLogger Cleaning</form:option>
						<form:option data-value="Maintenance" value="String Current Measurement">String Current Measurement</form:option>
						<form:option data-value="Maintenance" value="Mechanical PM">Mechanical PM</form:option>
						<form:option data-value="Maintenance" value="Vegetation">Vegetation</form:option>
                        <form:option data-value="Maintenance" value="Visual Inspection">Visual Inspection</form:option>
                         <form:option data-value="Maintenance" value="JMR Visit">JMR Visit</form:option>
                    </form:select> 
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Subject</label> </div>
                  <div class="col-md-8">
                    <form:input  placeholder="Subject" name="Subject"  type="text"  id="txtSubject" path="ticketDetail" />
                  </div>
              </div>
            </div>
            <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Priority</label> </div>
                  <div class="col-md-8">
                   <form:select class="ui fluid search selection dropdown width" id="ddlPriority" name="user" path="priority">
                        <form:option value="">Select </form:option>
                        <form:option data-value="3" value="3">High</form:option>
                        <form:option data-value="2" value="2">Medium</form:option>
                        <form:option data-value="1" value="1">Low</form:option>
                    </form:select>
  					<!-- <div class="ui fluid search selection dropdown">
  					<input type="hidden" name="user" />
  					<i class="dropdown icon"></i>
  					<div class="default text">Select Priority</div>
  					<div class="menu" path="priority">
						    <div class="item" data-value="3" value="3"><span style="color:#f9152f; margin-left:15px;">High</span></div>
						    <div class="item" data-value="1" value="2"><span style="color:#FF6600; margin-left:15px;">Medium</span></div>
						    <div class="item" data-value="2" value="1"><span style="color:#f5bc0c; margin-left:15px;">Low</span></div>
						  </div>
				</div> -->
                </div>
              </div>
            </div>
			
            <div class="col-md-12 m-t-15">
              <div class="form-group required field">
                <div class="col-md-4"> <label  class="fields m-t-10">Description</label> </div>
                  <div class="col-md-8">
                    <form:textarea placeholder="Ticket Description" style="resize: none;" id="txtTktdescription" name="txtTktdescription" path="description" ></form:textarea>
                  <!--   <p class="errortext">(Maximum 150 characters are allowed)</p> -->
                  </div>
              </div>
            </div>
				<div class="col-md-12 col-xs-12  m-t-15 center">
				       <div class="btn btn-success  submit">Create</div>
				        <div class="btn btn-primary clear m-l-15">Reset</div>
				  </div>
				 
				</div>
				<!-- <div class="ui error message"></div> -->
				</form:form>
	  		</div>
	  </div>
    </div> 
<!-- Address Popup End !-->
	<script>
      $('#gotop').gotop({
        customHtml: '<i class="fa fa-angle-up fa-2x"></i>',
        bottom: '2em',
        right: '2em'
      });
      </script>
	<script>
         $(document).ready(function() {
            $(window).fadeThis({
               speed: 500,
            });
         });
      </script>


	<script type='text/javascript'>
       window.onload=function(){
       $('.dropdown').on('show.bs.dropdown', function () {
          var length = parseInt($('.dropdown-menu').css('width'), 10) * -1;
          $('.dropdown-menu').css('left', length);
       })
       }
       </script>

	<script>
        $('#btnRight').click(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });
        $('#lstBox1').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnAllRight').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox1', '#lstBox2');
            e.preventDefault();
        });

        $('#btnLeft').click(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });

         $('#lstBox2').dblclick(function (e) {
            $('select').moveToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
        $('#btnAllLeft').click(function (e) {
            $('select').moveAllToListAndDelete('#lstBox2', '#lstBox1');
            e.preventDefault();
        });
    </script>




</body>
</html>


