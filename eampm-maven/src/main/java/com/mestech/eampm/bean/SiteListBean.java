package com.mestech.eampm.bean;

public class SiteListBean {

	 private String SiteID;
	 private String SiteCode;
	 private String SiteReference;
	 private String SiteName;
	 private String NetworkStatus;
		
	 private String TodayEnergy;
		
		private String PerformanceRatio;
		 private String Inverters;
		 private String LastUpdate;
		 private String LastDownTime;
		
		 
		 private String TotalEnergy;
		 public String getLastDownTime() {
			return LastDownTime;
		}
		public void setLastDownTime(String lastDownTime) {
			LastDownTime = lastDownTime;
		}
		
		
	 public String getSiteID() {
		return SiteID;
	}
	public void setSiteID(String siteID) {
		SiteID = siteID;
	}
	public String getSiteCode() {
		return SiteCode;
	}
	public void setSiteCode(String siteCode) {
		SiteCode = siteCode;
	}
	public String getSiteReference() {
		return SiteReference;
	}
	public void setSiteReference(String siteReference) {
		SiteReference = siteReference;
	}
	public String getSiteName() {
		return SiteName;
	}
	public void setSiteName(String siteName) {
		SiteName = siteName;
	}
	public String getNetworkStatus() {
		return NetworkStatus;
	}
	public void setNetworkStatus(String networkStatus) {
		NetworkStatus = networkStatus;
	}
	public String getTodayEnergy() {
		return TodayEnergy;
	}
	public void setTodayEnergy(String todayEnergy) {
		TodayEnergy = todayEnergy;
	}
	public String getTotalEnergy() {
		return TotalEnergy;
	}
	public void setTotalEnergy(String totalEnergy) {
		TotalEnergy = totalEnergy;
	}
	public String getPerformanceRatio() {
		return PerformanceRatio;
	}
	public void setPerformanceRatio(String performanceRatio) {
		PerformanceRatio = performanceRatio;
	}
	public String getInverters() {
		return Inverters;
	}
	public void setInverters(String inverters) {
		Inverters = inverters;
	}
	public String getLastUpdate() {
		return LastUpdate;
	}
	public void setLastUpdate(String lastUpdate) {
		LastUpdate = lastUpdate;
	}

	 
}
