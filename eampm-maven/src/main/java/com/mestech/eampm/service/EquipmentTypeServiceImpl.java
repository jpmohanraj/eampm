package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EquipmentTypeDAO; 
import com.mestech.eampm.model.EquipmentType;
 
@Service

public class EquipmentTypeServiceImpl  implements EquipmentTypeService {
	
	@Autowired
    private EquipmentTypeDAO equipmenttypeDAO;
 
    public void setequipmenttypeDAO(EquipmentTypeDAO equipmenttypeDAO) {
        this.equipmenttypeDAO = equipmenttypeDAO;
    }
 
    //@Override
    @Transactional
    public void addEquipmentType(EquipmentType equipmenttype) {
    	equipmenttypeDAO.addEquipmentType(equipmenttype);
    }
 
    //@Override
    @Transactional
    public void updateEquipmentType(EquipmentType equipmenttype) {
        equipmenttypeDAO.updateEquipmentType(equipmenttype);
    }
 
    //@Override
    @Transactional
    public List<EquipmentType> listEquipmentTypes() {
        return this.equipmenttypeDAO.listEquipmentTypes();
    }
 
    //@Override
    @Transactional
    public EquipmentType getEquipmentTypeById(int id) {
        return equipmenttypeDAO.getEquipmentTypeById(id);
    }
 
    
    @Transactional
    public EquipmentType getEquipmentTypeByMax(String MaxColumnName) {
        return equipmenttypeDAO.getEquipmentTypeByMax(MaxColumnName);
    }
    
    
    //@Override
    @Transactional
    public void removeEquipmentType(int id) {
        equipmenttypeDAO.removeEquipmentType(id);
    }
    
}
