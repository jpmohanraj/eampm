
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EventTypeDAO; 
import com.mestech.eampm.model.EventType;
 
@Service

public class EventTypeServiceImpl  implements EventTypeService {
	
	@Autowired
    private EventTypeDAO eventtypeDAO;
 
    public void seteventtypeDAO(EventTypeDAO eventtypeDAO) {
        this.eventtypeDAO = eventtypeDAO;
    }
 
    //@Override
    @Transactional
    public void addEventType(EventType eventtype) {
    	eventtypeDAO.addEventType(eventtype);
    }
 
    //@Override
    @Transactional
    public void updateEventType(EventType eventtype) {
        eventtypeDAO.updateEventType(eventtype);
    }
 
    //@Override
    @Transactional
    public List<EventType> listEventTypes() {
        return this.eventtypeDAO.listEventTypes();
    }
 
    //@Override
    @Transactional
    public EventType getEventTypeById(int id) {
        return eventtypeDAO.getEventTypeById(id);
    }
 
    //@Override
    @Transactional
    public void removeEventType(int id) {
        eventtypeDAO.removeEventType(id);
    }
    
}


