package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EquipmentParameterDAO; 
import com.mestech.eampm.model.EquipmentParameter;
 
@Service

public class EquipmentParameterServiceImpl  implements EquipmentParameterService {
	
	@Autowired
    private EquipmentParameterDAO equipmentparameterDAO;
 
    public void setequipmentparameterDAO(EquipmentParameterDAO equipmentparameterDAO) {
        this.equipmentparameterDAO = equipmentparameterDAO;
    }
 
    //@Override
    @Transactional
    public void addEquipmentParameter(EquipmentParameter equipmentparameter) {
    	equipmentparameterDAO.addEquipmentParameter(equipmentparameter);
    }
 
    //@Override
    @Transactional
    public void updateEquipmentParameter(EquipmentParameter equipmentparameter) {
        equipmentparameterDAO.updateEquipmentParameter(equipmentparameter);
    }
 
    //@Override
    @Transactional
    public List<EquipmentParameter> listEquipmentParameters() {
        return this.equipmentparameterDAO.listEquipmentParameters();
    }
 
    //@Override
    @Transactional
    public EquipmentParameter getEquipmentParameterById(int id) {
        return equipmentparameterDAO.getEquipmentParameterById(id);
    }
 
    //@Override
    @Transactional
    public void removeEquipmentParameter(int id) {
        equipmentparameterDAO.removeEquipmentParameter(id);
    }
    
}
