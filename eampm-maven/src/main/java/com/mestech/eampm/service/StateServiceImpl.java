

package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.StateDAO; 
import com.mestech.eampm.model.State;
 
@Service

public class StateServiceImpl  implements StateService {
	
	@Autowired
    private StateDAO stateDAO;
 
    public void setstateDAO(StateDAO stateDAO) {
        this.stateDAO = stateDAO;
    }
 
    //@Override
    @Transactional
    public void addState(State state) {
    	stateDAO.addState(state);
    }
 
    //@Override
    @Transactional
    public void updateState(State state) {
        stateDAO.updateState(state);
    }
 
    //@Override
    @Transactional
    public List<State> listStates() {
        return this.stateDAO.listStates();
    }
 
    //@Override
    @Transactional
    public State getStateById(int id) {
        return stateDAO.getStateById(id);
    }
 
    //@Override
    @Transactional
    public void removeState(int id) {
        stateDAO.removeState(id);
    }
    
}


