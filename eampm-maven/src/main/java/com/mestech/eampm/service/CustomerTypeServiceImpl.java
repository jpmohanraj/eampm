package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CustomerTypeDAO; 
import com.mestech.eampm.model.CustomerType;
 
@Service

public class CustomerTypeServiceImpl  implements CustomerTypeService {
	
	@Autowired
    private CustomerTypeDAO customertypeDAO;
 
    public void setCustomerTypeDAO(CustomerTypeDAO customertypeDAO) {
        this.customertypeDAO = customertypeDAO;
    }
 
    //@Override
    @Transactional
    public void addCustomerType(CustomerType customertype) {
    	customertypeDAO.addCustomerType(customertype);
    }
 
    //@Override
    @Transactional
    public void updateCustomerType(CustomerType customertype) {
        customertypeDAO.updateCustomerType(customertype);
    }
 
    //@Override
    @Transactional
    public List<CustomerType> listCustomerTypes() {
        return this.customertypeDAO.listCustomerTypes();
    }
 
    //@Override
    @Transactional
    public CustomerType getCustomerTypeById(int id) {
        return customertypeDAO.getCustomerTypeById(id);
    }
 
    //@Override
    @Transactional
    public void removeCustomerType(int id) {
        customertypeDAO.removeCustomerType(id);
    }
    
}
