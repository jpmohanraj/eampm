
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.RoleActivityDAO; 
import com.mestech.eampm.model.RoleActivity;
 
@Service

public class RoleActivityServiceImpl  implements RoleActivityService {
	
	@Autowired
    private RoleActivityDAO roleactivityDAO;
 
    public void setroleactivityDAO(RoleActivityDAO roleactivityDAO) {
        this.roleactivityDAO = roleactivityDAO;
    }
 
    //@Override
    @Transactional
    public void addRoleActivity(RoleActivity roleactivity) {
    	roleactivityDAO.addRoleActivity(roleactivity);
    }
 
    //@Override
    @Transactional
    public void updateRoleActivity(RoleActivity roleactivity) {
        roleactivityDAO.updateRoleActivity(roleactivity);
    }
 
    //@Override
    @Transactional
    public List<RoleActivity> listRoleActivities() {
        return this.roleactivityDAO.listRoleActivities();
    }
 
    //@Override
    @Transactional
    public RoleActivity getRoleActivityById(int id) {
        return roleactivityDAO.getRoleActivityById(id);
    }
 
    //@Override
    @Transactional
    public void removeRoleActivity(int id) {
        roleactivityDAO.removeRoleActivity(id);
    }
    
}

