package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.EquipmentDAO; 
import com.mestech.eampm.model.Equipment;
 
@Service

public class EquipmentServiceImpl  implements EquipmentService {
	
	@Autowired
    private EquipmentDAO equipmentDAO;
 
    public void setequipmentDAO(EquipmentDAO equipmentDAO) {
        this.equipmentDAO = equipmentDAO;
    }
 
    //@Override
    @Transactional
    public void addEquipment(Equipment equipment) {
    	equipmentDAO.addEquipment(equipment);
    }
 
    //@Override
    @Transactional
    public void updateEquipment(Equipment equipment) {
        equipmentDAO.updateEquipment(equipment);
    }
 
    //@Override
    @Transactional
    public List<Equipment> listEquipments() {
        return this.equipmentDAO.listEquipments();
    }
 
    
    //@Override
    @Transactional
    public List<Equipment> listEquipmentsBySiteId(int siterId) {
        return this.equipmentDAO.listEquipmentsBySiteId(siterId);
    }
    

    //@Override
    @Transactional
    public List<Equipment> listEquipmentsByUserId(int userId) {
        return this.equipmentDAO.listEquipmentsByUserId(userId);
    }
    
    
    
    //@Override
    @Transactional
    public Equipment getEquipmentById(int id) {
        return equipmentDAO.getEquipmentById(id);
    }
 
    
    @Transactional
    public Equipment getEquipmentByMax(String MaxColumnName) {
        return equipmentDAO.getEquipmentByMax(MaxColumnName);
    }
    
    
    //@Override
    @Transactional
    public void removeEquipment(int id) {
        equipmentDAO.removeEquipment(id);
    }
    
    
    

    //@Override
    @Transactional
    public List<Equipment> listInvertersByUserId(int userId) {
        return this.equipmentDAO.listInvertersByUserId(userId);
    }
    
    
    //@Override
    @Transactional
    public List<Equipment> listInverters() {
        return this.equipmentDAO.listInverters();
    }

    
    //@Override
    @Transactional
    public List<Equipment> listInvertersBySiteId(int siteId) {
        return this.equipmentDAO.listInvertersBySiteId(siteId);
    }
    
    
}
