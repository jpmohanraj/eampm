
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.DataTransactionDAO; 
import com.mestech.eampm.model.DataTransaction;
 
@Service
public class DataTransactionServiceImpl  implements DataTransactionService {
	
	@Autowired
    private DataTransactionDAO dataTransactionDAO;
 
    public void setdataTransactionDAO(DataTransactionDAO dataTransactionDAO) {
        this.dataTransactionDAO = dataTransactionDAO;
    }
 
    //@Override
    @Transactional
    public void addDataTransaction(DataTransaction dataTransaction) {
    	dataTransactionDAO.addDataTransaction(dataTransaction);
    }
 
    //@Override
    @Transactional
    public void updateDataTransaction(DataTransaction dataTransaction) {
        dataTransactionDAO.updateDataTransaction(dataTransaction);
    }
 
    //@Override
    @Transactional
    public List<DataTransaction> listDataTransactions() {
        return this.dataTransactionDAO.listDataTransactions();
    }
    

    //@Override
    @Transactional
	public List<DataTransaction> listDataTransactionsByFilter1(int equipmentId,int siteId, int lastNDays ){
        return this.dataTransactionDAO.listDataTransactionsByFilter1( equipmentId, siteId,  lastNDays );
    }
	
  //@Override
    @Transactional
	public List<DataTransaction> listDataTransactionsByFilter2(int siteId, int lastNDays ){
        return this.dataTransactionDAO.listDataTransactionsByFilter2( siteId,  lastNDays );
    }
	
  //@Override
    @Transactional
    public List<DataTransaction> listDataTransactionsForDataDownload(int siteId, String fromDate,String toDate,int equipmentId ) {
    
    	return this.dataTransactionDAO.listDataTransactionsForDataDownload(siteId, fromDate,toDate,equipmentId );
    		   
    }	
    
    
    @Transactional
	public List<DataTransaction> getDataTransactionListByUserId(int userId) {
        return this.dataTransactionDAO.getDataTransactionListByUserId(userId);
    }

    @Transactional
	public List<DataTransaction> getDataTransactionListByCustomerId(int customerId) {
        return this.dataTransactionDAO.getDataTransactionListByCustomerId(customerId);
    }

    @Transactional
	public List<DataTransaction> getDataTransactionListBySiteId(int siteId) {
        return this.dataTransactionDAO.getDataTransactionListBySiteId(siteId);
    }
 
    @Transactional
	public List<DataTransaction> getDataTransactionListByEquipmentId(int equipmentId) {
        return this.dataTransactionDAO.getDataTransactionListByEquipmentId(equipmentId);
    }
 
    
    //@Override
    @Transactional
    public DataTransaction getDataTransactionById(int id) {
        return dataTransactionDAO.getDataTransactionById(id);
    }
 
    
    @Transactional
    public DataTransaction getDataTransactionByMax(String MaxColumnName) {
        return dataTransactionDAO.getDataTransactionByMax(MaxColumnName);
    }
    
    
    //@Override
    @Transactional
    public void removeDataTransaction(int id) {
        dataTransactionDAO.removeDataTransaction(id);
    }
    
    
}
