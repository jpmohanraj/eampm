package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.SopDetailDAO; 
import com.mestech.eampm.model.SopDetail;
 
@Service

public class SopDetailServiceImpl  implements SopDetailService {
	
	@Autowired
    private SopDetailDAO sopdetailDAO;
 
    public void setsopdetailDAO(SopDetailDAO sopdetailDAO) {
        this.sopdetailDAO = sopdetailDAO;
    }
 
    //@Override
    @Transactional
    public void addSopDetail(SopDetail sopdetail) {
    	sopdetailDAO.addSopDetail(sopdetail);
    }
 
    //@Override
    @Transactional
    public void updateSopDetail(SopDetail sopdetail) {
        sopdetailDAO.updateSopDetail(sopdetail);
    }
 
    //@Override
    @Transactional
    public List<SopDetail> listSopDetails() {
        return this.sopdetailDAO.listSopDetails();
    }
 
    //@Override
    @Transactional
    public SopDetail getSopDetailById(int id) {
        return sopdetailDAO.getSopDetailById(id);
    }
 
    //@Override
    @Transactional
    public void removeSopDetail(int id) {
        sopdetailDAO.removeSopDetail(id);
    }
    
}

