package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.FilterDAO; 
import com.mestech.eampm.model.Filter;
 
@Service

public class FilterServiceImpl  implements FilterService {
	
	@Autowired
    private FilterDAO filterDAO;
 
    public void setfilterDAO(FilterDAO filterDAO) {
        this.filterDAO = filterDAO;
    }
 
    //@Override
    @Transactional
    public void addFilter(Filter filter) {
    	filterDAO.addFilter(filter);
    }
 
    //@Override
    @Transactional
    public void updateFilter(Filter filter) {
        filterDAO.updateFilter(filter);
    }
 
    //@Override
    @Transactional
    public List<Filter> listFilters() {
        return this.filterDAO.listFilters();
    }
 
    //@Override
    @Transactional
    public Filter getFilterById(int id) {
        return filterDAO.getFilterById(id);
    }
 
    //@Override
    @Transactional
    public void removeFilter(int id) {
        filterDAO.removeFilter(id);
    }
    
}
