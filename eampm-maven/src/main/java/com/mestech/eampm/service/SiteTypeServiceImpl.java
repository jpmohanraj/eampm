package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.SiteTypeDAO; 
import com.mestech.eampm.model.SiteType;
 
@Service

public class SiteTypeServiceImpl  implements SiteTypeService {
	
	@Autowired
    private SiteTypeDAO sitetypeDAO;
 
    public void setsitetypeDAO(SiteTypeDAO sitetypeDAO) {
        this.sitetypeDAO = sitetypeDAO;
    }
 
    //@Override
    @Transactional
    public void addSiteType(SiteType sitetype) {
    	sitetypeDAO.addSiteType(sitetype);
    }
 
    //@Override
    @Transactional
    public void updateSiteType(SiteType sitetype) {
        sitetypeDAO.updateSiteType(sitetype);
    }
 
    //@Override
    @Transactional
    public List<SiteType> listSiteTypes() {
        return this.sitetypeDAO.listSiteTypes();
    }
 
    //@Override
    @Transactional
    public SiteType getSiteTypeById(int id) {
        return sitetypeDAO.getSiteTypeById(id);
    }
 
    //@Override
    @Transactional
    public void removeSiteType(int id) {
        sitetypeDAO.removeSiteType(id);
    }
    
}
