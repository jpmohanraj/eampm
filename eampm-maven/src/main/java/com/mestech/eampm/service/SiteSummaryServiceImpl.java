package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.dao.SiteSummaryDAO;
import com.mestech.eampm.model.SiteSummary;


@Service
public class SiteSummaryServiceImpl  implements SiteSummaryService {
	
	@Autowired
    private SiteSummaryDAO SiteSummaryDAO;
 
    public void setSiteSummaryDAO(SiteSummaryDAO SiteSummaryDAO) {
        this.SiteSummaryDAO = SiteSummaryDAO;
    }
 
    //@Override
    @Transactional
    public void addSiteSummary(SiteSummary SiteSummary) {
    	SiteSummaryDAO.addSiteSummary(SiteSummary);
    }
 
    //@Override
    @Transactional
    public void updateSiteSummary(SiteSummary SiteSummary) {
        SiteSummaryDAO.updateSiteSummary(SiteSummary);
    }
 
    //@Override
    @Transactional
    public List<SiteSummary> listSiteSummarys() {
        return this.SiteSummaryDAO.listSiteSummarys();
    }
    

    //@Override
    @Transactional
	public List<SiteSummary> listSiteSummarysByFilter1(int equipmentId,int siteId, int lastNDays ){
        return this.SiteSummaryDAO.listSiteSummarysByFilter1( equipmentId, siteId,  lastNDays );
    }
	
	
    @Transactional
	public List<SiteSummary> getSiteSummaryListByUserId(int userId) {
        return this.SiteSummaryDAO.getSiteSummaryListByUserId(userId);
    }

    @Transactional
	public List<SiteSummary> getSiteSummaryListByCustomerId(int customerId) {
        return this.SiteSummaryDAO.getSiteSummaryListByCustomerId(customerId);
    }

    @Transactional
	public List<SiteSummary> getSiteSummaryListBySiteId(int siteId) {
        return this.SiteSummaryDAO.getSiteSummaryListBySiteId(siteId);
    }
 
    @Transactional
	public List<SiteSummary> getSiteSummaryListByEquipmentId(int equipmentId) {
        return this.SiteSummaryDAO.getSiteSummaryListByEquipmentId(equipmentId);
    }
 
    
    //@Override
    @Transactional
    public SiteSummary getSiteSummaryById(int id) {
        return SiteSummaryDAO.getSiteSummaryById(id);
    }
 
    
    @Transactional
    public SiteSummary getSiteSummaryByMax(String MaxColumnName) {
        return SiteSummaryDAO.getSiteSummaryByMax(MaxColumnName);
    }
    
    
    //@Override
    @Transactional
    public void removeSiteSummary(int id) {
        SiteSummaryDAO.removeSiteSummary(id);
    }
    
    
}
