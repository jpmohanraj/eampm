

package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.StatusDAO; 
import com.mestech.eampm.model.Status;
 
@Service

public class StatusServiceImpl  implements StatusService {
	
	@Autowired
    private StatusDAO statusDAO;
 
    public void setstatusDAO(StatusDAO statusDAO) {
        this.statusDAO = statusDAO;
    }
 
    //@Override
    @Transactional
    public void addStatus(Status status) {
    	statusDAO.addStatus(status);
    }
 
    //@Override
    @Transactional
    public void updateStatus(Status status) {
        statusDAO.updateStatus(status);
    }
 
    //@Override
    @Transactional
    public List<Status> listStatuses() {
        return this.statusDAO.listStatuses();
    }
 
    //@Override
    @Transactional
    public Status getStatusById(int id) {
        return statusDAO.getStatusById(id);
    }
 
    //@Override
    @Transactional
    public void removeStatus(int id) {
        statusDAO.removeStatus(id);
    }
    
}

