package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.ActivityDAO; 
import com.mestech.eampm.model.Activity;
 
@Service

public class ActivityServiceImpl  implements ActivityService {
	
	@Autowired
    private ActivityDAO activityDAO;
 
    public void setactivityDAO(ActivityDAO activityDAO) {
        this.activityDAO = activityDAO;
    }
 
    //@Override
    @Transactional
    public void addActivity(Activity activity) {
    	activityDAO.addActivity(activity);
    }
 
    //@Override
    @Transactional
    public void updateActivity(Activity activity) {
        activityDAO.updateActivity(activity);
    }
 
    //@Override
    @Transactional
    public List<Activity> listActivities() {
        return this.activityDAO.listActivities();
    }
 
    //@Override
    @Transactional
    public Activity getActivityById(int id) {
        return activityDAO.getActivityById(id);
    }
 
    //@Override
    @Transactional
    public void removeActivity(int id) {
        activityDAO.removeActivity(id);
    }
    
}
