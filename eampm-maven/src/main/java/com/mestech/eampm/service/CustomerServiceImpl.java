package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CustomerDAO; 
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.EquipmentType;
 
@Service

public class CustomerServiceImpl  implements CustomerService {
	
	@Autowired
    private CustomerDAO customerDAO;
 
    public void setcustomerDAO(CustomerDAO customerDAO) {
        this.customerDAO = customerDAO;
    }
 
    //@Override
    @Transactional
    public void addCustomer(Customer customer) {
    	customerDAO.addCustomer(customer);
    }
 
    //@Override
    @Transactional
    public void updateCustomer(Customer customer) {
        customerDAO.updateCustomer(customer);
    }
 
    //@Override
    @Transactional
    public List<Customer> listCustomers() {
        return this.customerDAO.listCustomers();
    }
 
    
    @Transactional
    public List<Customer> getCustomerListByUserId(int userId)
	{
        return customerDAO.getCustomerListByUserId(userId);
    }
	
    
    //@Override
    @Transactional
    public Customer getCustomerById(int id) {
        return customerDAO.getCustomerById(id);
    }
 
    
    @Transactional
    public Customer getCustomerByMax(String MaxColumnName) {
        return customerDAO.getCustomerByMax(MaxColumnName);
    }
 
    
    
    //@Override
    @Transactional
    public void removeCustomer(int id) {
        customerDAO.removeCustomer(id);
    }
    
}
