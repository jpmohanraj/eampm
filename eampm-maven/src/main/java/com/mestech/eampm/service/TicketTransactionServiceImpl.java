
package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.TicketTransactionDAO; 
import com.mestech.eampm.model.TicketTransaction;
 
@Service

public class TicketTransactionServiceImpl  implements TicketTransactionService {
	
	@Autowired
    private TicketTransactionDAO ticketTransactionDAO;
 
    public void setticketTransactionDAO(TicketTransactionDAO ticketTransactionDAO) {
        this.ticketTransactionDAO = ticketTransactionDAO;
    }
 
    //@Override
    @Transactional
    public void addTicketTransaction(TicketTransaction ticketTransaction) {
    	ticketTransactionDAO.addTicketTransaction(ticketTransaction);
    }
 
    //@Override
    @Transactional
    public void updateTicketTransaction(TicketTransaction ticketTransaction) {
        ticketTransactionDAO.updateTicketTransaction(ticketTransaction);
    }
 
    //@Override
    @Transactional
    public List<TicketTransaction> listTicketTransactions() {
        return this.ticketTransactionDAO.listTicketTransactions();
    }
 
    
	
  //@Override
    @Transactional
    public TicketTransaction getTicketTransactionByMax(String MaxColumnName) {
        return ticketTransactionDAO.getTicketTransactionByMax(MaxColumnName);
    }
    
    //@Override
    @Transactional
    public TicketTransaction getTicketTransactionById(int id) {
        return ticketTransactionDAO.getTicketTransactionById(id);
    }
 
    //@Override
    @Transactional
    public List<TicketTransaction> getTicketTransactionListByUserId(int userId) {
        return this.ticketTransactionDAO.getTicketTransactionListByUserId(userId);
    }
    
    //@Override
    @Transactional
    public List<TicketTransaction> getTicketTransactionListBySiteId(int siteId) {
        return this.ticketTransactionDAO.getTicketTransactionListBySiteId( siteId);
    }
    

    //@Override
    @Transactional
	public List<TicketTransaction> getTicketTransactionListByTicketId(int ticketId){
        return this.ticketTransactionDAO.getTicketTransactionListByTicketId( ticketId);
    }
	
	
    //@Override
    @Transactional
    public void removeTicketTransaction(int id) {
        ticketTransactionDAO.removeTicketTransaction(id);
    }
    
}



