package com.mestech.eampm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.mestech.eampm.dao.CurrencyDAO; 
import com.mestech.eampm.model.Currency;
 
@Service

public class CurrencyServiceImpl  implements CurrencyService {
	
	@Autowired
    private CurrencyDAO currencyDAO;
 
    public void setcurrencyDAO(CurrencyDAO currencyDAO) {
        this.currencyDAO = currencyDAO;
    }
 
    //@Override
    @Transactional
    public void addCurrency(Currency currency) {
    	currencyDAO.addCurrency(currency);
    }
 
    //@Override
    @Transactional
    public void updateCurrency(Currency currency) {
        currencyDAO.updateCurrency(currency);
    }
 
    //@Override
    @Transactional
    public List<Currency> listCurrencies() {
        return this.currencyDAO.listCurrencies();
    }
 
    //@Override
    @Transactional
    public Currency getCurrencyById(int id) {
        return currencyDAO.getCurrencyById(id);
    }
 
    //@Override
    @Transactional
    public void removeCurrency(int id) {
        currencyDAO.removeCurrency(id);
    }
    
}
