package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;


@Entity
@Table(name="mStatus")
public class Status implements Serializable{

	 private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "StatusID")
	 private Integer StatusId;
	 
	 @Column(name="StatusCode")
	 private String StatusCode;
	 
	 @Column(name="Status")
	 private String Status;
	 
	 @Column(name="StatusCategory")
	 private String StatusCategory;
	 
	 @Column(name="Description")
	 private String Description;
	 
	 @Column(name="ReferenceID")
	 private Integer ReferenceID;
	 
	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;
	 
	 @Column(name="LastUpdatedBy")
	 private Integer LastUpdatedBy;

	public Integer getStatusId() {
		return StatusId;
	}

	public void setStatusId(Integer statusId) {
		StatusId = statusId;
	}

	public String getStatusCode() {
		return StatusCode;
	}

	public void setStatusCode(String statusCode) {
		StatusCode = statusCode;
	}

	public String getStatus() {
		return Status;
	}

	public void setStatus(String status) {
		Status = status;
	}

	public String getStatusCategory() {
		return StatusCategory;
	}

	public void setStatusCategory(String statusCategory) {
		StatusCategory = statusCategory;
	}

	public String getDescription() {
		return Description;
	}

	public void setDescription(String description) {
		Description = description;
	}

	public Integer getReferenceID() {
		return ReferenceID;
	}

	public void setReferenceID(Integer referenceID) {
		ReferenceID = referenceID;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}
	 
	
}
