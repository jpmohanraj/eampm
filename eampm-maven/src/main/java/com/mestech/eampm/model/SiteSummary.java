package com.mestech.eampm.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="tSiteSummary")
public class SiteSummary {

	 private static final long serialVersionUID = -723583058586873479L;

	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "SummaryID")
	 private Integer SummaryID;
	
	
	 
	 @Column(name = "SiteID")
	 private Integer SiteID;
	 
	 
	 @Column(name = "Timestamp")
	 private Date Timestamp;
	 
	 
	 
	 @Column(name = "TotalEnergy", nullable = true)
	 private Double TotalEnergy;
	 
	 @Column(name = "TodayEnergy", nullable = true)
	 private Double TodayEnergy;
	 
	 @Column(name = "DifferencialTodayEnergy", nullable = true)
	 private Double DifferencialTodayEnergy;

	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;
	 
	 @Column(name="CreationDate")
	 private Date CreationDate;

	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 
		public void setActiveFlag(Integer activeFlag) {
			ActiveFlag = activeFlag;
		}

		public Date getCreationDate() {
			return CreationDate;
		}

		public void setCreationDate(Date creationDate) {
			CreationDate = creationDate;
		}

		public Date getLastUpdatedDate() {
			return LastUpdatedDate;
		}

		public void setLastUpdatedDate(Date lastUpdatedDate) {
			LastUpdatedDate = lastUpdatedDate;
		}

		
	 
	 
	 
	public Integer getSummaryID() {
		return SummaryID;
	}

	public void setSummaryID(Integer summaryID) {
		SummaryID = summaryID;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public Date getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}

	public Double getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(Double totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public Double getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(Double todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public Double getDifferencialTodayEnergy() {
		return DifferencialTodayEnergy;
	}

	public void setDifferencialTodayEnergy(Double differencialTodayEnergy) {
		DifferencialTodayEnergy = differencialTodayEnergy;
	}

	
	 
	 
	 
}
