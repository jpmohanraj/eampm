
package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import org.hibernate.annotations.CreationTimestamp;

/*
 * This is our model class and it corresponds to Country table in database
 */
@Entity
@Table(name="tDataTransaction")
public class DataTransaction implements Serializable{
	
	 private static final long serialVersionUID = -723583058586873479L;

	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "TransactionID")
	 private Integer TransactionId;
	
	
	 
	 @Column(name = "SiteID")
	 private Integer SiteID;
	 
	 @Column(name = "EquipmentID")
	 private Integer EquipmentID;
	 
	 @Column(name = "Timestamp")
	 private Date Timestamp;
	 
	 
	 
	 @Column(name = "InputCurrent_01", nullable = true)
	 private Double InputCurrent_01;
	 
	 @Column(name = "InputCurrent_02", nullable = true)
	 private Double InputCurrent_02;
	 
	 @Column(name = "InputCurrent_03", nullable = true)
	 private Double InputCurrent_03;
	 
	 @Column(name = "InputCurrent_04", nullable = true)
	 private Double InputCurrent_04;
	
	 @Column(name = "InputCurrent_05", nullable = true)
	private Double InputCurrent_05;
	
	 @Column(name = "InputCurrent_06", nullable = true)
	private Double InputCurrent_06;
	
	 @Column(name = "InputCurrent_07", nullable = true)
	private Double InputCurrent_07;
	
	 @Column(name = "InputCurrent_08", nullable = true)
	private Double InputCurrent_08;
	
	 @Column(name = "InputCurrent_09", nullable = true)
	private Double InputCurrent_09;
	
	 @Column(name = "InputCurrent_10", nullable = true)
		private Double InputCurrent_10;
	 
	 @Column(name = "InputVolatge_01", nullable = true)
	 private Double InputVolatge_01;
	 
	 @Column(name = "InputVolatge_02", nullable = true)
	 private Double InputVolatge_02;
	 
	 @Column(name = "InputVolatge_03", nullable = true)
	 private Double InputVolatge_03;
	 
	 @Column(name = "InputVolatge_04", nullable = true)
	 private Double InputVolatge_04;
	 
	 @Column(name = "InputVolatge_05", nullable = true)
	 private Double InputVolatge_05;
	 
	 @Column(name = "InputVolatge_06", nullable = true)
	 private Double InputVolatge_06;
	 
	 @Column(name = "InputVolatge_07", nullable = true)
	 private Double InputVolatge_07;
	 
	 @Column(name = "InputVolatge_08", nullable = true)
	 private Double InputVolatge_08;
	 
	 @Column(name = "InputVolatge_09", nullable = true)
	 private Double InputVolatge_09;
	 
	 @Column(name = "InputVolatge_10", nullable = true)
	 private Double InputVolatge_10;
	 
	 
	 @Column(name = "InputPower_01", nullable = true)
	 private Double InputPower_01;
	 
	 @Column(name = "InputPower_02", nullable = true)
	 private Double InputPower_02;
	 
	 @Column(name = "InputPower_03", nullable = true)
	 private Double InputPower_03;
	 
	 @Column(name = "InputPower_04", nullable = true)
	 private Double InputPower_04;
	 
	 @Column(name = "InputPower_05", nullable = true)
	 private Double InputPower_05;
	 
	 @Column(name = "InputPower_06", nullable = true)
	 private Double InputPower_06;
	 
	 @Column(name = "InputPower_07", nullable = true)
	 private Double InputPower_07;
	 
	 @Column(name = "InputPower_08", nullable = true)
	 private Double InputPower_08;
	 
	 @Column(name = "InputPower_09", nullable = true)
	 private Double InputPower_09;
	 
	 @Column(name = "InputPower_10", nullable = true)
	 private Double InputPower_10;
	 
	 @Column(name = "PhaseCurrent", nullable = true)
	 private Double PhaseCurrent;
	 
	 @Column(name = "PhaseCurrent_L1", nullable = true)
	 private Double PhaseCurrent_L1;
	 
	 @Column(name = "PhaseCurrent_L2", nullable = true)
	 private Double PhaseCurrent_L2;
	 
	 @Column(name = "PhaseCurrent_L3", nullable = true)
	 private Double PhaseCurrent_L3;
	 
	 @Column(name = "PhaseVolatge", nullable = true)
	 private Double PhaseVolatge;
	 
	 @Column(name = "PhaseVolatge_L1", nullable = true)
	 private Double PhaseVolatge_L1;
	 
	 @Column(name = "PhaseVolatge_L2", nullable = true)
	 private Double PhaseVolatge_L2;
	 
	 @Column(name = "PhaseVolatge_L3", nullable = true)
	 private Double PhaseVolatge_L3;
	 
	 @Column(name = "ApparentPower", nullable = true)
	 private Double ApparentPower;
	 
	 @Column(name = "ActivePower", nullable = true)
	 private Double ActivePower;
	 
	 @Column(name = "ReactivePower", nullable = true)
	 private Double ReactivePower;
	 
	 @Column(name = "PowerFactor", nullable = true)
	 private Double PowerFactor;
	 
	 @Column(name = "TodayEnergy", nullable = true)
	 private Double TodayEnergy;
	 
	 @Column(name = "TotalEnergy", nullable = true)
	 private Double TotalEnergy;
	 
	 @Column(name = "IsolationResistance", nullable = true)
	 private Double IsolationResistance;
	 
	 @Column(name = "OutputFrequency", nullable = true)
	 private Double OutputFrequency;
	 
	 @Column(name = "AmbientTemperature", nullable = true)
	 private Double AmbientTemperature;
	 
	 @Column(name = "ModuleTemperature", nullable = true)
	 private Double ModuleTemperature;
	 
	 @Column(name = "InverterTemperature", nullable = true)
	 private Double InverterTemperature;
	 
	 @Column(name = "WindSpeed", nullable = true)
	 private Double WindSpeed;
	 
	 @Column(name = "Rainfall", nullable = true)
	 private Double Rainfall;
	 
	 @Column(name = "TotalHoursOn", nullable = true)
	 private Double TotalHoursOn;
	 
	 @Column(name = "TodayHoursOn", nullable = true)
	 private Double TodayHoursOn;
	 
	 @Column(name = "PhasePowerBalancer", nullable = true)
	 private Double PhasePowerBalancer;
	 
	 @Column(name = "DifferentialCurrent", nullable = true)
	 private Double DifferentialCurrent;
	 
	 @Column(name = "Status", nullable = true)
	 private Integer Status;
	 
	 @Column(name = "ErrorCode", nullable = true)
	 private String ErrorCode;
	 
	 
	@Column(name="ActiveFlag")
	private Integer ActiveFlag;	
	
	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CreationDate", insertable=false)
	private Date CreationDate;	
	
	public Integer getTransactionId() {
		return TransactionId;
	}

	public void setTransactionId(Integer transactionId) {
		TransactionId = transactionId;
	}

	public Integer getSiteID() {
		return SiteID;
	}

	public void setSiteID(Integer siteID) {
		SiteID = siteID;
	}

	public Integer getEquipmentID() {
		return EquipmentID;
	}

	public void setEquipmentID(Integer equipmentID) {
		EquipmentID = equipmentID;
	}

	public Date getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(Date timestamp) {
		Timestamp = timestamp;
	}

	public Double getInputCurrent_01() {
		return InputCurrent_01;
	}

	public void setInputCurrent_01(Double inputCurrent_01) {
		InputCurrent_01 = inputCurrent_01;
	}

	public Double getInputCurrent_02() {
		return InputCurrent_02;
	}

	public void setInputCurrent_02(Double inputCurrent_02) {
		InputCurrent_02 = inputCurrent_02;
	}

	public Double getInputCurrent_03() {
		return InputCurrent_03;
	}

	public void setInputCurrent_03(Double inputCurrent_03) {
		InputCurrent_03 = inputCurrent_03;
	}

	public Double getInputCurrent_04() {
		return InputCurrent_04;
	}

	public void setInputCurrent_04(Double inputCurrent_04) {
		InputCurrent_04 = inputCurrent_04;
	}

	public Double getInputCurrent_05() {
		return InputCurrent_05;
	}

	public void setInputCurrent_05(Double inputCurrent_05) {
		InputCurrent_05 = inputCurrent_05;
	}

	public Double getInputCurrent_06() {
		return InputCurrent_06;
	}

	public void setInputCurrent_06(Double inputCurrent_06) {
		InputCurrent_06 = inputCurrent_06;
	}

	public Double getInputCurrent_07() {
		return InputCurrent_07;
	}

	public void setInputCurrent_07(Double inputCurrent_07) {
		InputCurrent_07 = inputCurrent_07;
	}

	public Double getInputCurrent_08() {
		return InputCurrent_08;
	}

	public void setInputCurrent_08(Double inputCurrent_08) {
		InputCurrent_08 = inputCurrent_08;
	}

	public Double getInputCurrent_09() {
		return InputCurrent_09;
	}

	public void setInputCurrent_09(Double inputCurrent_09) {
		InputCurrent_09 = inputCurrent_09;
	}

	public Double getInputCurrent_10() {
		return InputCurrent_10;
	}

	public void setInputCurrent_10(Double inputCurrent_10) {
		InputCurrent_10 = inputCurrent_10;
	}

	public Double getInputVolatge_01() {
		return InputVolatge_01;
	}

	public void setInputVolatge_01(Double inputVolatge_01) {
		InputVolatge_01 = inputVolatge_01;
	}

	public Double getInputVolatge_02() {
		return InputVolatge_02;
	}

	public void setInputVolatge_02(Double inputVolatge_02) {
		InputVolatge_02 = inputVolatge_02;
	}

	public Double getInputVolatge_03() {
		return InputVolatge_03;
	}

	public void setInputVolatge_03(Double inputVolatge_03) {
		InputVolatge_03 = inputVolatge_03;
	}

	public Double getInputVolatge_04() {
		return InputVolatge_04;
	}

	public void setInputVolatge_04(Double inputVolatge_04) {
		InputVolatge_04 = inputVolatge_04;
	}

	public Double getInputVolatge_05() {
		return InputVolatge_05;
	}

	public void setInputVolatge_05(Double inputVolatge_05) {
		InputVolatge_05 = inputVolatge_05;
	}

	public Double getInputVolatge_06() {
		return InputVolatge_06;
	}

	public void setInputVolatge_06(Double inputVolatge_06) {
		InputVolatge_06 = inputVolatge_06;
	}

	public Double getInputVolatge_07() {
		return InputVolatge_07;
	}

	public void setInputVolatge_07(Double inputVolatge_07) {
		InputVolatge_07 = inputVolatge_07;
	}

	public Double getInputVolatge_08() {
		return InputVolatge_08;
	}

	public void setInputVolatge_08(Double inputVolatge_08) {
		InputVolatge_08 = inputVolatge_08;
	}

	public Double getInputVolatge_09() {
		return InputVolatge_09;
	}

	public void setInputVolatge_09(Double inputVolatge_09) {
		InputVolatge_09 = inputVolatge_09;
	}

	public Double getInputVolatge_10() {
		return InputVolatge_10;
	}

	public void setInputVolatge_10(Double inputVolatge_10) {
		InputVolatge_10 = inputVolatge_10;
	}

	public Double getInputPower_01() {
		return InputPower_01;
	}

	public void setInputPower_01(Double inputPower_01) {
		InputPower_01 = inputPower_01;
	}

	public Double getInputPower_02() {
		return InputPower_02;
	}

	public void setInputPower_02(Double inputPower_02) {
		InputPower_02 = inputPower_02;
	}

	public Double getInputPower_03() {
		return InputPower_03;
	}

	public void setInputPower_03(Double inputPower_03) {
		InputPower_03 = inputPower_03;
	}

	public Double getInputPower_04() {
		return InputPower_04;
	}

	public void setInputPower_04(Double inputPower_04) {
		InputPower_04 = inputPower_04;
	}

	public Double getInputPower_05() {
		return InputPower_05;
	}

	public void setInputPower_05(Double inputPower_05) {
		InputPower_05 = inputPower_05;
	}

	public Double getInputPower_06() {
		return InputPower_06;
	}

	public void setInputPower_06(Double inputPower_06) {
		InputPower_06 = inputPower_06;
	}

	public Double getInputPower_07() {
		return InputPower_07;
	}

	public void setInputPower_07(Double inputPower_07) {
		InputPower_07 = inputPower_07;
	}

	public Double getInputPower_08() {
		return InputPower_08;
	}

	public void setInputPower_08(Double inputPower_08) {
		InputPower_08 = inputPower_08;
	}

	public Double getInputPower_09() {
		return InputPower_09;
	}

	public void setInputPower_09(Double inputPower_09) {
		InputPower_09 = inputPower_09;
	}

	public Double getInputPower_10() {
		return InputPower_10;
	}

	public void setInputPower_10(Double inputPower_10) {
		InputPower_10 = inputPower_10;
	}

	public Double getPhaseCurrent() {
		return PhaseCurrent;
	}

	public void setPhaseCurrent(Double phaseCurrent) {
		PhaseCurrent = phaseCurrent;
	}

	public Double getPhaseCurrent_L1() {
		return PhaseCurrent_L1;
	}

	public void setPhaseCurrent_L1(Double phaseCurrent_L1) {
		PhaseCurrent_L1 = phaseCurrent_L1;
	}

	public Double getPhaseCurrent_L2() {
		return PhaseCurrent_L2;
	}

	public void setPhaseCurrent_L2(Double phaseCurrent_L2) {
		PhaseCurrent_L2 = phaseCurrent_L2;
	}

	public Double getPhaseCurrent_L3() {
		return PhaseCurrent_L3;
	}

	public void setPhaseCurrent_L3(Double phaseCurrent_L3) {
		PhaseCurrent_L3 = phaseCurrent_L3;
	}

	public Double getPhaseVolatge() {
		return PhaseVolatge;
	}

	public void setPhaseVolatge(Double phaseVolatge) {
		PhaseVolatge = phaseVolatge;
	}

	public Double getPhaseVolatge_L1() {
		return PhaseVolatge_L1;
	}

	public void setPhaseVolatge_L1(Double phaseVolatge_L1) {
		PhaseVolatge_L1 = phaseVolatge_L1;
	}

	public Double getPhaseVolatge_L2() {
		return PhaseVolatge_L2;
	}

	public void setPhaseVolatge_L2(Double phaseVolatge_L2) {
		PhaseVolatge_L2 = phaseVolatge_L2;
	}

	public Double getPhaseVolatge_L3() {
		return PhaseVolatge_L3;
	}

	public void setPhaseVolatge_L3(Double phaseVolatge_L3) {
		PhaseVolatge_L3 = phaseVolatge_L3;
	}

	public Double getApparentPower() {
		return ApparentPower;
	}

	public void setApparentPower(Double apparentPower) {
		ApparentPower = apparentPower;
	}

	public Double getActivePower() {
		return ActivePower;
	}

	public void setActivePower(Double activePower) {
		ActivePower = activePower;
	}

	public Double getReactivePower() {
		return ReactivePower;
	}

	public void setReactivePower(Double reactivePower) {
		ReactivePower = reactivePower;
	}

	public Double getPowerFactor() {
		return PowerFactor;
	}

	public void setPowerFactor(Double powerFactor) {
		PowerFactor = powerFactor;
	}

	public Double getTodayEnergy() {
		return TodayEnergy;
	}

	public void setTodayEnergy(Double todayEnergy) {
		TodayEnergy = todayEnergy;
	}

	public Double getTotalEnergy() {
		return TotalEnergy;
	}

	public void setTotalEnergy(Double totalEnergy) {
		TotalEnergy = totalEnergy;
	}

	public Double getIsolationResistance() {
		return IsolationResistance;
	}

	public void setIsolationResistance(Double isolationResistance) {
		IsolationResistance = isolationResistance;
	}

	public Double getOutputFrequency() {
		return OutputFrequency;
	}

	public void setOutputFrequency(Double outputFrequency) {
		OutputFrequency = outputFrequency;
	}

	public Double getAmbientTemperature() {
		return AmbientTemperature;
	}

	public void setAmbientTemperature(Double ambientTemperature) {
		AmbientTemperature = ambientTemperature;
	}

	public Double getModuleTemperature() {
		return ModuleTemperature;
	}

	public void setModuleTemperature(Double moduleTemperature) {
		ModuleTemperature = moduleTemperature;
	}

	public Double getInverterTemperature() {
		return InverterTemperature;
	}

	public void setInverterTemperature(Double inverterTemperature) {
		InverterTemperature = inverterTemperature;
	}

	public Double getWindSpeed() {
		return WindSpeed;
	}

	public void setWindSpeed(Double windSpeed) {
		WindSpeed = windSpeed;
	}

	public Double getRainfall() {
		return Rainfall;
	}

	public void setRainfall(Double rainfall) {
		Rainfall = rainfall;
	}

	public Double getTotalHoursOn() {
		return TotalHoursOn;
	}

	public void setTotalHoursOn(Double totalHoursOn) {
		TotalHoursOn = totalHoursOn;
	}

	public Double getTodayHoursOn() {
		return TodayHoursOn;
	}

	public void setTodayHoursOn(Double todayHoursOn) {
		TodayHoursOn = todayHoursOn;
	}

	public Double getPhasePowerBalancer() {
		return PhasePowerBalancer;
	}

	public void setPhasePowerBalancer(Double phasePowerBalancer) {
		PhasePowerBalancer = phasePowerBalancer;
	}

	public Double getDifferentialCurrent() {
		return DifferentialCurrent;
	}

	public void setDifferentialCurrent(Double differentialCurrent) {
		DifferentialCurrent = differentialCurrent;
	}

	public Integer getStatus() {
		return Status;
	}

	public void setStatus(Integer status) {
		Status = status;
	}

	public String getErrorCode() {
		return ErrorCode;
	}

	public void setErrorCode(String errorCode) {
		ErrorCode = errorCode;
	}


	public Integer getActiveFlag() {
		return ActiveFlag;
	}

	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}

	public Date getCreationDate() {
		return CreationDate;
	}

	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}

	public Integer getCreatedBy() {
		return CreatedBy;
	}

	public void setCreatedBy(Integer createdBy) {
		CreatedBy = createdBy;
	}

	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}

	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}

	public Integer getLastUpdatedBy() {
		return LastUpdatedBy;
	}

	public void setLastUpdatedBy(Integer lastUpdatedBy) {
		LastUpdatedBy = lastUpdatedBy;
	}

	@Column(name="CreatedBy" , nullable=true)
	private Integer CreatedBy;	
	
	
	@Column(name="LastUpdatedDate", nullable=true)
	private Date LastUpdatedDate;	
	
	@Column(name="LastUpdatedBy", nullable=true)
	private Integer LastUpdatedBy;	
	
		
}
