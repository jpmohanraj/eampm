package com.mestech.eampm.model;


import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="mUser")
public class User implements Serializable{

	 private static final long serialVersionUID = -723583058586873479L;
	 
	 @Id
	 @GeneratedValue(strategy=GenerationType.IDENTITY)
	 @Column(name = "UserID")
	 private Integer UserId;
	 
	 @Column(name="UserCode")
	 private String UserCode;
	 
	 @Column(name="UserName")
	 private String UserName;
	 
	 @Column(name="Password")
	 private String Password;
	 
	 @Column(name="Designation")
	 private String Designation;
	 
	 @Column(name="Department")
	 private String Department;
	 
	 @Column(name="RoleID")
	 private Integer RoleID;
	 
	 @Column(name="BloodGroup")
	 private String BloodGroup;
	 
	 @Column(name="DateOfBirth")
	 private Date DateOfBirth;
	 
	 @Column(name="MobileNumber")
	 private String MobileNumber;
	 
	 @Column(name="EmailID")
	 private String EmailID;
	
 
	 @Column(name="ActiveFlag")
	 private Integer ActiveFlag;

	 @Column(name="CreationDate")
	 private Date CreationDate;

	
	 @Column(name="LastUpdatedDate")
	 private Date LastUpdatedDate;

	 

	 @Transient
	 private String ErrorMessage;
	 

	public String getErrorMessage() {
		return ErrorMessage;
	}


	public void setErrorMessage(String errorMessage) {
		ErrorMessage = errorMessage;
	}


	public Integer getUserId() {
		return UserId;
	}


	public void setUserId(Integer userId) {
		UserId = userId;
	}


	public String getUserCode() {
		return UserCode;
	}


	public void setUserCode(String userCode) {
		UserCode = userCode;
	}


	public String getUserName() {
		return UserName;
	}


	public void setUserName(String userName) {
		UserName = userName;
	}


	public String getDesignation() {
		return Designation;
	}


	public void setDesignation(String designation) {
		Designation = designation;
	}


	public String getDepartment() {
		return Department;
	}


	public void setDepartment(String department) {
		Department = department;
	}


	public Integer getRoleID() {
		return RoleID;
	}


	public void setRoleID(Integer roleID) {
		RoleID = roleID;
	}


	public String getBloodGroup() {
		return BloodGroup;
	}


	public void setBloodGroup(String bloodGroup) {
		BloodGroup = bloodGroup;
	}


	public Date getDateOfBirth() {
		return DateOfBirth;
	}


	public void setDateOfBirth(Date dateOfBirth) {
		DateOfBirth = dateOfBirth;
	}


	public String getMobileNumber() {
		return MobileNumber;
	}


	public void setMobileNumber(String mobileNumber) {
		MobileNumber = mobileNumber;
	}


	public String getEmailID() {
		return EmailID;
	}


	public void setEmailID(String emailID) {
		EmailID = emailID;
	}


	public Integer getActiveFlag() {
		return ActiveFlag;
	}


	public void setActiveFlag(Integer activeFlag) {
		ActiveFlag = activeFlag;
	}


	public Date getCreationDate() {
		return CreationDate;
	}


	public void setCreationDate(Date creationDate) {
		CreationDate = creationDate;
	}


	public Date getLastUpdatedDate() {
		return LastUpdatedDate;
	}


	public void setLastUpdatedDate(Date lastUpdatedDate) {
		LastUpdatedDate = lastUpdatedDate;
	}


	public String getPassword() {
		return Password;
	}


	public void setPassword(String password) {
		Password = password;
	}

	

	
}
