
package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.Currency;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.CountryService;
import com.mestech.eampm.service.CurrencyService;
import com.mestech.eampm.service.CustomerService;
import com.mestech.eampm.service.CustomerTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.StateService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class SiteController {
	
	 @Autowired
	    private SiteService siteService;

	 @Autowired
	    private CustomerService customerService;
	 @Autowired
	    private SiteTypeService sitetypeService;
	 @Autowired
	    private CountryService countryService;
	 @Autowired
	    private StateService stateService;
	 @Autowired
	    private CurrencyService currencyService;
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	 

	 public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	    

		 public  Map<String,String> getDropdownList(String DropdownName) 
			{
		    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
		    	
		    	if(DropdownName == "Customer")
		    	{
		    		List<Customer> ddlList = customerService.listCustomers();
		    		
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getCustomerId().toString(), ddlList.get(i).getCustomerName());
		    		}
		    		
		    	}
		    	else if(DropdownName == "SiteType")
		    	{
		    		List<SiteType> ddlList = sitetypeService.listSiteTypes();
		    		
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getSiteTypeId().toString(), ddlList.get(i).getSiteType());
		    		}
		    		
		    	}
		    	else if(DropdownName == "Currency")
		    	{
		    		List<Currency> ddlList = currencyService.listCurrencies();
		    		
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getCurrencyId().toString(), ddlList.get(i).getCurrencyName());
		    		}
		    		
		    	}
		    	else if(DropdownName == "OperationMode")
		    	{

					ddlMap.put("2", "Api Mode");
		    		ddlMap.put("1", "Fetch Mode");
					ddlMap.put("0", "Receive Mode");		    	
		    	}
		    	else if(DropdownName == "SubFolder")
		    	{
		    		ddlMap.put("0", "Don't allow subfolder files");
					ddlMap.put("1", "Allow subfolder files");		    	
		    	}
		    	else if(DropdownName == "Country")
		    	{
		    		List<Country> ddlList = countryService.listCountries();
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getCountryId().toString(), ddlList.get(i).getCountryName());
		    		}
		    	}
		    	else if(DropdownName == "State")
		    	{
		    		List<State> ddlList = stateService.listStates();
		    		for(int i=0;i< ddlList.size();i++)
		    		{
		    			ddlMap.put(ddlList.get(i).getStateId().toString(), ddlList.get(i).getStateName());
		    		}
		    	}
		    	
				return ddlMap;
			}
		 
		 
		 public List<Site> getUpdatedSiteList()
		 {
			 List<Site> ddlSitesList = siteService.listSites();
		      
			 
			 
			 
			 List<Customer> ddlCustomerList = customerService.listCustomers();
			 List<SiteType> ddlSiteTypeList = sitetypeService.listSiteTypes();
			 List<Country> ddlCountryList = countryService.listCountries();
			 List<State> ddlStateList = stateService.listStates();
			 List<Currency> ddlCurrencyList = currencyService.listCurrencies();
				 
			 
			 
	        	
			 
			 
		        for(int i=0;i<ddlSitesList.size();i++)
		        {
		        	List<Customer> lstCustomerList = new ArrayList<Customer>();
					List<SiteType> lstSiteTypeList = new ArrayList<SiteType>();
					List<Country> lstCountryList = new ArrayList<Country>();
					List<State> lstStateList = new ArrayList<State>();
					List<Currency> lstCurrencyList = new ArrayList<Currency>();
					 
		        	
		        	int cid = ddlSitesList.get(i).getCustomerID();
					int stype = ddlSitesList.get(i).getSiteTypeID();
					int countryid = ddlSitesList.get(i).getCountryID();
					int stateid = ddlSitesList.get(i).getStateID();
					int currid = ddlSitesList.get(i).getCurrencyID();
					
					 
					lstSiteTypeList = ddlSiteTypeList.stream().filter(p -> p.getSiteTypeId() == stype).collect(Collectors.toList());
					lstCustomerList = ddlCustomerList.stream().filter(p -> p.getCustomerId() == cid ).collect(Collectors.toList());
					lstCountryList = ddlCountryList.stream().filter(p -> p.getCountryId() == countryid).collect(Collectors.toList());
					lstStateList = ddlStateList.stream().filter(p -> p.getStateId() == stateid ).collect(Collectors.toList());
					lstCurrencyList = ddlCurrencyList.stream().filter(p -> p.getCurrencyId() == currid ).collect(Collectors.toList());
					
					
		        	if(lstSiteTypeList.size() >0)
		        	{
		        		ddlSitesList.get(i).setSiteTypeName(lstSiteTypeList.get(0).getSiteType());
		        	}
		        	
		        	if(lstCustomerList.size() >0)
		        	{
		        		ddlSitesList.get(i).setCustomerName(lstCustomerList.get(0).getCustomerName());
		        	}
		        	
		        	if(lstCountryList.size() >0)
		        	{
		        		ddlSitesList.get(i).setCountryName(lstCountryList.get(0).getCountryName());
		        	}
		        	
		        	
		        	if(lstStateList.size() >0)
		        	{
		        		ddlSitesList.get(i).setStateName(lstStateList.get(0).getStateName());
		        	}
		        	
		        	if(lstCurrencyList.size() >0)
		        	{
		        		ddlSitesList.get(i).setCurrencyName(lstCurrencyList.get(0).getCurrencyName());
		        	}
		        	
		        }

		        
		        
		        
			 
			    
		        return ddlSitesList;
		 }
		
		 

		 public String getSiteCode()
		 {
			 Site objSite =new Site();
			 objSite = siteService.getSiteByMax("SiteCode");
			 
			 
			 String SE = "";
			 
			 if(objSite != null)
			 {
				 SE = objSite.getSiteCode();
			 }
			 
			 
		    	if(SE ==null || SE =="")
		    	{
		    		SE ="SE00001";
		    	}
		    	else
		    	{
		    		try{
		    			int NextIndex = Integer.parseInt(SE.replaceAll("SE", "")) + 1;
		    			
		    			if(NextIndex >= 10000) {SE =  String.valueOf(NextIndex);}
		    			else if(NextIndex >= 1000) {SE = "0" + String.valueOf(NextIndex);}
		    			else if(NextIndex >= 100) {SE = "00" +  String.valueOf(NextIndex);}
		    			else if(NextIndex >= 10) {SE = "000" +  String.valueOf(NextIndex);}
		    			else {SE = "0000" +  String.valueOf(NextIndex);}
		    			
		    			SE = "SE" + SE;
		    		}
		    		catch(Exception e)
		    		{
		    			SE ="SE00001";
		    		}
		    	}
		    	
		    	return SE;
		 }

	 
	 
	    @RequestMapping(value = "/sites", method = RequestMethod.GET)
	    public String listSites(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	    	 
 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID()!=4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	    
	
	    	     
	    	     
	        model.addAttribute("site", new Site());
	        
	        model.addAttribute("siteList", getUpdatedSiteList());
	        model.addAttribute("sitetypeList", getDropdownList("SiteType"));
	        model.addAttribute("customerList", getDropdownList("Customer"));
	        model.addAttribute("currencyList", getDropdownList("Currency"));
	        model.addAttribute("countryList", getDropdownList("Country"));
	        model.addAttribute("stateList", getDropdownList("State"));
	        model.addAttribute("operationmodeList", getDropdownList("OperationMode"));
	        model.addAttribute("subfolderList", getDropdownList("SubFolder"));
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	        
	        return "site";
	    }
	 
	    // Same method For both Add and Update Site
	   // @RequestMapping(value = "/site/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewsite", method = RequestMethod.POST)
	    public String addsite(@ModelAttribute("site") Site site,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	 
	    	Date date = new Date();
		    if (site.getSiteId()==null || site.getSiteId() == 0) {
	            // new site, add it
		    	site.setSiteCode(getSiteCode());
		    	site.setCreationDate(date);
		    	
		    	
	            siteService.addSite(site);
	        } else {
	            // existing site, call update
	        	site.setLastUpdatedDate(date);
	            siteService.updateSite(site);
	        }
	 
	        return "redirect:/sites";
	 
	    }
	 
	    //@RequestMapping("/site/remove/{id}")
	    @RequestMapping("/removesite{id}")
	    public String removesite(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	 
	        siteService.removeSite(id);
	        return "redirect:/sites";
	    }
	 
	    //@RequestMapping("/site/edit/{id}")
	    @RequestMapping("/editsite{id}")
	    public String editsite(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	        model.addAttribute("site", siteService.getSiteById(id));

	        model.addAttribute("siteList", getUpdatedSiteList());
	        model.addAttribute("sitetypeList", getDropdownList("SiteType"));
	        model.addAttribute("customerList", getDropdownList("Customer"));
	        model.addAttribute("currencyList", getDropdownList("Currency"));
	        model.addAttribute("countryList", getDropdownList("Country"));
	        model.addAttribute("stateList", getDropdownList("State"));
	        model.addAttribute("operationmodeList", getDropdownList("OperationMode"));
	        model.addAttribute("subfolderList", getDropdownList("SubFolder"));
	        model.addAttribute("activeStatusList", getStatusDropdownList());

	        return "site";
	    }
}



