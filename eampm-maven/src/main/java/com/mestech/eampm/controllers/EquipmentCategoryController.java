package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.SiteType;
import com.mestech.eampm.model.State;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.SiteTypeService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;
import com.mestech.eampm.utility.Utility;

@Controller
public class EquipmentCategoryController {
	
	 @Autowired
	    private EquipmentCategoryService equipmentcategoryService;
	 
	 

	 @Autowired
	    private SiteTypeService sitetypeService;
	 
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	 
	 public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	 
	 public  Map<String,String> getDropdownList(String DropdownName) 
		{
	    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
	    	
	    	if(DropdownName == "SiteType")
	    	{
	    		List<SiteType> ddlList = sitetypeService.listSiteTypes();
	    		
	    		for(int i=0;i< ddlList.size();i++)
	    		{
	    			ddlMap.put(ddlList.get(i).getSiteTypeId().toString(), ddlList.get(i).getSiteType());
	    				
	    		}
	    		
	    	}
	    	else if(DropdownName == "CategoryGroup")
	    	{
	    		
				ddlMap.put("DL", "DL");
				ddlMap.put("PV", " PV");
				ddlMap.put("IC", "IC");
				ddlMap.put("NC", "NC");
				ddlMap.put("WS", "WS");
	    		ddlMap.put("INV", "INV");
	    		ddlMap.put("AUX", "AUX");
				ddlMap.put("CAB", "CAB");
				ddlMap.put("SCB", "SCB");
				ddlMap.put("LTP", "LTP");
				ddlMap.put("HTP", "HTP");
				ddlMap.put("TRN", "TRN");
				ddlMap.put("SYC", "SYC"); 
				ddlMap.put("Tracker", "Tracker");
				
				
	    	}
	    	
	    	
			return ddlMap;
		}
	 
	 
	 public List<EquipmentCategory> getUpdatedEquipmentCategoryList()
	 {
		 List<EquipmentCategory> ddlEquipmentCategoriesList = equipmentcategoryService.listEquipmentCategories();
     
		 for(int i=0;i<ddlEquipmentCategoriesList.size();i++)
		 {
				String SiteTypeName = sitetypeService.getSiteTypeById(ddlEquipmentCategoriesList.get(i).getSiteTypeID()).getSiteType();
				ddlEquipmentCategoriesList.get(i).setSiteTypeName(SiteTypeName);			
				
		 }
	        
	        return ddlEquipmentCategoriesList;
	 }
	 
	 
	 
	    @RequestMapping(value = "/equipmentcategories", method = RequestMethod.GET)
	    public String listEquipmentCategories(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	    	 
	    	 
 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID()!=4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	    
	
	    	     
	    	     
	        model.addAttribute("equipmentcategory", new EquipmentCategory());
	        
	        model.addAttribute("equipmentcategoryList",  getUpdatedEquipmentCategoryList());
	   	    model.addAttribute("sitetypeList", getDropdownList("SiteType"));
	        model.addAttribute("categorygroupList", getDropdownList("CategoryGroup"));
	        
	        	    
	    	model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	        return "equipmentcategory";
	    }
	 
	    // Same method For both Add and Update EquipmentCategory
	   // @RequestMapping(value = "/equipmentcategory/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewequipmentcategory", method = RequestMethod.POST)
	    public String addequipmentcategory(@ModelAttribute("equipmentcategory") EquipmentCategory equipmentcategory,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	
	    	Date date = new Date();
		      
	        if (equipmentcategory.getCategoryId()==null || equipmentcategory.getCategoryId() == 0) {
	            // new equipmentcategory, add it
	        	equipmentcategory.setCreationDate(date);
	            equipmentcategoryService.addEquipmentCategory(equipmentcategory);
	        } else {
	            // existing equipmentcategory, call update
	        	equipmentcategory.setLastUpdatedDate(date);
	            equipmentcategoryService.updateEquipmentCategory(equipmentcategory);
	        }
	 
	        return "redirect:/equipmentcategories";
	 
	    }
	 
	    //@RequestMapping("/equipmentcategory/remove/{id}")
	    @RequestMapping("/removeequipmentcategory{id}")
	    public String removeequipmentcategory(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	        equipmentcategoryService.removeEquipmentCategory(id);
	        return "redirect:/equipmentcategories";
	    }
	 
	    //@RequestMapping("/equipmentcategory/edit/{id}")
	    @RequestMapping("/editequipmentcategory{id}")
	    public String editequipmentcategory(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	        model.addAttribute("equipmentcategory", equipmentcategoryService.getEquipmentCategoryById(id));
	        
	        model.addAttribute("equipmentcategoryList",  getUpdatedEquipmentCategoryList());
	   	    model.addAttribute("sitetypeList", getDropdownList("SiteType"));
	        model.addAttribute("categorygroupList", getDropdownList("CategoryGroup"));
	        
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	return "equipmentcategory";
	    }
}
