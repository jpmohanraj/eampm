package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Activity;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.ActivityService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class ActivityController {
	
	 @Autowired
	    private ActivityService activityService;
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	 

		public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	   
	    @RequestMapping(value = "/activities", method = RequestMethod.GET)
	    public String listActivities(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID() != 4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	    
	    	     
	    	 
	        model.addAttribute("activity", new Activity());
	        model.addAttribute("activityList", activityService.listActivities());	
	    	model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
			
	        return "activity";
	    }
	 
	    // Same method For both Add and Update Activity
	   // @RequestMapping(value = "/activity/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewactivity", method = RequestMethod.POST)
	    public String addactivity(@ModelAttribute("activity") Activity activity,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	Date date = new Date();
	        if (activity.getActivityId()==null || activity.getActivityId() == 0) {
	            // new activity, add it
	        	activity.setCreationDate(date);
	            activityService.addActivity(activity);
	        } else {
	            // existing activity, call update
	        	activity.setLastUpdatedDate(date);
	            activityService.updateActivity(activity);
	        }
	 
	        return "redirect:/activities";
	 
	    }
	 
	    //@RequestMapping("/activity/remove/{id}")
	    @RequestMapping("/removeactivity{id}")
	    public String removeactivity(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	        activityService.removeActivity(id);
	        return "redirect:/activities";
	    }
	 
	    //@RequestMapping("/activity/edit/{id}")
	    @RequestMapping("/editactivity{id}")
	    public String editactivity(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
	        model.addAttribute("activity", activityService.getActivityById(id));
	        model.addAttribute("activityList", activityService.listActivities());
	        model.addAttribute("activeStatusList", getStatusDropdownList());
	    	return "activity";
	    }
}
