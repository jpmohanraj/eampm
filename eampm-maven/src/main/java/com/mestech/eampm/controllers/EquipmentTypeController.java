package com.mestech.eampm.controllers;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.mestech.eampm.bean.AccessListBean;
import com.mestech.eampm.model.Country;
import com.mestech.eampm.model.CountryRegion;
import com.mestech.eampm.model.EquipmentCategory;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.RoleActivity;
import com.mestech.eampm.model.User;
import com.mestech.eampm.service.EquipmentCategoryService;
import com.mestech.eampm.service.EquipmentTypeService;
import com.mestech.eampm.service.RoleActivityService;
import com.mestech.eampm.service.UserRoleService;
import com.mestech.eampm.service.UserService;

@Controller
public class EquipmentTypeController {
	
	 @Autowired
	    private EquipmentTypeService equipmenttypeService;
	 
	 @Autowired
	    private EquipmentCategoryService equipmentcategoryService;
	 
	 

	 @Autowired
	    private UserService userService;

	 @Autowired
	    private UserRoleService userRoleService;
	
	 @Autowired
	 private RoleActivityService roleActivityService;
	 

	 
	 
	 public Map<String,String> getStatusDropdownList() 
		{
			Map<String,String> ddlMap = new LinkedHashMap<String,String>();
			ddlMap.put("1", "Active");
			ddlMap.put("0", "In-active");
			return ddlMap;
		}
	 
	 
	 
	 public  Map<String,String> getDropdownList(String DropdownName) 
		{
	    	Map<String,String> ddlMap = new LinkedHashMap<String,String>();
	    	
	    	if(DropdownName == "EquipmentCategory")
	    	{
	    		List<EquipmentCategory> ddlList = equipmentcategoryService.listEquipmentCategories();
	    		
	    		for(int i=0;i< ddlList.size();i++)
	    		{
	    			ddlMap.put(ddlList.get(i).getCategoryId().toString(), ddlList.get(i).getEquipmentCategory());
	    				
	    		}
	    		
	    	}
	    	
			return ddlMap;
		}
	 
	 
	 public List<EquipmentType> getUpdatedEquipmentTypeList()
	 {
		 List<EquipmentType> ddlEquipmentTypesList = equipmenttypeService.listEquipmentTypes();
	       
	        for(int i=0;i<ddlEquipmentTypesList.size();i++)
	        {
	        	String CategoryName = equipmentcategoryService.getEquipmentCategoryById(ddlEquipmentTypesList.get(i).getCategoryID()).getEquipmentCategory();
	        	ddlEquipmentTypesList.get(i).setCategoryName(CategoryName);
	        }
	        
	        return ddlEquipmentTypesList;
	 }
	
	 
	 public String getEquipmentTypeCode()
	 {
		 String ET = equipmenttypeService.getEquipmentTypeByMax("EquipmentTypeCode").getEquipmentTypeCode();
	    	if(ET ==null || ET =="")
	    	{
	    		ET ="ET00001";
	    	}
	    	else
	    	{
	    		try{
	    			int NextIndex = Integer.parseInt(ET.replaceAll("ET", "")) + 1;
	    			
	    			if(NextIndex >= 10000) {ET =  String.valueOf(NextIndex);}
	    			else if(NextIndex >= 1000) {ET = "0" + String.valueOf(NextIndex);}
	    			else if(NextIndex >= 100) {ET = "00" +  String.valueOf(NextIndex);}
	    			else if(NextIndex >= 10) {ET = "000" +  String.valueOf(NextIndex);}
	    			else {ET = "0000" +  String.valueOf(NextIndex);}
	    			
	    			ET = "ET" + ET;
	    		}
	    		catch(Exception e)
	    		{
	    			ET ="ET00001";
	    		}
	    	}
	    	
	    	return ET;
	 }
	 
	 
	    @RequestMapping(value = "/equipmenttypes", method = RequestMethod.GET)
	    public String listEquipmentTypes(Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	    	 
 int id = Integer.parseInt(session.getAttribute("eampmuserid").toString());
	    	 
	    	 AccessListBean objAccessListBean = new AccessListBean();
	    	   
	    	    try
	    	    {
	    	    User objUser =  userService.getUserById(id);
	    		//UserRole objUserRole = userRoleService.getUserRoleById(objUser.getRoleID());
	    	    List<RoleActivity> lstRoleActivity = new ArrayList<RoleActivity>();
	    	 	
	    	    lstRoleActivity = roleActivityService.listRoleActivities().stream().filter(p -> p.getRoleID() == objUser.getRoleID() ).collect(Collectors.toList());
	    	   
	    	    objAccessListBean.setUserID(id);
	    	    objAccessListBean.setUserName(objUser.getUserName());
	    	    
	    	    
	    	    
	    	    if(objUser.getRoleID()!=4)
	    		{
	    			objAccessListBean.setCustomerListView("visible");
	    		}
	    	    
	    	    
	    	    for(int l=0;l<lstRoleActivity.size();l++)
	    	    {
	    	    	if(lstRoleActivity.get(l).getActivityID()==1 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setDashboard("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==2 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setOverview("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==3 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setSystemMonitoring("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==4 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setVisualization("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==5 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setAnalytics("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==6 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setPortfolioManagement("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==7 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setTicketing("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==8 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setForcasting("visible");
	    	    	}
	    	    	else if(lstRoleActivity.get(l).getActivityID()==9 && lstRoleActivity.get(l).getActiveFlag()==1)
	    	    	{
	    	    		objAccessListBean.setConfiguration("visible");
	    	    	}
	    	    }
	    	    
	    	    }
	    	    catch(Exception e)
	    	    {
	    	    	System.out.println(e.getMessage());
	    	    }
	    	    
	    	     model.addAttribute("access",objAccessListBean);
	    	    
	
	    	     
	    	     
	        model.addAttribute("equipmenttype", new EquipmentType());
	        model.addAttribute("equipmentcategoryList", getDropdownList("EquipmentCategory"));	
	        model.addAttribute("equipmenttypeList", getUpdatedEquipmentTypeList());	    
	    	model.addAttribute("activeStatusList", getStatusDropdownList());
	    	
	        return "equipmenttype";
	    }
	 
	    // Same method For both Add and Update EquipmentType
	   // @RequestMapping(value = "/equipmenttype/add", method = RequestMethod.POST)
	    @RequestMapping(value = "/addnewequipmenttype", method = RequestMethod.POST)
	    public String addequipmenttype(@ModelAttribute("equipmenttype") EquipmentType equipmenttype,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	 
	    	Date date = new Date();
		    if (equipmenttype.getEquipmentTypeId()==null || equipmenttype.getEquipmentTypeId() == 0) {
	            // new equipmenttype, add it
		    	
		    	
		    	equipmenttype.setEquipmentTypeCode(getEquipmentTypeCode());
		    	equipmenttype.setCreationDate(date);
	            equipmenttypeService.addEquipmentType(equipmenttype);
	        } else {
	            // existing equipmenttype, call update
	        	equipmenttype.setLastUpdatedDate(date);
	            equipmenttypeService.updateEquipmentType(equipmenttype);
	        }
	 
	        return "redirect:/equipmenttypes";
	 
	    }
	 
	    //@RequestMapping("/equipmenttype/remove/{id}")
	    @RequestMapping("/removeequipmenttype{id}")
	    public String removeequipmenttype(@PathVariable("id") int id,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	 
	        equipmenttypeService.removeEquipmentType(id);
	        return "redirect:/equipmenttypes";
	    }
	 
	    //@RequestMapping("/equipmenttype/edit/{id}")
	    @RequestMapping("/editequipmenttype{id}")
	    public String editequipmenttype(@PathVariable("id") int id, Model model,HttpSession session) {
	    	
	    	 if(session.getAttribute("eampmuserid")==null || session.getAttribute("eampmuserid").equals(""))
	    	 {
	    		 return "redirect:/login";
	    	 }
	    	 
	        model.addAttribute("equipmenttype", equipmenttypeService.getEquipmentTypeById(id));
	        model.addAttribute("equipmentcategoryList", getDropdownList("EquipmentCategory"));	
	        model.addAttribute("equipmenttypeList", getUpdatedEquipmentTypeList());	    
	    	model.addAttribute("activeStatusList", getStatusDropdownList());
	    	return "equipmenttype";
	    }
}
