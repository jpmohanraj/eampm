

package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.RoleActivity;

public interface RoleActivityDAO {
	 
	public void addRoleActivity(RoleActivity roleactivity);
	    
	public void updateRoleActivity(RoleActivity roleactivity);
	    
	public RoleActivity getRoleActivityById(int id);
	    
	public void removeRoleActivity(int id);
	    
	public List<RoleActivity> listRoleActivities();
}


