package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.Equipment;
import com.mestech.eampm.model.Site;

@Repository
public class EquipmentDAOImpl implements EquipmentDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addEquipment(Equipment equipment) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(equipment);
       
   }

   //@Override
   public void updateEquipment(Equipment equipment) {
       Session session = sessionFactory.getCurrentSession();
       session.update(equipment);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<Equipment> listEquipments() {
       Session session = sessionFactory.getCurrentSession();
       List<Equipment> EquipmentsList = session.createQuery("from Equipment").list();
       
       return EquipmentsList;
   }

   
   @SuppressWarnings("unchecked")
   //@Override
   public List<Equipment> listEquipmentsBySiteId(int siteId){
       Session session = sessionFactory.getCurrentSession();
       List<Equipment> EquipmentsList = session.createQuery("from Equipment where SiteId='" + siteId + "'").list();
       
       return EquipmentsList;
   }
   

   @SuppressWarnings("unchecked")
   //@Override
   public List<Equipment> listEquipmentsByUserId(int userId){
       Session session = sessionFactory.getCurrentSession();
       List<Equipment> EquipmentsList = session.createQuery("from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from Site where CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "') )").list();
	      
       
       return EquipmentsList;
   }
   
      
   
   //@Override
   public Equipment getEquipmentById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       Equipment equipment = (Equipment) session.get(Equipment.class, new Integer(id));
       return equipment;
   }

   
   public Equipment getEquipmentByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       Equipment equipment =  null;
       
       try
       {
    	   equipment = (Equipment) session.createQuery("from Equipment Order By " + MaxColumnName +  " desc")
    	   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {}
       catch(Exception ex)
       {}
       
       
       return equipment;
   }
   
   
   //@Override
   public void removeEquipment(int id) {
       Session session = sessionFactory.getCurrentSession();
       Equipment equipment = (Equipment) session.get(Equipment.class, new Integer(id));
       
       //De-activate the flag
       equipment.setActiveFlag(0);
     
       if(null != equipment){
           //session.delete(equipment);
    	   
    	   session.update(equipment);
       }
   }
   
   
   
   
   
   
   @SuppressWarnings("unchecked")
   //@Override
   public List<Equipment> listInverters() {
       Session session = sessionFactory.getCurrentSession();
       List<Equipment> EquipmentsList = session.createQuery("from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))").list();
       
       return EquipmentsList;
   }

   
   @SuppressWarnings("unchecked")
   //@Override
   public List<Equipment> listInvertersBySiteId(int siteId){
       Session session = sessionFactory.getCurrentSession();
       List<Equipment> EquipmentsList = session.createQuery("from Equipment where ActiveFlag='1' and SiteId='" + siteId + "' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))").list();
       
       return EquipmentsList;
   }
   

   @SuppressWarnings("unchecked")
   //@Override
   public List<Equipment> listInvertersByUserId(int userId){
       Session session = sessionFactory.getCurrentSession();
       List<Equipment> EquipmentsList = session.createQuery("from Equipment where ActiveFlag='1' and SiteId in (Select SiteId from Site where  ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "')) and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))").list();
                      
       
       return EquipmentsList;
   }
   
      
   
   
   
   
}
