
package com.mestech.eampm.dao;


import java.util.List;

import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;

public interface SiteDAO {
	 
	public void addSite(Site site);
	    
	public void updateSite(Site site);
	    
	public Site getSiteById(int id);
	
	public Site getSiteByName(String siteName);
    
	public List<Site> getSiteListByCustomerId(int customerId);
	
	public List<Site> listSiteById(int id);
	
	public Site getSiteByMax(String MaxColumnName);
	
	public List<Site> getSiteListByUserId(int userId);
	    
	public void removeSite(int id);
	    
	public List<Site> listSites();
}



