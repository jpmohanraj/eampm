



package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.SiteType;

public interface SiteTypeDAO {
	 
	public void addSiteType(SiteType sitetype);
	    
	public void updateSiteType(SiteType sitetype);
	    
	public SiteType getSiteTypeById(int id);
	    
	public void removeSiteType(int id);
	    
	public List<SiteType> listSiteTypes();
}

