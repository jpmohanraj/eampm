package com.mestech.eampm.dao;

import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerMap;
import com.mestech.eampm.model.EquipmentType;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;

@Repository
public class CustomerDAOImpl implements CustomerDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addCustomer(Customer customer) {
	   
	   
	   Session session = sessionFactory.openSession(); //create the session object
       session.beginTransaction();//create the transaction from the session object
       
       
       String CustomerCode = customer.getCustomerCode();
       
       CustomerMap customerMap = new CustomerMap();
       customerMap.setUserID(2);
       customerMap.setActiveFlag(customer.getActiveFlag());
       customerMap.setCreatedBy(customer.getCreatedBy());
       customerMap.setCreationDate(customer.getCreationDate());
       
       
       
       
       
       session.save(customer); 
       
       Customer customer1 = (Customer) session.createQuery("from Customer where CustomerCode='" + CustomerCode + "'").list().get(0);
       customerMap.setCustomerID(customer1.getCustomerId());
       
       session.save(customerMap);
       
       session.getTransaction().commit(); //close the transaction
       session.close(); //close the session
       
       
       
       
       /*Session session = sessionFactory.getCurrentSession();
       session.persist(customer);
       */
       
    
       
       
   }

   //@Override
   public void updateCustomer(Customer customer) {
       Session session = sessionFactory.getCurrentSession();
       session.update(customer);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<Customer> listCustomers() {
       Session session = sessionFactory.getCurrentSession();
       List<Customer> CustomersList = session.createQuery("from Customer").list();
      
       return CustomersList;
   }

   
   public List<Customer> getCustomerListByUserId(int userId)
	{
		  Session session = sessionFactory.getCurrentSession();
	       List<Customer> CustomerList = session.createQuery("from Customer where  ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "')").list();
	       
	       return CustomerList;
	}
	
   
   //@Override
   public Customer getCustomerById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       Customer customer = (Customer) session.get(Customer.class, new Integer(id));
       return customer;
   }
   
   public Customer getCustomerByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       Customer customer = null;
       
       try
       {
    	   customer = (Customer) session.createQuery("from Customer Order By " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {
    	   
       }
       catch(Exception ex)
       {
    	   
       }
       
       return customer;
   }
   
   

   //@Override
   public void removeCustomer(int id) {
       Session session = sessionFactory.getCurrentSession();
       Customer customer = (Customer) session.get(Customer.class, new Integer(id));
       
       //De-activate the flag
       customer.setActiveFlag(0);
       
       if(null != customer){
           //session.delete(customer);
           session.update(customer);
       }
   }
}
