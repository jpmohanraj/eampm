package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.SopDetail;

public interface SopDetailDAO {
	 
	public void addSopDetail(SopDetail sopdetail);
	    
	public void updateSopDetail(SopDetail sopdetail);
	    
	public SopDetail getSopDetailById(int id);
	    
	public void removeSopDetail(int id);
	    
	public List<SopDetail> listSopDetails();
}




