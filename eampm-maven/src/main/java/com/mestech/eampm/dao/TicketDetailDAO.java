package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.TicketDetail;

public interface TicketDetailDAO {
	 
	public void addTicketDetail(TicketDetail ticketdetail);
	    
	public void updateTicketDetail(TicketDetail ticketdetail);
	    
	public TicketDetail getTicketDetailById(int id);
	
	public List<TicketDetail> getTicketDetailListByUserId(int userId);
	
	public List<TicketDetail> getTicketDetailListBySiteId(int siteId);	
	    
	public List<TicketDetail> listTicketDetailsForDisplay(int siteId, String fromDate,String toDate,String category, String type, String priority );
		  
	public TicketDetail getTicketDetailByMax(String MaxColumnName);
	
	public List<TicketDetail> getTicketDetailListByCustomerId(int customerId);
	
	public TicketDetail getTicketDetailByTicketCode(String TicketCode);
    
	public void removeTicketDetail(int id);
	    
	public List<TicketDetail> listTicketDetails();
}
