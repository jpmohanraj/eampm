package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.SiteSummary;


@Repository
public class SiteSummaryDAOImpl implements SiteSummaryDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addSiteSummary(SiteSummary SiteSummary) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(SiteSummary);
       
   }

   //@Override
   public void updateSiteSummary(SiteSummary SiteSummary) {
       Session session = sessionFactory.getCurrentSession();
       session.update(SiteSummary);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<SiteSummary> listSiteSummarys() {
       Session session = sessionFactory.getCurrentSession();
       List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary").list();
       
       return SiteSummarysList;
   }

   
   @SuppressWarnings("unchecked")
   public List<SiteSummary> listSiteSummarysByFilter1(int equipmentId,int siteId, int lastNDays ) {
    
	   Session session = sessionFactory.getCurrentSession();
       List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary where EquipmentId='" + equipmentId + "' and SiteId='" + siteId + "' and  trunc(timestamp)=trunc(sysdate-" + lastNDays + ") order by Timestamp desc").list();
       return SiteSummarysList;
   }
   
   
   
  
   
   
   

   public List<SiteSummary> getSiteSummaryListByUserId(int userId)
	{
		  Session session = sessionFactory.getCurrentSession();
		  List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "'))").list();
	        
	       return SiteSummarysList;
	}
  
   
   public List<SiteSummary> getSiteSummaryListByCustomerId(int customerId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID='" + customerId + "')").list();
       
       return SiteSummarysList;
   }
   
   
   public List<SiteSummary> getSiteSummaryListBySiteId(int SiteId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary where SiteID = '" + SiteId + "' and TimeStamp <= trunc(sysdate+1) and  TimeStamp > trunc(to_date('01-01-2015','DD-MM-YYYY')) order by TimeStamp").list();
       
       return SiteSummarysList;
   }

	
	public List<SiteSummary> getSiteSummaryListByEquipmentId(int equipmentId) {
		Session session = sessionFactory.getCurrentSession();
	    List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary order by TimeStamp").list();
		//List<SiteSummary> SiteSummarysList = session.createQuery("from SiteSummary where EquipmentID = '" + equipmentId + "' and  trunc(TimeStamp)=trunc(sysdate) order by TimeStamp").list();
	     
	    return SiteSummarysList;
   }

   
   //@Override
   public SiteSummary getSiteSummaryById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       SiteSummary SiteSummary = (SiteSummary) session.get(SiteSummary.class, new Integer(id));
       return SiteSummary;
   }

   
   public SiteSummary getSiteSummaryByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       SiteSummary SiteSummary = (SiteSummary) session.createQuery("from SiteSummary Order By " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       return SiteSummary;
   }
   
   
   //@Override
   public void removeSiteSummary(int id) {
       Session session = sessionFactory.getCurrentSession();
       SiteSummary SiteSummary = (SiteSummary) session.get(SiteSummary.class, new Integer(id));
       
       //De-activate the flag
       SiteSummary.setActiveFlag(0);
     
       if(null != SiteSummary){
           //session.delete(SiteSummary);
    	   
    	   session.update(SiteSummary);
       }
   }
}
