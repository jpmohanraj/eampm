
package com.mestech.eampm.dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.SQLQuery;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.DataTransaction;

@Repository
public class DataTransactionDAOImpl implements DataTransactionDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addDataTransaction(DataTransaction dataTransaction) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(dataTransaction);
       
   }

   //@Override
   public void updateDataTransaction(DataTransaction dataTransaction) {
       Session session = sessionFactory.getCurrentSession();
       session.update(dataTransaction);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<DataTransaction> listDataTransactions() {
       Session session = sessionFactory.getCurrentSession();
       List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction").list();
       
       return DataTransactionsList;
   }

   
   @SuppressWarnings("unchecked")
   public List<DataTransaction> listDataTransactionsByFilter1(int equipmentId,int siteId, int lastNDays ) {
    
	   Session session = sessionFactory.getCurrentSession();
       List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction where EquipmentId='" + equipmentId + "' and SiteId='" + siteId + "' and  trunc(timestamp)=trunc(sysdate-" + lastNDays + ") order by Timestamp desc").list();
       return DataTransactionsList;
   }
   
   
   
   

   
   @SuppressWarnings("unchecked")
   public List<DataTransaction> listDataTransactionsByFilter2(int siteId, int lastNDays ) {
    
	   Session session = sessionFactory.getCurrentSession();
       List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction where SiteId='" + siteId + "' and  trunc(timestamp)>=trunc(sysdate-" + lastNDays + ") order by EquipmentId,Timestamp").list();
       return DataTransactionsList;
   }
   
   
   

   
   @SuppressWarnings("unchecked")
   public List<DataTransaction> listDataTransactionsForDataDownload(int siteId, String fromDate,String toDate,int equipmentId ) {
    
	   List<DataTransaction> DataTransactionsList = new  ArrayList<DataTransaction>();
	   String EquipmentCondition = "";
	   
	   if(equipmentId!=0 && equipmentId!=-1)
	   {
		   EquipmentCondition = " and EquipmentID ='" + equipmentId + "' ";
	   }
	   
	 try
	 {
	   
	   Session session = sessionFactory.getCurrentSession();
	   DataTransactionsList  = session.createQuery("from DataTransaction where SiteId='" + siteId + "' and  trunc(timestamp)>=trunc(to_date('" + fromDate + "','dd-mm-yyyy')) and  trunc(timestamp)<=trunc(to_date('" + toDate + "','dd-mm-yyyy')) " + EquipmentCondition + "  order by EquipmentId,Timestamp").list();
	 }
	 catch(Exception e)
	 {}
      
	 return DataTransactionsList;
   }
   
   
   
   
   
   

   public List<DataTransaction> getDataTransactionListByUserId(int userId)
	{
		  Session session = sessionFactory.getCurrentSession();
		  List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "'))").list();
	        
	       return DataTransactionsList;
	}
  
   
   public List<DataTransaction> getDataTransactionListByCustomerId(int customerId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID='" + customerId + "')").list();
       
       return DataTransactionsList;
   }
   
   
   public List<DataTransaction> getDataTransactionListBySiteId(int SiteId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction where SiteID = '" + SiteId + "'").list();
       
       return DataTransactionsList;
   }

	
	public List<DataTransaction> getDataTransactionListByEquipmentId(int equipmentId) {
		Session session = sessionFactory.getCurrentSession();
	    List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction where EquipmentID = '" + equipmentId + "' order by TimeStamp").list();
		//List<DataTransaction> DataTransactionsList = session.createQuery("from DataTransaction where EquipmentID = '" + equipmentId + "' and  trunc(TimeStamp)=trunc(sysdate) order by TimeStamp").list();
	     
	    return DataTransactionsList;
   }

   
   //@Override
   public DataTransaction getDataTransactionById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       DataTransaction dataTransaction = (DataTransaction) session.get(DataTransaction.class, new Integer(id));
       return dataTransaction;
   }

   
   public DataTransaction getDataTransactionByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       DataTransaction dataTransaction = (DataTransaction) session.createQuery("from DataTransaction Order By " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       return dataTransaction;
   }
   
   
   //@Override
   public void removeDataTransaction(int id) {
       Session session = sessionFactory.getCurrentSession();
       DataTransaction dataTransaction = (DataTransaction) session.get(DataTransaction.class, new Integer(id));
       
       //De-activate the flag
       dataTransaction.setActiveFlag(0);
     
       if(null != dataTransaction){
           //session.delete(dataTransaction);
    	   
    	   session.update(dataTransaction);
       }
   }
}
