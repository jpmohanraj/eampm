


package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.Status;

public interface StatusDAO {
	 
	public void addStatus(Status status);
	    
	public void updateStatus(Status status);
	    
	public Status getStatusById(int id);
	    
	public void removeStatus(int id);
	    
	public List<Status> listStatuses();
}



