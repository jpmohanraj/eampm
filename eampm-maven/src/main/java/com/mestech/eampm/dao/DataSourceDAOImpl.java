package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.mestech.eampm.model.DataSource;

@Repository
public class DataSourceDAOImpl implements DataSourceDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addDataSource(DataSource dataSource) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(dataSource);
       
   }

   //@Override
   public void updateDataSource(DataSource dataSource) {
       Session session = sessionFactory.getCurrentSession();
       session.update(dataSource);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<DataSource> listDataSources() {
       Session session = sessionFactory.getCurrentSession();
       List<DataSource> DataSourcesList = session.createQuery("from DataSource").list();
       
       return DataSourcesList;
   }


   public List<DataSource> getDataSourceListByUserId(int userId)
	{
		  Session session = sessionFactory.getCurrentSession();
		  List<DataSource> DataSourcesList = session.createQuery("from DataSource where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "'))").list();
	        
	       return DataSourcesList;
	}
  
   
   public List<DataSource> getDataSourceListByCustomerId(int customerId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<DataSource> DataSourcesList = session.createQuery("from DataSource where SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID='" + customerId + "')").list();
       
       return DataSourcesList;
   }
   
   
   public List<DataSource> getDataSourceListBySiteId(int SiteId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<DataSource> DataSourcesList = session.createQuery("from DataSource where SiteID = '" + SiteId + "' order by EquipmentId,TimeStamp").list();
       
       return DataSourcesList;
   }

	


   
   
	public List<DataSource> getDataSourceListByEquipmentId(int equipmentId) {
		Session session = sessionFactory.getCurrentSession();
	    List<DataSource> DataSourcesList = session.createQuery("from DataSource where EquipmentID = '" + equipmentId + "' order by TimeStamp").list();
	       
	    return DataSourcesList;
   }

   
   //@Override
   public DataSource getDataSourceById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       DataSource dataSource = (DataSource) session.get(DataSource.class, new Integer(id));
       return dataSource;
   }

   
   public DataSource getDataSourceByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       DataSource dataSource = (DataSource) session.createQuery("from DataSource Order By " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       return dataSource;
   }
   
   
   //@Override
   public void removeDataSource(int id) {
       Session session = sessionFactory.getCurrentSession();
       DataSource dataSource = (DataSource) session.get(DataSource.class, new Integer(id));
       
       //De-activate the flag
       dataSource.setActiveFlag(0);
     
       if(null != dataSource){
           //session.delete(dataSource);
    	   
    	   session.update(dataSource);
       }
   }
   
   
   
   
   

   
   @SuppressWarnings("unchecked")
   //@Override
   public List<DataSource> listDataSourceForInverters() {
       Session session = sessionFactory.getCurrentSession();
       List<DataSource> DataSourcesList = session.createQuery("from DataSource where EquipmentID in (Select EquipmentId from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR'))))").list();
       
       return DataSourcesList;
   }

//@Override
   public List<DataSource> getDataSourceForInvertersListByUserId(int userId)
                {
                                  Session session = sessionFactory.getCurrentSession();
                                  List<DataSource> DataSourcesList = session.createQuery("from DataSource where  EquipmentID in (Select EquipmentId from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))) and SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "'))").list();
                        
                       return DataSourcesList;
                }
  
 //@Override
   public List<DataSource> getDataSourceForInvertersListByCustomerId(int customerId)
   {
                   Session session = sessionFactory.getCurrentSession();
       List<DataSource> DataSourcesList = session.createQuery("from DataSource where  EquipmentID in (Select EquipmentId from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))) and SiteID in (Select SiteId from Site where ActiveFlag='1' and CustomerID='" + customerId + "')").list();
       
       return DataSourcesList;
   }
   
 //@Override
   public List<DataSource> getDataSourceForInvertersListBySiteId(int SiteId)
   {
                   Session session = sessionFactory.getCurrentSession();
       List<DataSource> DataSourcesList = session.createQuery("from DataSource where  EquipmentID in (Select EquipmentId from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))) and SiteID = '" + SiteId + "' order by EquipmentId,TimeStamp").list();
       
       return DataSourcesList;
   }

                
   
                public List<DataSource> getDataSourceForInvertersListByEquipmentId(int equipmentId) {
                                Session session = sessionFactory.getCurrentSession();
                    List<DataSource> DataSourcesList = session.createQuery("from DataSource where  EquipmentID in (Select EquipmentId from Equipment where ActiveFlag='1' and EquipmentTypeID in (select EquipmentTypeId from EquipmentType where  ActiveFlag='1' and CategoryID in (select CategoryId from EquipmentCategory where  ActiveFlag='1' and EquipmentCategory in ('CENTRLINVRTR','STRINGINVRTR')))) and EquipmentID = '" + equipmentId + "' order by TimeStamp").list();
                       
                    return DataSourcesList;
   }

   
   
   
   
}
