package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.EquipmentParameter;

public interface EquipmentParameterDAO {
	 
	public void addEquipmentParameter(EquipmentParameter equipmentparameter);
	    
	public void updateEquipmentParameter(EquipmentParameter equipmentparameter);
	    
	public EquipmentParameter getEquipmentParameterById(int id);
	    
	public void removeEquipmentParameter(int id);
	    
	public List<EquipmentParameter> listEquipmentParameters();
}
