
package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.RoleActivity;

@Repository
public class RoleActivityDAOImpl implements RoleActivityDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addRoleActivity(RoleActivity roleactivity) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(roleactivity);
       
       System.out.println("DAO Impl Completed");
   }

   //@Override
   public void updateRoleActivity(RoleActivity roleactivity) {
       Session session = sessionFactory.getCurrentSession();
       session.update(roleactivity);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<RoleActivity> listRoleActivities() {
       Session session = sessionFactory.getCurrentSession();
       List<RoleActivity> RoleActivitiesList = session.createQuery("from RoleActivity").list();
       
       return RoleActivitiesList;
   }

   //@Override
   public RoleActivity getRoleActivityById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       RoleActivity roleactivity = (RoleActivity) session.get(RoleActivity.class, new Integer(id));
       return roleactivity;
   }

   //@Override
   public void removeRoleActivity(int id) {
       Session session = sessionFactory.getCurrentSession();
       RoleActivity roleactivity = (RoleActivity) session.get(RoleActivity.class, new Integer(id));
       if(null != roleactivity){
           session.delete(roleactivity);
       }
   }
}

