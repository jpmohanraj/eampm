package com.mestech.eampm.dao;

import java.util.List;

import com.mestech.eampm.model.SiteStatus;

public interface SiteStatusDAO {

	public List<SiteStatus> getSiteStatusListByCustomerId(int customerId);

	public List<SiteStatus> listSiteStatus();
	
	public List<SiteStatus> getSiteStatusListByUserId(int userId);

	public SiteStatus getSiteStatusBySiteId(int siteId);
}
