package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.EquipmentCategory;

@Repository
public class EquipmentCategoryDAOImpl implements EquipmentCategoryDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addEquipmentCategory(EquipmentCategory equipmentcategory) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(equipmentcategory);
       
   }

   //@Override
   public void updateEquipmentCategory(EquipmentCategory equipmentcategory) {
       Session session = sessionFactory.getCurrentSession();
       session.update(equipmentcategory);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<EquipmentCategory> listEquipmentCategories() {
       Session session = sessionFactory.getCurrentSession();
       List<EquipmentCategory> EquipmentCategoriesList = session.createQuery("from EquipmentCategory").list();
       
       return EquipmentCategoriesList;
   }

   //@Override
   public EquipmentCategory getEquipmentCategoryById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       EquipmentCategory equipmentcategory = (EquipmentCategory) session.get(EquipmentCategory.class, new Integer(id));
       return equipmentcategory;
   }

   //@Override
   public void removeEquipmentCategory(int id) {
       Session session = sessionFactory.getCurrentSession();
       EquipmentCategory equipmentcategory = (EquipmentCategory) session.get(EquipmentCategory.class, new Integer(id));
      
       //De-activate the flag
       equipmentcategory.setActiveFlag(0);
      
       if(null != equipmentcategory){
           //session.delete(equipmentcategory);    	   
    	   session.update(equipmentcategory);
       }
   }
}
