package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.Event;

public interface EventDAO {
	 
	public void addEvent(Event event);
	    
	public void updateEvent(Event event);
	    
	public Event getEventById(int id);
	    
	public void removeEvent(int id);
	    
	public List<Event> listEvents();
}
