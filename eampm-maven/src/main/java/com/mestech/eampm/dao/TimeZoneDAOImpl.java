package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.TimeZone;

@Repository
public class TimeZoneDAOImpl implements TimeZoneDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addTimeZone(TimeZone timezone) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(timezone);
       
   }

   //@Override
   public void updateTimeZone(TimeZone timezone) {
       Session session = sessionFactory.getCurrentSession();
       session.update(timezone);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<TimeZone> listTimeZones() {
       Session session = sessionFactory.getCurrentSession();
       List<TimeZone> TimeZonesList = session.createQuery("from TimeZone").list();
       
       return TimeZonesList;
   }

   //@Override
   public TimeZone getTimeZoneById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       TimeZone timezone = (TimeZone) session.get(TimeZone.class, new Integer(id));
       return timezone;
   }

   //@Override
   public void removeTimeZone(int id) {
       Session session = sessionFactory.getCurrentSession();
       TimeZone timezone = (TimeZone) session.get(TimeZone.class, new Integer(id));
       
       //De-activate the flag
       timezone.setActiveFlag(0);
       
       
       if(null != timezone){
           //session.delete(timezone);
    	   
    	   session.update(timezone);
       }
   }
}



