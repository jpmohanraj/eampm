

package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.UserRole;

public interface UserRoleDAO {
	 
	public void addUserRole(UserRole userrole);
	    
	public void updateUserRole(UserRole userrole);
	    
	public UserRole getUserRoleById(int id);
	    
	public void removeUserRole(int id);
	    
	public List<UserRole> listUserRoles();
}




