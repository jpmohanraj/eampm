package com.mestech.eampm.dao;


import java.util.List;

import com.mestech.eampm.model.DataTransaction;

public interface DataTransactionDAO {
	 
	public void addDataTransaction(DataTransaction dataTransaction);
	    
	public void updateDataTransaction(DataTransaction dataTransaction);
		  
	public DataTransaction getDataTransactionById(int id);

    public DataTransaction getDataTransactionByMax(String MaxColumnName);
	    
	public void removeDataTransaction(int id);
	    
	public List<DataTransaction> listDataTransactions();
	
	public List<DataTransaction> listDataTransactionsByFilter1(int equipmentId,int siteId, int lastNDays );
	
	public List<DataTransaction> listDataTransactionsByFilter2(int siteId, int lastNDays );
	
	public List<DataTransaction> listDataTransactionsForDataDownload(int siteId, String fromDate,String toDate,int equipmentId );
		   
	public List<DataTransaction> getDataTransactionListByUserId(int userId);
	
	public List<DataTransaction> getDataTransactionListByCustomerId(int customerId);
	
	public List<DataTransaction> getDataTransactionListBySiteId(int siteId);

	public List<DataTransaction> getDataTransactionListByEquipmentId(int equipmentId);
}
