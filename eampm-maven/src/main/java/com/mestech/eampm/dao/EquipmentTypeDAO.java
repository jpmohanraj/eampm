package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.EquipmentType;

public interface EquipmentTypeDAO {
	 
	public void addEquipmentType(EquipmentType equipmenttype);
	    
	public void updateEquipmentType(EquipmentType equipmenttype);
	    
	public EquipmentType getEquipmentTypeById(int id);

    public EquipmentType getEquipmentTypeByMax(String MaxColumnName);
    
	public void removeEquipmentType(int id);
	    
	public List<EquipmentType> listEquipmentTypes();
}
