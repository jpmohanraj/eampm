package com.mestech.eampm.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.DataTransaction;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.TicketDetail;

@Repository
public class TicketDetailDAOImpl implements TicketDetailDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addTicketDetail(TicketDetail ticketdetail) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(ticketdetail);
       
   }

   //@Override
   public void updateTicketDetail(TicketDetail ticketdetail) {
       Session session = sessionFactory.getCurrentSession();
       session.update(ticketdetail);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<TicketDetail> listTicketDetails() {
       Session session = sessionFactory.getCurrentSession();
       List<TicketDetail> TicketDetailList = session.createQuery("from TicketDetail").list();
       
       
       
       return TicketDetailList;
   }

   

   
   @SuppressWarnings("unchecked")
   public List<TicketDetail> listTicketDetailsForDisplay(int siteId, String fromDate,String toDate,String category, String state, String priority ) {
    
	   List<TicketDetail> TicketDetailList = new  ArrayList<TicketDetail>();
	   String WhereCondition = "";
	   
	   
	   if(siteId!=0 && siteId!=-1)
	   {
		   WhereCondition = WhereCondition + " and SiteID ='" + siteId + "' ";
	   }
	   if(fromDate!=null && toDate!=null &&  fromDate!="" && toDate!="")
	   {
		   WhereCondition = WhereCondition + " and  trunc(CreationDate)>=trunc(to_date('" + fromDate + "','dd/mm/yyyy')) and  trunc(CreationDate)<=trunc(to_date('" + toDate + "','dd/mm/yyyy')) ";
	   }
	   if(category!=null && category!="")
	   {
		   WhereCondition = WhereCondition + " and TicketCategory ='" + category + "' ";
	   }
	   if(state!=null && state!="")
	   {
		   WhereCondition = WhereCondition + " and State ='" + state + "' ";
	   }
	   if(priority!=null && priority!="" && priority!="-1" && priority!="0")
	   {
		   WhereCondition = WhereCondition + " and Priority ='" + priority + "' ";
	   }
	   
	   
	 try
	 {
	  // System.out.println("Mohan : " +WhereCondition);
	   Session session = sessionFactory.getCurrentSession();
	   TicketDetailList  = session.createQuery("from TicketDetail where ActiveFlag='1'  " + WhereCondition + "  order by TicketID").list();
	   
	  
	 }
	 catch(Exception e)
	 {}
      
	 return TicketDetailList;
   }
   
   
   
   
   //@Override
   public TicketDetail getTicketDetailById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       TicketDetail ticketdetail = (TicketDetail) session.get(TicketDetail.class, new Integer(id));
       return ticketdetail;
   }


   public TicketDetail getTicketDetailByTicketCode(String TicketCode) {
       Session session = sessionFactory.getCurrentSession();  
       TicketDetail ticketdetail = null;
       
       try
       {
    	   ticketdetail = (TicketDetail) session.createQuery("from TicketDetail where TicketCode='" + TicketCode +"'" )
   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {
    	   
       }
       catch(Exception ex)
       {
    	   
       }
       
       return ticketdetail;
   }
   
   
   
   
   
   public TicketDetail getTicketDetailByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       TicketDetail ticketdetail = null;
       
       try
       {
    	   ticketdetail = (TicketDetail) session.createQuery("from TicketDetail Order By " + MaxColumnName +  " desc")
   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {
    	   
       }
       catch(Exception ex)
       {
    	   
       }
       
       return ticketdetail;
   }
   
   
   
   
   public List<TicketDetail> getTicketDetailListByUserId(int userId)
	{
		  Session session = sessionFactory.getCurrentSession();
	       List<TicketDetail> TicketDetailList = session.createQuery("from TicketDetail where  ActiveFlag='1' and SiteID in (Select SiteId from Site where CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "'))").list();
	       
	       return TicketDetailList;
	}
	
   
   
   public List<TicketDetail> getTicketDetailListBySiteId(int siteId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<TicketDetail> TicketDetailList = session.createQuery("from TicketDetail where ActiveFlag='1' and SiteID='" + siteId + "'").list();
       
       return TicketDetailList;
   }

	
   public List<TicketDetail> getTicketDetailListByCustomerId(int customerId)
   {
          Session session = sessionFactory.getCurrentSession();
          List<TicketDetail> TicketDetailList = session.createQuery("from TicketDetail where  ActiveFlag='1' and SiteID in (Select SiteId from Site where CustomerID ='" + customerId + "')").list();
        
       return TicketDetailList;
   }

	
   
   
   //@Override
   public void removeActivity(int id) {
       Session session = sessionFactory.getCurrentSession();
       TicketDetail ticketdetail = (TicketDetail) session.get(TicketDetail.class, new Integer(id));
       
       //De-activate the flag
       ticketdetail.setActiveFlag(0);
       
       if(null != ticketdetail){
    	   session.update(ticketdetail);
           //session.delete(activity);
       }
   }

@Override
public void removeTicketDetail(int id) {
	// TODO Auto-generated method stub
	
}
}
