

package com.mestech.eampm.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.persistence.NoResultException;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.fasterxml.classmate.AnnotationConfiguration;
import com.mestech.eampm.model.Customer;
import com.mestech.eampm.model.CustomerMap;
import com.mestech.eampm.model.Site;
import com.mestech.eampm.model.SiteStatus;
import com.mestech.eampm.service.SiteServiceImpl;

@Repository
public class SiteDAOImpl implements SiteDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addSite(Site site) {
	
	   
	   Session session = sessionFactory.openSession(); //create the session object
       session.beginTransaction();//create the transaction from the session object
       
       SiteStatus siteStatus = new SiteStatus();
       
       String SiteName = site.getSiteName();
       
       siteStatus.setCustomerID(site.getCustomerID());
       siteStatus.setActiveFlag(site.getActiveFlag());
       siteStatus.setCreatedBy(site.getCreatedBy());
       siteStatus.setCreationDate(site.getCreationDate());
       siteStatus.setSiteStatus("1");
       
       
       
       
       session.save(site); 
       
       Site site1 = (Site) session.createQuery("from Site where SiteName='" + SiteName + "'").list().get(0);
       siteStatus.setSiteId(site1.getSiteId());
       
       session.save(siteStatus);
       
       session.getTransaction().commit(); //close the transaction
       session.close(); //close the session
       
       
	   
       //Session session = sessionFactory.getCurrentSession();
       //session.persist(site);	
       
       
	       
   }

   //@Override
   public void updateSite(Site site) {
	   
	   Session session = sessionFactory.openSession(); //create the session object
       session.beginTransaction();//create the transaction from the session object
       
       SiteStatus siteStatus = new SiteStatus();
       
       siteStatus.setSiteId(site.getSiteId());
       siteStatus.setCustomerID(site.getCustomerID());
       siteStatus.setActiveFlag(site.getActiveFlag());
       siteStatus.setLastUpdatedBy(site.getLastUpdatedBy());
       siteStatus.setLastUpdatedDate(site.getLastUpdatedDate());
       siteStatus.setSiteStatus("1");
       
       
   
       
       
       session.update(site); 
       session.update(siteStatus);
       
       session.getTransaction().commit(); //close the transaction
       session.close(); //close the session
       
       
      /* Session session = sessionFactory.getCurrentSession();
       session.update(site);*/
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<Site> listSiteById(int id) {
       Session session1 = sessionFactory.getCurrentSession();
       List<Site> SitesList = session1.createQuery("from Site where SiteId='"+id+"'").list();
       
       return SitesList;
   }
   
   @SuppressWarnings("unchecked")
   //@Override
   public List<Site> listSites() {
       Session session = sessionFactory.getCurrentSession();
       List<Site> SitesList = session.createQuery("from Site").list();
       
       return SitesList;
   }

   //@Override
   public Site getSiteById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       Site site = (Site) session.get(Site.class, new Integer(id));
       return site;
   }
 
   public Site getSiteByName(String siteName) {
	   Session session = sessionFactory.getCurrentSession();
       Site site = (Site) session.createQuery("from Site where ActiveFlag='1' and SiteName='" + siteName + "'").uniqueResult();
       
       return site;
   }
   
   
   public List<Site> getSiteListByCustomerId(int customerId)
   {
	   Session session = sessionFactory.getCurrentSession();
       List<Site> SitesList = session.createQuery("from Site where ActiveFlag='1' and CustomerID='" + customerId + "'").list();
       
       return SitesList;
   }

	public List<Site> getSiteListByUserId(int userId)
	{
		  Session session = sessionFactory.getCurrentSession();
	       List<Site> SiteList = session.createQuery("from Site where  ActiveFlag='1' and CustomerID in (Select CustomerID from CustomerMap where UserID='" + userId + "')").list();
	       
	       return SiteList;
	}
	
   public Site getSiteByMax(String MaxColumnName) {
       Session session = sessionFactory.getCurrentSession();  
       Site site= null;
       
       try
       {
    	   site = (Site) session.createQuery("from Site Order By " + MaxColumnName +  " desc")
    	   		    .setMaxResults(1).getSingleResult();
       }
       catch(NoResultException ex)
       {}
       catch(Exception ex)
       {}
    	   
    	   
    	   
       return site;
   }
   
   
   
   //@Override
   public void removeSite(int id) {
       Session session = sessionFactory.getCurrentSession();
       Site site = (Site) session.get(Site.class, new Integer(id));
     
       //De-activate the flag
       site.setActiveFlag(0);
     
       
       if(null != site){
           //session.delete(site);
    	   session.update(site);
       }
   }
}


