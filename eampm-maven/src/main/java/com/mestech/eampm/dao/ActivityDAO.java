package com.mestech.eampm.dao;


import java.util.List;
 
import com.mestech.eampm.model.Activity;

public interface ActivityDAO {
	 
	public void addActivity(Activity activity);
	    
	public void updateActivity(Activity activity);
	    
	public Activity getActivityById(int id);
	    
	public void removeActivity(int id);
	    
	public List<Activity> listActivities();
}
