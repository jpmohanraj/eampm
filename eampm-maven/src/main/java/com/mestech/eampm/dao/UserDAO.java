

package com.mestech.eampm.dao;

import java.util.List;
 
import com.mestech.eampm.model.User;

public interface UserDAO {
	 
	public void addUser(User user);
	    
	public void updateUser(User user);
	    
	public User getUserById(int id);
	    
	public User getUserByName(String userName);
	 
	public void removeUser(int id);
	    
	public List<User> listUsers();
	
	public List<User> listFieldUsers();
}


