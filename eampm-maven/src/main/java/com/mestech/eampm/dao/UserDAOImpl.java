

package com.mestech.eampm.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
 
import com.mestech.eampm.model.User;

@Repository
public class UserDAOImpl implements UserDAO {

	@Autowired
    private SessionFactory sessionFactory;
   
   //@Override
   public void addUser(User user) {
	   
       Session session = sessionFactory.getCurrentSession();
       session.persist(user);
       
       System.out.println("DAO Impl Completed");
   }

   //@Override
   public void updateUser(User user) {
       Session session = sessionFactory.getCurrentSession();
       session.update(user);
   }

   @SuppressWarnings("unchecked")
   //@Override
   public List<User> listUsers() {
       Session session = sessionFactory.getCurrentSession();
       List<User> UsersList = session.createQuery("from User").list();
       
       return UsersList;
   }

   
   @SuppressWarnings("unchecked")
   //@Override
   public List<User> listFieldUsers() {
       Session session = sessionFactory.getCurrentSession();
       List<User> UsersList = session.createQuery("from User where RoleID in (Select RoleId from UserRole where RoleName='FieldUser')").list();
       
       return UsersList;
   }
   
   
   //@Override
   public User getUserById(int id) {
       Session session = sessionFactory.getCurrentSession();       
       User user = (User) session.get(User.class, new Integer(id));
       return user;
   }
   
   public User getUserByName(String userName) {
	   
	   Session session = sessionFactory.getCurrentSession();
       List<User> UsersList = session.createQuery("from User where UserName ='" + userName + "'").list();
       User user = new User();
       
       
       if(UsersList !=null)
       {
    	   if(UsersList.size() >= 1)
           {
    		   user = UsersList.get(0);
           }
                
          
       }
            
      
       
       return user;
   }
   
   
  

   //@Override
   public void removeUser(int id) {
       Session session = sessionFactory.getCurrentSession();
       User user = (User) session.get(User.class, new Integer(id));
       if(null != user){
           session.delete(user);
       }
   }
}

